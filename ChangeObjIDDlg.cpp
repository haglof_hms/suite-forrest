// ChangeObjIDDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "ChangeObjIDDlg.h"

#include "ResLangFileReader.h"

// CChangeObjIDDlg dialog

IMPLEMENT_DYNAMIC(CChangeObjIDDlg, CDialog)

BEGIN_MESSAGE_MAP(CChangeObjIDDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_SEARCH_NEW, &CChangeObjIDDlg::OnBnClickedBtnSearchNew)
	ON_BN_CLICKED(IDC_BTN_SEARCH_ADD, &CChangeObjIDDlg::OnBnClickedBtnSearchAdd)
	ON_BN_CLICKED(IDC_BTN_ALL, &CChangeObjIDDlg::OnBnClickedBtnAll)
	ON_BN_CLICKED(IDOK, &CChangeObjIDDlg::OnBnClickedBtnOK)
	ON_BN_CLICKED(IDC_BUTTON5, &CChangeObjIDDlg::OnBnClickedBtnSelectAll)
	ON_BN_CLICKED(IDC_BUTTON6, &CChangeObjIDDlg::OnBnClickedBtnDeSelectAll)
	ON_EN_CHANGE(IDC_EDIT8, OnEdObjIDChange)

	ON_NOTIFY(NM_CLICK, ID_REPORT_CHANGE_OBJID, OnReportItemClick)
	ON_BN_CLICKED(IDCANCEL, &CChangeObjIDDlg::OnBnClickedCancel)
	ON_WM_CLOSE()
END_MESSAGE_MAP()
	
CChangeObjIDDlg::CChangeObjIDDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChangeObjIDDlg::IDD, pParent)
{
	m_bIsDirty = FALSE;
}

CChangeObjIDDlg::~CChangeObjIDDlg()
{
	m_ilIcons.DeleteImageList();
}

void CChangeObjIDDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertiesDlg)
	DDX_Control(pDX, IDC_LBL7_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL7_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL7_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL7_4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL7_5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL7_6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL7_7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL7_8, m_wndLbl8);
	DDX_Control(pDX, IDC_LBL7_9, m_wndLbl9);
	DDX_Control(pDX, IDC_LBL7_10, m_wndLbl10);

	DDX_Control(pDX, IDC_EDIT1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT8, m_wndEdit8);

	DDX_Control(pDX, IDC_BTN_SEARCH_NEW, m_wndSearchNewBtn);
	DDX_Control(pDX, IDC_BTN_SEARCH_ADD, m_wndSearchAddBtn);
	DDX_Control(pDX, IDC_BTN_ALL, m_wndAddAllBtn);
	
	DDX_Control(pDX, IDC_BUTTON5, m_wndCheckAll);
	DDX_Control(pDX, IDC_BUTTON6, m_wndUncheckAll);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP

}

BOOL CChangeObjIDDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING320)));

			m_wndLbl1.SetWindowText((xml.str(IDS_STRING3200)));
			m_wndLbl2.SetWindowText((xml.str(IDS_STRING3201)));
			m_wndLbl3.SetWindowText((xml.str(IDS_STRING3202)));
			m_wndLbl4.SetWindowText((xml.str(IDS_STRING3203)));
			m_wndLbl5.SetWindowText((xml.str(IDS_STRING3204)));
			m_wndLbl6.SetWindowText((xml.str(IDS_STRING3205)));
			m_wndLbl7.SetWindowText((xml.str(IDS_STRING3206)));
			m_wndLbl8.SetWindowText((xml.str(IDS_STRING3213)));
			m_wndLbl9.SetWindowText((xml.str(IDS_STRING3224)));

			m_wndSearchNewBtn.SetWindowText((xml.str(IDS_STRING3207)));
			m_wndSearchAddBtn.SetWindowText((xml.str(IDS_STRING3208)));
			m_wndAddAllBtn.SetWindowText((xml.str(IDS_STRING3209)));
			
			m_wndCheckAll.SetWindowText((xml.str(IDS_STRING3211)));
			m_wndUncheckAll.SetWindowText((xml.str(IDS_STRING3212)));
			m_wndOKBtn.SetWindowText(xml.str(IDS_STRING3210));
			m_wndCancelBtn.SetWindowText((xml.str(IDS_STRING307/*2915*/)));	//#4483 �ndrat texten fr�n Avbryt till St�ng p� knappen
		}
		xml.clean();
	}

	m_wndLbl10.SetLblFontEx(-1,FW_BOLD);

	setupReport();

	getProperties();

	populateData();
	setItemsMatched();

	m_wndOKBtn.EnableWindow(FALSE);

	return TRUE;
}

void CChangeObjIDDlg::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CXTPReportRows *pRows = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		if (pItemNotify->pColumn->GetItemIndex() == 0)
		{
			if (isItemSelected()) return;
		}	// if (pItemNotify->pColumn->GetItemIndex() == 0)
	}
}


BOOL CChangeObjIDDlg::isItemSelected(void)
{
	CXTPReportRows *pRows = NULL;
	CSelectedPropertiesDataRec *pRec = NULL;
	pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CSelectedPropertiesDataRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				if (pRec->getColumnCheck(COLUMN_0))
				{
					// Condition for TRUE is that there's selctions in From and To Database; 081008 p�d
					m_wndOKBtn.EnableWindow(!m_wndEdit8.getText().IsEmpty());
					return TRUE;
				}	// if (pRec->getColumnCheck())
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
	m_wndOKBtn.EnableWindow(FALSE);

	return FALSE;
}

// CChangeObjIDDlg message handlers

// PRIVATE
void CChangeObjIDDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_REPORT_CHANGE_OBJID, TRUE, FALSE))
		{
			return;
		}
	}

	if (m_wndReport.GetSafeHwnd() == NULL)
	{
		return;
	}
	else
	{	

		VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
		CBitmap bmp;
		VERIFY(bmp.LoadBitmap(IDB_BITMAP3));
		m_ilIcons.Add(&bmp, RGB(255, 0, 255));

		m_wndReport.SetImageList(&m_ilIcons);

		m_wndReport.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				// Select
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""), 20,FALSE,12));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
				// Object ID
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING3200), 80));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
				// County
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING3201), 150));
				pCol->SetEditable( FALSE );
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
				// Municipal
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING3202), 120));
				pCol->SetEditable( FALSE );
				// Propertynumber
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, xml.str(IDS_STRING3203), 120));
				pCol->SetEditable( FALSE );
				// Propertyname
				pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_5, xml.str(IDS_STRING3204), 200));
				pCol->SetEditable( FALSE );

				m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport.SetMultipleSelection( TRUE );
				m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport.AllowEdit(TRUE);
				m_wndReport.FocusSubItems(TRUE);
				m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport.SetFocus();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(ID_REPORT_CHANGE_OBJID),5,110,rect.right - 10,rect.bottom - 145);
			}
			xml.clean();
		}	// if (fileExists(sLangFN))

	}
}

void CChangeObjIDDlg::populateData(void)
{
	// Clear report for next question
	m_wndReport.ResetContent();
	// Do collecting data from Database; 080122 p�d
	if (m_pDB != NULL)
	{
		if (m_vecProperties.size() > 0)
		{
			// Setup data in report; 080122 p�d
			for (UINT ii = 0;ii < m_vecProperties.size();ii++)
			{
				m_wndReport.AddRecord(new CSelectedPropertiesDataRec(ii,m_vecProperties[ii]));
			}
		}
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}


void CChangeObjIDDlg::getProperties(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(m_vecProperties);
	}
}

void CChangeObjIDDlg::setupSQLFromSelections(bool clear)
{
	CStringArray arrSQL;
	CString sSQL;
	CString sIdentity = _T("");
	CString sCounty = _T("");
	CString sMunicipal = _T("");
	CString sRegion = _T("");
	CString sNumber = _T("");
	CString sName = _T("");
	CString sBlock = _T("");
	CString sUnit = _T("");

	arrSQL.RemoveAll();
	// Try to find out how many items's entered;
	if (!m_wndEdit1.getText().IsEmpty())
	{
		sIdentity.Format(_T("obj_id LIKE \'%s%s%s\' "),_T("%"),m_wndEdit1.getText(),_T("%"));
		arrSQL.Add(sIdentity);
	}
	if (!m_wndEdit2.getText().IsEmpty())
	{
		sCounty.Format(_T("county_name LIKE \'%s%s%s\' "),_T("%"),m_wndEdit2.getText(),_T("%"));
		arrSQL.Add(sCounty);
	}
	if (!m_wndEdit3.getText().IsEmpty())
	{
		sMunicipal.Format(_T("municipal_name LIKE \'%s%s%s\' "),_T("%"),m_wndEdit3.getText(),_T("%"));
		arrSQL.Add(sMunicipal);
	}
	if (!m_wndEdit4.getText().IsEmpty())
	{
		sNumber.Format(_T("prop_number LIKE \'%s%s%s\' "),_T("%"),m_wndEdit4.getText(),_T("%"));
		arrSQL.Add(sNumber);
	}
	if (!m_wndEdit5.getText().IsEmpty())
	{
		sName.Format(_T("prop_name LIKE \'%s%s%s\' "),_T("%"),m_wndEdit5.getText(),_T("%"));
		arrSQL.Add(sName);
	}
	if (!m_wndEdit6.getText().IsEmpty())
	{
		sBlock.Format(_T("block_number LIKE \'%s%s%s\' "),_T("%"),m_wndEdit6.getText(),_T("%"));
		arrSQL.Add(sBlock);
	}
	if (!m_wndEdit7.getText().IsEmpty())
	{
		sUnit.Format(_T("unit_number LIKE \'%s%s%s\' "),_T("%"),m_wndEdit7.getText(),_T("%"));
		arrSQL.Add(sUnit);
	}

	if (arrSQL.GetCount() > 0)
	{
		sSQL.Format(_T("select * from %s where "),TBL_PROPERTY);
		// How many items in array; 080122 p�d
		for (int i = 0;i < arrSQL.GetCount();i++)
		{
			if (i > 0)
				sSQL += _T(" and ");

			sSQL += arrSQL.GetAt(i);
		}
	}
	else
		sSQL.Format(_T("select * from %s"),TBL_PROPERTY);	// Select all properties; 080122 p�d

	// Clear report for next question
	if (clear) m_wndReport.ResetContent();
	// Do collecting data from Database; 080122 p�d
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(sSQL,m_vecProperties);

		if (m_vecProperties.size() > 0)
		{
			// Setup data in report; 080122 p�d
			for (UINT ii = 0;ii < m_vecProperties.size();ii++)
			{
				if (!isItemAlreadyInReport(m_vecProperties[ii]))
				{
					m_wndReport.AddRecord(new CSelectedPropertiesDataRec(ii,m_vecProperties[ii]));
				}
			}
		}
	}
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();

}

// Check to see if item's already added to Report-list, on Search:Add button; 091008 p�d
BOOL CChangeObjIDDlg::isItemAlreadyInReport(CTransaction_property &prop)
{
	CString sPropFullName,S;
	CSelectedPropertiesDataRec *pRec = NULL;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	if (pRecs != NULL)
	{
		// Setup the full name of property. I.e. Name + Block:Unit; 091005 p�d
		if ((prop.getBlock().IsEmpty() || prop.getBlock() == _T("0")) &&
				(prop.getUnit().IsEmpty() || prop.getUnit() == _T("0")))
		{
			sPropFullName = prop.getPropertyName();
		}
		else if ((!prop.getBlock().IsEmpty() || prop.getBlock() != _T("0")) &&
						 (prop.getUnit().IsEmpty() || prop.getUnit() == _T("0")))
		{
			sPropFullName.Format(_T("%s %s"),prop.getPropertyName(),prop.getBlock());
		}
		else if ((!prop.getBlock().IsEmpty() || prop.getBlock() != _T("0")) &&
						 (!prop.getUnit().IsEmpty() || prop.getUnit() != _T("0")))
		{
			sPropFullName.Format(_T("%s %s:%s"),prop.getPropertyName(),prop.getBlock(),prop.getUnit());
		}

		for (int i = 0;i < pRecs->GetCount();i++)
		{
			pRec = (CSelectedPropertiesDataRec*)pRecs->GetAt(i);
			if (pRec != NULL)
			{
/*
				S.Format(_T("County   %s  %s\nMunicipal  %s  %s\nPropNum  %s  %s\nPropName  %s  %s"),
					prop.getCountyName(),pRec->getColumnText(COLUMN_2),
					prop.getMunicipalName(),pRec->getColumnText(COLUMN_3),
					prop.getPropertyNum(),pRec->getColumnText(COLUMN_4),
					sPropFullName,pRec->getColumnText(COLUMN_5));
				AfxMessageBox(S);
*/
				if (prop.getObjectID().CompareNoCase(pRec->getColumnText(COLUMN_1)) == 0 &&
						prop.getCountyName().CompareNoCase(pRec->getColumnText(COLUMN_2)) == 0 &&
						prop.getMunicipalName().CompareNoCase(pRec->getColumnText(COLUMN_3)) == 0 &&
						prop.getPropertyNum().CompareNoCase(pRec->getColumnText(COLUMN_4)) == 0 &&
						sPropFullName.CompareNoCase(pRec->getColumnText(COLUMN_5)) == 0)
				{
					return TRUE;	// Found it
				}
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)

	return FALSE;	// No match
}

void CChangeObjIDDlg::setItemsMatched(void)
{
	CString sNumOf;
	// Set number of items in Report = number of matches; 091008 p�d
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		sNumOf.Format(_T("%d"),pRows->GetCount());
		m_wndLbl10.SetWindowTextW(sNumOf);
	}
}

void CChangeObjIDDlg::OnBnClickedBtnSearchNew()
{
	setupSQLFromSelections(true);
	setItemsMatched();
}

void CChangeObjIDDlg::OnBnClickedBtnSearchAdd()
{
	setupSQLFromSelections(false);
	setItemsMatched();
}

void CChangeObjIDDlg::OnBnClickedBtnAll()
{
	clearAll();

	getProperties();	// Get ALL properties in database; 090813 p�d
	populateData();
	setItemsMatched();

}

void CChangeObjIDDlg::clearAll(void)
{
	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit4.SetWindowText(_T(""));
	m_wndEdit5.SetWindowText(_T(""));
	m_wndEdit6.SetWindowText(_T(""));
	m_wndEdit7.SetWindowText(_T(""));
	m_wndEdit8.SetWindowText(_T(""));
}

void CChangeObjIDDlg::OnBnClickedBtnOK()
{
	CString S;
	CXTPReportRecords *pRecs = m_wndReport.GetRecords();
	CTransaction_property recProp;
	CString sNewObjID(m_wndEdit8.getText());
	if (pRecs != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			CSelectedPropertiesDataRec *pRec = (CSelectedPropertiesDataRec*)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				if (pRec->getColumnCheck(COLUMN_0))
				{
					recProp = pRec->getRecord();
					if (m_pDB != NULL)
					{
						m_pDB->updPropertyObjID(recProp.getID(),sNewObjID);
						m_bIsDirty = TRUE;
					}
				}
			}
		}
//		OnOK();
	}
	clearAll();
	getProperties();	// Get ALL properties in database; 090813 p�d
	populateData();
	setItemsMatched();
}


void CChangeObjIDDlg::OnBnClickedBtnSelectAll()
{
	CSelectedPropertiesDataRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CSelectedPropertiesDataRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				pRec->setColumnCheck(COLUMN_0,TRUE);
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
	m_wndOKBtn.EnableWindow(!m_wndEdit8.getText().IsEmpty() && isItemSelected());
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();

}

void CChangeObjIDDlg::OnBnClickedBtnDeSelectAll()
{
	CSelectedPropertiesDataRec *pRec = NULL;
	CXTPReportRows *pRows = m_wndReport.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CSelectedPropertiesDataRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				pRec->setColumnCheck(COLUMN_0,FALSE);
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
	m_wndOKBtn.EnableWindow(FALSE);
	m_wndReport.Populate();
	m_wndReport.UpdateWindow();
}

void CChangeObjIDDlg::OnEdObjIDChange()
{
	m_wndOKBtn.EnableWindow(!m_wndEdit8.getText().IsEmpty() && isItemSelected());
}

void CChangeObjIDDlg::OnBnClickedCancel()
{
	//#4483 om n�got �ndrats s� skicka IDOK s� att fastighetsvyn populeras om
	if(m_bIsDirty)
		OnOK();
	else
		OnCancel();
}

void CChangeObjIDDlg::OnClose()
{
	//#4483 om n�got �ndrats s� skicka IDOK s� att fastighetsvyn populeras om
	if(m_bIsDirty)
		EndDialog(IDOK);

	CDialog::OnClose();
}

