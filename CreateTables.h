#if !defined(AFX_CREATETABLES_H)
#define AFX_CREATETABLES_H

#include "StdAfx.h"

// CreateTables in Database; 080701 p�d

// "Ers�tter sql-script"


const LPCTSTR table_ElvP30Spec_Obj= _T("CREATE TABLE dbo.%s (")
									_T("spc_id int NOT NULL,")
									_T("obj_id int NOT NULL,")
									_T("spc_name NVARCHAR(45),")
									_T("notes NVARCHAR(255),")
									_T("p30_spec int,")
									_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
									_T("PRIMARY KEY (spc_id,obj_id)")
									_T(");");


//***************************************************************************************************
// Tall;Gran;Bj�rk;�vrigt l�v;�vrigt barr;Contorta;Torra;Alm;Ek;Ask;L�nn;Lind;Bok;Gr�al;
// Klibbal;F�gelb�r;Sitka;L�rk;Avenbok;S�lg;Pil;Poppel;Asp;Douglas;Fj�llbj�rk;
const LPCTSTR table_FstSpeciesSVE = _T("CREATE TABLE dbo.%s (")
									_T("spc_id int NOT NULL,")
									_T("spc_name NVARCHAR(45),")
									_T("notes NVARCHAR(255),")
									_T("p30_spec int NOT NULL,")
									_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
									_T("PRIMARY KEY (spc_id)")
									_T(");")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(1,'Tall','Tr�dslag 1',1);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(2,'Gran','Tr�dslag 2',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(3,'Bj�rk','Tr�dslag 3',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(4,'�vrigt l�v','Tr�dslag 4',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(5,'�vrigt barr','Tr�dslag 5',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(6,'Contorta','Tr�dslag 6',1);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(7,'Torra','Tr�dslag 7',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(8,'Alm','Tr�dslag 8',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(9,'Ek','Tr�dslag 9',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(10,'Ask','Tr�dslag 10',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(11,'L�nn','Tr�dslag 11',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(12,'Lind','Tr�dslag 12',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(13,'Bok','Tr�dslag 13',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(14,'Gr�al','Tr�dslag 14',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(15,'Klibbal','Tr�dslag 15',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(16,'F�gelb�r','Tr�dslag 16',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(17,'Sitka','Tr�dslag 17',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(18,'L�rk','Tr�dslag 18',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(19,'Avenbok','Tr�dslag 19',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(20,'S�lg','Tr�dslag 20',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(21,'Pil','Tr�dslag 21',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(22,'Poppel','Tr�dslag 22',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(23,'Asp','Tr�dslag 23',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(24,'Douglas','Tr�dslag 24',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(25,'Fj�llbj�rk','Tr�dslag 25',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(26,'L�v','Tr�dslag 26',3);");

const LPCTSTR table_FstSpeciesENU = _T("CREATE TABLE dbo.%s (")
									_T("spc_id int NOT NULL,")
									_T("spc_name NVARCHAR(45),")
									_T("notes NVARCHAR(255),")
									_T("p30_spec int NOT NULL,")
									_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
									_T("PRIMARY KEY (spc_id)")
									_T(");")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(1,'Pine','Specie 1',1);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(2,'Spruce','Specie 2',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(3,'Birch','Specie 3',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(4,'Leaf tree','Specie 4',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(5,'Coniferous tree','Specie 5',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(6,'Contorta','Specie 6',1);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(7,'Dry tree','Specie 7',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(8,'Elm','Specie 8',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(9,'Oak','Specie 9',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(10,'Ach','Specie 10',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(11,'Maple','Specie 11',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(12,'Lime','Specie 12',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(13,'Beeh','Specie 13',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(14,'Grey Alder','Specie 14',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(15,'Common Alder','Specie 15',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(18,'Lark','Specie 18',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(19,'Avenbok','Specie 19',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(20,'Goat Willow','Specie 20',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(23,'Aspen','Specie 23',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(24,'Douglas','Specie 24',2);");


//#3862 Norska tr�dslag
const LPCTSTR table_FstSpeciesNOR = _T("CREATE TABLE dbo.%s (")
									_T("spc_id int NOT NULL,")
									_T("spc_name NVARCHAR(45),")
									_T("notes NVARCHAR(255),")
									_T("p30_spec int NOT NULL,")
									_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
									_T("PRIMARY KEY (spc_id)")
									_T(");")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(1,'Furu','Treslag 1',1);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(2,'Gran','Treslag 2',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(3,'Bj�rk','Treslag 3',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(4,'�vrig l�v','Treslag 4',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(5,'�vrig bar','Treslag 5',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(6,'Contorta','Treslag 6',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(7,'Alm','Treslag 7',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(8,'Eik','Treslag 8',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(9,'Ask','Treslag 9',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(10,'L�nn','Treslag 10',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(11,'Lind','Treslag 11',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(12,'B�k','Treslag 12',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(13,'Gr�or','Treslag 13',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(14,'Svartor','Treslag 14',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(15,'S�tkirseb�r','Treslag 15',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(16,'Sitka','Treslag 16',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(17,'Lerk','Treslag 17',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(18,'Selje','Treslag 18',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(19,'Pil','Treslag 19',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(20,'Poppel','Treslag 20',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(21,'Osp','Treslag 21',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(22,'Douglas','Treslag 22',2);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(23,'Fjellbj�rk','Treslag 23',3);")
_T("INSERT INTO fst_species_table (spc_id,spc_name,notes,p30_spec) values(24,'L�v','Treslag 24',3);");

//********************************************************************************************************
const LPCTSTR table_FstPostnumber = _T("CREATE TABLE dbo.%s (")
									 _T("id int NOT NULL,")
									 _T("postnumber NVARCHAR(45),")
									 _T("postname NVARCHAR(45),")
									 _T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
									 _T("PRIMARY KEY (id));");
//********************************************************************************************************
const LPCTSTR table_FstCategories = _T("CREATE TABLE dbo.%s (")
									_T("id int NOT NULL IDENTITY,")
									_T("category NVARCHAR(45),")
									_T("notes NVARCHAR(255),")
									_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
									_T("PRIMARY KEY (id));");
//********************************************************************************************************
const LPCTSTR table_FstContacts = _T("CREATE TABLE dbo.%s (")
									_T("id int NOT NULL IDENTITY,")
									_T("pnr_orgnr NVARCHAR(50),")		/* Personnummer/Organinsationsnummer */
									_T("name_of NVARCHAR(50),")			/* Namn */
									_T("company NVARCHAR(50),")			/* F�retag */
									_T("address NVARCHAR(50),")			/* Adress */
									_T("co_address NVARCHAR(50),")		/* C/O Adress */
									_T("post_num NVARCHAR(10),")		/* Postnummer */
									_T("post_address NVARCHAR(60),")	/* Postadress */
									_T("county NVARCHAR(40),")			/* L�n */
									_T("country NVARCHAR(40),")			/* Land */
									_T("phone_work NVARCHAR(25),")		/* Telefon arbete */
									_T("phone_home NVARCHAR(25),")		/* Telefon hem */
									_T("fax_number NVARCHAR(25),")		/* Faxnummer */
									_T("mobile NVARCHAR(30),")			/* Mobil */
									_T("e_mail NVARCHAR(50),")			/* E-mail adress */
									_T("web_site NVARCHAR(50),")		/* Hemsida */
									_T("notes NTEXT,")					/* Noteringar BLOB */
									_T("created_by NVARCHAR(20),")		/* Skapad av */
									_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
									_T("vat_number NVARCHAR(40),")		/* Momsredovisningsnummer */
									_T("connect_id int,")				/* Pekar p� ett F�retag i detta register */
									_T("type_of int,")					/* Kan vara 0 = Person,1 = F�retag */
									_T("picture varbinary(MAX),")		/* Inneh�ller ex. Logo */
									_T("bankgiro NVARCHAR(40),")		/* Bankgironummer; 2012-11-21 */
									_T("plusgiro NVARCHAR(40),")		/* Plusgironummer; 2012-11-21 */
									_T("bankkonto NVARCHAR(40),")		/* Bankkontonummer; 2012-11-21 */
									_T("levnummer NVARCHAR(40),")		/* Leverant�rsnummer; 2012-11-21 */
									_T("PRIMARY KEY (id));");
//********************************************************************************************************
const LPCTSTR table_FstCategoriesForContacts = _T("CREATE TABLE dbo.%s (")
												_T("contact_id int NOT NULL,")
												_T("category_id int NOT NULL,")
												_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
												_T("PRIMARY KEY (contact_id,category_id),")
												_T("CONSTRAINT FK_Contact ")
												_T("FOREIGN KEY (contact_id) ")
												_T("REFERENCES fst_contacts_table (id) ")
												_T("ON DELETE CASCADE ")
												_T("ON UPDATE CASCADE,")
												_T("CONSTRAINT FK_Category ")
												_T("FOREIGN KEY (category_id) ")
												_T("REFERENCES fst_category_table(id) ")
												_T("ON DELETE CASCADE ")
												_T("ON UPDATE CASCADE);");
//********************************************************************************************************
const LPCTSTR table_FstProperties = _T("CREATE TABLE dbo.%s (")
									_T("id int NOT NULL IDENTITY,")
									_T("county_code NVARCHAR(5),")		/* L�nskod */
									_T("municipal_code NVARCHAR(5),")	/* Kommunkod */
									_T("parish_code NVARCHAR(5),")		/* F�rsamling kod */
									_T("county_name NVARCHAR(31),")		/* Namn p� L�n */
									_T("municipal_name NVARCHAR(31),")	/* Namn p� Kommun */
									_T("parish_name NVARCHAR(31),")		/* Namn p� F�rsamling */
									_T("prop_number NVARCHAR(20),")		/* Fastighetsnummer */
									_T("prop_name NVARCHAR(40),")		/* Namn p� Fastighet */
									_T("block_number NVARCHAR(5),")		/* Block */
									_T("unit_number NVARCHAR(5),")		/* Enhet */
									_T("areal_ha float,")				/* Areal (Total) */
									_T("areal_measured_ha float,")		/* Inm�tt areal f�r fastigheten */
									_T("created_by NVARCHAR(20),")		/* Skapad av (Username) */
									_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
									_T("obj_id NVARCHAR(30),")			/* Id for selection of Properties */
									_T("mottagarref NVARCHAR(40),")			/* Mottagarreferens */
								  _T("prop_full_name NVARCHAR(40),")	/* Name of prop incl. block and unit */
									_T("prop_coord NVARCHAR(80),")
									_T("PRIMARY KEY (id));");
//********************************************************************************************************
const LPCTSTR table_FstPropertyOwners = _T("CREATE TABLE dbo.%s (")
										_T("prop_id int NOT NULL,")
										_T("contact_id int NOT NULL,")
										_T("owner_share nvarchar(10),")	/* Andel, i br�kform. Ex 1/2 */
										_T("is_contact smallint,")	/* Yes/No kontaktperson f�r ex. Aviseringar */
										_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
										_T("PRIMARY KEY (prop_id,contact_id),")
										_T("CONSTRAINT FK_Contact1 ")
										_T("FOREIGN KEY (contact_id) ")
										_T("REFERENCES fst_contacts_table (id) ")
										_T("ON DELETE CASCADE ")
										_T("ON UPDATE CASCADE,")
										_T("CONSTRAINT FK_Property ")
										_T("FOREIGN KEY (prop_id) ")
										_T("REFERENCES fst_property_table(id) ")
										_T("ON DELETE CASCADE ")
										_T("ON UPDATE CASCADE);");
//********************************************************************************************************
const LPCTSTR table_FstCountyMunicipalParish = _T("CREATE TABLE dbo.%s (")
												_T("county_id int NOT NULL,")
												_T("municipal_id int NOT NULL,")
												_T("parish_id int NOT NULL,")
												_T("county_name NVARCHAR(31),")		/* Namn p� L�n */
												_T("municipal_name NVARCHAR(31),")	/* Namn p� Kommun */
												_T("parish_name NVARCHAR(31),")		/* Namn p� Socken */
												_T("created datetime NOT NULL default CURRENT_TIMESTAMP,")
												_T("PRIMARY KEY (county_id,municipal_id,parish_id));");

//********************************************************************************************************
// Create table for external documents; 080925 p�d
const LPCTSTR table_FstExternalDocs = _T("CREATE TABLE dbo.%s (")
										_T("extdoc_id int IDENTITY(1,1) NOT NULL,")
										_T("extdoc_link_tbl nvarchar(127) NOT NULL,")
										_T("extdoc_link_id int,")			/* Ex. ObjectID,PropertyID,ContatctID etc. */
										_T("extdoc_link_type tinyint,")
										_T("extdoc_url nvarchar(255),")		/* URL to document */
										_T("extdoc_note nvarchar(127),")	/* Small note */
										_T("extdoc_created_by nvarchar(30),")
										_T("extdoc_created nvarchar(20),")
										_T("PRIMARY KEY (extdoc_id,extdoc_link_tbl));");

//********************************************************************************************************
// ALTER TABLE SCRIPTS; 080813 p�d
const LPCTSTR alter_table_FstProperty = _T("alter table %s add obj_id nvarchar(30) null;");

// This script also does a check to see if column already exists; 090825 p�d
const LPCTSTR alter_FstContacts090825_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = '%s' and column_name = 'vat_number') ")
											_T("alter table %s add vat_number nvarchar(40)");

// This script also does a check to see if column already exists; 090825 p�d
const LPCTSTR alter_FstContacts090825_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = '%s' and column_name = 'connect_id') ")
											_T("alter table %s add connect_id int");

// This script also does a check to see if column already exists; 090825 p�d
const LPCTSTR alter_FstContacts090825_3 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = '%s' and column_name = 'type_of') ")
											_T("alter table %s add type_of int");

// This script also does a check to see if column already exists; 090825 p�d
const LPCTSTR alter_FstContacts090825_4 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = '%s' and column_name = 'picture') ")
											_T("alter table %s add picture varbinary(MAX)");

// This script also does a check to see if column already exists; 091005 p�d
const LPCTSTR alter_FstContacts110315_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
												_T("where table_name = '%s' and column_name = 'co_address') ")
												_T("alter table %s add co_address nvarchar(50)");

// This script also does a check to see if column already exists; 091005 p�d
const LPCTSTR alter_FstProperty091005_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = '%s' and column_name = 'prop_full_name') ")
											_T("alter table %s add prop_full_name nvarchar(40)");

// Alter length of owner_share to 10, 20110126 J� #1937, if 10 is changed also change #define MAX_OWNER_SHARE_CHARS 10 in stdafx.h to appropriate
const LPCTSTR alter_FstPropertyOwnerShare = _T("if exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'owner_share')  \
																				 alter table %s alter column owner_share nvarchar(20)");	//HMS-28 �ndrat fr�n 10 till 20 tecken
 

// This script also does a check to see if column already exists; 121121 p�d
const LPCTSTR alter_FstContacts121121_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
												_T("where table_name = '%s' and column_name = 'bankgiro') ")
												_T("alter table %s add bankgiro nvarchar(40)");

// This script also does a check to see if column already exists; 121121 p�d
const LPCTSTR alter_FstContacts121121_2 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
												_T("where table_name = '%s' and column_name = 'plusgiro') ")
												_T("alter table %s add plusgiro nvarchar(40)");

// This script also does a check to see if column already exists; 121121 p�d
const LPCTSTR alter_FstContacts121121_3 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
												_T("where table_name = '%s' and column_name = 'bankkonto') ")
												_T("alter table %s add bankkonto nvarchar(40)");

// This script also does a check to see if column already exists; 121121 p�d
const LPCTSTR alter_FstContacts121121_4 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
												_T("where table_name = '%s' and column_name = 'levnummer') ")
												_T("alter table %s add levnummer nvarchar(40)");

// This script also does a check to see if column already exists; 091005 p�d
const LPCTSTR alter_FstProperty121121_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = '%s' and column_name = 'mottagarref') ")
											_T("alter table %s add mottagarref nvarchar(40)");

// This script also does a check to see if column already exists; 091005 p�d
const LPCTSTR alter_FstProperty130905_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = '%s' and column_name = 'prop_coord') ")
											_T("alter table %s add prop_coord nvarchar(80)");

// HMS-76 �ndra antal tecken f�r adressf�ltet f�r kontakter till 150, 20220209 J�
// OBS!!! om denna �ndras sedan �ndra �ven #define MAX_ADDRESS_CHARS 150 i stdafx.h till ny l�ngd
const LPCTSTR alter_FstContactAddress20220209 = _T("if exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'address')  \
																				 alter table %s alter column address nvarchar(150)");	

// HMS-76 �ndra antal tecken f�r co adressf�ltet f�r kontakter till 150, 20220209 J�
// OBS!!! om denna �ndras sedan �ndra �ven #define MAX_ADDRESS_CHARS 150 i stdafx.h till ny l�ngd
const LPCTSTR alter_FstContactCoAddress20220209 = _T("if exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'co_address')  \
																				 alter table %s alter column co_address nvarchar(150)");	

//#HMS 94 info om p30 pris 
const LPCTSTR alter_FstSpecies20220916_1= _T("if not exists (select column_name from INFORMATION_SCHEMA.columns ")
											_T("where table_name = '%s' and column_name = 'p30_spec') ")
											_T("alter table %s add p30_spec int NOT NULL DEFAULT 0");


#endif