// PostnumFormView.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "MDIBaseFrame.h"
#include "PostnumFormView.h"

#include "ContactsTabView.h"
#include "ContactsFormView.h"

/////////////////////////////////////////////////////////////////////////////////////
//	Reportclasses for Species; 060317 p�d

/////////////////////////////////////////////////////////////////////////////////////
// CPostnumberFormView

long CPostnumberFormView::m_lLAST_RECORD_ID = 0;

IMPLEMENT_DYNCREATE(CPostnumberFormView, CXTPReportView)

BEGIN_MESSAGE_MAP(CPostnumberFormView, CXTPReportView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_SHOWWINDOW()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_COMMAND(ID_TBBTN_OPEN, OnImportDataFile)
	ON_COMMAND(ID_TBBTN_CREATE, OnCreatePostnumbers)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportItemKeyDown)
END_MESSAGE_MAP()

CPostnumberFormView::CPostnumberFormView()
	: CXTPReportView()
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
	m_pXML = NULL;

}

CPostnumberFormView::~CPostnumberFormView()
{
}

void CPostnumberFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTPReportView::DoDataExchange(pDX);
}

void CPostnumberFormView::OnDestroy()
{
	if (m_pDB != NULL)
	{
		delete m_pDB;
	}
	if (m_pXML != NULL)
	{
		delete m_pXML;
	}

	m_LangStr.clear();

	SaveReportState();
	CXTPReportView::OnDestroy();	
}

BOOL CPostnumberFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTPReportView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

BOOL CPostnumberFormView::OnEraseBkgnd(CDC *pDC)
{
	CRect clip;
  GetReportCtrl().GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);


	pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));

	return FALSE;
}

void CPostnumberFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	if (! m_bInitialized )
	{

		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		if (fileExists(m_sLangFN))
		{
			m_pXML = new RLFReader;
			m_pXML->LoadEx(m_sLangFN,m_LangStr);

		}

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
		
		setupReport1();

		m_bInitialized = TRUE;
		m_bIsPostnumberAdded = FALSE;
	
		LoadReportState();
	}
}
/*
void CPostnumberFormView::OnKeyDown(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	AfxMessageBox("1");
	if (nChar == VK_RETURN)
	{
		// Need to have Contacts open; 071204 p�d
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pTabView)
		{
					CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
					if (pRow != NULL)
					{
						CPostnumReportRec *pRec = (CPostnumReportRec *)pRow->GetRecord();
						if (pRec != NULL)
						{
							// Get ContactsFormView
							CContactsFormView *pView = (CContactsFormView *)pTabView->getContactsFormView();
							if (pView)
							{
								UINT nIndex = pRec->getID();
								if (nIndex >= 0 && nIndex < m_vecPostnumbersDataFromDB.size())
								{
									if (::MessageBox(this->GetSafeHwnd(),m_sAddZipCodeToProperty,m_sMessageCap,MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
									{
										pView->setPostNumberAndPostAddress(m_vecPostnumbersDataFromDB[nIndex]);
										CMDIPostnumberFrame *pFrame = (CMDIPostnumberFrame *)getFormViewParentByID(IDD_REPORTVIEW0);
										if (pFrame != NULL)
										{
											::SendMessage(pFrame->GetSafeHwnd(),WM_SYSCOMMAND,SC_CLOSE,0);
										}
									}
								}	// if (nIndex >= 0 && nIndex < m_vecPostnumbersDataFromDB.size())
							}	// if (pView)
						}					
					}	// if (pRow != NULL)
		}	// if (pTabView)
	}
	CXTPReportView::OnKeyUp(nChar,nRepCnt,nFlags);
}
*/

void CPostnumberFormView::OnShowWindow(BOOL bShow,UINT nStatus)
{
	CXTPReportView::OnShowWindow(bShow,nStatus);
}

BOOL CPostnumberFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}

	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}


void CPostnumberFormView::doSetNavigationBar()
{
	if (m_vecPostnumbersDataFromDB.size() > 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}

}

// CPostnumberFormView diagnostics

#ifdef _DEBUG
void CPostnumberFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CPostnumberFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
}
#endif //_DEBUG


void CPostnumberFormView::setFilterText(LPCTSTR filter,int column)
{
	CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
	if (pCols && column < pCols->GetCount())
	{
		for (int i = 0;i < pCols->GetCount();i++)
		{
			pCols->GetAt(i)->SetFiltrable( i == column );
		}
	}
	GetReportCtrl().AllowEdit( FALSE );
	GetReportCtrl().SetFilterText(filter);
	GetReportCtrl().Populate();
}

// PRIVATE METHODS

BOOL CPostnumberFormView::addPostnumbersToReport(void)
{
	int nNextPostnumberID = -1;
	// Find the empty row and point to it; 081203 p�d
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	//if (pRows != NULL)
	if (m_pDB != NULL)
		nNextPostnumberID = m_pDB->getMaxPostnumberID()+1;
	if (nNextPostnumberID > -1)
	{
		if (nNextPostnumberID <= m_lLAST_RECORD_ID)
			nNextPostnumberID = m_lLAST_RECORD_ID + 1;

		CPostnumReportRec *pRec = new CPostnumReportRec(nNextPostnumberID,CTransaction_postnumber(nNextPostnumberID,_T(""),_T(""),_T("")));
		if (pRec)
		{
			GetReportCtrl().AddRecord(pRec);
			GetReportCtrl().Populate();
			GetReportCtrl().UpdateWindow();

			// Find the empty row and point to it; 081203 p�d
			CXTPReportRows *pRows = GetReportCtrl().GetRows();
			if (pRows != NULL)
			{
				for (int i = 0;i < pRows->GetCount();i++)
				{
					CPostnumReportRec *pRec1 = (CPostnumReportRec *)pRows->GetAt(i)->GetRecord();
					if (pRec1 != NULL)
					{
						if (pRec1->getColumnText(COLUMN_0).IsEmpty())
						{
							GetReportCtrl().SetFocusedRow(pRows->GetAt(i),TRUE);
							break;
						}	// if (pRec1->getColumnText(COLUMN_0).IsEmpty())
					}	// if (pRec1 != NULL)
				}	// for (int i = 0;i < pRows->GetCount();i++)
			}	// if (pRows != NULL)
			m_lLAST_RECORD_ID = nNextPostnumberID;
		}	// if (pRec)
	}

	doSetNavigationBar();
	return TRUE;
}

// This method'll also check if there's any items added.
// I.e. number of items in "m_vecPostnumbersInReport"
// is larger than in "m_vecPostnumbersDataFormDB"; 071203 p�d
BOOL CPostnumberFormView::getPostnumbersFromReport(void)
{
	return TRUE;
}


BOOL CPostnumberFormView::getPostnumbersFromDB(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{

			bReturn = m_pDB->getPostnumbers(m_vecPostnumbersDataFromDB);

			if (m_vecPostnumbersDataFromDB.size() == 0) m_lLAST_RECORD_ID = 0;
			else m_lLAST_RECORD_ID = m_vecPostnumbersDataFromDB.size();
		}	// if (pDB != NULL)
	}
	return bReturn;
}

BOOL CPostnumberFormView::savePostnumbersToDB(void)
{
	CTransaction_postnumber rec;
	CPostnumReportRec *pRec = NULL;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CString S;
	if (m_bConnected)
	{
		if (m_pDB != NULL && pRows != NULL)
		{
			GetReportCtrl().Populate();
			for (long i = 0;i < pRows->GetCount();i++)
			{
				pRec = (CPostnumReportRec*)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
				{
					if (pRec->getIsDirty(0) || pRec->getIsDirty(1))
					{
						rec = CTransaction_postnumber(pRec->getID(),pRec->getColumnText(1),pRec->getColumnText(0),_T(""));

						if (!m_pDB->addPostnumber(rec))
							m_pDB->updPostnumber(rec);
					}	// if (pRec->getIsDirty(0) || pRec->getIsDirty(1))
				}	// if (pRec != NULL)
			}	// for (UINT i = 0;i < m_vecPostnumbersDataChangedOrAdded.size();i++)
		}	// if (m_pDB != NULL && pRows != NULL)
	}	// if (m_bConnected)

	return TRUE;
}

BOOL CPostnumberFormView::removePostnumberFromDB(void)
{
	BOOL bFound = FALSE;
	CTransaction_postnumber recPostnum;
	CString sPostNumber;
	// Ask user if he realy want to delete zipcode; 071204 p�d
	if (m_bConnected)
	{
		if (::MessageBox(this->GetSafeHwnd(),(m_LangStr[IDS_STRING275]),(m_LangStr[IDS_STRING207]),MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{

			CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
			if (pRow != NULL)
			{
				CPostnumReportRec *pRec = (CPostnumReportRec *)pRow->GetRecord();
				if (pRec != NULL)
				{
					sPostNumber = pRec->getColumnText(0);
					bFound = FALSE;
					if (!sPostNumber.IsEmpty())
					{
						// Check if the row to be deleted already's in the table.
						// If not, no need to run "removePostnumber"; 081204 p�d
						for (UINT i = 0;i < m_vecPostnumbersDataFromDB.size();i++)
						{
							recPostnum = m_vecPostnumbersDataFromDB[i];
							if (sPostNumber.CompareNoCase(recPostnum.getNumber()) == 0)
							{
								bFound = TRUE;
								break;
							}	// if (sPostNumber.CompareNoCase(recPostnum.getNumber()) == 0)
						}	// for (UINT i = 0;i < m_vecPostnumbersDataFromDB.size();i++)
					}	// if (!sPostNumber.IsEmpty())
					
					// Check if the row to removed is empty, hence, not in DB; 081204 p�d
					if (!bFound)
					{
						CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
						if (pRecs != NULL)
						{
							pRecs->RemoveRecord(pRec);
							GetReportCtrl().Populate();
							GetReportCtrl().UpdateWindow();
						}	// if (pRows != NULL)
					}
					else
					{
						CTransaction_postnumber recInReport = CTransaction_postnumber(pRec->getID(),
																																					pRec->getColumnText(1),
																																					pRec->getColumnText(0),
																																					_T(""));
						if (m_pDB != NULL)
						{
							m_pDB->removePostnumber(recInReport);
						}	// if (pDB != NULL)			
//						populateReport();
					}	
				}	// if (pRec != NULL)
			}	// if (pRec != NULL)
			return TRUE;
		}
	}
	return FALSE;
}

int CPostnumberFormView::getNextPostnumberID(void)
{
	int nMaxPostnumberID = 0;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			nMaxPostnumberID = m_pDB->getMaxPostnumberID();
			if (nMaxPostnumberID == 0)
				return 1;
			else
				return (nMaxPostnumberID + 1);
		}	// if (pDB != NULL)			
	}
	return nMaxPostnumberID;
}

// PROTECTED METHODS

BOOL CPostnumberFormView::setupReport1(void)
{
	if (GetReportCtrl().GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		GetReportCtrl().ShowWindow(SW_NORMAL);
		CXTPReportColumn *pCol = NULL;
		pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(0, (m_LangStr[IDS_STRING208]), 50));
		pCol->AllowRemove(FALSE);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		
		pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(1, (m_LangStr[IDS_STRING209]), 120));
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;

		GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
		GetReportCtrl().SetMultipleSelection( FALSE );
		GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
		GetReportCtrl().AllowEdit(TRUE);
		GetReportCtrl().FocusSubItems(TRUE);

		// Read hms_user_types table in hms_administrator scheme(database); 060216 p�d
		getPostnumbersFromDB();
		populateReport();

	}

	return TRUE;

}

void CPostnumberFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType, cx, cy);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CPostnumberFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			addPostnumbersToReport();
			savePostnumbersToDB();
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

			//{
			//	getPostnumbersFromDB();
			//}
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			::SetCursor(::LoadCursor(NULL,IDC_WAIT));
			getPostnumbersFromDB();
			if (savePostnumbersToDB())
			{
				getPostnumbersFromDB();
				populateReport();
			}
			::SetCursor(::LoadCursor(NULL,IDC_ARROW));
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			getPostnumbersFromDB();
			if (removePostnumberFromDB())
			{
				getPostnumbersFromDB();
				populateReport();
			}
			break;
		}	// case ID_DELETE_ITEM :
	}
	return 0L;
}

// Handle transaction on database species table; 060317 p�d

BOOL CPostnumberFormView::populateReport(void)
{
	// Get species from database; 060317 p�d
	//getPostnumbersFromDB();
	GetReportCtrl().ResetContent();

	if (m_vecPostnumbersDataFromDB.size() > 0)
	{
		GetReportCtrl().ResetContent();

		for (UINT i = 0;i < m_vecPostnumbersDataFromDB.size();i++)
		{
			CTransaction_postnumber rec = m_vecPostnumbersDataFromDB[i];
			GetReportCtrl().AddRecord(new	CPostnumReportRec(i,rec));
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)

	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();

	doSetNavigationBar();

	return TRUE;
}

void CPostnumberFormView::OnImportDataFile(void)
{
	CMDIPostnumberFrame *pFrame = (CMDIPostnumberFrame *)getFormViewParentByID(IDD_REPORTVIEW0);
	vecTransactionPostnumber vec;
	CString sFileName;

	CFileDialog f(TRUE,_T(".skv"),_T(""),4|2|OFN_FILEMUSTEXIST,_T("Semikolon *.skv|*.skv|"));
	if (f.DoModal() != IDOK) return;
	UpdateWindow();
	sFileName = f.GetPathName();
	if (pFrame != NULL)
		pFrame->getStatusBar().SetPaneText(1,m_LangStr[IDS_STRING3245]);
	readPostnumberTable(sFileName,vec);
	if (vec.size() > 0)
	{
		// Set wait cursor; 081017 p�d
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));

		GetReportCtrl().ResetContent();
/*
		if (pFrame != NULL)
		{
			pFrame->getProgressCtrl().SetRange(0,vec.size());
			pFrame->getProgressCtrl().SetPos(0);
			pFrame->getProgressCtrl().SetStep(1);
			pFrame->getProgressCtrl().ShowWindow(SW_NORMAL);
		}	// if (pFrame != NULL)
*/
		for (UINT i = 0;i < vec.size();i++)
		{
			CTransaction_postnumber rec = vec[i];
//			if (pFrame != NULL)
//				pFrame->getProgressCtrl().StepIt();
			GetReportCtrl().AddRecord(new	CPostnumReportRec(i,rec));
		}

		if (pFrame)
		{
//			pFrame->getProgressCtrl().ShowWindow(SW_HIDE);
			pFrame->getStatusBar().SetPaneText(1,m_LangStr[IDS_STRING216]);
		}

		// Set back to arrow cursor (default); 081017 p�d
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));

	}	// if (vec.size() > 0)
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CPostnumberFormView::OnCreatePostnumbers(void)
{
	CMDIPostnumberFrame *pFrame = (CMDIPostnumberFrame *)getFormViewParentByID(IDD_REPORTVIEW0);
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CPostnumReportRec *pRec = NULL;
	if (m_pDB != NULL && pRows != NULL)
	{
		if (pFrame != NULL)
		{
			UpdateWindow();
			pFrame->getStatusBar().SetPaneText(1,m_LangStr[IDS_STRING3246]);
/*
			pFrame->getProgressCtrl().SetRange(0,pRows->GetCount());
			pFrame->getProgressCtrl().SetPos(0);
			pFrame->getProgressCtrl().SetStep(1);
			pFrame->getProgressCtrl().ShowWindow(SW_NORMAL);
*/
		}	// if (pFrame != NULL)

		// Set wait cursor; 081017 p�d
		::SetCursor(::LoadCursor(NULL,IDC_WAIT));

		for (long i = 0;i < pRows->GetCount();i++)
		{
			if ((pRec = (CPostnumReportRec *)pRows->GetAt(i)->GetRecord()) != NULL)
			{
				CTransaction_postnumber rec = CTransaction_postnumber(i+1,pRec->getColumnText(1),pRec->getColumnText(0),getUserName().MakeUpper());
				// Check Postnumber and Postaddress; 090512 p�d
				//if (!pRec->getColumnText(1).IsEmpty() && !pRec->getColumnText(0))
				//{
					if (!m_pDB->addPostnumber(rec))
						m_pDB->updPostnumber(rec);
//					if (pFrame != NULL)
//						pFrame->getProgressCtrl().StepIt();

				//}	// if (!pRec->getColumnText(1).IsEmpty() && !pRec->getColumnText(0))
			}	
		}	// for (long i = 0;i < pRows->GetCount();i++)

		if (pFrame)
		{
//			pFrame->getProgressCtrl().ShowWindow(SW_HIDE);
			pFrame->getStatusBar().SetPaneText(1,m_LangStr[IDS_STRING216]);
		}

		// Set back to arrow cursor (default); 081017 p�d
		::SetCursor(::LoadCursor(NULL,IDC_ARROW));

	}	// if (m_pDB != NULL)
}

void CPostnumberFormView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		CPostnumReportRec *pRec = (CPostnumReportRec *)pItemNotify->pItem->GetRecord();
		// Need to have Contacts open; 071204 p�d
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pTabView)
		{
			// Get ContactsFormView
			CContactsFormView *pView = (CContactsFormView *)pTabView->getContactsFormView();
			if (pView)
			{
				UINT i = 0;
				// Find index of postnumer based on ID; 090225 p�d
				for (i = 0;i < m_vecPostnumbersDataFromDB.size();i++)
				{
					if (m_vecPostnumbersDataFromDB[i].getID() == pRec->getID())
						break;
				}
				UINT nIndex = i;
				if (nIndex >= 0 && nIndex < m_vecPostnumbersDataFromDB.size())
				{
					if (::MessageBox(this->GetSafeHwnd(),(m_LangStr[IDS_STRING279]),(m_LangStr[IDS_STRING207]),MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					{
						pView->setPostNumberAndPostAddress(m_vecPostnumbersDataFromDB[nIndex]);
						// Quit this module form; 071204 p�d
						CMDIPostnumberFrame *pFrame = (CMDIPostnumberFrame *)getFormViewParentByID(IDD_REPORTVIEW0);
						if (pFrame != NULL)
						{
							::SendMessage(pFrame->GetSafeHwnd(),WM_SYSCOMMAND,SC_CLOSE,0);
						}	// if (pFrame != NULL)
					}	// if (::MessageBox(this->GetSafeHwnd(),m_sAddZipCodeToProperty,m_sMessageCap,MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				}	// if (nIndex >= 0 && nIndex < m_vecPostnumbersDataFromDB.size())
			}	// if (pView)
		}	// if (pTabView)
	}	// if (pItemNotify->pRow)
}

void CPostnumberFormView::OnReportItemKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (!GetReportCtrl().GetFocusedRow())
		return;

	if (lpNMKey->nVKey == VK_RETURN)
	{		
		// Need to have Contacts open; 071204 p�d
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pTabView)
		{
			CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
			if (pRow != NULL)
			{
				CPostnumReportRec *pRec = (CPostnumReportRec *)pRow->GetRecord();
				if (pRec != NULL)
				{
					// Get ContactsFormView
					CContactsFormView *pView = (CContactsFormView *)pTabView->getContactsFormView();
					if (pView)
					{
						UINT nIndex = pRec->getID();
						if (nIndex >= 0 && nIndex < m_vecPostnumbersDataFromDB.size())
						{
							if (::MessageBox(this->GetSafeHwnd(),(m_LangStr[IDS_STRING279]),(m_LangStr[IDS_STRING207]),MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
							{
								pView->setPostNumberAndPostAddress(m_vecPostnumbersDataFromDB[nIndex]);
								// Quit this module form; 071204 p�d
								CMDIPostnumberFrame *pFrame = (CMDIPostnumberFrame *)getFormViewParentByID(IDD_REPORTVIEW0);
								if (pFrame != NULL)
								{
									::SendMessage(pFrame->GetSafeHwnd(),WM_SYSCOMMAND,SC_CLOSE,0);
								}	// if (pFrame != NULL)
							}	// if (::MessageBox(this->GetSafeHwnd(),m_sAddZipCodeToProperty,m_sMessageCap,MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
						}	// if (nIndex >= 0 && nIndex < m_vecPostnumbersDataFromDB.size())
					}	// if (pView)
				}					
			}	// if (pRow != NULL)
		}	// if (pTabView)
	}	// 	if (lpNMKey->nVKey == VK_RETURN)
}

void CPostnumberFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_CONTACTS_POSTNR_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CPostnumberFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_CONTACTS_POSTNR_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}
