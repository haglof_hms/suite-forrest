// Forrest.cpp :  Defines the initialization routines for the DLL.
//


#include "stdafx.h"
#include "Forrest.h"

#include "ResLangFileReader.h"

#include "MDIBaseFrame.h"
#include "SpeciesFormView.h"
#include "PostnumFormView.h"
#include "CategoryFormView.h"
#include "ContactsSelListFormView.h"
#include "CMPFormView.h"
#include "PropertySelListFormView.h"
//#include "PropertyRemoveSelListFormView.h"
#include "ImportDataFormView.h"
#include "ExternalDocsReportView.h"
#include "StandListFormView.h"
#include "SearchAndReplaceFormView.h"

#include "ContactsTabView.h"
#include "PropertyTabView.h"

#include "CreateTables.h"
/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

#include "afxdllx.h"    // standard MFC Extension DLL routines

#include <vector>

std::vector<HINSTANCE> m_vecHInstTable;

static CWinApp* pApp = AfxGetApp();
CMultiDocTemplate *pSpecies = NULL;

static AFX_EXTENSION_MODULE ForrestDLL = { NULL, NULL };

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	m_hInstance = hInstance;
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("Forrest.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(ForrestDLL, hInstance))
			return 0;

	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("Forrest.DLL Terminating!\n");
		// Terminate the library before destructors are called
		AfxTermExtensionModule(ForrestDLL);
	}
	return 1;   // ok
}

// Exported DLL initialization is run in context of running application
void InitSuite(CStringArray *user_modules,vecINDEX_TABLE &vecIndex,vecINFO_TABLE &vecInfo)
{
	CString sVersion;
	CString sCopyright;
	CString sCompany;
	// create a new CDynLinkLibrary for this app
	new CDynLinkLibrary(ForrestDLL);

	CString sModuleFN = getModuleFN(m_hInstance);
	// Setup the language filename
	CString sLangFN;
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	m_vecHInstTable.clear();

	///////////////////////////////////////////////////////////////////////////////////////
	// Doc\Views to be displayed on HMSShell navigation bar.
	// I.e. included in the vecIndex vector; 070123 p�d
	CMultiDocTemplate *pSpecies = new CMultiDocTemplate(IDD_FORMVIEW,	
																	RUNTIME_CLASS(CMDIFrameDoc),
																	RUNTIME_CLASS(CMDISpeciesFrame),
																	RUNTIME_CLASS(CSpeciesFormView));
	pApp->AddDocTemplate(pSpecies);
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW0,	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIPostnumberFrame),
			RUNTIME_CLASS(CPostnumberFormView)));
	vecIndex.push_back(INDEX_TABLE(IDD_REPORTVIEW0,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW2,	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDICategoryFrame),
			RUNTIME_CLASS(CCategoryFormView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW2,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW3, 	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIContactsFrame),
			RUNTIME_CLASS(CContactsTabView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW3,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW5, 	
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIPropertyFrame),
			RUNTIME_CLASS(CPropertyTabView)));
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW5,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW2, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDICountyMunicipalParishFrame),
			RUNTIME_CLASS(CCMPFormView))); // CMP = County,Maunicipal and Parish; 070123 p�d
	vecIndex.push_back(INDEX_TABLE(IDD_REPORTVIEW2,sModuleFN,sLangFN,TRUE));
	// Added 2008-07-10 P�D
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW7, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIImportDataFrame),
			RUNTIME_CLASS(CImportDataReportView))); 
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW7,sModuleFN,sLangFN,TRUE));

	// Added 2008-09-25 P�D
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW8, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CMDIExternalDocsFrame),
			RUNTIME_CLASS(CExternalDocsReportView))); 
	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW8,sModuleFN,sLangFN,TRUE));

	// Added 2008-09-25 P�D
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW10, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CSearchAndReplaceFrame),
			RUNTIME_CLASS(CSearchAndReplaceFormView))); 
//	vecIndex.push_back(INDEX_TABLE(IDD_FORMVIEW8,sModuleFN,sLangFN,TRUE));

	///////////////////////////////////////////////////////////////////////////////////////
	// Doc\Views NOT to be displayed on HMSShell navigation bar.
	// I.e. don't include the Item in the vecIndex vector; 070123 p�d

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW1, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CContactsSelectListFrame),
			RUNTIME_CLASS(CContactsSelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	vecIndex.push_back(INDEX_TABLE(IDD_REPORTVIEW1,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW3, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CPropertySelectListFrame),
			RUNTIME_CLASS(CPropertySelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	vecIndex.push_back(INDEX_TABLE(IDD_REPORTVIEW3,sModuleFN,sLangFN,TRUE));

	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW4, 
			RUNTIME_CLASS(CMDIFrameDoc),
			RUNTIME_CLASS(CStandSelectionListFrame),
			RUNTIME_CLASS(CStandSelListFormView)));
// Remove comment to add Contact selection list to Index for
// HMSShell navigator tree; 060107 p�d
//	vecIndex.push_back(INDEX_TABLE(IDD_REPORTVIEW4,sModuleFN,sLangFN,TRUE));


	// Get version information; 060803 p�d
	const LPCTSTR VER_NUMBER						= _T("FileVersion");
	const LPCTSTR VER_COMPANY						= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	sVersion		= getVersionInfo(m_hInstance,VER_NUMBER);
	sCopyright	= getVersionInfo(m_hInstance,VER_COPYRIGHT);
	sCompany		= getVersionInfo(m_hInstance,VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,1 /* Suite */,
															 (TCHAR*)sLangFN.GetBuffer(),
															 (TCHAR*)sVersion.GetBuffer(),
															 (TCHAR*)sCopyright.GetBuffer(),
															 (TCHAR*)sCompany.GetBuffer()));

	/* *****************************************************************************
		Load user module(s), specified in the ShellTree data file for this SUITE
	****************************************************************************** */

	typedef CRuntimeClass *(*Func)(CWinApp *,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);
  Func proc;
	// Try to get modules connected to this Suite; 051129 p�d
	if (user_modules->GetCount() > 0)
	{
		for (int i = 0;i < user_modules->GetCount();i++)
		{
			CString sPath;
			sPath.Format(_T("%s%s"),getModulesDir(),user_modules->GetAt(i));
			// Check if the file exists, if not tell USER; 051213 p�d
			if (fileExists(sPath))
			{
				HINSTANCE hInst = AfxLoadLibrary(sPath);
				if (hInst != NULL)
				{
					m_vecHInstTable.push_back(hInst);
#ifdef UNICODE
				USES_CONVERSION;
					proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], W2A(INIT_MODULE_FUNC) );
#else
					proc = (Func)GetProcAddress((HMODULE)m_vecHInstTable[m_vecHInstTable.size() - 1], (INIT_MODULE_FUNC) );
#endif
					if (proc != NULL)
					{
						// call the function
						proc(pApp,sModuleFN,vecIndex,vecInfo);
					}	// if (proc != NULL)
				}	// if (hInst != NULL)

			}	// if (fileExists(sPath))
			else
			{
				// Set Messages from language file; 051213 p�d
				::MessageBox(0,_T("File doesn't exist\n" + sPath),_T("Error"),MB_OK);
			}
		}	// for (int i = 0;i < m_sarrModules.GetCount();i++)
	}	// if (m_sarrModules.GetCount() > 0)

	// Do a check to see if database tables are created; 080131 p�d
	// Empty argument = use default database; 081001 p�d
	DoDatabaseTables(_T(""));
	DoAlterTables(_T(""));
/*
	// NOT USED (BY PL); 080903 p�d
	int nIsAddData = getAddDataToTables();
	if (nIsAddData == 1)
	{
		DoAddPostnumbersToTable();
		DoAddCountyMunicipalAndParishToTable();
		setAddDataToTables(0);	// Don't add any more; 080903 p�d
	}
*/
}

void DoDatabaseTables(LPCTSTR db_name)
{
	vecScriptFiles vec;
	CString sLang(getLangSet());
	// Check if there's a connection set; 070329 p�d
	if (getIsDBConSet() == 1)
	{
		// Set Species depending on language; 090904 p�d
		if (sLang.CompareNoCase(_T("SVE")) == 0)
			vec.push_back(Scripts(TBL_SPECIES,table_FstSpeciesSVE,db_name));
		else if(sLang.CompareNoCase(_T("NOR")) == 0)	//#3862 f�r Norge
			vec.push_back(Scripts(TBL_SPECIES,table_FstSpeciesNOR,db_name));
		else // Use English (American), as default; 090904 p�d
			vec.push_back(Scripts(TBL_SPECIES,table_FstSpeciesENU,db_name));

		vec.push_back(Scripts(TBL_P30SPECIES_OBJECT,table_ElvP30Spec_Obj,db_name));

		vec.push_back(Scripts(TBL_POSTNUMBER,table_FstPostnumber,db_name));
		vec.push_back(Scripts(TBL_CATEGORY,table_FstCategories,db_name));
		vec.push_back(Scripts(TBL_CONTACTS,table_FstContacts,db_name));
		vec.push_back(Scripts(TBL_CATEGORY_CONTACTS,table_FstCategoriesForContacts,db_name));
		vec.push_back(Scripts(TBL_PROPERTY,table_FstProperties,db_name));
		vec.push_back(Scripts(TBL_PROP_OWNER_TYPE,table_FstPropertyOwners,db_name));
		vec.push_back(Scripts(TBL_COUNTY_MUNICIPAL_PARISH,table_FstCountyMunicipalParish,db_name));
		vec.push_back(Scripts(TBL_EXTERNAL_DOCS,table_FstExternalDocs,db_name));


		runSQLScriptFileEx1(vec,Scripts::TBL_CREATE);
		vec.clear();
	}
}

void DoAlterTables(LPCTSTR db_name)
{
	vecScriptFiles vec;
	// Check if there's a connection set; 090525 p�d
	if (getIsDBConSet() == 1)
	{
		// Alter table scripts
		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContacts090825_1,db_name));
		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContacts090825_2,db_name));
		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContacts090825_3,db_name));
		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContacts090825_4,db_name));

		vec.push_back(Scripts(TBL_PROPERTY,alter_FstProperty091005_1,db_name));
		//Added altering owner_share to text length 10, 20110126 J� #1937.
		vec.push_back(Scripts(TBL_PROP_OWNERS,alter_FstPropertyOwnerShare,db_name));
		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContacts110315_1,db_name));

		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContacts121121_1,db_name));
		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContacts121121_2,db_name));
		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContacts121121_3,db_name));
		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContacts121121_4,db_name));
		// HMS-76 �ndra antal tecken f�r adressf�ltet f�r kontakter till 150, 20220209 J�
		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContactAddress20220209,db_name));
		vec.push_back(Scripts(TBL_CONTACTS,alter_FstContactCoAddress20220209,db_name));

		vec.push_back(Scripts(TBL_PROPERTY,alter_FstProperty121121_1,db_name));

		// #3726: koordinat-kolumn i fst_property_table
		vec.push_back(Scripts(TBL_PROPERTY, alter_FstProperty130905_1, db_name));


		//#HMS-94 20220916 J� l�gga till info om vilket p30 pris tr�dslag som tr�dslaget skall knytas till...dvs tall gran eller l�v...1/2/3.
		vec.push_back(Scripts(TBL_SPECIES,alter_FstSpecies20220916_1,db_name));
		
		runSQLScriptFileEx1(vec,Scripts::TBL_ALTER);
		vec.clear();

		checkP30Species(db_name);
	}
}


void checkP30Species(LPCTSTR db_name)
{
	vecScriptFiles vec;
	CString cSql=_T("");

	//#HMS-94 20220919 J� G� ignom tr�dslagen och s�tt r�tt p30 tr�dslag(1=tall,2=gran,3=l�v) som skall g�lla f�r respektive tr�dslag, samma som i createtables TBL_SPECIES
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 1 ELSE p30_spec END WHERE spc_id = 1");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 2 ELSE p30_spec END WHERE spc_id = 2");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 3");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 4");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 2 ELSE p30_spec END WHERE spc_id = 5");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 1 ELSE p30_spec END WHERE spc_id = 6");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 2 ELSE p30_spec END WHERE spc_id = 7");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 8");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 9");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 10");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 11");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 12");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 13");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 14");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 15");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 16");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 2 ELSE p30_spec END WHERE spc_id = 17");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 2 ELSE p30_spec END WHERE spc_id = 18");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 19");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 20");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 21");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 22");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 23");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 2 ELSE p30_spec END WHERE spc_id = 24");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 25");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id = 26");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	//S�tt �vriga tr�dslag till l�v
	cSql=_T("UPDATE %s set p30_spec = CASE WHEN p30_spec < 1 THEN 3 ELSE p30_spec END WHERE spc_id > 26");vec.push_back(Scripts(TBL_SPECIES,cSql,db_name));
	runSQLScriptFileEx1(vec,Scripts::TBL_ALTER);
	vec.clear();
}

void DoAddPostnumbersToTable(void)
{
	CString sPath;
	sPath.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_SETUP_DATA_FILES,POSTNUMBER_FN);
	setupPostnumberTable(sPath);
}

// Create postnumbers in "fst_postnumber_table"; 080131 p�d
void DoAddCountyMunicipalAndParishToTable(void)
{
	CString sPath;
	sPath.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_SETUP_DATA_FILES,COUNTY_MUNICIPAL_PARISH_FN);
	setupCountyMunicipalAndParishTable(sPath);
}

void OpenSuite(int idx,LPCTSTR func,CWnd *wnd,vecINDEX_TABLE &vecIndex,int *ret)
{
	CDocTemplate *pTemplate;
	CString sDocName;
	CString sResStr;
	CString sModuleFN;
	CString sVecIndexTableModuleFN;
	CString sLangFN;
	CString sCaption;
	int nTableIndex;
	int nDocCounter;
	BOOL bFound = FALSE;
	BOOL bIsOneInst;
	CString sDocTitle;
	CString S;
	ASSERT(pApp != NULL);

	// Get path and filename of this SUITE; 051213 p�d
	sModuleFN = getModuleFN(m_hInstance);

	// Find template name for idx value; 051124 p�d
	if (vecIndex.size() == 0)
		return;

	for (UINT i = 0;i < vecIndex.size();i++)
	{
		// Get index of this Window, as set in Doc template
		nTableIndex = vecIndex[i].nTableIndex;
		// Get filename including searchpath to THIS SUITE, as set in
		// the table index vector, for suites and module(s); 051213 p�d
		sVecIndexTableModuleFN = vecIndex[i].szSuite;
		if (nTableIndex == idx && 
				sModuleFN.Compare(sVecIndexTableModuleFN) == 0)
		{
/*
		S.Format(_T("nTableIndex %d\nsVecIndexTableModuleFN %s"),
			nTableIndex,sVecIndexTableModuleFN);
		AfxMessageBox(S);
/*/
			// Need to setup the Actual filename here, because we need to 
			// get the Language set in registry, on Openning a View; 051214 p�d
			// Get language filename
			sLangFN = vecIndex[i].szLanguageFN;

			bFound = TRUE;
			bIsOneInst = vecIndex[i].bOneInstance;
			break;
		}	// if (nTableIndex == idx)
	}	// for (UINT i = 0;i < vecIndex.size();i++)

	// Make sure the languagefile is in directory; 081215 p�d
	if (!fileExists(sLangFN))
	{
		S.Format(_T("NB! Can not open module.\n\nPlease check languagefile(s) and other dependencies."));
		AfxMessageBox(S);
		return;
	}

	if (bFound)
	{

		// Get the stringtable resource, matching the TableIndex
		// This string is compared to the title of the document; 051212 p�d
		sResStr.LoadString(nTableIndex);

		RLFReader xml;
		StrMap strMap;
		if (xml.LoadEx(sLangFN,strMap))
		{
			sCaption = strMap[nTableIndex];
		}
		xml.clean();
/*
	S.Format(_T("nTableIndex %d\nidx  %d\nsVecIndexTableModuleFN %s\nsModuleFN %s\nsLangFN %s\nsCaption %s"),
						nTableIndex,
						idx,
						sVecIndexTableModuleFN,
						sModuleFN,
						sLangFN,
						sCaption);
	AfxMessageBox(S);
*/
		// Check if the document or module is in this SUITE; 051213 p�d
		POSITION pos = pApp->GetFirstDocTemplatePosition();
		while(pos != NULL)
		{
			pTemplate = pApp->GetNextDocTemplate(pos);
			pTemplate->GetDocString(sDocName, CDocTemplate::docName);
			ASSERT(pTemplate != NULL);
			// Need to add a linefeed, infront of the docName.
			// This is because, for some reason, the document title,
			// set in resource, must have a linefeed.
			// OBS! Se documentation for CMultiDocTemplate; 051212 p�d
			sDocName = '\n' + sDocName;
			if (pTemplate && sDocName.Compare(sResStr) == 0)
			{
				
				if (bIsOneInst)
				{
					POSITION posDOC = pTemplate->GetFirstDocPosition();

					while(posDOC != NULL)
					{
						CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
						POSITION posView = pDocument->GetFirstViewPosition();
						if(posView != NULL)
						{
							CView* pView = pDocument->GetNextView(posView);
							pView->GetParent()->BringWindowToTop();
							pView->GetParent()->SetFocus();
							// Check if window is minimiced.
							// If so, show window in normal state; 071030 p�d
							if (pView->GetParent()->IsIconic())
							{
								pView->GetParent()->ShowWindow(SW_NORMAL);
							}
							posDOC = (POSITION)1;
							break;
						}	// if(posView != NULL)
					}	// while(posDOC != NULL)

					if (posDOC == NULL)
					{

						pTemplate->OpenDocumentFile(NULL);

						// Find the CDocument for this tamplate, and set title.
						// Title is set in Languagefile; OBS! The nTableIndex
						// matches the string id in the languagefile; 051129 p�d
						POSITION posDOC = pTemplate->GetFirstDocPosition();
						while (posDOC != NULL)
						{
							CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
							// Set the caption of the document. Can be a resource string,
							// a string set in the language xml-file etc.
							sDocTitle.Format(_T("%s"),sCaption);
							pDocument->SetTitle(sDocTitle);
							pDocument->m_bAutoDelete = TRUE;
							
							POSITION posView = pDocument->GetFirstViewPosition();
							if(posView != NULL)
							{
								CView* pView = pDocument->GetNextView(posView);
								// Check if window is minimiced.
								// If so, show window in normal state; 071030 p�d
								if (pView->GetParent()->IsIconic())
								{
									pView->GetParent()->ShowWindow(SW_NORMAL);
								}
								posDOC = (POSITION)1;
								break;
							}	// if(posView != NULL)
						}	// while (posDOC != NULL)
						break;
					}	// if (posDOC == NULL)
				}	// if (bIsOneInst)
				else
				{
						pTemplate->OpenDocumentFile(NULL);

						// Find the CDocument for this tamplate, and set title.
						// Title is set in Languagefile; OBS! The nTableIndex
						// matches the string id in the languagefile; 051129 p�d
						POSITION posDOC = pTemplate->GetFirstDocPosition();
						nDocCounter = 1;

						while (posDOC != NULL)
						{
							CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
							// Set the caption of the document. Can be a resource string,
							// a string set in the language xml-file etc.
							sDocTitle.Format(_T("%s (%d)"),sCaption,nDocCounter);
							pDocument->SetTitle(sDocTitle);
							pDocument->m_bAutoDelete = TRUE;
							nDocCounter++;
						}

						break;
				}
			}	// if (pTemplate && sDocName.Compare(sResStr) == 0)
		}	// while(pos != NULL)
		*ret = 1;
	}	// if (bFound)
	else
	{
		*ret = 0;
	}
}
