#pragma once
#ifndef EXCELIMPORTLIBXL_H
#define EXCELIMPORTLIBXL_H

#include "Resource.h"
#include "libxl.h"

using namespace libxl;

class CExcelImportLibXl
{
public:
	CExcelImportLibXl();	// constructor
	CExcelImportLibXl(CString File, CString SheetOrSeparator, bool Backup = true,bool Convert_Numbers = true);
	~CExcelImportLibXl(); // Perform some cleanup functions
	bool ReadRow(CStringArray &RowValues, long row = 0); // Read a row from spreadsheet. Default is read the next row
	void GetFieldNames (CStringArray &FieldNames); // Get the header row from spreadsheet
	long GetTotalRows(); // Get total number of rows in  spreadsheet

	int GetSheetCount();	// get amount of sheets in workbook
	CString GetSheetName(int nIndex);	// get the name of a sheet
	int SetCurrentSheet(int nIndex);	// set which sheet to use
	int GetColCount();	// get amount of columns in current sheet

	CString GetCellValue(int nRow, int nCol);
	int Open(CString csFile, CString csSheetOrSeparator, bool bBackup, bool bConvert_Numbers);
private:
	int Open(CString csFile);	// open Excel file
	Book* m_Book;
	int m_nSheetIndex;
	int m_nFiletype;	// type of file EXCEL or CSV

	long m_dCurrentRow; // Index of current row, starting from 1
	long m_dTotalRows; // Total number of rows in spreadsheet
	
	CString m_sSeparator;

	CStringArray m_atempArray; // Temporary array for use by functions
	CStringArray m_aFieldNames; // Header row in spreadsheet
	CStringArray m_aRows; // Content of all the rows in spreadsheet
};


class CExcelibXl_create_template
{
//private:
	Book *m_book;
	Sheet *m_sheet;
	CString m_sFileName;
public:
	 CExcelibXl_create_template();
	 CExcelibXl_create_template(CString file_name);
	 virtual ~CExcelibXl_create_template();

	 void createTemplate(HWND hWnd = NULL,CString msg_cap = L"",CString msg = L"");
};
#endif
