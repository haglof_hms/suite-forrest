// StandListFormView.cpp : implementation file
//

#include "stdafx.h"
#include "PropertyTabView.h"
#include "Forrest.h"
#include "MDIBaseFrame.h"
#include "StandListFormView.h"
#include "StandsInPropertyFormView.h"

#include "ResLangFileReader.h"

/////////////////////////////////////////////////////////////////////////////
// CStandsReportFilterEditControl

IMPLEMENT_DYNCREATE(CStandsReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CStandsReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CStandsReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CString S;
	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != "");
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

// CStandSelListFormView

IMPLEMENT_DYNCREATE(CStandSelListFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CStandSelListFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)
	ON_COMMAND(ID_TBBTN_OPEN, OnShowFieldFilter)
	ON_COMMAND(ID_TBBTN_CREATE, OnShowFieldFilterOff)
	ON_COMMAND(ID_TBTN_ADD, OnAddStandToProperty)
END_MESSAGE_MAP()

CStandSelListFormView::CStandSelListFormView()
	: CXTPReportView()
{
	m_pDB = NULL;
	m_nSelectedColumn = -1;	// Nothing selected yet; 070125 p�d
}

CStandSelListFormView::~CStandSelListFormView()
{
}

void CStandSelListFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setupReport();

	populateReport();

	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);

	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT2_1, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if (m_wndLbl.GetSafeHwnd() == NULL)
	{
		m_wndLbl.SubclassDlgItem(IDC_LABEL2_2, &pWnd->m_wndFilterEdit);
		m_wndLbl.SetBkColor(INFOBK);
	}

	if (m_wndLbl1.GetSafeHwnd() == NULL)
	{
		m_wndLbl1.SubclassDlgItem(IDC_LABEL2_1, &pWnd->m_wndFilterEdit);
		m_wndLbl1.SetBkColor(INFOBK);
		m_wndLbl1.SetLblFont(14,FW_BOLD);
	}

	LoadReportState();
}

BOOL CStandSelListFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CStandSelListFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;
	SaveReportState();
	CXTPReportView::OnDestroy();	
}

BOOL CStandSelListFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CStandSelListFormView diagnostics

#ifdef _DEBUG
void CStandSelListFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CStandSelListFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CStandSelListFormView message handlers

// CStandSelListFormView message handlers
void CStandSelListFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CStandSelListFormView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Catch message sent from (WM_USER_MSG_SUITE)
LRESULT CStandSelListFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_SHOWVIEW_MSG :
		{
			break;
		}
	}	// switch (wParam)

	return 0L;
}


// Create and add Assortment settings reportwindow
BOOL CStandSelListFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
				m_sGroupByThisField	= (xml.str(IDS_STRING244));
				m_sGroupByBox				= (xml.str(IDS_STRING245));
				m_sFieldChooser			= (xml.str(IDS_STRING246));
				m_sFilterOn					= (xml.str(IDS_STRING2430));

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP3));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					GetReportCtrl().SetImageList(&m_ilIcons);

					GetReportCtrl().ShowWindow( SW_NORMAL );
					// Add these 3 lines to add scrollbars for View; 070319 p�d
					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, _T(""), 18,FALSE,12));
					pCol->AllowRemove(FALSE);
					pCol->SetEditable( TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING2486)), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING2483)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING2484)), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_RIGHT );
					pCol->SetAlignment( DT_RIGHT );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml.str(IDS_STRING2485)), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_RIGHT );
					pCol->SetAlignment( DT_RIGHT );

					populateReport();		

					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml.Load(m_sLangFN))
			xml.clean();
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CStandSelListFormView::populateReport(void)
{
	double fM3Sk = 0.0;
	double fGy = 0.0;
	// Get Stand with No property, from database; 090812 p�d
	getStandsNoProperty();
	// populate report; 090812 p�d
	GetReportCtrl().ResetContent();

	if (m_vecStandsNoProperty.size() > 0)
	{
		for (UINT i = 0;i < m_vecStandsNoProperty.size();i++)
		{
			CTransaction_trakt rec = m_vecStandsNoProperty[i];
			
			fM3Sk = 0.0;
			fGy = 0.0;
			getStandData(rec.getTraktID(),&fM3Sk,&fGy);
			GetReportCtrl().AddRecord(new CNoPropStandsReportRec(rec.getTraktID(),rec.getTraktNum(),rec.getTraktName(),fM3Sk,fGy));
		}
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();

}


void CStandSelListFormView::getStandsNoProperty(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktNoProperty(m_vecStandsNoProperty);
	}
}

void CStandSelListFormView::getStandData(int trakt_id,double *m3sk,double *gy)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(trakt_id,m3sk,gy);
	}
}

void CStandSelListFormView::setFilterWindow(void)
{
	if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	{
		CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
		CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
		int nColumn = pColumn->GetIndex();
		if (pCols && nColumn < pCols->GetCount())
		{
			for (int i = 0;i < pCols->GetCount();i++)
			{
				pCols->GetAt(i)->SetFiltrable( i == nColumn );
			}	// for (int i = 0;i < pCols->GetCount();i++)
		}	// if (pCols && nColumn < pCols->GetCount())
		m_wndLbl.SetWindowText(m_sFilterOn + _T(" :"));
		m_wndLbl1.SetWindowText(pColumn->GetCaption());
	}	// if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	else
	{
		m_wndLbl.SetWindowText(m_sFilterOn + _T(" :"));
		m_wndLbl1.SetWindowText(_T(""));
	}
}

void CStandSelListFormView::OnShowFieldFilter()
{
	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pWnd != NULL)
	{
		setFilterWindow();
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}
}

void CStandSelListFormView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));
	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}
}

void CStandSelListFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pColumn)
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
	}

	// Check if Fileter window is visible. If so change filter column to selected column; 090224 p�d
	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pWnd != NULL)
	{
		if (pWnd->m_wndFilterEdit.IsVisible())
		{
			setFilterWindow();
			pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
		}	// if (pWnd->m_wndFilterEdit.IsVisible())
	}	// if (pWnd != NULL)
	pWnd = NULL;

}

void CStandSelListFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupByBox);

	if (GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS, MF_BYCOMMAND|MF_CHECKED);
	}

	if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn* pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_GROUP_BYTHIS:

			if (pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
			{
				pColumns->GetGroupsOrder()->Add(pColumn);
			}
			GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
			GetReportCtrl().Populate();
			break;
		case ID_SHOW_GROUPBOX:
			GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;
	}

}

void CStandSelListFormView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (!GetReportCtrl().GetFocusedRow())
		return;
}

void CStandSelListFormView::OnAddStandToProperty(void)
{
	CString S;
	CNoPropStandsReportRec *pRec = NULL;
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
	if (pTabView)
	{
		CPropertyFormView *pView0 = (CPropertyFormView *)pTabView->getPropertyFormView();
		if (pView0 != NULL)
		{
			int nPropID = pView0->getActiveProperty().getID();
			// We'll start to connect selected Stands to this property; 090812 p�d
			if (pRecs != NULL)
			{
				for (int i = 0;i < pRecs->GetCount();i++)
				{
					pRec = (CNoPropStandsReportRec*)pRecs->GetAt(i);
					if (pRec->getColumnCheck(COLUMN_0))
					{
						// Connect Stands to Propery; 090812 p�d
						if (m_pDB != NULL)
							m_pDB->updConnectPropertyToStand(pRec->getTraktID(),nPropID);
					}	// if (pRec->getColumnCheck(COLUMN_0))
				}	// for (int i = 0;i < pRecs->GetCount();i++)
			}	// if (pRecs != NULL)
		}	// if (pView0 != NULL)

		// Get CStandsInPropertyFormView; 090812 p�d
		CStandsInPropertyFormView *pView = (CStandsInPropertyFormView *)pTabView->getPropStandsFormView();
		if (pView)
		{
			pView->doRePopulate();
			pView = NULL;
		}	// if (pView)
		pTabView = NULL;
	}	// if (pTabView)
}

// CStandSelListFormView message handlers

void CStandSelListFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("FilterText"), _T(""));
	// Get selected column index into registry; 070125 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("SelColIndex"),0);

	setFilterWindow();
	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);
	CStandSelectionListFrame* pWnd = (CStandSelectionListFrame *)getFormViewParentByID(IDD_REPORTVIEW4);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(sFilterText != "");
	}
}

void CStandSelListFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("FilterText"), sFilterText);
	// Set selected column index into registry; 070125 p�d
	AfxGetApp()->WriteProfileInt((REG_WP_STANDS_SELLIST_REPORT_KEY), _T("SelColIndex"), m_nSelectedColumn);

}

