// StandsInPropertyFormView.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "StandsInPropertyFormView.h"

#include "ResLangFileReader.h"

#include "PropertyTabView.h"

// CStandsInPropertyFormView

IMPLEMENT_DYNCREATE(CStandsInPropertyFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CStandsInPropertyFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, IDC_PROP_STANDS_REPORT, OnReportItemClick)
	ON_NOTIFY(NM_KEYDOWN, IDC_PROP_STANDS_REPORT, OnReportKeyDown)

//	ON_BN_CLICKED(IDC_BUTTON3_1, &CStandsInPropertyFormView::OnBnClickedButton31)
//	ON_BN_CLICKED(IDC_BUTTON3_2, &CStandsInPropertyFormView::OnBnClickedButton32)

	ON_BN_CLICKED(ID_TOOLS_ADD_STAND_TO_PROP, &CStandsInPropertyFormView::OnBnClickedButton31)
	ON_BN_CLICKED(ID_TOOLS_DEL_STAND_FROM_PROP, &CStandsInPropertyFormView::OnBnClickedButton32)

END_MESSAGE_MAP()


CStandsInPropertyFormView::CStandsInPropertyFormView()
	: CXTResizeFormView(CStandsInPropertyFormView::IDD)
{
	m_bInitialized = FALSE;
}

CStandsInPropertyFormView::~CStandsInPropertyFormView()
{
}

void CStandsInPropertyFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;
	m_ilIcons.DeleteImageList();
	SaveReportState();
	CXTResizeFormView::OnDestroy();	
}

void CStandsInPropertyFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP
}

BOOL CStandsInPropertyFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CStandsInPropertyFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	//SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		// Setup language filename; 051214 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport2();

		m_bInitialized = TRUE;

		LoadReportState();
		VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
		CBitmap bmp;
		VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
		m_ilIcons.Add(&bmp, RGB(255, 0, 255));
		m_wndReport2.SetImageList(&m_ilIcons);
	}

}

BOOL CStandsInPropertyFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CStandsInPropertyFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	
	if (m_wndReport2.GetSafeHwnd())
	{
		setResize(&m_wndReport2,1,2,cx - 2,cy - 4);
	}

}

void CStandsInPropertyFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	int nCol=-1;
	CRect rect;
	POINT pt;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	nCol=pItemNotify->pColumn->GetItemIndex();
	switch (nCol)
	{
	//Lagt till funktion f�r att skicka ett meddelande att �pnna ett objekt till UMLandvalue, Feature #2590,#2529 20111125 J�
	case COLUMN_0 :
		// Do a hit-test; 080513 p�d
		rect = pItemNotify->pColumn->GetRect();		
		pt = pItemNotify->pt;
		// Check if the user clicked on the Icon or not; 080513 p�d
		if (hitTest_X(pt.x,rect.left,13)) 
		{
			CPropStandsReportRec *pRec = (CPropStandsReportRec*)pItemNotify->pItem->GetRecord();
			if (pRec != NULL)
			{
				if(pRec->getObjId()>-1)
				{
					//Open contact details for this specific owner
					CString sLangFN(getLanguageFN(getLanguageDir(),REG_LANDVALUE_KEY,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
					showFormView(5200,sLangFN);
					//Send message to forest suite to open contact form with a specific contact id, feature #2263 J� 20110808
					AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x04, 
						(LPARAM)&_doc_identifer_msg(_T("Module118"),_T("Module113"),_T(""),pRec->getObjId(),m_nActivePropertyID,0));
				}

			}
		}
		break;
	}
}

void CStandsInPropertyFormView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result)
{
}

BOOL CStandsInPropertyFormView::populateReport(void)
{
	CString sObjID,S;
	double fM3Sk = 0.0;
	double fGy = 0.0;
	vecPropInObjects vecPInObj;
	// Get Owners for property, from database; 090810 p�d
	getStandsForProperty(&m_nActivePropertyID);
	// populate report; 090810 p�d
	m_wndReport2.ResetContent();

	if (m_vecTraktForProperty.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktForProperty.size();i++)
		{
			fM3Sk = 0.0;
			fGy = 0.0;
			CTransaction_trakt rec = m_vecTraktForProperty[i];
			if (rec.getTraktPropID() == m_nActivePropertyID)
			{
				vecPInObj.clear();
				getStandData(rec.getTraktID(),&fM3Sk,&fGy);
				isPropertyUsedInObject(rec.getTraktID(),m_nActivePropertyID,vecPInObj);
				//Lagt till funktion f�r att skriva ut en ny rad f�r varje objekt som fastigheten ing�r i, Feature #2590,#2529 20111125 J�
				if(vecPInObj.size()>0)
				{
					for(int nObj=0;nObj<vecPInObj.size();nObj++)
						m_wndReport2.AddRecord(new CPropStandsReportRec(rec.getTraktID(),m_sYes,m_sNo,rec.getTraktNum(),rec.getTraktName(),fM3Sk,fGy,vecPInObj[nObj].getObjName(),vecPInObj[nObj].getObjID()));	
				}
				else
				{
				m_wndReport2.AddRecord(new CPropStandsReportRec(rec.getTraktID(),m_sYes,m_sNo,rec.getTraktNum(),rec.getTraktName(),fM3Sk,fGy,_T(""),-1));
				}
			}
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)
	m_wndReport2.Populate();
	m_wndReport2.UpdateWindow();

	return TRUE;
}


void CStandsInPropertyFormView::doRePopulate(void)
{
	CString S;
	double fM3Sk = 0.0;
	double fGy = 0.0;
	vecPropInObjects vecPInObj;
	// Get Owners for property, from database; 090810 p�d
	if (m_pDB != NULL)
		m_pDB->getTraktForProperty(m_nActivePropertyID,m_vecTraktForProperty);	
	// populate report; 090810 p�d
	m_wndReport2.ResetContent();

	if (m_vecTraktForProperty.size() > 0)
	{
		for (UINT i = 0;i < m_vecTraktForProperty.size();i++)
		{
			fM3Sk = 0.0;
			fGy = 0.0;
			CTransaction_trakt rec = m_vecTraktForProperty[i];
			if (rec.getTraktPropID() == m_nActivePropertyID)
			{
				vecPInObj.clear();
				getStandData(rec.getTraktID(),&fM3Sk,&fGy);
				isPropertyUsedInObject(rec.getTraktID(),m_nActivePropertyID,vecPInObj);
				//Lagt till funktion f�r att skriva ut en ny rad f�r varje objekt som fastigheten ing�r i, Feature #2590,#2529 20111125 J�
				if(vecPInObj.size()>0)
				{
					for(int nObj=0;nObj<vecPInObj.size();i++)
						m_wndReport2.AddRecord(new CPropStandsReportRec(rec.getTraktID(),m_sYes,m_sNo,rec.getTraktNum(),rec.getTraktName(),fM3Sk,fGy,vecPInObj[nObj].getObjName(),vecPInObj[nObj].getObjID()));	
				}
				else
				{
					m_wndReport2.AddRecord(new CPropStandsReportRec(rec.getTraktID(),m_sYes,m_sNo,rec.getTraktNum(),rec.getTraktName(),fM3Sk,fGy,_T(""),-1));
				}
			}
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)
	m_wndReport2.Populate();
	m_wndReport2.UpdateWindow();
}

BOOL CStandsInPropertyFormView::getStandsForProperty(int *prop_id)
{
	BOOL bReturn = FALSE;
	CTransaction_property recProp;
	CPropertyTabView *pView = (CPropertyTabView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		CPropertyFormView* pView0 = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView0 != NULL)
		{
			recProp = pView0->getActiveProperty();
			// Get Owners for property, from database; 060317 p�d
			if (m_pDB != NULL)
			{
				m_pDB->getTraktForProperty(recProp.getID(),m_vecTraktForProperty);	
				*prop_id = recProp.getID();
				bReturn = TRUE;
			}	// if (m_pDB != NULL)
		}
	}

	return bReturn;
}

void CStandsInPropertyFormView::getStandData(int trakt_id,double *m3sk,double *gy)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTraktData(trakt_id,m3sk,gy);
	}
}


void CStandsInPropertyFormView::isPropertyUsedInObject(int trakt_id,int prop_id,vecPropInObjects &vecPInObj)
{
	CString sObjList;
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->isPropertyUsed_list(trakt_id,prop_id,vecPInObj);
		}	// if (m_pDB != NULL)
	}
}

// CStandsInPropertyFormView diagnostics

#ifdef _DEBUG
void CStandsInPropertyFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CStandsInPropertyFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG



// CStandsInPropertyFormView message handlers

BOOL CStandsInPropertyFormView::setupReport2(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport2.GetSafeHwnd() == 0)
	{
		if (!m_wndReport2.Create(this,IDC_PROP_STANDS_REPORT,FALSE,FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport2.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport2.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sYes = xml.str(IDS_STRING240);
				m_sNo = xml.str(IDS_STRING241);

				m_sMsgCap = xml.str(IDS_STRING213);
				m_sMsgDoRemoveStandCap = xml.str(IDS_STRING3316);
				m_sMsgRemoveStandCap.Format(_T("%s\n%s\n\n"),xml.str(IDS_STRING3317),xml.str(IDS_STRING3318));

				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING2487), 50));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING2486), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING2483), 100));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING2484), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_wndReport2.AddColumn(new CXTPReportColumn(COLUMN_4, xml.str(IDS_STRING2485), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				m_wndReport2.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport2.SetMultipleSelection( FALSE );
				m_wndReport2.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport2.SetGridStyle( FALSE, xtpReportGridSolid );
				m_wndReport2.GetPaintManager()->SetFixedRowHeight(FALSE);
				m_wndReport2.AllowEdit(FALSE);
				m_wndReport2.FocusSubItems(TRUE);
				m_wndReport2.SetFocus();
			}
			xml.clean();
		}	// if (fileExists(sLangFN))
	}

	return TRUE;

}

void CStandsInPropertyFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_PROPSTANDS_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		m_wndReport2.SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;

}

void CStandsInPropertyFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	m_wndReport2.SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_PROPSTANDS_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

}

void CStandsInPropertyFormView::OnBnClickedButton31()
{
	showFormView(IDD_REPORTVIEW4,m_sLangFN,1);
}

void CStandsInPropertyFormView::OnBnClickedButton32()
{
	CXTPReportRow *pRow = m_wndReport2.GetFocusedRow();
	if (pRow)
	{
		CPropStandsReportRec *pRec = (CPropStandsReportRec*)pRow->GetRecord();
		if (pRec->getCanBeChanged())
		{
			if (::MessageBox(this->GetSafeHwnd(),m_sMsgDoRemoveStandCap,m_sMsgCap,MB_ICONINFORMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
			{
				if (m_pDB != NULL)
					m_pDB->updRemovePropertyFromStand(pRec->getTraktID());
			
				populateReport();
			}
		}
		else
			::MessageBox(this->GetSafeHwnd(),m_sMsgRemoveStandCap,m_sMsgCap,MB_ICONSTOP | MB_OK);
	}	// if (pItemNotify->pRow)
}
