// PostnumFormView.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "PropertyFormView.h"
#include "PropertyTabView.h"

#include "ResLangFileReader.h"

//#include "CMPTabView.h"
#include "CMPFormView.h"

/////////////////////////////////////////////////////////////////////////////////////
//	Reportclasses for Species; 060317 p�d



/////////////////////////////////////////////////////////////////////////////////////
// CCMPFormView

IMPLEMENT_DYNCREATE(CCMPFormView, CXTPReportView)


BEGIN_MESSAGE_MAP(CCMPFormView, CXTPReportView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportItemKeyDown)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportItemValueClick)
	ON_COMMAND(ID_TBBTN_OPEN, OnImportDataFile)
	ON_COMMAND(ID_TBBTN_CREATE, OnCreateCMP)
END_MESSAGE_MAP()

CCMPFormView::CCMPFormView()
	: CXTPReportView()
{
	m_bInitialized = FALSE;
	m_enumACTION = UPD_ITEM;
	m_pDB = NULL;
}

CCMPFormView::~CCMPFormView()
{
}

void CCMPFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTPReportView::DoDataExchange(pDX);
}

void CCMPFormView::OnDestroy()
{
//	getCMPsFromReport();
//	saveCMPsToDB(FALSE);

	if (m_pDB != NULL)
		delete m_pDB;

	SaveReportState();
	CXTPReportView::OnDestroy();	
}

BOOL CCMPFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTPReportView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

BOOL CCMPFormView::OnEraseBkgnd(CDC *pDC)
{
	CRect clip;
  GetReportCtrl().GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));

	return FALSE;
}

void CCMPFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	if (! m_bInitialized )
	{

		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport1();
	
		m_bInitialized = TRUE;

		LoadReportState();

	}
}

BOOL CCMPFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

// CCMPFormView diagnostics

#ifdef _DEBUG
void CCMPFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CCMPFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
}
#endif //_DEBUG


void CCMPFormView::setFilterText(LPCTSTR filter,int column)
{
	CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
	if (pCols && column < pCols->GetCount())
	{
		for (int i = 0;i < pCols->GetCount();i++)
		{
			pCols->GetAt(i)->SetFiltrable( i == column );
		}
	}
	GetReportCtrl().AllowEdit( FALSE );
	GetReportCtrl().SetFilterText(filter);
	GetReportCtrl().Populate();
}

// PRIVATE METHODS


// PROTECTED METHODS

BOOL CCMPFormView::setupReport1(void)
{
	CXTPReportColumn *pCol = NULL;

	if (GetReportCtrl().GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		GetReportCtrl().ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{

				m_sMessageCap = (xml.str(IDS_STRING207));
				m_delCMPMsg = (xml.str(IDS_STRING276));
				m_addNewCMPMsg = (xml.str(IDS_STRING277));
				m_sAddCMPToProperty = (xml.str(IDS_STRING280));

				m_sCMPValueError = (xml.str(IDS_STRING3200));

				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING2530)), 30));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING2540)), 30));
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;

				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING2550)), 30));
				pCol->GetEditOptions()->m_dwEditStyle |= ES_NUMBER; 
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;

				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING253)), 100));
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml.str(IDS_STRING254)), 100));
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;

				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, (xml.str(IDS_STRING255)), 100));
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;

				GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
				GetReportCtrl().SetMultipleSelection( FALSE );
				GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );

				// Read hms_user_types table in hms_administrator scheme(database); 060216 p�d
				getCMPsFromDB();
				populateReport();

				GetReportCtrl().AllowEdit(TRUE);
				GetReportCtrl().FocusSubItems(TRUE);
				GetReportCtrl().SetFocus();
			}
			xml.clean();
		}	// if (fileExists(sLangFN))

	}

	return TRUE;

}

void CCMPFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType, cx, cy);
}

void CCMPFormView::doSetNavigationBar()
{
	if (m_vecCMPsData.size() > 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}

}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CCMPFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			if( saveCMPsToDB() )		// Save CMP before adding a new one; 090511 p�d. (fixed 2013-02-13 refs #3559)
				addCMPToReport();
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			::SetCursor(::LoadCursor(NULL,IDC_WAIT));
			getCMPsFromDB();
			if (saveCMPsToDB())
			{
				getCMPsFromDB();
				populateReport();
			}
			::SetCursor(::LoadCursor(NULL,IDC_ARROW));

			//getCMPsFromReport();
			//saveCMPsToDB();
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			getCMPsFromDB();
			if (removeCMPFromDB())
			{
				getCMPsFromDB();
				populateReport();
			}
			break;
		}	// case ID_DELETE_ITEM :
	}
	return 0L;
}

// Handle transaction on database species table; 060317 p�d

BOOL CCMPFormView::populateReport(void)
{
	GetReportCtrl().ResetContent();
	if (m_vecCMPsData.size() > 0)
	{
		for (UINT i = 0;i < m_vecCMPsData.size();i++)
		{
			CTransaction_county_municipal_parish rec = m_vecCMPsData[i];
			GetReportCtrl().AddRecord(new CCMPReportRec(i,rec));
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)

	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();

	doSetNavigationBar();

	return TRUE;
}

void CCMPFormView::setReportFocus(void)
{
	CCMPReportRec *pRec = NULL;
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (GetReportCtrl().GetSafeHwnd() != NULL)
	{
		pRows = GetReportCtrl().GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow != NULL)
			{
				pRec = (CCMPReportRec*)pRow->GetRecord();
				if (pRec != NULL)
				{
					if (pRec->getColumnInt(0) == 0)
					{
						pRow->SetSelected(TRUE);
						GetReportCtrl().SetFocusedRow(pRow);
					}
				}
			}
		}
	}
}

BOOL CCMPFormView::addCMPToReport()
{
	m_enumACTION = NEW_ITEM;

	CCMPReportRec *pRec = new CCMPReportRec();
	if (pRec)
	{
		GetReportCtrl().AddRecord(pRec);
		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();
		// Find the empty row and point to it; 081203 p�d
		CXTPReportRows *pRows = GetReportCtrl().GetRows();
		if (pRows != NULL)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				CCMPReportRec *pRec1 = (CCMPReportRec *)pRows->GetAt(i)->GetRecord();
				if (pRec1 != NULL)
				{
					if (pRec1->getColumnText(COLUMN_3).IsEmpty())
					{
						GetReportCtrl().SetFocusedRow(pRows->GetAt(i),TRUE);
						break;
					}	// if (pRec1->getColumnText(COLUMN_0).IsEmpty())
				}	// if (pRec1 != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)
		}	// if (pRows != NULL)
	}
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	return TRUE;
}

BOOL CCMPFormView::saveCMPsToDB(void)
{
	CTransaction_county_municipal_parish rec;
	CCMPReportRec *pRec = NULL, *pRecTmp = NULL;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	if (m_bConnected)
	{
		if (m_pDB != NULL && pRows != NULL)
		{
			GetReportCtrl().Populate();
			for (long i = 0;i < pRows->GetCount();i++)
			{
				pRec = (CCMPReportRec*)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
				{
					if (pRec->getIsDirty(0) || 
						  pRec->getIsDirty(1) ||
						  pRec->getIsDirty(2) ||
						  pRec->getIsDirty(3) ||
						  pRec->getIsDirty(4) ||
						  pRec->getIsDirty(5))
					{
						rec = CTransaction_county_municipal_parish(pRec->getID(),
							pRec->getColumnInt(0),
							pRec->getColumnInt(1),
							pRec->getColumnInt(2),
							pRec->getColumnText(3),
							pRec->getColumnText(4),
							pRec->getColumnText(5),
							_T(""));


						// Before we add data to DB, we'll do a check to
						// make sure the user haven't added an already entered
						// County,Municipal and/or Parish code; 090203 p�d
						// modified 2013-02-13 Anders (refs #3559)
						for (long j = 0; j < pRows->GetCount(); j++)
						{
							if ( j == i ) continue;

							pRecTmp = (CCMPReportRec*)pRows->GetAt(j)->GetRecord();
							if (pRecTmp != NULL)
							{
								if (rec.getCountyID() == pRecTmp->getColumnInt(0) &&
									rec.getMunicipalID() == pRecTmp->getColumnInt(1) &&
									rec.getParishID() == pRecTmp->getColumnInt(2))
								{
									::MessageBox(this->GetSafeHwnd(), m_addNewCMPMsg, m_sMessageCap, MB_ICONASTERISK | MB_OK);
									return FALSE;
								}
							}
						}

						if (!m_pDB->addCMP(rec))
							m_pDB->updCMP(rec);
						else
							m_vecCMPsData.push_back(rec);
					}	// if (pRec->getIsDirty(0) || pRec->getIsDirty(1))
				}	// if (pRec != NULL)
			}	// for (UINT i = 0;i < m_vecPostnumbersDataChangedOrAdded.size();i++)
		}	// if (m_pDB != NULL && pRows != NULL)
	}	// if (m_bConnected)

	m_enumACTION = UPD_ITEM;

	return TRUE;
}

BOOL CCMPFormView::removeCMPFromDB()
{
	BOOL bFound = FALSE;
	m_enumACTION = UPD_ITEM;
	CTransaction_county_municipal_parish recCMP;

	int nCountyID;
	int nMunicipalID;
	int nParishID;

	// Ask user if he realy want to delete zipcode; 071204 p�d
	if (::MessageBox(this->GetSafeHwnd(),m_delCMPMsg,m_sMessageCap,MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
		if (pRow != NULL)
		{
			CCMPReportRec *pRec = (CCMPReportRec *)pRow->GetRecord();
			if (pRec != NULL)
			{
				nCountyID = pRec->getColumnInt(0);
				nMunicipalID = pRec->getColumnInt(1);
				nParishID = pRec->getColumnInt(2);
				bFound = FALSE;
				// Check if the row to be deleted already's in the table.
				// If not, no need to run "removePostnumber"; 081204 p�d
				for (UINT i = 0;i < m_vecCMPsData.size();i++)
				{
					recCMP = m_vecCMPsData[i];
					if (nCountyID == recCMP.getCountyID() && nMunicipalID == recCMP.getMunicipalID() && nParishID == recCMP.getParishID())
					{
						bFound = TRUE;
						break;
					}	// if (sPostNumber.CompareNoCase(recPostnum.getNumber()) == 0)
				}	// for (UINT i = 0;i < m_vecPostnumbersDataFromDB.size();i++)		

				// Check if the row to removed is empty, hence, not in DB; 081204 p�d
				if (!bFound)
				{
					CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
					if (pRecs != NULL)
					{
						pRecs->RemoveRecord(pRec);
						GetReportCtrl().Populate();
						GetReportCtrl().UpdateWindow();
					}	// if (pRows != NULL)
				}
				else
				{
					CTransaction_county_municipal_parish recInReport = CTransaction_county_municipal_parish(pRec->getID(),
																																																	pRec->getColumnInt(0),
																																																	pRec->getColumnInt(1),
																																																	pRec->getColumnInt(2),
																																																	pRec->getColumnText(3),
																																																	pRec->getColumnText(4),
																																																	pRec->getColumnText(5),
																																																	_T(""));
					if (m_pDB != NULL)
					{
						m_pDB->delCMP(recInReport);
					}	// if (pDB != NULL)			
				}	// if (!bFound)
			}	// if (pRec != NULL)
		}	// if (pRec != NULL)
		return TRUE;
	}
	return FALSE;
}

BOOL CCMPFormView::getCMPsFromReport()
{
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	if (pRecs != NULL)
	{
		m_vecCMPsDataChangedOrAdded.clear();
		GetReportCtrl().Populate();
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			CCMPReportRec *pRec = (CCMPReportRec*)pRecs->GetAt(i);
			// Only add if there's data added; 081204 p�d
			if (!pRec->getColumnText(3).IsEmpty() &&
					!pRec->getColumnText(4).IsEmpty() &&
					!pRec->getColumnText(5).IsEmpty())
			{
				// Check if item's dirty. If so add to list of
				// edited or added items to be inserted/updated in
				// database; 071204 p�d
				if (pRec->getIsDirty(0) || 
						pRec->getIsDirty(1) ||
						pRec->getIsDirty(2) ||
						pRec->getIsDirty(3) ||
						pRec->getIsDirty(4) ||
						pRec->getIsDirty(5) )				
				{
					m_vecCMPsDataChangedOrAdded.push_back(CTransaction_county_municipal_parish(pRec->getID(),
																																										 pRec->getColumnInt(0),
																																										 pRec->getColumnInt(1),
																																										 pRec->getColumnInt(2),
																																										 pRec->getColumnText(3),
																																										 pRec->getColumnText(4),
																																										 pRec->getColumnText(5),
																																										_T("")));
				}
			}
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)
	return TRUE;
}

BOOL CCMPFormView::getCMPsFromDB(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getCMPs(m_vecCMPsData);
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}

	return bReturn;
}

void CCMPFormView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	if (pItemNotify->pRow)
	{
		CCMPReportRec *pRec = (CCMPReportRec *)pItemNotify->pItem->GetRecord();

		CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
		if (pTabView)
		{
			// Get ContactsFormView
			CPropertyFormView *pView = (CPropertyFormView *)pTabView->getPropertyFormView();
			if (pView)
			{
				UINT nIndex = pRec->getIndex();
				if (nIndex >= 0 && nIndex < m_vecCMPsData.size())
				{
					if (::MessageBox(this->GetSafeHwnd(),m_sAddCMPToProperty,m_sMessageCap,MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					{
						pView->setCountyMunicpalAndParish(m_vecCMPsData[nIndex]);
						// Quit this module form; 071204 p�d
						CMDICountyMunicipalParishFrame *pFrame = (CMDICountyMunicipalParishFrame *)getFormViewParentByID(IDD_REPORTVIEW2);
						if (pFrame != NULL)
						{
							::SendMessage(pFrame->GetSafeHwnd(),WM_SYSCOMMAND,SC_CLOSE,0);
						}	// if (pFrame != NULL)
					}
				}	// if (nIndex >= 0 && nIndex < m_vecCMPsData.size())
			}	// if (pView)
		}	// if (pTabView)
	}	// if (pItemNotify->pRow)
}

void CCMPFormView::OnReportItemKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (lpNMKey->nVKey == VK_RETURN)
	{		
		// Need to have Contacts open; 071204 p�d
		CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
		if (pTabView)
		{
			CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
			if (pRow != NULL)
			{
				CCMPReportRec *pRec = (CCMPReportRec *)pRow->GetRecord();
				if (pRec != NULL)
				{
					// Get Property FormView; 071204 p�d
					CPropertyFormView *pView = (CPropertyFormView *)pTabView->getPropertyFormView();
					if (pView)
					{
						UINT nIndex = pRec->getIndex();
						if (nIndex >= 0 && nIndex < m_vecCMPsData.size())
						{
							if (::MessageBox(this->GetSafeHwnd(),m_sAddCMPToProperty,m_sMessageCap,MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
							{
								pView->setCountyMunicpalAndParish(m_vecCMPsData[nIndex]);
								// Quit this module form; 071204 p�d
								CMDICountyMunicipalParishFrame *pFrame = (CMDICountyMunicipalParishFrame *)getFormViewParentByID(IDD_REPORTVIEW2);
								if (pFrame != NULL)
								{
									::SendMessage(pFrame->GetSafeHwnd(),WM_SYSCOMMAND,SC_CLOSE,0);
								}	// if (pFrame != NULL)
							}	// if (::MessageBox(this->GetSafeHwnd(),m_sAddZipCodeToProperty,m_sMessageCap,MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
						}	// if (nIndex >= 0 && nIndex < m_vecPostnumbersDataFromDB.size())
					}	// if (pView)
				}					
			}	// if (pRow != NULL)
		}	// if (pTabView)
	}	// 	if (lpNMKey->nVKey == VK_RETURN)
}

void CCMPFormView::OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CString sMsg;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		GetReportCtrl().Populate();
		CXTPReportColumn *pCol = pItemNotify->pColumn;
		CXTPReportRow *pRow = pItemNotify->pRow;
		if (pRow != NULL && pCol != NULL)
		{
			CCMPReportRec *pRec = (CCMPReportRec*)pRow->GetRecord();
			if (pRec != NULL)
			{
				if (pRec->getColumnInt(COLUMN_0) > 32000)
				{
					sMsg.Format(_T("%s : %d"),m_sCMPValueError,MAX_CMP_ID);
					::MessageBox(this->GetSafeHwnd(),sMsg,m_sMessageCap,MB_ICONASTERISK | MB_OK);
					pRec->setColumnInt(COLUMN_0,0);
					GetReportCtrl().Populate();
					GetReportCtrl().UpdateWindow();
				}
				if (pRec->getColumnInt(COLUMN_1) > 32000)
				{
					sMsg.Format(_T("%s : %d"),m_sCMPValueError,MAX_CMP_ID);
					::MessageBox(this->GetSafeHwnd(),sMsg,m_sMessageCap,MB_ICONASTERISK | MB_OK);
					pRec->setColumnInt(COLUMN_1,0);
					GetReportCtrl().Populate();
					GetReportCtrl().UpdateWindow();
				}
				if (pRec->getColumnInt(COLUMN_2) > 32000)
				{
					sMsg.Format(_T("%s : %d"),m_sCMPValueError,MAX_CMP_ID);
					::MessageBox(this->GetSafeHwnd(),sMsg,m_sMessageCap,MB_ICONASTERISK | MB_OK);
					pRec->setColumnInt(COLUMN_2,0);
					GetReportCtrl().Populate();
					GetReportCtrl().UpdateWindow();
				}
			}	// if (pRec != NULL)
		}	// if (pRow != NULL)
	}	// if (pItemNotify != NULL)
}

void CCMPFormView::OnImportDataFile(void)
{
	vecTransatcionCMP vec;
	CString sFileName;

	CFileDialog f(TRUE,_T(".skv"),_T(""),4|2|OFN_FILEMUSTEXIST,_T("Semikolon *.skv|*.skv|"));
	if (f.DoModal() != IDOK) return;
	sFileName = f.GetPathName();
	readCountyMunicipalAndParishTable(sFileName,vec);
	if (vec.size() > 0)
	{
		GetReportCtrl().ResetContent();

		for (UINT i = 0;i < vec.size();i++)
		{
			CTransaction_county_municipal_parish rec = vec[i];
			GetReportCtrl().AddRecord(new	CCMPReportRec(i,rec));
		}
	}	// if (vec.size() > 0)
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CCMPFormView::OnCreateCMP(void)
{
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CCMPReportRec *pRec = NULL;
	if (m_pDB != NULL && pRows != NULL)
	{
		for (long i = 0;i < pRows->GetCount();i++)
		{
			if ((pRec = (CCMPReportRec *)pRows->GetAt(i)->GetRecord()) != NULL)
			{
				CTransaction_county_municipal_parish rec = CTransaction_county_municipal_parish(i+1,pRec->getColumnInt(COLUMN_0),pRec->getColumnInt(COLUMN_1),pRec->getColumnInt(COLUMN_2),
																																														pRec->getColumnText(COLUMN_3),pRec->getColumnText(COLUMN_4),pRec->getColumnText(COLUMN_5),
																																														getUserName().MakeUpper());
				if (!m_pDB->addCMP(rec))
					m_pDB->updCMP(rec);
			}	
		}	// for (long i = 0;i < pRows->GetCount();i++)
	}	// if (m_pDB != NULL)
}

void CCMPFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_CMP_FRAME_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CCMPFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_CMP_FRAME_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}
