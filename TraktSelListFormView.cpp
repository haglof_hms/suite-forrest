// ContactsSelListFormView.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "MDIBaseFrame.h"
#include "PropertyTabView.h"
#include "PropertyFormView.h"
#include "PropertySelListFormView.h"

#include "ResLangFileReader.h"

#include "XTPPreviewView.h"

/////////////////////////////////////////////////////////////////////////////
// CPropertyReportFilterEditControl

IMPLEMENT_DYNCREATE(CPropertyReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CPropertyReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CPropertyReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CString S;
	CPropertySelectListFrame* pWnd = (CPropertySelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW3);
	if (pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != "");
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

// CPropertySelListFormView

IMPLEMENT_DYNCREATE(CPropertySelListFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPropertySelListFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_COMMAND(ID_TBBTN_COLUMNS, OnShowFieldChooser)
	ON_COMMAND(ID_TBBTN_FILTER, OnShowFieldFilter)
	ON_COMMAND(ID_TBBTN_FILTER_OFF, OnShowFieldFilterOff)
	ON_COMMAND(ID_TBBTN_PRINT, OnPrintPreview)
	ON_COMMAND(ID_TBBTN_REFRESH, OnRefresh)
END_MESSAGE_MAP()

CPropertySelListFormView::CPropertySelListFormView()
	: CXTPReportView()
{
	m_pDB = NULL;
	m_nSelectedColumn = -1;
}

CPropertySelListFormView::~CPropertySelListFormView()
{
}

void CPropertySelListFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	getProperties();

	setupReport();

	CPropertySelectListFrame* pWnd = (CPropertySelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW3);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST1, &pWnd->m_wndFieldChooser);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if (m_wndLbl.GetSafeHwnd() == NULL)
	{
		m_wndLbl.SubclassDlgItem(IDC_LABEL, &pWnd->m_wndFilterEdit);
		m_wndLbl.SetBkColor(INFOBK);
	}

	if (m_wndLbl1.GetSafeHwnd() == NULL)
	{
		m_wndLbl1.SubclassDlgItem(IDC_LABEL1, &pWnd->m_wndFilterEdit);
		m_wndLbl1.SetBkColor(INFOBK);
		m_wndLbl1.SetLblFont(14,FW_BOLD);
	}


	LoadReportState();
}

BOOL CPropertySelListFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CPropertySelListFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;
	SaveReportState();
	CXTPReportView::OnDestroy();	
}

BOOL CPropertySelListFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPropertySelListFormView diagnostics

#ifdef _DEBUG
void CPropertySelListFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CPropertySelListFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CPropertySelListFormView message handlers

// CPropertySelListFormView message handlers
void CPropertySelListFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CPropertySelListFormView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CPropertySelListFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);

	// Setup language filename; 051214 p�d
	CString sLangFN;
	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
				m_sGroupByThisField	= _T(xml->str(IDS_STRING244));
				m_sGroupByBox				= _T(xml->str(IDS_STRING245));
				m_sFieldChooser			= _T(xml->str(IDS_STRING246));

				m_sFilterOn					= _T(xml->str(IDS_STRING2430));

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					GetReportCtrl().ShowWindow( SW_NORMAL );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(0, _T(xml->str(IDS_STRING253)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(1, _T(xml->str(IDS_STRING254)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(2, _T(xml->str(IDS_STRING255)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(3, _T(xml->str(IDS_STRING2560)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(4, _T(xml->str(IDS_STRING256)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(5, _T(xml->str(IDS_STRING257)), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(6, _T(xml->str(IDS_STRING258)), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(7, _T(xml->str(IDS_STRING259)), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(8, _T(xml->str(IDS_STRING260)), 50));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					populateReport();		

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CPropertySelListFormView::populateReport(void)
{
	CString sInvType,sOrigin,sEnteredAs;
	GetReportCtrl().GetRecords()->RemoveAll();
	for (UINT i = 0;i < m_vecPropertyData.size();i++)
	{
		CTransaction_property data = m_vecPropertyData[i];
		GetReportCtrl().AddRecord(new CPropertyReportDataRec(i,data));
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CPropertySelListFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pColumn)
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
	}

	if (pItemNotify->pRow)
	{
		CPropertyReportDataRec *pRec = (CPropertyReportDataRec*)pItemNotify->pItem->GetRecord();
		CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
		if (pTabView)
		{
			// Get ContactsFormView
			CPropertyFormView *pView = (CPropertyFormView *)pTabView->getPropertyFormView();
			if (pView)
			{
				pView->doPopulate(pRec->getIndex());
			}	// if (pView)
		}	// if (pTabView)
	}	// if (pItemNotify->pRow)
}

void CPropertySelListFormView::OnShowFieldChooser()
{
	CPropertySelectListFrame* pWnd = (CPropertySelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW3);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooser.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooser, bShow, FALSE);
	}	// if (pWnd != NULL)
}

void CPropertySelListFormView::OnShowFieldFilter()
{
	CPropertySelectListFrame* pWnd = (CPropertySelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW3);
	if (pWnd != NULL)
	{
		if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
		{
			CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
			CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
			int nColumn = pColumn->GetIndex();
			if (pCols && nColumn < pCols->GetCount())
			{
				for (int i = 0;i < pCols->GetCount();i++)
				{
					pCols->GetAt(i)->SetFiltrable( i == nColumn );
				}	// for (int i = 0;i < pCols->GetCount();i++)
			}	// if (pCols && nColumn < pCols->GetCount())
			m_wndLbl.SetWindowText(m_sFilterOn + " :");
			m_wndLbl1.SetWindowText(pColumn->GetCaption());
		}	// if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
		
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != "");
	}

}

void CPropertySelListFormView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText("");
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText("");
	CPropertySelectListFrame* pWnd = (CPropertySelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW3);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}	// if (pWnd != NULL)
}

void CPropertySelListFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupByBox);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	if (GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS, MF_BYCOMMAND|MF_CHECKED);
	}

	if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn* pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_GROUP_BYTHIS:

			if (pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
			{
				pColumns->GetGroupsOrder()->Add(pColumn);
			}
			GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
			GetReportCtrl().Populate();
			break;
		case ID_SHOW_GROUPBOX:
			GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;
		case ID_SHOW_FIELDCHOOSER:
			OnShowFieldChooser();
			break;
	}

}

void CPropertySelListFormView::OnPrintPreview()
{
	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this,
		RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		AfxMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}

}

void CPropertySelListFormView::OnRefresh()
{
	getProperties();
	populateReport();
}

// CPropertySelListFormView message handlers

void CPropertySelListFormView::getProperties(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(m_vecPropertyData);
	}	// if (m_pDB != NULL)
}

void CPropertySelListFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary(_T(REG_WP_PROPERTY_SELLEIST_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString(_T(REG_WP_PROPERTY_SELLEIST_REPORT_KEY), _T("FilterText"), "");
	// Get selected column index into registry; 070125 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(_T(REG_WP_PROPERTY_SELLEIST_REPORT_KEY), _T("SelColIndex"),0);

	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(sFilterText != "");
	}
}

void CPropertySelListFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(_T(REG_WP_PROPERTY_SELLEIST_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString(_T(REG_WP_PROPERTY_SELLEIST_REPORT_KEY), _T("FilterText"), sFilterText);

	// Set selected column index into registry; 070125 p�d
	AfxGetApp()->WriteProfileInt(_T(REG_WP_PROPERTY_SELLEIST_REPORT_KEY), _T("SelColIndex"), m_nSelectedColumn);

}

