// ContactsSelListFormView.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "MDIBaseFrame.h"
#include "ContactsTabView.h"
#include "ContactsFormView.h"
#include "ContactsSelListFormView.h"

#include "SearchAndReplaceFormView.h"

#include "ResLangFileReader.h"

#include "XTPPreviewView.h"

#include "PropertyTabView.h"

/////////////////////////////////////////////////////////////////////////////
// CContactsReportFilterEditControl

IMPLEMENT_DYNCREATE(CContactsReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CContactsReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CContactsReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CString S;
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != "");
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

// CContactsSelListFormView

IMPLEMENT_DYNCREATE(CContactsSelListFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CContactsSelListFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)
	ON_COMMAND(ID_TBBTN_COLUMNS, OnShowFieldChooser)
	ON_COMMAND(ID_TBBTN_FILTER, OnShowFieldFilter)
	ON_COMMAND(ID_TBBTN_FILTER_OFF, OnShowFieldFilterOff)
	ON_COMMAND(ID_TBBTN_PRINT, OnPrintPreview)
	ON_COMMAND(ID_TBBTN_REFRESH, OnRefresh)
	ON_COMMAND(ID_TBBTN_ADD, OnAddContactToProperty)

	ON_UPDATE_COMMAND_UI(ID_TBBTN_ADD, OnUpdateDBToolbarBtnAdd)

END_MESSAGE_MAP()

CContactsSelListFormView::CContactsSelListFormView()
	: CXTPReportView()
{
	m_vecContactsData = new vecTransactionContacts;
	m_pDB = NULL;
	m_nSelectedColumn = -1;	// Nothing selected yet; 070125 p�d
	// This data member is used to set value, based on
	// value set in showFormView(...), to set if
	// OnReportItemClick will result in sending a message
	// to, in this case the Property Owner window, or not; 070126 p�d
	m_nDoAddPropertyOwners	= -1;
	objId=-1;
}

CContactsSelListFormView::~CContactsSelListFormView()
{
}

void CContactsSelListFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	CContactsTabView *pView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
	if (pView)
	{
		m_nDBIndex = pView->getContactsFormView()->getDBIndex();
	}

	setupReport();

	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNLIST, &pWnd->m_wndFieldChooser);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if (m_wndLbl.GetSafeHwnd() == NULL)
	{
		m_wndLbl.SubclassDlgItem(IDC_LABEL1, &pWnd->m_wndFilterEdit);
		m_wndLbl.SetBkColor(INFOBK);
	}

	if (m_wndLbl1.GetSafeHwnd() == NULL)
	{
		m_wndLbl1.SubclassDlgItem(IDC_LABEL2, &pWnd->m_wndFilterEdit);
		m_wndLbl1.SetBkColor(INFOBK);
		m_wndLbl1.SetLblFont(14,FW_BOLD);
	}
/*
	m_Info = SEARCH_REPLACE_INFO();
	m_sSQLSearchString.Format(_T("select * from %s where type_of=%d"),TBL_CONTACTS,1 /* 1 = Company * /);	

	CSearchAndReplaceFormView* pWnd1 = (CSearchAndReplaceFormView *)getFormViewParentByID(IDD_FORMVIEW10);
	if (pWnd1)
	{
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pTabView != NULL)
		{
			CContactsFormView* pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pView != NULL)
			{
				m_Info = pView->getSEARCH_REPLACE_INFO();
				pView = NULL;
			}		
			pTabView = NULL;
		}
		if (m_Info.m_origin == ORIGIN_PROPERTY)
			m_sSQLSearchString.Format(_T("select * from %s where %s LIKE '%s' order by id"),TBL_PROPERTY,getColName(m_Info.m_focusedEdit),m_sSearchText);
		else if (m_Info.m_origin == ORIGIN_CONTACT)
			m_sSQLSearchString.Format(_T("select * from %s where %s LIKE '%s' order by id"),TBL_CONTACTS,getColName(m_Info.m_focusedEdit),m_sSearchText);
	}
*/
	LoadReportState();
}

BOOL CContactsSelListFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CContactsSelListFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;
	SaveReportState();
	CXTPReportView::OnDestroy();	
}

BOOL CContactsSelListFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CContactsSelListFormView diagnostics

#ifdef _DEBUG
void CContactsSelListFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CContactsSelListFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CContactsSelListFormView message handlers

// CContactsSelListFormView message handlers
void CContactsSelListFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CContactsSelListFormView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Catch message sent from (WM_USER_MSG_SUITE)
LRESULT CContactsSelListFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_SHOWVIEW_MSG :
		{
			m_nDoAddPropertyOwners	= (int)lParam;
			CXTPReportColumn *pCol0 = GetReportCtrl().GetColumns()->Find(0);
			if (pCol0) pCol0->SetVisible(m_nDoAddPropertyOwners == 1);
			

			if(m_nDoAddPropertyOwners==1) {
				CPropertyTabView *pView = (CPropertyTabView*)getFormViewByID(IDD_FORMVIEW5);
				if (pView != NULL)
				{
					CPropertyFormView* pView0 = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(0)->GetHandle()));
					if (pView0 != NULL)
					{
						// Get Owners for property, from database; 060317 p�d
						if (m_pDB != NULL)
						{
							objId=m_pDB->getObjectIdFromPropertyId(pView0->getActiveProperty().getID());
						}	// if (m_pDB != NULL)
					}
				}
			}
			getContacts();
			populateReport();

			break;
		}
		case (ID_WPARAM_VALUE_FROM + 0x03) :
		{
			OutputDebugString(_T("ContactsSellListFormView"));
			break;
		}
	}	// switch (wParam)

	return 0L;
}

// Create and add Assortment settings reportwindow
BOOL CContactsSelListFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
				m_sGroupByThisField	= (xml.str(IDS_STRING244));
				m_sGroupByBox				= (xml.str(IDS_STRING245));
				m_sFieldChooser			= (xml.str(IDS_STRING246));
				m_sFilterOn					= (xml.str(IDS_STRING2430));

				m_sarrTypeOfContact.Add(xml.str(IDS_STRING2353));
				m_sarrTypeOfContact.Add(xml.str(IDS_STRING2354));
				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP3));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					GetReportCtrl().SetImageList(&m_ilIcons);

					GetReportCtrl().ShowWindow( SW_NORMAL );
					// Add these 3 lines to add scrollbars for View; 070319 p�d
					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(0, _T(""), 18,FALSE,12));
					pCol->AllowRemove(FALSE);
					pCol->SetEditable( TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(1, (xml.str(IDS_STRING221)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(2, (xml.str(IDS_STRING222)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(3, (xml.str(IDS_STRING223)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(4, (xml.str(IDS_STRING224)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(5, (xml.str(IDS_STRING323)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_LEFT );
					pCol->SetAlignment( DT_LEFT );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(6, (xml.str(IDS_STRING225)), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(7, (xml.str(IDS_STRING226)), 120));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(8, (xml.str(IDS_STRING227)), 80));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(9, (xml.str(IDS_STRING228)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					sColText.Format(_T("%s %s"),xml.str(IDS_STRING229),xml.str(IDS_STRING2290));
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(10, (sColText), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					sColText.Format(_T("%s %s"),xml.str(IDS_STRING229),xml.str(IDS_STRING2291));
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(11, (sColText), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(12, (xml.str(IDS_STRING247)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(13, (xml.str(IDS_STRING230)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(14, (xml.str(IDS_STRING231)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(15, (xml.str(IDS_STRING232)), 150));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(16, (xml.str(IDS_STRING2350)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(17, (xml.str(IDS_STRING2370)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(18, (xml.str(IDS_STRING2371)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(19, (xml.str(IDS_STRING2372)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(20, (xml.str(IDS_STRING2373)), 100));
					pCol->SetEditable( FALSE );
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );



					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().GetRecords()->SetCaseSensitive(FALSE);

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml.Load(m_sLangFN))
			xml.clean();
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CContactsSelListFormView::populateReport(void)
{
	CXTPReportRecord *pRec = NULL;
	CTransaction_contacts dataSel;
	CString sType;
	if (m_nDBIndex >= 0 && m_nDBIndex < m_vecContactsData->size())
	{
		dataSel = (*m_vecContactsData)[m_nDBIndex];
	}
//	getContacts();
	GetReportCtrl().GetRecords()->RemoveAll();
	for (UINT i = 0;i < m_vecContactsData->size();i++)
	{
		CTransaction_contacts data = (*m_vecContactsData)[i];
		if (data.getTypeOf() > -1 && data.getTypeOf() < m_sarrTypeOfContact.GetCount())
			sType = m_sarrTypeOfContact.GetAt(data.getTypeOf());
		else
			sType = _T("");
		if (data.getID() == dataSel.getID())
		{
			pRec = GetReportCtrl().AddRecord(new CContactsReportDataRec(i,data,sType)); //m_sarrTypeOfContact));
		}
		else
		{
			GetReportCtrl().AddRecord(new CContactsReportDataRec(i,data,sType)); //m_sarrTypeOfContact));
		}
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	if (pRec)
	{
		CXTPReportRow *pRow = GetReportCtrl().GetRows()->Find(pRec);
		if (pRow)
		{
			GetReportCtrl().SetFocusedRow(pRow);
		}
	}

}

void CContactsSelListFormView::setFilterWindow(void)
{
	if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	{
		CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
		CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
		int nColumn = pColumn->GetIndex();
		if (pCols && nColumn < pCols->GetCount())
		{
			for (int i = 0;i < pCols->GetCount();i++)
			{
				pCols->GetAt(i)->SetFiltrable( i == nColumn );
			}	// for (int i = 0;i < pCols->GetCount();i++)
		}	// if (pCols && nColumn < pCols->GetCount())
		m_wndLbl.SetWindowText(m_sFilterOn + _T(" :"));
		m_wndLbl1.SetWindowText(pColumn->GetCaption());
	}	// if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	else
	{
		m_wndLbl.SetWindowText(m_sFilterOn + _T(" :"));
		m_wndLbl1.SetWindowText(_T(""));
	}
}

void CContactsSelListFormView::OnShowFieldChooser()
{
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooser.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooser, bShow, FALSE);
	}
}

void CContactsSelListFormView::OnShowFieldFilter()
{
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		setFilterWindow();
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}
}

void CContactsSelListFormView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}
}

void CContactsSelListFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pColumn)
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
	}
	if (m_nDoAddPropertyOwners == 0)
	{
		if (pItemNotify->pRow)
		{
			CContactsReportDataRec *pRec = (CContactsReportDataRec*)pItemNotify->pItem->GetRecord();
			CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
			if(!pTabView) {
					CString sLangFN(getLanguageFN(getLanguageDir(),_T("Forrest"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
					showFormView(113,sLangFN);
					pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
			}
			if (pTabView)
			{			
				// Get ContactsFormView
				CContactsFormView *pView = (CContactsFormView *)pTabView->getContactsFormView();
				if (pView)
				{
					int id = pRec->getIndex();
					pView->doPopulate(id,m_vecContactsData);
				}	// if (pView)

			}	// if (pTabView)
		}	// if (pItemNotify->pRow)
	}	// if (m_nDoTransacions == 1)

	// Check if Fileter window is visible. If so change filter column to selected column; 090224 p�d
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		if (pWnd->m_wndFilterEdit.IsVisible())
		{
			setFilterWindow();
			pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
		}	// if (pWnd->m_wndFilterEdit.IsVisible())
	}	// if (pWnd != NULL)
	pWnd = NULL;
}

void CContactsSelListFormView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	// Close form
	PostMessage(WM_COMMAND, ID_FILE_CLOSE);
}

void CContactsSelListFormView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupByBox);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	if (GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS, MF_BYCOMMAND|MF_CHECKED);
	}

	if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn* pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_GROUP_BYTHIS:

			if (pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
			{
				pColumns->GetGroupsOrder()->Add(pColumn);
			}
			GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
			GetReportCtrl().Populate();
			break;
		case ID_SHOW_GROUPBOX:
			GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;
		case ID_SHOW_FIELDCHOOSER:
			OnShowFieldChooser();
			break;
	}

}

void CContactsSelListFormView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (!GetReportCtrl().GetFocusedRow())
		return;
}


void CContactsSelListFormView::OnPrintPreview()
{
	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this,
		RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		AfxMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}

}

void CContactsSelListFormView::OnRefresh()
{
	populateReport();
}

void CContactsSelListFormView::OnAddContactToProperty()
{
	if (m_nDoAddPropertyOwners == 1)
	{
		CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
		if (pTabView)
		{
			CMDIPropOwnerFormView *pView = (CMDIPropOwnerFormView *)pTabView->getPropOwnerFormView();
			if (pView)
			{
				CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
				if (pRecs != NULL)
				{
					for (int i = 0;i < pRecs->GetCount();i++)
					{
						CContactsReportDataRec *pRec = (CContactsReportDataRec *)pRecs->GetAt(i);
						if (pRec != NULL)
						{
							if (pRec->getColumnCheck(0))
							{
								UINT nIndex = pRec->getIndex();
								if (nIndex >= 0 && nIndex < m_vecContactsData->size())
								{
									pView->setPropOwner((*m_vecContactsData)[nIndex]);
								}	// if (nIndex >= 0 && nIndex < m_vecContactsData.size())
								pRec->setColumnCheck(0,FALSE);
							}	// if (pRec->getColumnCheck(0))
						}	// if (pRec != NULL)
					}	// for (int i = 0;i < pRecs->GetCount();i++)
					
					// Update report; 070129 p�d
					GetReportCtrl().Populate();
				}	// if (pRecs != NULL)
			}	// if (pView)
		}	// if (pTabView)
	}	// if (m_nDoAddPropertyOwners == 1)
	else if (m_nDoAddPropertyOwners == 2)
	{
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pTabView)
		{
			// Get ContactsFormView
			CContactsFormView *pView = (CContactsFormView *)pTabView->getContactsFormView();
			if (pView)
			{
				CContactsReportDataRec *pRec = (CContactsReportDataRec*)GetReportCtrl().GetFocusedRow()->GetRecord();
				if (pRec != NULL)
				{
					pView->setCompany(pRec->getColumnText(2));
				}
			}	// if (pView)
		}	// if (pTabView)
	}
}

void CContactsSelListFormView::OnUpdateDBToolbarBtnAdd(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_nDoAddPropertyOwners > 0);
}

// CContactsSelListFormView message handlers

// Skapade 2 nya funktioner av getContacts f�r att kunna anv�nda olika sql fr�gor. 2016-06-10 PH
void CContactsSelListFormView::getContacts(void)
{
	CString sSQL;
	sSQL.Format(_T("select * from %s where type_of=%d"),TBL_CONTACTS,1 /* 1 = Company */);
	getContacts(sSQL);
}

void CContactsSelListFormView::getContacts(CString sSQL)
{
	if (m_pDB != NULL)
	{
		if (m_nDoAddPropertyOwners == 2)	// Add Owner, set as Company to a Owner; 090922 p�d
		{
			m_pDB->getContacts(sSQL,*m_vecContactsData);
		}
		else {// Add Owner(s) to Property; 090922 p�d
			m_pDB->getContacts(*m_vecContactsData);
			if(objId!=-1) {
				m_pDB->getContactsForObjectId(objId,*m_vecContactsData,false);
			}
		}
	}	// if (m_pDB != NULL)

	if (m_vecContactsData->size() == 0) m_nDoAddPropertyOwners = 0;
}

//Added 2016-06-10 PH
void CContactsSelListFormView::getContactsForObjectId(int id) {
	if(m_pDB != NULL) {
		m_pDB->getContactsForObjectId(id,*m_vecContactsData);
		m_nDoAddPropertyOwners=0;
	}
}

void CContactsSelListFormView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("FilterText"), _T(""));
	// Get selected column index into registry; 070125 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"),0);

	setFilterWindow();
	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);
	CContactsSelectListFrame* pWnd = (CContactsSelectListFrame *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(sFilterText != "");
	}
}

void CContactsSelListFormView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("FilterText"), sFilterText);
	// Set selected column index into registry; 070125 p�d
	AfxGetApp()->WriteProfileInt((REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"), m_nSelectedColumn);

}

