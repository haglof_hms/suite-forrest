#pragma once

#include "Resource.h"

#include "DragDropListBoxAndTreeCtrl.h"

// CColumnMatchDlg dialog

class CColumnMatchDlg : public CDialog
{
	DECLARE_DYNAMIC(CColumnMatchDlg)

	CString m_sRootText;
	int m_nCounter;
	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;

	CMyDDListBox m_wndLCtrl;
	CPADExtTreeCtrl m_wndTList;
	//CXTTreeCtrl m_wndTList;

	CImageList m_ilTreeIcons;

	CButton m_btnOK;
	CButton m_btnCancel;

	void setLanguage(void);

	void setupToMatchColumns(void);
	CStringArray m_sarrColumns;
	void setupNonMatchingColumns(void);

	HTREEITEM getTreeCtrlData(const CTreeCtrl& treeCtrl, HTREEITEM hItem);
	int getLevel(const CTreeCtrl& tree,HTREEITEM hItem);

	vecMapColumns m_mapCols;
public:
	CColumnMatchDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CColumnMatchDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

	void setColumns(CStringArray &sarr)
	{
		m_sarrColumns.Append(sarr);
	}

	void setMap(vecMapColumns &map)
	{
		m_mapCols = map;
	}

	void getMap(vecMapColumns &map)
	{
		map = m_mapCols;
	}

	BOOL isColMatch(LPCTSTR col_name,BOOL from_valid,int *index);

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CDragDropTreeCtrl)
	afx_msg void OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
