#if !defined(AFX_FORRESTDB_H)
#define AFX_FORRESTDB_H

#include "StdAfx.h"


/////////////////////////////////////////////////////////////////////////////////////////////////
// IDENTIFER FOR REPORT-BMP's; 080925 p�d
#define BMP_MAGNINIFICATION_GLASS		5	// In IDB_BITMAP5

class CPropInObjects
{
//private:
	int m_nObjID;
	CString m_sObjName;
public:
	// Default constructor
	CPropInObjects(void)
	{
		m_nObjID=-1;
		m_sObjName=_T("");
	}

	CPropInObjects(int nObjID,LPCTSTR sObjName)
	{
		m_nObjID = nObjID;
		m_sObjName = (sObjName);
	}
	// Copy constructor
	CPropInObjects(const CPropInObjects &c)
	{
		*this = c;
	}

	int getObjID(void)
	{ return m_nObjID; }
	CString getObjName(void)
	{ return m_sObjName;}
};
typedef std::vector<CPropInObjects> vecPropInObjects;		

/////////////////////////////////////////////////////////////////////////////
// PrivItem for inplace button; 070306 p�d
class CInplaceBtn : public CXTPReportRecordItemText
{
//private:
	CString m_sValue;
	CString m_sFilterText;
public:
	CInplaceBtn(LPCTSTR sValue,LPCTSTR filter_text) : CXTPReportRecordItemText()
	{
		m_sValue = sValue;
		SetValue(m_sValue);
		m_sFilterText = filter_text;
		// Check if file exists, if NOT
		// mark it RED; 080926 p�d
//		if (!fileExists(m_sValue))
//			SetTextColor(RED);
//		else if (isURL(m_sValue))
//			SetTextColor(BLUE);
//		else
//			SetTextColor(BLACK);
	}
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
		m_sValue = szText;
		SetValue(m_sValue);
//		if (!fileExists(m_sValue))
//			SetTextColor(RED);
//		else if (isURL(m_sValue))
//			SetTextColor(BLUE);
//		else
//			SetTextColor(BLACK);
	}

	CString getInplaceItem(void)	{	return m_sValue; 	}
	
	void setInplaceItem(LPCTSTR value)	
	{ 
		m_sValue = value; 
		SetValue(value);
	}
protected:
	virtual void OnInplaceButtonDown(CXTPReportInplaceButton* pButton);
};

/////////////////////////////////////////////////////////////////////////////////
// CContactsReportDataRec

class CContactsReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};

public:
	CContactsReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CCheckItem(FALSE));

		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CContactsReportDataRec(UINT index,CTransaction_contacts data,CString type_of) //CStringArray& type_of_company)	 
	{
		m_nIndex = index;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem((data.getPNR_ORGNR())));
		AddItem(new CTextItem((data.getName())));
		AddItem(new CTextItem((data.getCompany())));
		AddItem(new CTextItem((cleanCRLF(data.getAddress(),L", "))));
		AddItem(new CTextItem((cleanCRLF(data.getCoAddress(),L", "))));
		AddItem(new CTextItem((cleanCRLF(data.getPostNum(),L""))));
		AddItem(new CTextItem((cleanCRLF(data.getPostAddress(),L""))));
		AddItem(new CTextItem((data.getCounty())));
		AddItem(new CTextItem((data.getCountry())));
		AddItem(new CTextItem((data.getPhoneWork())));
		AddItem(new CTextItem((data.getPhoneHome())));
		AddItem(new CTextItem((data.getFaxNumber())));
		AddItem(new CTextItem((data.getMobile())));
		AddItem(new CTextItem((data.getEMail())));
		AddItem(new CTextItem((data.getWebSite())));
		AddItem(new CTextItem(type_of));
		AddItem(new CTextItem(data.getBankgiro()));
		AddItem(new CTextItem(data.getPlusgiro()));
		AddItem(new CTextItem(data.getBankkonto()));
		AddItem(new CTextItem(data.getLevnummer()));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	BOOL getColumnCheck(int item)
	{
		return ((CCheckItem*)GetItem(item))->getChecked();
	}

	void setColumnCheck(int item,BOOL bChecked)
	{
		((CCheckItem*)GetItem(item))->setChecked(bChecked);
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};



/////////////////////////////////////////////////////////////////////////////////
// CPropertyReportDataRec

class CPropertyReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	//Lagt till fastighetsid f�r att kunna ta r�tt p� det och radera flera fastigheter fr�n listviewn feature #2453 20111109 J�
	int m_nPropId;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CPropertyReportDataRec(void)
	{
		m_nIndex = -1;
		m_nPropId = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CPropertyReportDataRec(UINT index,CTransaction_property data)	 
	{
		CString sValue;
		m_nIndex = index;
		m_nPropId =data.getID();
		AddItem(new CTextItem((data.getObjectID())));
		AddItem(new CTextItem((data.getCountyName())));
		AddItem(new CTextItem((data.getMunicipalName())));
		AddItem(new CTextItem((data.getParishName())));
		AddItem(new CTextItem((data.getPropertyNum())));
		AddItem(new CTextItem((data.getPropertyName())));
		AddItem(new CTextItem((data.getBlock())));
		AddItem(new CTextItem((data.getUnit())));
		sValue.Format(_T("%.1f"),data.getAreal());
		AddItem(new CTextItem((sValue)));
		sValue.Format(_T("%.1f"),data.getArealMeasured());
		AddItem(new CTextItem((sValue)));
		AddItem(new CTextItem((data.getMottagarref())));
	}


	int getPropId(void)
	{
		return m_nPropId;
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

// Property/object transaction class; 20111006 J�
class CTransaction_prop_obj
{

	int     m_nPropID;
	CString m_sPropCountyCode;
	CString m_sPropMunicipalCode;
	CString m_sPropParishCode;
	CString m_sPropCountyName;
	CString m_sPropMunicipalName;
	CString m_sPropParishName;
	CString m_sPropNum;
	CString m_sPropName;
	CString m_sPropBlock;
	CString m_sPropUnit;
	double	m_fPropAreal;
	double	m_fPropArealMeasured;
	CString m_sPropCreatedBy;
	CString m_sPropCreated;
	CString m_sPropObjID;
	CString m_sPropFullName;
	CString	m_sObjId;
	CString m_sObjName;
	CString m_sObjLittra;
	
public:
	// Default constructor
	CTransaction_prop_obj(void)
	{
		m_nPropID=-1;
		m_sPropCountyCode=_T("");
		m_sPropMunicipalCode=_T("");
		m_sPropParishCode=_T("");
		m_sPropCountyName=_T("");
		m_sPropMunicipalName=_T("");
		m_sPropParishName=_T("");
		m_sPropNum=_T("");
		m_sPropName=_T("");
		m_sPropBlock=_T("");
		m_sPropUnit=_T("");
		m_fPropAreal=0.0;
		m_fPropArealMeasured=0.0;
		m_sPropCreatedBy=_T("");
		m_sPropCreated=_T("");
		m_sPropObjID=_T("");
		m_sPropFullName=_T("");
		m_sObjId=_T("");
		m_sObjName=_T("");
		m_sObjLittra=_T("");
	}

	CTransaction_prop_obj(	int  PropID,
	LPCTSTR PropCountyCode,
	LPCTSTR PropMunicipalCode,
	LPCTSTR PropParishCode,
	LPCTSTR PropCountyName,
	LPCTSTR PropMunicipalName,
	LPCTSTR PropParishName,
	LPCTSTR PropNum,
	LPCTSTR PropName,
	LPCTSTR PropBlock,
	LPCTSTR PropUnit,
	double	PropAreal,
	double	PropArealMeasured,
	LPCTSTR PropCreatedBy,
	LPCTSTR PropCreated,
	LPCTSTR PropObjID,
	LPCTSTR PropFullName,
	LPCTSTR	ObjId,
	LPCTSTR ObjName,
	LPCTSTR ObjLittra)
	{
	m_nPropID=PropID;
	m_sPropCountyCode=(PropCountyCode);
	m_sPropMunicipalCode=(PropMunicipalCode);
	m_sPropParishCode=(PropParishCode);
	m_sPropCountyName=(PropCountyName);
	m_sPropMunicipalName=(PropMunicipalName);
	m_sPropParishName=(PropParishName);
	m_sPropNum=(PropNum);
	m_sPropName=(PropName);
	m_sPropBlock=(PropBlock);
	m_sPropUnit=(PropUnit);
	m_fPropAreal=PropAreal;
	m_fPropArealMeasured=PropArealMeasured;
	m_sPropCreatedBy=(PropCreatedBy);
	m_sPropCreated=(PropCreated);
	m_sPropObjID=(PropObjID);
	m_sPropFullName=(PropFullName);
	m_sObjId=(ObjId);
	m_sObjName=(ObjName);
	m_sObjLittra=(ObjLittra);	
	}

	// Copy constructor
	CTransaction_prop_obj(const CTransaction_prop_obj &c)
	{
		*this = c;
	}

	inline BOOL operator ==(CTransaction_prop_obj &c) const
	{
		return (m_nPropID == c.m_nPropID &&
						m_sPropCountyCode == c.m_sPropCountyCode &&
						m_sPropMunicipalCode == c.m_sPropMunicipalCode &&
						m_sPropParishCode == c.m_sPropParishCode &&
						m_sPropCountyName == c.m_sPropCountyName &&
						m_sPropMunicipalName == c.m_sPropMunicipalName &&
						m_sPropParishName == c.m_sPropParishName &&
						m_sPropNum == c.m_sPropNum &&
						m_sPropName == c.m_sPropName &&
						m_sPropBlock == c.m_sPropBlock &&
						m_sPropUnit == c.m_sPropUnit &&
						m_fPropAreal == c.m_fPropAreal &&
						m_fPropArealMeasured == c.m_fPropArealMeasured &&
						m_sPropCreatedBy == c.m_sPropCreatedBy &&
						m_sPropCreated == c.m_sPropCreated &&
						m_sPropObjID == c.m_sPropObjID &&
						m_sPropFullName == c.m_sPropFullName &&
						m_sObjId== c.m_sObjId &&
						m_sObjName== c.m_sObjName &&
						m_sObjLittra== c.m_sObjLittra
						);

	}


	int getPropID(void)
	{	return m_nPropID;}
	CString getPropCountyCode(void)
	{	return m_sPropCountyCode;}
	CString getPropMunicipalCode(void)
	{	return m_sPropMunicipalCode;}
	CString getPropParishCode(void)
	{	return m_sPropParishCode;}
	CString getPropCountyName(void)
	{	return m_sPropCountyName;}
	CString getPropMunicipalName(void)
	{	return m_sPropMunicipalName;}
	CString getPropParishName(void)
	{	return m_sPropParishName;}
	CString getPropNum(void)
	{	return m_sPropNum;}
	CString getPropName(void)
	{	return m_sPropName;}
	CString getPropBlock(void)
	{	return m_sPropBlock;}
	CString getPropUnit(void)
	{	return m_sPropUnit;}
	double getPropAreal(void)
	{	return m_fPropAreal;}
	double getPropArealMeasured(void)
	{	return m_fPropArealMeasured;}
	CString getPropCreatedBy(void)
	{	return m_sPropCreatedBy;}
	CString getPropCreated(void)
	{	return m_sPropCreated;}
	CString getPropObjectID(void)
	{	return m_sPropObjID;}
	CString getPropFullName(void)
	{	return m_sPropFullName;}
	CString getObjName(void)
	{	return m_sObjName;}
	CString getObjId(void)
	{	return m_sObjId;}
	CString getObjLittra(void)
	{	return m_sObjLittra;}

};

typedef std::vector<CTransaction_prop_obj> vecTransactionPropertyObj;


/////////////////////////////////////////////////////////////////////////////////
// CPropertyReportDataRec

class CPropObjReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : 
				CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:
	CPropObjReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CPropObjReportDataRec(UINT index,CTransaction_prop_obj data)	 
	{
		CString sValue;
		m_nIndex = index;
		AddItem(new CTextItem((data.getPropObjectID())));
		AddItem(new CTextItem((data.getPropCountyName())));
		AddItem(new CTextItem((data.getPropMunicipalName())));
		AddItem(new CTextItem((data.getPropParishName())));
		AddItem(new CTextItem((data.getPropNum())));
		AddItem(new CTextItem((data.getPropName())));
		AddItem(new CTextItem((data.getPropBlock())));
		AddItem(new CTextItem((data.getPropUnit())));
		sValue.Format(_T("%.1f"),data.getPropAreal());
		AddItem(new CTextItem((sValue)));
		sValue.Format(_T("%.1f"),data.getPropArealMeasured());
		AddItem(new CTextItem((sValue)));
		AddItem(new CTextItem((data.getObjId())));
		AddItem(new CTextItem((data.getObjName())));
		AddItem(new CTextItem((data.getObjLittra())));

	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}
	
};

/////////////////////////////////////////////////////////////////////////////////
//	CExcel_import
class CExcel_import
{
	//private:
	CString m_sKey;
	CString m_sCounty;
	CString m_sMunicipal;
	CString m_sParish;
	CString m_sPropNum;
	CString m_sPropName;
	CString m_sName;
	CString m_sBlock;
	CString m_sUnit;
	CString m_sOwnerShare;
	CString m_sOwnerName;
	CString m_sOwnerAddress;
	CString m_sOwnerCoAddress;
	CString m_sOwnerPostNum;
	CString m_sOwnerPostAddress;
	CString m_sOwnerPhoneHome;
	CString m_sOwnerPhoneWork;
	CString m_sOwnerCellPhone;
	CString m_sOwnerFax;
	CString m_sOwnerRegion;
	CString m_sOwnerCountry;
	CString m_sOwnerNotes;
	CString m_sOwnerEMail;
	CString m_sOwnerSecNr;
	CString m_sObjectID;
	CString m_sMottagarref;
	CString m_sBankgiro;
	CString m_sPlusgiro;
	CString m_sBankkonto;
	CString m_sLevnummer;
	BOOL m_bDefaultContact;

public:
	CExcel_import(void)
	{
		m_sKey = ("");
		m_sCounty = ("");
		m_sMunicipal = ("");
		m_sParish = ("");
		m_sPropNum = ("");
		m_sPropName = ("");
		m_sName = ("");
		m_sBlock = ("");
		m_sUnit = ("");
		m_sOwnerShare = ("");
		m_sOwnerName = ("");
		m_sOwnerAddress = ("");
		m_sOwnerCoAddress = ("");
		m_sOwnerPostNum = ("");
		m_sOwnerPostAddress = ("");
		m_sOwnerPhoneHome = ("");
		m_sOwnerPhoneWork = ("");
		m_sOwnerCellPhone = ("");
		m_sOwnerFax = _T("");
		m_sOwnerRegion = _T("");
		m_sOwnerCountry = _T("");
		m_sOwnerNotes = _T("");
		m_sOwnerEMail = ("");
		m_sOwnerSecNr = _T("");
		m_sObjectID = _T("");
		m_sMottagarref = _T("");
		m_sBankgiro = _T("");
		m_sPlusgiro = _T("");
		m_sBankkonto = _T("");
		m_sLevnummer = _T("");

		m_bDefaultContact = TRUE;
	}
	CExcel_import(LPCTSTR key,LPCTSTR county,LPCTSTR municipal,LPCTSTR parish,
													  LPCTSTR prop_num,LPCTSTR prop_name,LPCTSTR name,LPCTSTR block,LPCTSTR unit,
														LPCTSTR owner_share,LPCTSTR owner_name,LPCTSTR owner_address,LPCTSTR owner_co_address,
														LPCTSTR owner_post_num,LPCTSTR owner_post_address,
														LPCTSTR owner_phone_home,LPCTSTR owner_phone_work,
														LPCTSTR owner_cell_phone,LPCTSTR fax,LPCTSTR region,LPCTSTR country,LPCTSTR notes,
														LPCTSTR owner_email,LPCTSTR owner_secnr,LPCTSTR obj_id,LPCTSTR mottagarref,LPCTSTR bankgiro,LPCTSTR plusgiro,LPCTSTR bankkonto,LPCTSTR levnummer,
														BOOL bContact)
	{
		m_sKey = (key);
		m_sCounty = (county);
		m_sMunicipal = (municipal);
		m_sParish = (parish);
		m_sPropNum = (prop_num);
		m_sPropName = (prop_name);
		m_sName = (name);
		m_sBlock = (block);
		m_sUnit = (unit);
		m_sOwnerShare = (owner_share);
		m_sOwnerName = (owner_name);
		m_sOwnerAddress = (owner_address);
		m_sOwnerCoAddress = (owner_co_address);
		m_sOwnerPostNum = (owner_post_num);
		m_sOwnerPostAddress = (owner_post_address);
		m_sOwnerPhoneHome = (owner_phone_home);
		m_sOwnerPhoneWork = (owner_phone_work);
		m_sOwnerCellPhone = (owner_cell_phone);
		m_sOwnerFax = fax;	// Added 2009-08-17 p�d
		m_sOwnerRegion = region;// Added 2009-08-17 p�d
		m_sOwnerCountry = country;// Added 2009-08-17 p�d
		m_sOwnerNotes = notes;// Added 2009-08-17 p�d
		m_sOwnerEMail = (owner_email);
		m_sOwnerSecNr=owner_secnr;//Lagt till personnummer 20111003 J� Feature #2392
		m_sObjectID = (obj_id);
		m_sMottagarref = mottagarref;
		m_sBankgiro = bankgiro;
		m_sPlusgiro = plusgiro;
		m_sBankkonto = bankkonto;
		m_sLevnummer = levnummer;

		m_bDefaultContact = bContact;
	}

	CString getKey()							{ return m_sKey; }
	CString getCounty()						{ return m_sCounty; }
	void setCounty(LPCTSTR v)			{ m_sCounty = v; }
	CString getMunicipal()				{ return m_sMunicipal; }
	void setMunicipal(LPCTSTR v)	{ m_sMunicipal = v; }
	CString getParish()						{ return m_sParish; }
	void setParish(LPCTSTR v)			{ m_sParish = v; }
	CString getPropNum()					{ return m_sPropNum; }
	CString getPropName()					{ return m_sPropName; }
	CString getName()							{ return m_sName; }
	CString getBlock()						{ return m_sBlock; }
	CString getUnit()							{ return m_sUnit; }
	CString getOwnerShare()				{ return m_sOwnerShare; }
	CString getOwnerName()				{ return m_sOwnerName; }
	CString getOwnerAddress()			{ return m_sOwnerAddress; }
	CString getOwnerCoAddress()			{ return m_sOwnerCoAddress; }
	CString getOwnerPostNum()			{ return m_sOwnerPostNum; }
	CString getOwnerPostAddress()	{ return m_sOwnerPostAddress; }
	CString getOwnerPhoneHome()		{ return m_sOwnerPhoneHome; }
	CString getOwnerPhoneWork()		{ return m_sOwnerPhoneWork; }
	CString getOwnerCellPhone()		{ return m_sOwnerCellPhone; }
	CString getOwnerFax()					{ return m_sOwnerFax; }
	CString getOwnerRegion()			{ return m_sOwnerRegion; }
	CString getOwnerCountry()			{ return m_sOwnerCountry; }
	CString getOwnerNotes()				{ return m_sOwnerNotes; }
	CString getOwnerEMail()				{ return m_sOwnerEMail; }
	CString getOwnerSecNr()				{ return m_sOwnerSecNr; }
	CString getObjectID()					{ return m_sObjectID; }

	CString getMottagarref()			{ return m_sMottagarref; }
	CString getBankgiro()					{ return m_sBankgiro; }
	CString getPlusgiro()					{ return m_sPlusgiro; }
	CString getBankkonto()				{ return m_sBankkonto; }
	CString getLevnummer()				{ return m_sLevnummer; }

	BOOL getDefaultContact()	{ return m_bDefaultContact; }

};

typedef std::vector<CExcel_import> vecExcel_import;


/////////////////////////////////////////////////////////////////////////////////
// CImportReportDataRec

class CImportReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
		int m_nID;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
		int getDwData(void)	{ return m_nID; }
		void setDwData(int id)	{ m_nID = id; }

		virtual void OnConstraintChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs,CXTPReportRecordItemConstraint* pConstraint)
		{
			if (pConstraint != NULL)
				m_nID = pConstraint->m_dwData;
			else
				m_nID = -1;

			CXTPReportRecordItem::OnConstraintChanged(pItemArgs,pConstraint);
		}
	};

	class CBoolItem : public CXTPReportRecordItem
	{
		bool m_bValue;
		int m_nID;

		public:
		CBoolItem(bool bValue) : CXTPReportRecordItem()
		{
			HasCheckbox(TRUE);
			m_bValue = bValue;
			SetChecked(m_bValue);
		}

/*		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
		{
			m_sText = szText;
			SetValue(m_sText);
		}*/

/*		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}*/
		int getDwData(void)	{ return m_nID; }
		void setDwData(int id)	{ m_nID = id; }

		virtual void OnConstraintChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs,CXTPReportRecordItemConstraint* pConstraint)
		{
			if (pConstraint != NULL)
				m_nID = pConstraint->m_dwData;
			else
				m_nID = -1;

			CXTPReportRecordItem::OnConstraintChanged(pItemArgs,pConstraint);
		}
	};

public:
	CImportReportDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CBoolItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T(""))); 
		AddItem(new CTextItem(_T(""))); 
		AddItem(new CTextItem(_T(""))); 
		AddItem(new CTextItem(_T(""))); 
		AddItem(new CTextItem(_T(""))); 
	}

	CImportReportDataRec(UINT index,CExcel_import& rec)	 
	{
		m_nIndex = index;
		AddItem(new CTextItem((rec.getCounty())));
		AddItem(new CTextItem((rec.getMunicipal())));
		AddItem(new CTextItem((rec.getParish())));
		AddItem(new CTextItem((rec.getPropNum())));
		AddItem(new CTextItem((rec.getPropName())));
		AddItem(new CTextItem((rec.getObjectID())));
		AddItem(new CTextItem((rec.getMottagarref())));
		AddItem(new CTextItem((rec.getOwnerShare())));
		AddItem(new CBoolItem( rec.getDefaultContact() ));
		AddItem(new CTextItem((rec.getOwnerName())));
		AddItem(new CTextItem((rec.getOwnerAddress())));
		AddItem(new CTextItem((rec.getOwnerCoAddress())));
		AddItem(new CTextItem((rec.getOwnerPostNum())));
		AddItem(new CTextItem((rec.getOwnerPostAddress())));
		AddItem(new CTextItem((rec.getOwnerPhoneHome())));
		AddItem(new CTextItem((rec.getOwnerPhoneWork())));
		AddItem(new CTextItem((rec.getOwnerCellPhone())));
		AddItem(new CTextItem((rec.getOwnerFax())));
		AddItem(new CTextItem((rec.getOwnerRegion())));
		AddItem(new CTextItem((rec.getOwnerCountry())));
		AddItem(new CTextItem((rec.getOwnerNotes())));
		AddItem(new CTextItem((rec.getOwnerEMail())));
		AddItem(new CTextItem((rec.getOwnerSecNr())));
		AddItem(new CTextItem((rec.getBankgiro())));
		AddItem(new CTextItem((rec.getPlusgiro())));
		AddItem(new CTextItem((rec.getBankkonto())));
		AddItem(new CTextItem((rec.getLevnummer())));
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	void setColumnText(int item,LPCTSTR text)	
	{ 
		((CTextItem*)GetItem(item))->setTextItem(text);
	}

	int getColumnDwData(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getDwData();
	}
	void setColumnDwData(int item,int id)	
	{ 
		((CTextItem*)GetItem(item))->setDwData(id);
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}

	bool getColumnBool(int nItem)
	{
		return GetItem(nItem)->IsChecked();
	}
	void  setColumnBool(int nItem, bool bValue)
	{
		GetItem(nItem)->SetChecked(bValue);
	}
};



/////////////////////////////////////////////////////////////////////////////////
// CURLDataRec

class CURLReportDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CTransaction_external_docs recExtDoc;
protected:
	class CIconItem : public CXTPReportRecordItem
	{
	//private:
	public:
		CIconItem(int icon_id) : CXTPReportRecordItem()
		{
			SetIconIndex(icon_id);
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
		int m_nID;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
		int getDwData(void)	{ return m_nID; }
		void setDwData(int id)	{ m_nID = id; }

		virtual void OnConstraintChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs,CXTPReportRecordItemConstraint* pConstraint)
		{
			if (pConstraint != NULL)
				m_nID = pConstraint->m_dwData;
			else
				m_nID = -1;

			CXTPReportRecordItem::OnConstraintChanged(pItemArgs,pConstraint);
		}
	};
public:
	CURLReportDataRec(CString filter_text = _T("Alla *.*|*.*|"))
	{
		m_nIndex = -1;
		AddItem(new CIconItem(BMP_MAGNINIFICATION_GLASS));
		AddItem(new CInplaceBtn(_T(""),(filter_text)));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem((getUserName().MakeUpper())));
		AddItem(new CTextItem((getDBDateTime())));
	}

	CURLReportDataRec(UINT index,LPCTSTR filter_text,CTransaction_external_docs& rec)	 
	{
		m_nIndex = index;
		recExtDoc = rec;
		AddItem(new CIconItem(BMP_MAGNINIFICATION_GLASS));
		AddItem(new CInplaceBtn((rec.getExtDocURL()),(filter_text)));
		AddItem(new CTextItem((rec.getExtDocNote())));
		AddItem(new CTextItem((rec.getExtDocDoneBy())));
		AddItem(new CTextItem((rec.getExtDocCreated())));
	}

	CString getInplaceColumnText(int item)	
	{ 
		return ((CInplaceBtn*)GetItem(item))->getInplaceItem();
	}
	void setInplaceColumnText(int item,LPCTSTR text)	
	{ 
		return ((CInplaceBtn*)GetItem(item))->setInplaceItem(text);
	}

	CString getColumnText(int item)	
	{ 
		return ((CTextItem*)GetItem(item))->getTextItem();
	}
	void setColumnText(int item,LPCTSTR text)	
	{ 
		return ((CTextItem*)GetItem(item))->setTextItem(text);
	}

	UINT getIndex(void)
	{
		return m_nIndex;
	}

	CTransaction_external_docs& getRecord(void)	 { return recExtDoc; }
};


/////////////////////////////////////////////////////////////////////////////////
// class record to hold informtion on Counties or Municipals per selected
// county or parish per selected County and Municipal; 080710 p�d
class CCMP_data
{
	//private:
	int m_nID;
	CString m_sName;
public:
	CCMP_data(void)
	{
		m_nID = -1;
		m_sName = _T("");
	}
	CCMP_data(int id,LPCTSTR name)
	{
		m_nID = id;
		m_sName = (name);
	}
	CCMP_data(const CCMP_data &c)
	{
		*this = c;
	}

	int getID()		{ return m_nID; }
	CString getName()	{ return m_sName; }
	void setName(LPCTSTR v)	{ m_sName = v; }
};

typedef std::vector<CCMP_data> vecCMP_data;



class CForrestDB : public CDBBaseClass_SQLApi //CDBHandlerBase
{
//private:
	BOOL spcExist(CTransaction_species &);
	BOOL cmpExists(CTransaction_county_municipal_parish &);	// County,Municipal and Parish (CMP)
	BOOL postnumberExists(CTransaction_postnumber &);
	BOOL categoryExists(CTransaction_category &);
	BOOL contactExists(CTransaction_contacts &);
	BOOL propertyExists(CTransaction_property &);
	
	BOOL propOwnersExists(int prop_id);
	BOOL extDocExists(LPCTSTR tbl_name,int link_id);

public:
	CForrestDB(void);
	CForrestDB(SAClient_t client,LPCTSTR db_name = _T(""),LPCTSTR user_name = _T(""),LPCTSTR psw = _T(""));
	CForrestDB(DB_CONNECTION_DATA &db_connection);

	//Gjort denna public Bug #2474 20111020 J�
	BOOL propOwnersExists(CTransaction_prop_owners &);

	//#5008 PH 20160614
	BOOL addPropertyToObject(int prop_id,int obj_id);

	//#5010 PH	20160614
	CString savedSQL;

	BOOL getSpecies(vecTransactionSpecies &);
	int getSpecieNextID(void);
	BOOL addSpecie(CTransaction_species &);
	BOOL updSpecie(CTransaction_species &);
	BOOL delSpecie(CTransaction_species &);

	BOOL getPostnumbers(vecTransactionPostnumber &);
	BOOL addPostnumber(CTransaction_postnumber &);
	BOOL updPostnumber(CTransaction_postnumber &);
	BOOL removePostnumber(CTransaction_postnumber &);
	int getMaxPostnumberID(void);
	
	BOOL getCategories(vecTransactionCategory &);
	BOOL addCategory(CTransaction_category &);
	BOOL updCategory(CTransaction_category &);
	BOOL removeCategory(CTransaction_category &);

	BOOL getContacts(vecTransactionContacts &);
	BOOL getContacts(vecTransactionContacts &,BOOL);
	BOOL getContacts(CString sql,vecTransactionContacts &,bool clearVector = true);
	BOOL getContactsForObjectId(int,vecTransactionContacts &,bool clearVector = true);
	BOOL addContact(CTransaction_contacts &);
	BOOL updContact(CTransaction_contacts &);
	BOOL updContactImport(CTransaction_contacts &);
	//Lagt till en funktion som bara uppdaterar datat som kommer fr�n excelimporten samt bara de v�rden som har data bug #2274 20111020 J�
	BOOL updContact(CString sOwnerName,CString sAddress,CString sCoAddress,CString sPostNum,CString sPostAddress,CString sPhoneWork,CString sPhoneHome
							,CString sMobile,CString sFax,CString sRegion,CString sCountry,CString sNotes,CString sEMail,CString sSecNr,int nId,CTransaction_contacts &);
	BOOL removeContact(CTransaction_contacts &);
	int getLastContactID(void);
	BOOL resetContactIdentityField(void);
	int getNumOfRecordsInContacts(void);
	int getNumOfRecordsInContacts(LPCTSTR count_field,LPCTSTR count_value);

	int getObjectIdFromPropertyId(int propId);

	BOOL getCategoriesForContact(vecTransactionCategoriesForContacts &);
	BOOL addCategoriesForContact(vecTransactionCategoriesForContacts &);
	BOOL addCategoriesForContact(int contact_id,int category_id);
	BOOL removeCategoriesForContact(int contact_id);

	BOOL getCMPs(vecTransatcionCMP &);
	BOOL getCounties(vecTransatcionCMP &);
	BOOL getCountiesAndMunicipality(vecTransatcionCMP &);
	BOOL addCMP(CTransaction_county_municipal_parish &);
	BOOL updCMP(CTransaction_county_municipal_parish &);
	BOOL delCMP(CTransaction_county_municipal_parish &);

	BOOL getCounties(vecCMP_data &);
	BOOL getMunicipals(int county_id,vecCMP_data &);
	BOOL getMunicipals(LPCTSTR county,vecCMP_data &);
	BOOL getParishs(int county_id,int municipal_id,vecCMP_data &);
	BOOL getParishs(LPCTSTR county,LPCTSTR municipal,vecCMP_data &);

	BOOL getProperties(vecTransactionProperty &);
	BOOL getProperties(vecTransactionProperty &,BOOL);
	BOOL getProperties(vecTransactionPropertyObj &,int nContactId);
	BOOL getProperties(CString sql,vecTransactionProperty &vec);
	BOOL getPropertiesForObjectId(int id,vecTransactionProperty &vec);
	BOOL addProperty(CTransaction_property &);
	BOOL updPropertyExcel(CTransaction_property &rec);
	BOOL updProperty(CTransaction_property &);
	BOOL updPropertyObjID(int prop_id,LPCTSTR obj_id);
	BOOL removeProperty(CTransaction_property &);
	int getLastPropertyID(void);
	BOOL resetPropertyIdentityField(void);
	int getNumOfRecordsInProperty(void);
	int getNumOfRecordsInProperty(LPCTSTR count_field,LPCTSTR count_value);
	BOOL isPropertyUsed(int prop_id);
	BOOL isPropertyUsed_list(int trakt_id,int prop_id,vecPropInObjects &);
	BOOL isPropertyUnique(CTransaction_property &rec);
	BOOL runSQLQuestion(LPCTSTR sql);

	BOOL getPropOwners(vecTransactionPropOwners &);
	BOOL getPropOwnersForProperty(int,vecTransactionPropOwnersForPropertyData &);
	BOOL addPropOwner(CTransaction_prop_owners &);
	//#5010 PH 20160614
	BOOL addPropOwner(int propId,int contactId);
	//Lagt till en funktion som bara uppdaterar datat som kommer fr�n excelimporten samt bara de v�rden som har data bug #2274 20111020 J�
	BOOL updPropOwner(CTransaction_prop_owners &rec);
	BOOL removePropOwner(CTransaction_prop_owners &);
	BOOL removePropOwner(int prop_id);
	BOOL removePropOwner(int prop_id,int contact_id);	// Added 100127 p�d
	BOOL isPropertyOwnerUsed(int contact_id);

	BOOL getExternalDocuments(LPCTSTR tbl_name,int link_id,vecTransaction_external_docs &);
	BOOL addExternalDocuments(CTransaction_external_docs rec);
	BOOL delExternalDocuments(LPCTSTR tbl_name,int link_id);
	BOOL delExternalDocument(int id,LPCTSTR tbl_name,int link_id);

	BOOL getTraktForProperty(int property_id,vecTransactionTrakt &vec);

	BOOL getTraktNoProperty(vecTransactionTrakt &vec);

	BOOL getTraktData(int trakt_id,double *m3sk,double *gy);

	BOOL updConnectPropertyToStand(int trakt_id,int prop_id);
	BOOL updRemovePropertyFromStand(int trakt_id);

	BOOL saveImage(CString path, CTransaction_contacts rec);
	BOOL removeImage(CTransaction_contacts rec);
	BOOL loadImage(CString path, int contact_id);
};



#endif