#pragma once

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////
// CPropStandsReportRec

class CPropStandsReportRec : public CXTPReportRecord
{
	bool m_bCanBeChanged;
	int m_nTraktID;
	int m_nObjId;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);

			CXTPReportControl *pCtrl = NULL;
			if (pItemArgs != NULL)
			{
				pCtrl = pItemArgs->pControl;
				if (pCtrl != NULL)
				{
					pCtrl->GetParent()->SendMessage(WM_COMMAND,pCtrl->GetDlgCtrlID(),0);
				}
			}
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};
public:

	CPropStandsReportRec(void)
	{
		m_bCanBeChanged = FALSE;
		m_nTraktID = -1;
		m_nObjId=-1;
		AddItem(new CExIconItem(_T(""),3));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz2dec));
	}

	CPropStandsReportRec(int trakt_id,LPCTSTR yes,LPCTSTR no,LPCTSTR stand_number,LPCTSTR stand_name,double volume,double gy,CString sObjName,int nObjId)
	{
		m_nTraktID = trakt_id;
		m_nObjId=nObjId;
		m_bCanBeChanged = sObjName.IsEmpty();
		if (sObjName.IsEmpty())
			AddItem(new CExIconItem((sObjName),8));		
		else
			AddItem(new CExIconItem((sObjName),3));		
		AddItem(new CTextItem(stand_number));
		AddItem(new CTextItem(stand_name));
		AddItem(new CFloatItem(volume,sz2dec));
		AddItem(new CFloatItem(gy,sz1dec));

		if (!m_bCanBeChanged)
		{
			this->GetItem(COLUMN_0)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_1)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_2)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_3)->SetBackgroundColor(LTCYAN);
			this->GetItem(COLUMN_4)->SetBackgroundColor(LTCYAN);
		}
		else
		{
			this->GetItem(COLUMN_0)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_1)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_2)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_3)->SetBackgroundColor(WHITE);
			this->GetItem(COLUMN_4)->SetBackgroundColor(WHITE);
		}

	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

	bool getCanBeChanged(void)	{ return m_bCanBeChanged; }

	int getTraktID(void)	{ return m_nTraktID; }
	int getObjId(void){return m_nObjId;}
};


// CStandsInPropertyFormView form view

class CStandsInPropertyFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CStandsInPropertyFormView)

	CImageList m_ilIcons;
	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sYes;
	CString m_sNo;

	CString m_sMsgCap;
	CString m_sMsgDoRemoveStandCap;
	CString m_sMsgRemoveStandCap;

	int m_nActivePropertyID;

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;


	vecTransactionTrakt m_vecTraktForProperty;
	BOOL getStandsForProperty(int *prop_id);
	void getStandData(int trakt_id,double *m3sk,double *gy);
	void isPropertyUsedInObject(int trakt_id,int prop_id,vecPropInObjects &vecPInObj);

	BOOL populateReport(void);

protected:
	CStandsInPropertyFormView();           // protected constructor used by dynamic creation
	virtual ~CStandsInPropertyFormView();

	// My data members
	CMyReportCtrl m_wndReport2;

	BOOL setupReport2(void);
	void LoadReportState(void);
	void SaveReportState(void);

public:
	enum { IDD = IDD_FORMVIEW9 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

		void doRePopulate(void);

		BOOL isTraktInProperty(void) 
		{ 
			return (m_wndReport2.GetRows()->GetCount() > 0);
		}
protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnDestroy();
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
//	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result);
//	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton31();
	afx_msg void OnBnClickedButton32();
};


