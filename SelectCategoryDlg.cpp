// SelectCategoryDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "SelectCategoryDlg.h"

#include "ResLangFileReader.h"
#include ".\selectcategorydlg.h"

// CSelectCategoryDlg dialog

IMPLEMENT_DYNAMIC(CSelectCategoryDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CSelectCategoryDlg, CXTResizeDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CSelectCategoryDlg::CSelectCategoryDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CSelectCategoryDlg::IDD, pParent)
{
	m_contact = CTransaction_contacts();
	m_pDB = NULL;
}

CSelectCategoryDlg::~CSelectCategoryDlg()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CSelectCategoryDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP, m_wndGroup);
	DDX_Control(pDX, IDC_LIST1, m_wndLB1);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	//}}AFX_DATA_MAP

}

BOOL CSelectCategoryDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();

	m_wndLB1.Initialize();

	m_sLangAbrev = getLangSet();
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	setLanguage();

	getCategories();
	getCategoriesForContact();
	addCategories();
	return TRUE;  // return TRUE  unless you set the focus to a control

}

BOOL CSelectCategoryDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}
	}
	return CXTResizeDialog::OnCopyData(pWnd, pData);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSelectCategoryDlg::OnPaint()
{
	CXTResizeDialog::OnPaint();
}

// CSelectCategoryDlg message handlers

// My methods
void CSelectCategoryDlg::setLanguage(void)
{

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText(xml.str(IDS_STRING235));
			m_wndOKBtn.SetWindowText(xml.str(IDS_STRING216));
			m_wndCancelBtn.SetWindowText(xml.str(IDS_STRING217));

			xml.clean();
		}
	}
}

BOOL CSelectCategoryDlg::getCategories(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected =	m_pDB->getCategories(m_vecCategoryData))
				bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}


BOOL CSelectCategoryDlg::getCategoriesForContact(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getCategoriesForContact(m_vecCatgoriesForContactData))
				bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

void CSelectCategoryDlg::addCategories(void)
{
	// Add userdefined categories to listbox; 061221 p�d
	if (m_vecCategoryData.size() > 0)
	{
		m_wndLB1.ResetContent();
		for (UINT i = 0;i < m_vecCategoryData.size();i++)
		{
			m_wndLB1.AddString(m_vecCategoryData[i].getCategory());
			m_wndLB1.SetCheck(i,(isCategorySelected(m_vecCategoryData[i]) ? 1 : 0));
		}	// for (UINT i = 0;i < m_vecCategoryData.size();i++)
	}	// if (m_vecCategoryData.size() > 0)
}

BOOL CSelectCategoryDlg::isCategorySelected(CTransaction_category rec)
{
	CString S;
	CTransaction_categories_for_contacts data;
	// Try to match Categories selected for this contact to categories in table
	// and set as checked; 061221 p�d
	if (m_vecCatgoriesForContactData.size() > 0)
	{
		for (UINT i = 0;i < m_vecCatgoriesForContactData.size();i++)
		{
			data = m_vecCatgoriesForContactData[i];
			if (m_contact.getID() == data.getContactID() && 
					rec.getID() == data.getCategoryID())
			{
				return TRUE;
			}	// if (m_contact.getID() == data.getContactID() && rec.getID() == data.getCategoryID())
		}	// for (UINT i = 0;i < m_vecCatgoriesForContactData.size();i++)
	}	// if (m_vecCatgoriesForContactData.size() > 0)

	return FALSE;

}

void CSelectCategoryDlg::OnBnClickedOk()
{
	CString sToCat;
	CTransaction_category data;
	// Setup a string (semicolon), for selected categories; 061221 p�d
	m_vecSelectedCatgoriesForContactData.clear();
	if (m_wndLB1.GetCount() > 0)
	{
		for (int i = 0;i < m_wndLB1.GetCount();i++)
		{
			if (m_wndLB1.GetCheck(i) == 1)
			{
				data = m_vecCategoryData[i];
				sToCat.Format(_T("%s;"),data.getCategory());
				m_sSelectedCategories += sToCat;

				// Setup m_vecSelectedCatgoriesForContactData to hold categories for
				// this contact; 061221 p�d
				m_vecSelectedCatgoriesForContactData.push_back(CTransaction_categories_for_contacts(m_contact.getID(),data.getID(),_T("")));
			}	// if (m_wndLB1.GetCheck(i) == 1)
		}	// for (int i = 0;i < m_wndLB1.GetCount();i++)
		
		// Remove last semicolon; 061221 p�d
		m_sSelectedCategories.Delete(m_sSelectedCategories.GetLength()-1);
	}	// if (m_wndLB1.GetCount() > 0)

	OnOK();
}
