// CategoryFormView.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "CategoryFormView.h"

#include "ResLangFileReader.h"

// CCategoryFormView

IMPLEMENT_DYNCREATE(CCategoryFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CCategoryFormView, CXTResizeFormView)
	ON_WM_SIZE()
	//ON_WM_ERASEBKGND()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

CCategoryFormView::CCategoryFormView()
	: CXTResizeFormView(CCategoryFormView::IDD)
{
	m_bInitialized = FALSE;
	m_pDB = NULL;
}

CCategoryFormView::~CCategoryFormView()
{
	if (m_pDB != NULL)
		delete m_pDB;
}

void CCategoryFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CCategoryFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

BOOL CCategoryFormView::OnEraseBkgnd(CDC *pDC)
{
	CRect clip;
  m_wndReport1.GetWindowRect(&clip);		// get rect of the control

  ScreenToClient(&clip);
  pDC->ExcludeClipRect(&clip);

  pDC->GetClipBox(&clip);
  pDC->FillSolidRect(clip, GetSysColor(COLOR_WINDOW));

	return FALSE;
}

void CCategoryFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{

		m_sLangAbrev = getLangSet();
		// Setup language filename; 051214 p�d
//		m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,m_sLangAbrev,LANGUAGE_FN_EXT);
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setupReport1();
	
		m_bInitialized = TRUE;
	}
}

BOOL CCategoryFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

// CCategoryFormView diagnostics

#ifdef _DEBUG
void CCategoryFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CCategoryFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


void CCategoryFormView::doSetNavigationBar()
{
	if (m_vecCategoryData.size() > 0)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	}

}

// CCategoryFormView message handlers

BOOL CCategoryFormView::getCategories(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getCategories(m_vecCategoryData))
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

// PROTECTED METHODS

void CCategoryFormView::setResize(CWnd *wnd,int x,int y,int w,int h,BOOL use_winpos)
{
	CWnd *pWnd = wnd;
	if (pWnd)
	{
		if (!use_winpos)
		{
			pWnd->MoveWindow(x,y,w,h);
		}
		else
		{
			pWnd->SetWindowPos(&CWnd::wndBottom, x, y, w, h,SWP_NOACTIVATE);
		}
	}
}

BOOL CCategoryFormView::setupReport1(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_wndReport1.GetSafeHwnd() == 0)
	{
		if (!m_wndReport1.Create(this,IDC_CATEGORY_REPORT,FALSE,FALSE))
		{
			return FALSE;
		}
	}

	if (m_wndReport1.GetSafeHwnd() == NULL)
	{
		return FALSE;
	}
	else
	{	
		m_wndReport1.ShowWindow(SW_NORMAL);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{

				m_sMsgCap	= xml.str(IDS_STRING213);
				m_sCategoryID	= xml.str(IDS_STRING210);
				m_sCategory	= xml.str(IDS_STRING211);
				m_sCategoryNotes	= xml.str(IDS_STRING212);
				m_sOKBtn	= xml.str(IDS_STRING216);
				m_sCancelBtn	= xml.str(IDS_STRING217);
				m_sText1	= xml.str(IDS_STRING218);
				m_sText2	= xml.str(IDS_STRING219);
				m_sText3	= xml.str(IDS_STRING220);
				m_sDoneSavingMsg =	xml.str(IDS_STRING2151);

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(0, (m_sCategoryID), 30,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetEditable( FALSE );
				pCol->SetVisible( FALSE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(1, (m_sCategory), 80));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_CENTER );
				pCol->SetAlignment( DT_CENTER );

				pCol = m_wndReport1.AddColumn(new CXTPReportColumn(2, (m_sCategoryNotes), 120));
				pCol->SetEditable( TRUE );
				pCol->SetHeaderAlignment( DT_LEFT );
				pCol->SetAlignment( DT_LEFT );

				m_wndReport1.GetReportHeader()->AllowColumnRemove(FALSE);
				m_wndReport1.SetMultipleSelection( FALSE );
				m_wndReport1.SetGridStyle( TRUE, xtpReportGridSolid );
				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();

				// Read hms_user_types table in hms_administrator scheme(database); 060216 p�d
				getCategories();
				populateReport();

				// Need to set size of Report control; 051219 p�d
				RECT rect;
				GetClientRect(&rect);
				setResize(GetDlgItem(IDC_CATEGORY_REPORT),1,1,rect.right - 1,rect.bottom - 1);

				m_wndReport1.AllowEdit(TRUE);
				m_wndReport1.FocusSubItems(TRUE);
				m_wndReport1.SetFocus();
			}
			xml.clean();
		}	// if (fileExists(sLangFN))

	}

	return TRUE;

}

void CCategoryFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
	RECT rect;
	GetClientRect(&rect);

	setResize(GetDlgItem(IDC_CATEGORY_REPORT),1,1,rect.right - 2,rect.bottom - 2);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CCategoryFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			addCategory();
			if (saveCategory())
			{
				getCategories();
				populateReport();
				setReportFocus();
			}
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			getCategories();
			if (saveCategory())
			{
				getCategories();
				populateReport();
			}
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			getCategories();
			if (removeCategory())
			{
				getCategories();
				populateReport();
			}
			break;
		}	// case ID_DELETE_ITEM :
		
	}	// switch (wParam)

	return 0L;
}

// Handle transaction on database species table; 060317 p�d

BOOL CCategoryFormView::populateReport(void)
{
	// Get species from database; 060317 p�d
	//getCategories();

	// populate report; 060317 p�d
	m_wndReport1.ClearReport();

	if (m_vecCategoryData.size() > 0)
	{
		for (UINT i = 0;i < m_vecCategoryData.size();i++)
		{
			CTransaction_category rec = m_vecCategoryData[i];
			m_wndReport1.AddRecord(new CCategoryReportRec(i,rec));
		}	// for (UINT i = 0;i < vec.size();i++)
	}	// if (vec.size() > 0)

	m_wndReport1.Populate();
	m_wndReport1.UpdateWindow();

	doSetNavigationBar();

	return TRUE;
}

BOOL CCategoryFormView::addCategory(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		m_wndReport1.AddRecord(new CCategoryReportRec());
		m_wndReport1.Populate();
		m_wndReport1.UpdateWindow();

		pRows = m_wndReport1.GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow != NULL)
			{
				pRow->SetSelected(TRUE);
				m_wndReport1.SetFocusedRow(pRow);
			}
		}
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

		return TRUE;
	}
	return TRUE;
}

void CCategoryFormView::setReportFocus(void)
{
	CXTPReportRows *pRows = NULL;
	CXTPReportRow *pRow = NULL;
	if (m_wndReport1.GetSafeHwnd() != NULL)
	{
		pRows = m_wndReport1.GetRows();
		if (pRows)
		{
			pRow = pRows->GetAt(pRows->GetCount()-1);
			if (pRow != NULL)
			{
				pRow->SetSelected(TRUE);
				m_wndReport1.SetFocusedRow(pRow);
			}
		}
	}
}

BOOL CCategoryFormView::saveCategory(void)
{
	CTransaction_category rec;
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			// Add records from Report to vector; 060317 p�d
			m_wndReport1.Populate();
			CXTPReportRecords *pRecs = m_wndReport1.GetRecords();
			if (pRecs->GetCount() > 0)
			{
				for (int i = 0;i < pRecs->GetCount();i++)
				{
					CCategoryReportRec *pRec = (CCategoryReportRec *)pRecs->GetAt(i);

					rec = CTransaction_category(pRec->getColumnInt(0),
 										 									pRec->getColumnText(1),
											 								pRec->getColumnText(2),
																			_T(""));
					if (!m_pDB->addCategory(rec))
						m_pDB->updCategory(rec);
				}	// for (int i = 0;i < pRecs->GetCount();i++)

	// Commented out (PL); 070402 p�d
	//			::MessageBox(0,m_sDoneSavingMsg,m_sMsgCap,MB_ICONINFORMATION | MB_OK);
			}	// if (pRecs->GetCount() > 0)

			m_wndReport1.setIsDirty(FALSE);
			bReturn = TRUE;
		}	// if (pDB != NULL)
	}

	return bReturn;
}

BOOL CCategoryFormView::removeCategory(void)
{
	CXTPReportRow *pRow = NULL;
	CTransaction_category data;
	CCategoryReportRec *pRec = NULL;
	CString sMsg;

	// Add records from Report to vector; 060317 p�d
	m_wndReport1.Populate();

	pRow = m_wndReport1.GetFocusedRow();
	if (pRow != NULL)
	{
		pRec = (CCategoryReportRec *)pRow->GetRecord();
		if (pRec != NULL)
		{
			if (m_vecCategoryData.size() > 0)
			{
				for (UINT i = 0;i < m_vecCategoryData.size();i++)
				{
					data = m_vecCategoryData[i];
					if (pRec->getColumnText(1) == data.getCategory())
						break;
				}
			}
			else
				data = CTransaction_category(pRec->getID(),pRec->getColumnText(0),pRec->getColumnText(1),_T(""));		
			if (m_bConnected)
			{
				if (m_pDB != NULL)
				{
					// Get Regions in database; 061002 p�d	
					//data = m_vecCategoryData[nIndex];
					if (!isCategoryUsed(data))
					{
						// Setup a message for user upon deleting machine; 061010 p�d
						sMsg.Format(_T("%s\n\n%s : %s\n%s : %s\n\n%s\n\n%s"),
												m_sText1,
												//m_sCategoryID,
												//data.getID(),
												m_sCategory,
												data.getCategory(),
												m_sCategoryNotes,
												data.getNotes(),
												m_sText2,
												m_sText3);

						if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_DEFBUTTON2 | MB_YESNO) == IDYES)
						{
							// Delete Category
							m_pDB->removeCategory(data);
						}	// if (::MessageBox(0,sMsg,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
					}	// if (!isMachineUsed(data))
				}	// if (m_pDB != NULL)
			}	// if (m_bConnected)
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)
	return TRUE;
}


BOOL CCategoryFormView::isCategoryUsed(CTransaction_category &)
{
	return FALSE;
}
