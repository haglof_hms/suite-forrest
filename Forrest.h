#ifndef _FORREST_H_
#define _FORREST_H_


#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

// Initialize the DLL, register the classes etc
extern "C" AFX_EXT_API void InitSuite(CStringArray *,vecINDEX_TABLE &,vecINFO_TABLE &);

// Create tables
extern "C" AFX_EXT_API void DoDatabaseTables(LPCTSTR db_name);
extern "C" AFX_EXT_API void DoAlterTables(LPCTSTR);
extern "C" AFX_EXT_API void checkP30Species(LPCTSTR db_name);
extern "C" AFX_EXT_API void DoAddPostnumbersToTable(void);
extern "C" AFX_EXT_API void DoAddCountyMunicipalAndParishToTable(void);

// Open a document view
extern "C" AFX_EXT_API void OpenSuite(int idx,LPCTSTR func,CWnd *,vecINDEX_TABLE &,int *ret);


#endif