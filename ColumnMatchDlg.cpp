// ColumnMatchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "ColumnMatchDlg.h"

#include "ResLangFileReader.h"



const LPCTSTR RESOURCE_DLL							= _T("HMSToolBarIcons32.dll");		// Resource dll, holds icons for e.g. toolbars; 051208 p�d

const LPCTSTR RSTR_TREE_FOLDER_CLOSED		= _T("Treefolderclosed_16x16");
const LPCTSTR RSTR_TREE_FOLDER_OPEN			= _T("Treefolderopen_16x16");
const LPCTSTR RSTR_TREE_FORM						= _T("Treeform_16x16");
const LPCTSTR RSTR_TREE_REPORT					= _T("Treereport_16x16");
const LPCTSTR RSTR_TREE_HELP						= _T("Treehelp_16x16");

///////////////////////////////////////////////////////////////////////////////
// CColumnMatchDlg dialog

IMPLEMENT_DYNAMIC(CColumnMatchDlg, CDialog)

BEGIN_MESSAGE_MAP(CColumnMatchDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CColumnMatchDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CColumnMatchDlg::CColumnMatchDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CColumnMatchDlg::IDD, pParent)
{
	m_ilTreeIcons.Create(12, 12, ILC_MASK|ILC_COLOR32, 1, 1);
	CString sTBResFN = getToolBarResourceFN();
	HICON hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN , 57);
	if (hIcon) m_ilTreeIcons.Add(hIcon);
}

CColumnMatchDlg::~CColumnMatchDlg()
{
}

void CColumnMatchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchPropertyDlg)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);

	DDX_Control(pDX, IDC_LIST2, m_wndLCtrl);
	DDX_Control(pDX, IDC_TREE1, m_wndTList);

	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP

}

BOOL CColumnMatchDlg::OnInitDialog()
{
	CDialog::OnInitDialog();


	m_wndLbl1.SetBkColor(INFOBK);

	setLanguage();

	for (int i = 0;i < NUM_OF_COLUMNS;i++)
	{
		m_mapCols[COLUMN_NAMES[i]] = -1;
	}

	m_wndTList.SetImageList(&m_ilTreeIcons, TVSIL_NORMAL);

	setupToMatchColumns();
	setupNonMatchingColumns();

	return TRUE;
}

// CColumnMatchDlg message handlers

void CColumnMatchDlg::setLanguage(void)
{
	// Setup language filename; 051214 p�d
	CString sLangFN;
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING2910)));
			m_wndLbl1.SetWindowText((xml.str(IDS_STRING2911)));
			m_wndLbl2.SetWindowText((xml.str(IDS_STRING2912)));
			m_wndLbl3.SetWindowText((xml.str(IDS_STRING2913)));

			m_btnOK.SetWindowText((xml.str(IDS_STRING2914)));
			m_btnCancel.SetWindowText((xml.str(IDS_STRING2915)));

			m_wndLCtrl.InsertColumn( 0,(xml.str(IDS_STRING2916)), 0, 170 );
			m_sRootText = (xml.str(IDS_STRING2917));

			m_wndLCtrl.setMessageStr(xml.str(IDS_STRING213),
															 xml.str(IDS_STRING3319),
															 xml.str(IDS_STRING3320));

		}
		xml.clean();
	}
}

void CColumnMatchDlg::setupToMatchColumns(void)
{
	CString S;
	int nRetIndex;
	HTREEITEM hRoot,hItem;
	TVINSERTSTRUCT tvInsert;
	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = NULL;
	tvInsert.item.mask = TVIF_TEXT;
	tvInsert.item.pszText = m_sRootText.GetBuffer();

	hRoot = m_wndTList.InsertItem(&tvInsert);
	m_wndTList.SetItemColor(hRoot,BLUE);
	for (int i = 0;i < NUM_OF_COLUMNS;i++)
	{
		hItem =	m_wndTList.InsertItem(TVIF_TEXT,(COLUMN_NAMES[i]),0,0,0,0,0,hRoot,NULL);
		m_wndTList.SetItemData(hItem,i);
		m_wndTList.SetItemBold(hItem,TRUE);

		if (isColMatch(COLUMN_NAMES[i],TRUE,&nRetIndex))
		{
//			HTREEITEM hChild = m_wndTList.InsertItem(TVIF_TEXT,COLUMN_NAMES[i],0,0,0,0,0,hItem,NULL);
			HTREEITEM hChild = m_wndTList.InsertItem(TVIF_TEXT,m_sarrColumns.GetAt(nRetIndex),0,0,0,0,0,hItem,NULL);
//			if (hChild)	m_wndTList.SetItemData(hChild,i);
			if (hChild)	m_wndTList.SetItemData(hChild,nRetIndex);
/*
			S.Format(_T("i = %d\nCOLUMN_NAMES %s\nKolname %s\nnRetIndex %d"),i,COLUMN_NAMES[i],m_sarrColumns.GetAt(nRetIndex),nRetIndex);
			AfxMessageBox(S);
*/
		}	// if (isColMatch(m_sarrColumns.GetAt(i)))

		hItem = NULL;
	}

	m_wndTList.Expand(hRoot,TVE_EXPAND);
}

void CColumnMatchDlg::setupNonMatchingColumns(void)
{
	int nRetIndex;
	if (m_sarrColumns.GetCount() > 0)
	{
		for (int i = m_sarrColumns.GetCount()-1;i >= 0;i--)
		{
			if (!isColMatch(m_sarrColumns.GetAt(i),FALSE,&nRetIndex))
			{
				if(m_sarrColumns.GetAt(i).Compare(L"")!=0) { //#5063 PH Tog bort tomma f�lt fr�n okopplade f�lt.
					m_wndLCtrl.InsertItem(0,m_sarrColumns.GetAt(i));
					m_wndLCtrl.SetItemData(0,i);
				}
			}
		}
	}
}

BOOL CColumnMatchDlg::isColMatch(LPCTSTR col_name,BOOL from_valid,int *index)
{
	CString sColName(col_name);
	if (!from_valid)
	{
		for (int i = 0;i < NUM_OF_COLUMNS;i++)
		{
			if (sColName.CompareNoCase(COLUMN_NAMES[i]) == 0)
			{
				*index = i;
				return TRUE;
			}
		}
	}
	else
	{
		for (int i = m_sarrColumns.GetCount()-1;i >= 0;i--)
		{
			CString sValidColName(m_sarrColumns.GetAt(i));
			if (sValidColName.CompareNoCase(sColName) == 0)
			{
				*index = i;
				return TRUE;
			}
		}
	}
	return FALSE;
}

// Added 080811 p�d
// Read columns set; 080811 p�d
void CColumnMatchDlg::OnBnClickedOk()
{
	HTREEITEM hTreeItem = m_wndTList.GetRootItem();
  if ((hTreeItem != NULL))
  {
		if (m_wndTList.ItemHasChildren(hTreeItem))
		{
			m_nCounter = 0;
			while (hTreeItem != NULL)
			{
				hTreeItem = getTreeCtrlData(m_wndTList,hTreeItem);	
			}	// while (hTreeItem != NULL)
		}	// if (ItemHasChildren(hTreeItem))
	}	// if ((hTreeItem != NULL) && (TVHT_ONITEM & nFlags))

	OnOK();
}

HTREEITEM CColumnMatchDlg::getTreeCtrlData(const CTreeCtrl& treeCtrl, HTREEITEM hItem)
{
	CString S;
	if (hItem == NULL) return NULL;

	int nLevel = getLevel(treeCtrl,hItem);
	int nItemData = -1;

	// No column dragged, set to select no data from file for column; 080811 p�d
	if (nLevel == 1 && !treeCtrl.ItemHasChildren(hItem))
	{
		return treeCtrl.GetNextItem(hItem, TVGN_NEXT);
	}

	// Return child of this item; 080811 p�d
	if (treeCtrl.ItemHasChildren(hItem))
	{
		return treeCtrl.GetNextItem(hItem, TVGN_CHILD);
	}
	// Get information on column number for dragged, on this column; 080811 p�d
	else
	{
		// return the next item after our parent
		hItem = treeCtrl.GetParentItem(hItem);
		HTREEITEM hChild = treeCtrl.GetChildItem(hItem);
		if (hChild)
		{
			if (nLevel == 2)
			{			
				nItemData = treeCtrl.GetItemData(hChild);
				m_mapCols[treeCtrl.GetItemText(hItem)] = nItemData;
			}

		}
		// next item that follows our parent
		return treeCtrl.GetNextItem(hItem, TVGN_NEXT);
	}

	return NULL;
}

int CColumnMatchDlg::getLevel(const CTreeCtrl& tree,HTREEITEM hItem)
{
	CString S;
	int nLevel = 0;

	while ((hItem = tree.GetParentItem( hItem )) != NULL)
	{
		nLevel++;
	}
	return nLevel;
}
