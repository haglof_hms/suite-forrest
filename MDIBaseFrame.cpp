// MDIDataBaseFrame.cpp : implementation file
//

#include "stdafx.h"
// Afx privae include file.
#include <AfxPriv.h>

#include "Resource.h"
#include "MDIBaseFrame.h"

#include "SpeciesFormView.h"
#include "PostnumFormView.h"
#include "CMPFormView.h"
#include "CategoryFormView.h"
#include "ContactsFormView.h"
#include "PropertyFormView.h"
#include "ExternalDocsReportView.h"

#include "ResLangFileReader.h"

#include "PropertyTabView.h"
#include "ContactsTabView.h"

#include "SearchAndReplaceFormView.h"

#include "ContactsSelListFormView.h"

#include "ImportDataFormView.h"


static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR            // status line indicator
/*
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
*/
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
//
IMPLEMENT_XTP_CONTROL( CMyControlPopup, CXTPControlPopup)

CMyControlPopup::CMyControlPopup(void)
{
	vecMenuItems.clear();
	vecSubMenuItems.clear();
	vecDisabledItems.clear();
}

CMyControlPopup::~CMyControlPopup(void)
{
	vecMenuItems.clear();
	vecSubMenuItems.clear();
	vecDisabledItems.clear();
}

BOOL CMyControlPopup::OnSetPopup(BOOL bPopup)
{
	CMenu menu;
	HMENU sub_menu;
	// Make sure there's any menuitems to add; 080425 p�d
	if (bPopup && vecMenuItems.size() > 0)
	{
		menu.CreatePopupMenu();
		for (UINT i = 0;i < vecMenuItems.size();i++)
		{
			_menu_items item = vecMenuItems[i];
			// create menu items
			if (item.m_nID > 0)		
			{
				menu.AppendMenu(MF_STRING, item.m_nID, item.m_sText);

				if (isExcluded(item.m_nID))
				{
					menu.EnableMenuItem(item.m_nID,MF_BYCOMMAND | MF_DISABLED | MF_GRAYED);
				}	// if (isIncluded(item.m_nID))

			}
			else if (item.m_nID == 0)		
			{
				menu.AppendMenu(MF_SEPARATOR, -1, _T(""));
			}
			else if (item.m_nID <= -1 && vecSubMenuItems.size() > 0) // Setup a Sub-menu
			{
				if (vecSubMenuItems.size() > 0)
				{
					sub_menu = CreatePopupMenu();
					for (UINT i1 = 0;i1 < vecSubMenuItems.size();i1++)
					{
						_menu_items sub_item = vecSubMenuItems[i1];
						if (sub_item.m_nSubMenuID == item.m_nSubMenuID)
						{
							// create sub menu item(s)
							AppendMenu(sub_menu,MF_STRING, sub_item.m_nID, sub_item.m_sText);
						}
						else if (sub_item.m_nID == 0)
						{
							AppendMenu(sub_menu,MF_SEPARATOR, -1, _T(""));
						}
					}
					menu.AppendMenu(MF_POPUP, (UINT)sub_menu, item.m_sText);
				}
			}	// for (UINT i = 0;i < vecSubMenuItems.size();i++)
		}
		SetCommandBar(&menu);
		menu.DestroyMenu();
	}
	return CXTPControlPopup::OnSetPopup(bPopup);
}

BOOL CMyControlPopup::isExcluded(int item_id)
{
	if (vecDisabledItems.size() == 0) return FALSE;
	for (UINT i = 0;i < vecDisabledItems.size();i++)
		if (item_id == vecDisabledItems[i]) return TRUE;
	return FALSE;
}

void CMyControlPopup::addMenuIDAndText(int id,LPCTSTR text,int sub_menu_id)
{
	vecMenuItems.push_back(_menu_items(id,text,sub_menu_id));
}

void CMyControlPopup::setSubMenuIDAndText(int id,LPCTSTR text,int sub_menu_id)
{
	vecSubMenuItems.push_back(_menu_items(id,text,sub_menu_id));
}

void CMyControlPopup::setExcludedItems(vecInt &vec)
{
	if (vec.size() > 0)
	{
		vecDisabledItems.clear();
		vecDisabledItems = vec;
	}	// if (vec.size() > 0)
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc

IMPLEMENT_DYNCREATE(CMDIFrameDoc, CDocument)

BEGIN_MESSAGE_MAP(CMDIFrameDoc, CDocument)
	//{{AFX_MSG_MAP(CMDIFrameDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc construction/destruction

CMDIFrameDoc::CMDIFrameDoc()
{
}

CMDIFrameDoc::~CMDIFrameDoc()
{
}

BOOL CMDIFrameDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc serialization

void CMDIFrameDoc::Serialize(CArchive& ar)
{
	CDocument::Serialize(ar);
}

/////////////////////////////////////////////////////////////////////////////
// CMDIFrameDoc diagnostics

#ifdef _DEBUG
void CMDIFrameDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMDIFrameDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CMDIFrameDoc commands

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
IMPLEMENT_DYNAMIC(CMyMultiDocTemplate, CMultiDocTemplate)

CMyMultiDocTemplate::CMyMultiDocTemplate(HINSTANCE hInst, UINT nIDResource, CRuntimeClass* pDocClass, CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass )
: CMultiDocTemplate(nIDResource, pDocClass, pFrameClass, pViewClass)
{
	m_hInst = hInst ;
	m_ResourceID = nIDResource ;
}

CMyMultiDocTemplate::~CMyMultiDocTemplate()
{

}

void CMyMultiDocTemplate::LoadTemplate()
{
	// call the base class member after switching the resource to the DLL, if this is a DLL document type
	HINSTANCE	hOld ;

	hOld = AfxGetResourceHandle() ;			// save
	AfxSetResourceHandle(m_hInst) ;			// set
	CMultiDocTemplate::LoadTemplate() ;		// now get from correct resources!
	AfxSetResourceHandle(hOld) ;			// restore
}

CDocument* CMyMultiDocTemplate::OpenDocumentFile(LPCTSTR lpszPathName, BOOL bMakeVisible)
{
	// switch to the correct resources and then resturn the document
	CDocument	*pDoc = NULL ;
	HINSTANCE	hOld ;

	hOld = AfxGetResourceHandle() ;			// save
	AfxSetResourceHandle(m_hInst) ;			// set
	pDoc = CMultiDocTemplate::OpenDocumentFile(lpszPathName, bMakeVisible) ;
	AfxSetResourceHandle(hOld) ;			// restore
	return pDoc ;
}

#ifdef _DEBUG
void CMyMultiDocTemplate::AssertValid() const
{
	CMultiDocTemplate::AssertValid();
}

void CMyMultiDocTemplate::Dump(CDumpContext& dc) const
{
	CMultiDocTemplate::Dump(dc);
}
#endif

/////////////////////////////////////////////////////////////////////////////
// CMDISpeciesFrame

IMPLEMENT_DYNCREATE(CMDISpeciesFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CMDISpeciesFrame, CChildFrameBase)
	//{{AFX_MSG_MAP(CMDISpeciesFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDISpeciesFrame::CMDISpeciesFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDISpeciesFrame::~CMDISpeciesFrame()
{
}

void CMDISpeciesFrame::OnDestroy(void)
{

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SPECIEFRAME_KEY);
	SavePlacement(this, csBuf);

	CChildFrameBase::OnDestroy();
}

BOOL CMDISpeciesFrame::DestroyWindow()
{
	return CChildFrameBase::DestroyWindow();
}


int CMDISpeciesFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CChildFrameBase::OnCreate(lpCreateStruct) == -1)
		return -1;
	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING213);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING205),
						xml.str(IDS_STRING206));
		}	// if (xml.Load(m_sLangFN))
		xml.clean();
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDISpeciesFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CChildFrameBase::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDISpeciesFrame::OnClose(void)
{

	CSpeciesFormView *pView = (CSpeciesFormView *)getFormViewByID(IDD_FORMVIEW);
	if (pView)
	{
//		if (pView->getIsDirty())
//		{
// Commented out (PL); 070402 p�d
//			if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
//			{
					pView->saveSpecies();
//			}	// if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
//		}	// if (pView->getIsDirty())
	}	// if (pView)
	
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CChildFrameBase::OnClose();
}

// CMDISpeciesFrame diagnostics

#ifdef _DEBUG
void CMDISpeciesFrame::AssertValid() const
{
	CChildFrameBase::AssertValid();
}

void CMDISpeciesFrame::Dump(CDumpContext& dc) const
{
	CChildFrameBase::Dump(dc);
}

#endif //_DEBUG


void CMDISpeciesFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SPECIES;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SPECIES;

	CChildFrameBase::OnGetMinMaxInfo(lpMMI);
}

void CMDISpeciesFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CChildFrameBase::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
 
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
	{
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
	}
}

void CMDISpeciesFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

}

// load the placement in OnShowWindow()
void CMDISpeciesFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CChildFrameBase::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_SPECIEFRAME_KEY);
		LoadPlacement(this, csBuf);
  }


}

void CMDISpeciesFrame::OnSize(UINT nType,int cx,int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDISpeciesFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}


// MY METHODS


// CMDISpeciesFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CMDIPostnumberFrame

IMPLEMENT_DYNCREATE(CMDIPostnumberFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIPostnumberFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIPostnumberFrame)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIPostnumberFrame::CMDIPostnumberFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDIPostnumberFrame::~CMDIPostnumberFrame()
{
}

void CMDIPostnumberFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_POSTNUMFRAME_KEY);
	SavePlacement(this, csBuf);
}

void CMDIPostnumberFrame::OnClose(void)
{

	CPostnumberFormView *pView = (CPostnumberFormView*)getFormViewByID(IDD_REPORTVIEW0);
	if (pView != NULL)
	{
		pView->doSaveToDB();
		pView = NULL;
	}

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnClose();
}


int CMDIPostnumberFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	int iIndex = m_wndStatusBar.GetPaneCount();

	// Create the edit control and add it to the status bar
	if (!m_wndProgressCtrl.Create(WS_CHILD|WS_VISIBLE|PBS_SMOOTH,
		CRect(0,0,0,0), &m_wndStatusBar, 0))
	{
		TRACE0("Failed to create edit control.\n");
		return -1;
	}

	// Initialize the pane info and add the control.
	int nIndex = m_wndStatusBar.CommandToIndex(ID_INDICATOR_PROG);
	ASSERT (nIndex != -1);

	RECT rect;
	GetWindowRect(&rect);
	m_wndStatusBar.SetPaneWidth(0, 70);
	m_wndStatusBar.SetPaneStyle(0, SBPS_NOBORDERS); //m_wndStatusBar.GetPaneStyle(0) | SBPS_NOBORDERS);
	m_wndStatusBar.SetPaneWidth(1, 200);
	m_wndStatusBar.SetPaneStyle(1, SBPS_STRETCH | SBPS_NORMAL);
//	m_wndStatusBar.SetPaneStyle(2, SBPS_STRETCH | SBPS_NORMAL); //m_wndStatusBar.GetPaneStyle(1) | SBPS_STRETCH | SBPS_NORMAL);

	// add the indicator to the status bar.
	m_wndStatusBar.AddIndicator(ID_INDICATOR_PROG, PROGRESS_CTRL_INDEX);
	m_wndStatusBar.SetPaneWidth(PROGRESS_CTRL_INDEX, 300);
	m_wndStatusBar.SetPaneStyle(PROGRESS_CTRL_INDEX, SBPS_STRETCH | SBPS_NORMAL); //m_wndStatusBar.GetPaneStyle(PROGRESS_CTRL_INDEX));
	m_wndStatusBar.AddControl(&m_wndProgressCtrl, ID_INDICATOR_PROG, FALSE);

	// initialize progress control.
	m_wndProgressCtrl.SetRange (0, 100);
	m_wndProgressCtrl.SetPos (0);
	m_wndProgressCtrl.SetStep (10);
	m_wndProgressCtrl.ShowWindow(SW_HIDE);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.SetPosition(xtpBarTop);

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		HICON hIcon = NULL;
		//HMODULE hResModule = NULL;
		CXTPControl *pCtrl = NULL;
		CString sTBResFN = getToolBarResourceFN();
		if (xml.Load(m_sLangFN))
		{
			m_wndStatusBar.SetPaneText(0,xml.str(IDS_STRING3240));
			m_wndStatusBar.SetPaneText(1,xml.str(IDS_STRING216));

			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR2)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_IMPORT,xml.str(IDS_STRING312));	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_EXECUTE,xml.str(IDS_STRING313));	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_EXECUTE,_T(""),FALSE);	//
						p->GetAt(3)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR1)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDIPostnumberFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIPostnumberFrame diagnostics

#ifdef _DEBUG
void CMDIPostnumberFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIPostnumberFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CMDIPostnumberFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_POSTNUM;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_POSTNUM;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIPostnumberFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
	
	if(bActivate)
	{
	}
}

void CMDIPostnumberFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CPostnumberFormView *pView = (CPostnumberFormView *)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}	// if (pView)

}

// load the placement in OnShowWindow()
void CMDIPostnumberFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_POSTNUMFRAME_KEY);
		LoadPlacement(this, csBuf);

	}
}

void CMDIPostnumberFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

void CMDIPostnumberFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIPostnumberFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}


// MY METHODS


// CMDIPostnumberFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CMDICategoryFrame

IMPLEMENT_DYNCREATE(CMDICategoryFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDICategoryFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDICategoryFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDICategoryFrame::CMDICategoryFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDICategoryFrame::~CMDICategoryFrame()
{
}

void CMDICategoryFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CATEGORYFRAME_KEY);
	SavePlacement(this, csBuf);
}

int CMDICategoryFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING213);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING214),
						xml.str(IDS_STRING215));
		}	// if (xml.Load(m_sLangFN))
		xml.clean();
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDICategoryFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDICategoryFrame diagnostics

#ifdef _DEBUG
void CMDICategoryFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDICategoryFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDICategoryFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CATEGORY;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CATEGORY;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDICategoryFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDICategoryFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CCategoryFormView *pView = (CCategoryFormView *)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}	// if (pView)

}

// load the placement in OnShowWindow()
void CMDICategoryFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CATEGORYFRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDICategoryFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDICategoryFrame::OnClose(void)
{
	CCategoryFormView *pView = (CCategoryFormView *)GetActiveView();
	if (pView)
	{
//		if (pView->getIsDirty())
//		{
// Commented out (PL); 070402 p�d
//			if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
//			{
				pView->saveCategory();
//			}	// if (::MessageBox(0,m_sMsg1,m_sMsgCap,MB_ICONSTOP | MB_YESNO) == IDYES)
//		}	// if (pView->getIsDirty())
	}	// if (pView)
	
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnClose();
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDICategoryFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}


// MY METHODS


// CMDICategoryFrame message handlers

/* 
*	2006-12-18 P�D
* HANDLING CONTACTS
*/

/////////////////////////////////////////////////////////////////////////////
// CMDIContactsFrame

IMPLEMENT_DYNCREATE(CMDIContactsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIContactsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIContactsFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_OPEN, OnUpdateTBBTNImport)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_CREATE, OnUpdateTBBTNCreate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIContactsFrame::CMDIContactsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bIsTBtnEnabledOpen = TRUE;
	m_bIsTBtnEnabledCreate = TRUE;

}

CMDIContactsFrame::~CMDIContactsFrame()
{
}

void CMDIContactsFrame::OnDestroy(void)
{

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CONTACTSFRAME_KEY);
	SavePlacement(this, csBuf);

}

int CMDIContactsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.SetPosition(xtpBarTop);

	// Setup language filename; 051214 p�d
	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING213);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING2360),
						xml.str(IDS_STRING237));

			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();

					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR2)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_IMPORT,xml.str(IDS_STRING312));	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SEARCH,xml.str(IDS_STRING310));	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_IMPORT,_T(""),FALSE);	//
						p->GetAt(3)->SetVisible(FALSE);

					}	// if (nBarID == IDR_TOOLBAR1)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
		}	// if (xml.Load(m_sLangFN))
		xml.clean();
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDIContactsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIContactsFrame diagnostics

#ifdef _DEBUG
void CMDIContactsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIContactsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIContactsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CONTACTS;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CONTACTS;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIContactsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIContactsFrame::OnSetFocus(CWnd*)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	
	CContactsFormView *pView = dynamic_cast<CContactsFormView*>(GetActiveView());
	
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}	// if (pView)

}

// load the placement in OnShowWindow()
void CMDIContactsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CONTACTSFRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIContactsFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIContactsFrame::OnClose(void)
{

	CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
	CContactsFormView* pView;
	int propId=-1;
	if (pTabView != NULL)
	{
		pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView != NULL)
		{
			propId=pView->getPropId();
			pView->saveContact(0,0);
		}	// if (pView != NULL)
	}	// if (pTabView != NULL)
	
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnClose();

	if(propId!=-1) {
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
			(LPARAM)&_doc_identifer_msg(_T("Module118"),_T("Module5200"),_T("Module118"),12345,propId,0));
	}
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIContactsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	int nContactId=-1;
	int nObjId=-1;
	int nPropId=-1;
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_REPORTVIEW1,m_sLangFN,0);

		//Copy pointer to TransactionContacts from ContactsFormView to ContactsListView 2016-06-13 PH
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
			if (pTabView != NULL)
			{
				CContactsFormView* pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
				if (pView != NULL)
				{
					CContactsSelListFormView *contactsListView = (CContactsSelListFormView *)getFormViewByID(IDD_REPORTVIEW1);
					if (contactsListView != NULL)
					{
						contactsListView->setTransactionContacts(pView->getTransactionContacts());
					}
				}
			}
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	}
	else if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
	{
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		{
			if (msg->getValue1() == 113)
			{
				CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
				if (pTabView != NULL)
				{
					CContactsFormView* pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
					if (pView != NULL)
					{
						pView->refreshContacts();
						pView = NULL;
					}		
					pTabView = NULL;
				}
			}	// if (msg->getValue1() == 113)
			//Added function for showing specific contact id, feature #2265 20110808 J�
			if(msg->getValue1() == 3)//Message from UMLandvalue, show specific contact
			{

				//Retreive contact id
				nContactId=msg->getValue2();
				nObjId=msg->getValue3();

				if(nContactId!=-1)
				{
					//Load contact with that Id
					CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
					if (pTabView != NULL)
					{
						CContactsFormView* pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
						if (pView != NULL)
						{
							if(nObjId!=-1) {
								pView->getContactsForObject(nObjId);
							}
							if(!pView->doPopulateOnContactID(nContactId))
							{
								if (fileExists(m_sLangFN))
								{
									RLFReader xml;
									if (xml.Load(m_sLangFN))
									{
										AfxMessageBox(xml.str(IDS_STRING2934));
									}
								}
							}							
						}		
						pTabView = NULL;
					}
				}
			}
			if(msg->getValue1() == 4)//Message from UMLandvalue, new contact
			{

				//Retreive contact id
				nPropId=msg->getValue2();

				if(nPropId!=-1)
				{
					//Create new contact
					CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
					if (pTabView != NULL)
					{
						CContactsFormView* pView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
						if (pView != NULL)
						{
							pView->newOwnerToProperty(nPropId);						
						}		
						pTabView = NULL;
					}
				}
			}
		}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))
	}	
	else
	{

		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}


// MY METHODS
void CMDIContactsFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}


void CMDIContactsFrame::OnUpdateTBBTNImport(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledOpen);
}


void CMDIContactsFrame::OnUpdateTBBTNCreate(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledCreate);
}

// CMDIContactsFrame message handlers

void CMDIContactsFrame::setEnableToolbar(BOOL enable_open,BOOL enable_create)
{
	m_bIsTBtnEnabledOpen = enable_open;
	m_bIsTBtnEnabledCreate = enable_create;
}


/////////////////////////////////////////////////////////////////////////////
// CContactsSelectListFrame

IMPLEMENT_DYNCREATE(CContactsSelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CContactsSelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CContactsSelectListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_FILTER_OFF, OnUpdateTBBTNFilterOff)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CContactsSelectListFrame::CContactsSelectListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bEnableTBBTNFilterOff = FALSE;
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CContactsSelectListFrame::~CContactsSelectListFrame()
{
}

void CContactsSelectListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CONTACTS_SELLEIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CContactsSelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooser.Create(this, IDD_FIELD_CHOOSER,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS))
		return -1;      // fail to create

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_FILTER))
		return -1;      // fail to create

	// docking for field chooser
	m_wndFieldChooser.EnableDocking(0);
	setLanguage();

	ShowControlBar(&m_wndFieldChooser, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooser, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// docking for filter editing
	// Set to 0 = No Docking; 090224 p�d
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CContactsSelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CONTACTS_SELLEIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CContactsSelectListFrame::OnSetFocus(CWnd*)
{
}

BOOL CContactsSelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CContactsSelectListFrame diagnostics

#ifdef _DEBUG
void CContactsSelectListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CContactsSelectListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CContactsSelectListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CONTACTS_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CONTACTS_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CContactsSelectListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CContactsSelectListFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CContactsSelectListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CContactsSelectListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
	if (sizeof(*msg) == sizeof(_doc_identifer_msg)&&lParam!=NULL)
	{
		if(msg->getValue1() == 3)//Message from UMLandvalue
		{
			//Retreive object id
			int nObjectId=msg->getValue2();
			if(nObjectId>0)
			{
				//Load contact with that Id
				CContactsSelListFormView *contactsListView = (CContactsSelListFormView *)getFormViewByID(IDD_REPORTVIEW1);
				if (contactsListView != NULL)
				{
					OutputDebugString(_T("Hittad\n"));

					contactsListView->getContactsForObjectId(nObjectId);
					contactsListView->populateReport();

					contactsListView = NULL;
				}
				else {
					OutputDebugString(_T("Inte Hittad\n"));
				}
			}
		}
	}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))

	return 0L;
}

void CContactsSelectListFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

// MY METHODS

void  CContactsSelectListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sToolTipFilter = xml.str(IDS_STRING300);
			m_sToolTipFilterOff = xml.str(IDS_STRING301);
			m_sToolTipPrintOut = xml.str(IDS_STRING302);
			m_sToolTipSearch = xml.str(IDS_STRING310);
			m_sToolTipAdd = xml.str(IDS_STRING321);

			m_wndFieldChooser.SetWindowText((xml.str(IDS_STRING242)));
			m_wndFilterEdit.SetWindowText((xml.str(IDS_STRING243)));
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CContactsSelectListFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR1)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_FILTER,m_sToolTipFilter);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_FILTER_OFF,m_sToolTipFilterOff);	//
					setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_PRINT,m_sToolTipPrintOut);	//
					setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_SEARCH,m_sToolTipSearch,FALSE);	//
					setToolbarBtn(sTBResFN,p->GetAt(4),RES_TB_ADD,m_sToolTipAdd);	//
				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}

// CContactsSelectListFrame message handlers

/////////////////////////////////////////////////////////////////////////////
// CStandSelectionListFrame

IMPLEMENT_DYNCREATE(CStandSelectionListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CStandSelectionListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CStandSelectionListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_CREATE, OnUpdateTBBTNFilterOff)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CStandSelectionListFrame::CStandSelectionListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bEnableTBBTNFilterOff = FALSE;
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CStandSelectionListFrame::~CStandSelectionListFrame()
{
}

void CStandSelectionListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_STANDS_SELLIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CStandSelectionListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR3);

	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT2,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_FILTER))
		return -1;      // fail to create

	// docking for filter editing
	// Set to 0 = No Docking; 090224 p�d
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setLanguage();
	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CStandSelectionListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_STANDS_SELLIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CStandSelectionListFrame::OnSetFocus(CWnd*)
{
}

BOOL CStandSelectionListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CStandSelectionListFrame diagnostics

#ifdef _DEBUG
void CStandSelectionListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CStandSelectionListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CStandSelectionListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CONTACTS_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CONTACTS_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CStandSelectionListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CStandSelectionListFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CStandSelectionListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CStandSelectionListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

void CStandSelectionListFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

// MY METHODS

void  CStandSelectionListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sToolTipFilter = xml.str(IDS_STRING300);
			m_sToolTipFilterOff = xml.str(IDS_STRING301);
			m_sToolTipAdd = xml.str(IDS_STRING317);

			m_wndFilterEdit.SetWindowText((xml.str(IDS_STRING243)));
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CStandSelectionListFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR3)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_FILTER,m_sToolTipFilter);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_FILTER_OFF,m_sToolTipFilterOff);	//
					setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_ADD,m_sToolTipAdd);	//
				}	// if (nBarID == IDR_TOOLBAR3)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}

// CStandSelectionListFrame message handlers

/////////////////////////////////////////////////////////////////////////////
// CMDICountyMunicipalParishFrame

IMPLEMENT_DYNCREATE(CMDICountyMunicipalParishFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDICountyMunicipalParishFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDICountyMunicipalParishFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDICountyMunicipalParishFrame::CMDICountyMunicipalParishFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDICountyMunicipalParishFrame::~CMDICountyMunicipalParishFrame()
{
}

void CMDICountyMunicipalParishFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CMP_FRAME_KEY);
	SavePlacement(this, csBuf);
}

int CMDICountyMunicipalParishFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.SetPosition(xtpBarTop);

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		HICON hIcon = NULL;
		HMODULE hResModule = NULL;
		CXTPControl *pCtrl = NULL;
		CString sTBResFN = getToolBarResourceFN();
		if (xml.Load(m_sLangFN))
		{
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR2)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_IMPORT,xml.str(IDS_STRING312));	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_EXECUTE,xml.str(IDS_STRING313));	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_SEARCH,_T(""),FALSE);	//
						p->GetAt(3)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR1)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))


	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

void CMDICountyMunicipalParishFrame::OnClose(void)
{

	CCMPFormView *pView = (CCMPFormView*)getFormViewByID(IDD_REPORTVIEW2);
	if (pView != NULL)
	{
		pView->doSaveToDB();
		pView = NULL;
	}

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnClose();
}

BOOL CMDICountyMunicipalParishFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDICountyMunicipalParishFrame diagnostics

#ifdef _DEBUG
void CMDICountyMunicipalParishFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDICountyMunicipalParishFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDICountyMunicipalParishFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_CMP;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_CMP;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDICountyMunicipalParishFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDICountyMunicipalParishFrame::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CCMPFormView *pView = (CCMPFormView *)GetActiveView();
	if (pView)
	{
		pView->doSetNavigationBar();
		pView = NULL;
	}	// if (pView)


}

// load the placement in OnShowWindow()
void CMDICountyMunicipalParishFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CMP_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDICountyMunicipalParishFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

void CMDICountyMunicipalParishFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDICountyMunicipalParishFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)

	return 0L;
}


// MY METHODS


// CMDICountyMunicipalParishFrame message handlers


/* 
*	2007-01-16 P�D
* HANDLING PROPERTIES
*/

/////////////////////////////////////////////////////////////////////////////
// CMDIPropertyFrame

HWND CMDIPropertyFrame::m_TopWindow = NULL;

IMPLEMENT_DYNCREATE(CMDIPropertyFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIPropertyFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIPropertyFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_OPEN, OnUpdateTBBTNImport)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_CREATE, OnUpdateTBBTNCreate)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_TOOLS, OnUpdateTBBTNTools)
	
	ON_COMMAND(ID_BUTTON33788, OnGisTBBtn)
	ON_UPDATE_COMMAND_UI(ID_BUTTON33788, OnUpdateTBBTNGis)

	ON_XTP_CREATECONTROL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIPropertyFrame::CMDIPropertyFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_pToolsPopup = NULL;
	m_bIsTBtnEnabledOpen = TRUE;
	m_bIsTBtnEnabledCreate = TRUE;
	m_bIsTBtnEnabledTools = TRUE;
	m_bGISInstalled = TRUE;
	m_bSysCommand = FALSE;
}

CMDIPropertyFrame::~CMDIPropertyFrame()
{
}

void CMDIPropertyFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTYFRAME_KEY);
	SavePlacement(this, csBuf);
}
/*
// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIPropertyFrame::OnClose(void)
{
	BOOL bOkToClose = TRUE;
	if (!m_bSysCommand)
	{
		bOkToClose = FALSE;
		CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
		if (pTabView != NULL)
		{
			CPropertyFormView* pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pView != NULL)
			{
				CMDIPropOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CMDIPropOwnerFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(1)->GetHandle()));
				if (pView1)
				{
					pView1->isDataChanged();
				}		
				
				if (pView->saveProperty(0))
				{
					pView = NULL;
					bOkToClose = TRUE;
				}
				else
					pView = NULL;
			}		

			pTabView = NULL;
		}
	}

	if (bOkToClose)
	{

		// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	 // Check if the top-most window is closing
	 if (m_hWnd==m_TopWindow)
		m_TopWindow = NULL;

	 CMDIChildWnd::OnClose();
	}
}
*/
void CMDIPropertyFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		m_bSysCommand = TRUE;
		CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
		if (pTabView != NULL)
		{
			CPropertyFormView* pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pView != NULL)
			{
				if (pView->saveProperty(0))
				{
					pView = NULL;
					CMDIChildWnd::OnSysCommand(nID,lParam);
				}
				else
					pView = NULL;
			}		
			pTabView = NULL;
		}
	}
	else
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

int CMDIPropertyFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Check if GIS module is installed (this will be used to enabled/disable toolbar button)
	m_bGISInstalled = fileExists(getSuitesDir() + _T("UMGIS.dll"));


	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.SetPosition(xtpBarTop);


	// Setup language filename; 051214 p�d
//	m_sLangFN.Format(_T("%s%s%s%s"),getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING213);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING2360),
						xml.str(IDS_STRING237));
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();

					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR2)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_IMPORT,xml.str(IDS_STRING312));	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SEARCH,xml.str(IDS_STRING310));	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_TOOLS,xml.str(IDS_STRING318));	//
						setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_GIS,xml.str(IDS_STRING325));	//
					}	// if (nBarID == IDR_TOOLBAR1)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

BOOL CMDIPropertyFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	m_TopWindow = m_hWnd;

	return TRUE;
}

// CMDIPropertyFrame diagnostics

#ifdef _DEBUG
void CMDIPropertyFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIPropertyFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIPropertyFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PROPERTY;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PROPERTY;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIPropertyFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIPropertyFrame::OnSetFocus(CWnd* wnd)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
//	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);
/*
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
*/
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
	if (pTabView != NULL)
	{
		CPropertyFormView* pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView != NULL)
		{
			pView->doSetNavigationBar();
			pView = NULL;
		}		
		pTabView = NULL;
	}

	CMDIChildWnd::OnSetFocus(wnd);
}

// load the placement in OnShowWindow()
void CMDIPropertyFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTYFRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIPropertyFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIPropertyFrame::OnClose(void)
{
	CPropertyTabView *pView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
	if (pView)
	{
		CMDIPropOwnerFormView* pView1 = DYNAMIC_DOWNCAST(CMDIPropOwnerFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(1)->GetHandle()));
		if (pView1)
		{
			pView1->isDataChanged();
		}		

		CPropertyFormView* pView0 = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView0)
		{
			pView0->isDataChanged(0);
		}		
	}

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

 // Check if the top-most window is closing
 if (m_hWnd==m_TopWindow)
  m_TopWindow = NULL;

	CMDIChildWnd::OnClose();
}

void CMDIPropertyFrame::OnUpdateTBBTNImport(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledOpen);
}

void CMDIPropertyFrame::OnUpdateTBBTNCreate(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledCreate);
}

void CMDIPropertyFrame::OnUpdateTBBTNTools(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bIsTBtnEnabledTools);
}

void CMDIPropertyFrame::OnUpdateTBBTNGis(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bGISInstalled);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIPropertyFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_REPORTVIEW3,m_sLangFN);
	}
	else if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
	{
		CString S;
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		{
			if (msg->getValue1() == 122)
			{
				CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
				if (pTabView != NULL)
				{
					CPropertyFormView* pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
					if (pView != NULL)
					{
						pView->refreshProperties();
						pView = NULL;
					}		
					pTabView = NULL;
				}
			}	// if (msg->getValue1() == 122)
			if (msg->getValue1() == 10)	// From GIS; 090810 p�d
			{
				CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
				if (pTabView != NULL)
				{
					CPropertyFormView* pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
					if (pView != NULL)
					{
						pView->getPropertiesForObject(msg->getValue3());
						pView->doPopulate(msg->getValue2(),false);
						pView = NULL;
					}		
				}
			}	// if (msg->getValue1() == 10)
		}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))
	}		
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}


// MY METHODS
void CMDIPropertyFrame::OnGisTBBtn()
{
	CString sLangFN;
	CTransaction_property propRec = CTransaction_property();

	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
	if (pTabView != NULL)
	{
		CPropertyFormView* pView = DYNAMIC_DOWNCAST(CPropertyFormView, CWnd::FromHandle(pTabView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pView != NULL)
		{
			sLangFN = getLanguageFN(getLanguageDir(),_T("UMGIS"),getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
			showFormViewEx(888,sLangFN);
			propRec = pView->getActiveProperty();
			if (propRec.getID() > -1)
			{
				AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
								(LPARAM)&_doc_identifer_msg(_T("Module118"),_T("Module888"),_T("Module118"),10,propRec.getID(),0));
			}
		}
	}
}

void CMDIPropertyFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

int CMDIPropertyFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_TBBTN_TOOLS)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_vecExcudedItemsOnTab0.clear();
				m_vecExcudedItemsOnTab1.clear();
				m_vecExcudedItemsOnTab2.clear();

				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_ADD_STAND_TO_PROP);
				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_DEL_STAND_FROM_PROP);
				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_ADD_CONTACT);
				m_vecExcudedItemsOnTab0.push_back(ID_TOOLS_REMOVE_CONTACT);

				m_vecExcudedItemsOnTab1.push_back(ID_TOOLS_CHANGE_OBJID);
				m_vecExcudedItemsOnTab1.push_back(ID_TOOLS_ADD_STAND_TO_PROP);
				m_vecExcudedItemsOnTab1.push_back(ID_TOOLS_DEL_STAND_FROM_PROP);

				m_vecExcudedItemsOnTab2.push_back(ID_TOOLS_CHANGE_OBJID);
				m_vecExcudedItemsOnTab2.push_back(ID_TOOLS_ADD_CONTACT);
				m_vecExcudedItemsOnTab2.push_back(ID_TOOLS_REMOVE_CONTACT);

				m_pToolsPopup = new CMyControlPopup();
				m_pToolsPopup->SetStyle(xtpButtonIcon);
				m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab0);
				
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_CHANGE_OBJID,xml.str(IDS_STRING320));
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_ADD_CONTACT,xml.str(IDS_STRING311));
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_REMOVE_CONTACT,xml.str(IDS_STRING322));	// Added 100127 p�d
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_ADD_STAND_TO_PROP,xml.str(IDS_STRING317));
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_DEL_STAND_FROM_PROP,xml.str(IDS_STRING319));

				lpCreateControl->pControl = m_pToolsPopup;
				xml.clean();		
			}
		}
		return TRUE;
	}
	return FALSE;
}


void CMDIPropertyFrame::setExcludedToolItems(int tab_selected)
{
	switch (tab_selected)
	{
		case 0 :
			m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab0);
		break;
		case 1 :
			m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab1);
		break;
		case 2 :
			m_pToolsPopup->setExcludedItems(m_vecExcudedItemsOnTab2);
		break;
	};
}

void CMDIPropertyFrame::setEnableToolbar(BOOL enable_open,BOOL enable_create,BOOL enable_tools)
{
	m_bIsTBtnEnabledOpen = enable_open;
	m_bIsTBtnEnabledCreate = enable_create;
	m_bIsTBtnEnabledTools = enable_tools;
}

// CMDIPropertyFrame message handlers


/////////////////////////////////////////////////////////////////////////////
// CPropertySelectListFrame

IMPLEMENT_DYNCREATE(CPropertySelectListFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CPropertySelectListFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CPropertySelectListFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_TBBTN_FILTER_OFF, OnUpdateTBBTNFilterOff)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPropertySelectListFrame::CPropertySelectListFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_bEnableTBBTNFilterOff = FALSE;
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CPropertySelectListFrame::~CPropertySelectListFrame()
{
}

void CPropertySelectListFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTY_SELLEIST_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

int CPropertySelectListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);

	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooser.Create(this, IDD_FIELD_CHOOSER1,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_COLUMNS))
		return -1;      // fail to create

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT1,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_FILTER))
		return -1;      // fail to create

	// docking for field chooser
	m_wndFieldChooser.EnableDocking(0);
	setLanguage();

	ShowControlBar(&m_wndFieldChooser, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooser, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// docking for filter editing
	// Set to 0 = No Docking; 090224 p�d
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CPropertySelectListFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_PROPERTY_SELLEIST_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CPropertySelectListFrame::OnSetFocus(CWnd*)
{
}

BOOL CPropertySelectListFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPropertySelectListFrame diagnostics

#ifdef _DEBUG
void CPropertySelectListFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CPropertySelectListFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CPropertySelectListFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_PROPERTY_SELLIST;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_PROPERTY_SELLIST;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CPropertySelectListFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CPropertySelectListFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CPropertySelectListFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CPropertySelectListFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
		}	// while (pos != NULL)
	}	// if (pDoc != NULL)
	return 0L;
}

void CPropertySelectListFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

// MY METHODS

void  CPropertySelectListFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sToolTipFilter = xml.str(IDS_STRING300);
			m_sToolTipFilterOff = xml.str(IDS_STRING301);
			m_sToolTipPrintOut = xml.str(IDS_STRING302);
			m_sToolTipSearch = xml.str(IDS_STRING310);

			m_wndFieldChooser.SetWindowText((xml.str(IDS_STRING242)));
			m_wndFilterEdit.SetWindowText((xml.str(IDS_STRING243)));
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CPropertySelectListFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR1)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_FILTER,m_sToolTipFilter);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_FILTER_OFF,m_sToolTipFilterOff);	//
					setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_PRINT,m_sToolTipPrintOut);	//
					setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_IMPORT,_T(""),FALSE);	//
					setToolbarBtn(sTBResFN,p->GetAt(4),RES_TB_IMPORT,_T(""),FALSE);	//

				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}

// CPropertySelectListFrame message handlers



/* 
*	2008-08-10 P�D
* HANDLING IMPORT OF DATA FROM E.G. EXCEL WORKSHEET
*/

/////////////////////////////////////////////////////////////////////////////
// CMDIImportDataFrame

IMPLEMENT_DYNCREATE(CMDIImportDataFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIImportDataFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIImportDataFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()

	ON_XTP_CREATECONTROL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIImportDataFrame::CMDIImportDataFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
	m_pToolsPopup = NULL;
}

CMDIImportDataFrame::~CMDIImportDataFrame()
{
}

void CMDIImportDataFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_IMPORT_DATA_KEY);
	SavePlacement(this, csBuf);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIImportDataFrame::OnClose(void)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnClose();
}

int CMDIImportDataFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	int iIndex = m_wndStatusBar.GetPaneCount();

	// Create the edit control and add it to the status bar
	if (!m_wndProgressCtrl.Create(WS_CHILD|WS_VISIBLE|PBS_SMOOTH,
		CRect(0,0,0,0), &m_wndStatusBar, 0))
	{
		TRACE0("Failed to create edit control.\n");
		return -1;
	}

	// Initialize the pane info and add the control.
	int nIndex = m_wndStatusBar.CommandToIndex(ID_INDICATOR_PROG);
	ASSERT (nIndex != -1);

	RECT rect;
	GetWindowRect(&rect);
	m_wndStatusBar.SetPaneWidth(0, 70);
	m_wndStatusBar.SetPaneStyle(0, SBPS_NOBORDERS); //m_wndStatusBar.GetPaneStyle(0) | SBPS_NOBORDERS);
	m_wndStatusBar.SetPaneWidth(1, 200);
	m_wndStatusBar.SetPaneStyle(1, SBPS_STRETCH | SBPS_NORMAL);
//	m_wndStatusBar.SetPaneStyle(2, SBPS_STRETCH | SBPS_NORMAL); //m_wndStatusBar.GetPaneStyle(1) | SBPS_STRETCH | SBPS_NORMAL);

	// add the indicator to the status bar.
	m_wndStatusBar.AddIndicator(ID_INDICATOR_PROG, PROGRESS_CTRL_INDEX);
	m_wndStatusBar.SetPaneWidth(PROGRESS_CTRL_INDEX, 300);
	m_wndStatusBar.SetPaneStyle(PROGRESS_CTRL_INDEX, SBPS_STRETCH | SBPS_NORMAL); //m_wndStatusBar.GetPaneStyle(PROGRESS_CTRL_INDEX));
	m_wndStatusBar.AddControl(&m_wndProgressCtrl, ID_INDICATOR_PROG, FALSE);


	// initialize progress control.
	m_wndProgressCtrl.SetRange (0, 100);
	m_wndProgressCtrl.SetPos (0);
	m_wndProgressCtrl.SetStep (10);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.SetPosition(xtpBarTop);

	// Setup language filename; 090428 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		HICON hIcon = NULL;
		HMODULE hResModule = NULL;
		CXTPControl *pCtrl = NULL;
		CString sTBResFN = getToolBarResourceFN();
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING213);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING2360),
						xml.str(IDS_STRING237));

			m_wndStatusBar.SetPaneText(0,xml.str(IDS_STRING3240));

			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR2)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_IMPORT,xml.str(IDS_STRING312));	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SAVE,xml.str(IDS_STRING313));	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_TOOLS,xml.str(IDS_STRING318));	//
						p->GetAt(3)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR1)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDIImportDataFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIImportDataFrame diagnostics

#ifdef _DEBUG
void CMDIImportDataFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIImportDataFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIImportDataFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_IMPORT_DATA;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_IMPORT_DATA;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIImportDataFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIImportDataFrame::OnSetFocus(CWnd*)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

}

// load the placement in OnShowWindow()
void CMDIImportDataFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_IMPORT_DATA_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIImportDataFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

void CMDIImportDataFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())

	CMDIChildWnd::OnPaint();
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIImportDataFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	if (wParam == ID_DBNAVIG_LIST)
	{
		showFormView(IDD_REPORTVIEW3,m_sLangFN);
	}
	if (wParam == ID_WPARAM_VALUE_FROM + 0x02) {
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if(msg->getValue1()==1) {
			CImportDataReportView *importView = (CImportDataReportView *)getFormViewByID(IDD_FORMVIEW7);
			importView->setObjectId(msg->getValue2());
		}
	}
	else
	{
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
	}
	return 0L;
}


int CMDIImportDataFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_TBBTN_TOOLS)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup = new CMyControlPopup();
				m_pToolsPopup->SetStyle(xtpButtonIcon);
				
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_ADD_CHANGE_OBJID,xml.str(IDS_STRING3230));
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_CREATE_EXCEL_TMPL,xml.str(IDS_STRING32785));

				lpCreateControl->pControl = m_pToolsPopup;
				xml.clean();		
			}
		}
		return TRUE;
	}
	return FALSE;
}

// MY METHODS


// CMDIImportDataFrame message handlers



/* 
*	2008-09-25 P�D
* HANDLING EXTERNAL DOCUMENTS
*/

/////////////////////////////////////////////////////////////////////////////
// CMDIExternalDocsFrame

IMPLEMENT_DYNCREATE(CMDIExternalDocsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMDIExternalDocsFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CMDIExternalDocsFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMDIExternalDocsFrame::CMDIExternalDocsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CMDIExternalDocsFrame::~CMDIExternalDocsFrame()
{
}

void CMDIExternalDocsFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_EXTERNAL_DOCS_KEY);
	SavePlacement(this, csBuf);
}

// Check on closing window, if Trakt is dirty. If so, aks user if
// data should be saved to database; 061017 p�d
void CMDIExternalDocsFrame::OnClose(void)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CExternalDocsReportView *pRepView = (CExternalDocsReportView*)getFormViewByID(IDD_FORMVIEW8);
	if (pRepView != NULL)
	{
		pRepView->doSaveExternalDocs();

		pRepView = NULL;
	}
	

	CMDIChildWnd::OnClose();
}

int CMDIExternalDocsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.SetPosition(xtpBarTop);

	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		HICON hIcon = NULL;
		HMODULE hResModule = NULL;
		CXTPControl *pCtrl = NULL;
		CString sTBResFN = getToolBarResourceFN();
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING213);
			m_sMsg1.Format(_T("%s\n\n%s"),
						xml.str(IDS_STRING2360),
						xml.str(IDS_STRING237));

			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR2)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_ADD,xml.str(IDS_STRING314));	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_REMOVE,xml.str(IDS_STRING315));	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_IMPORT,_T(""),FALSE);	//
						p->GetAt(3)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR1)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())

			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

BOOL CMDIExternalDocsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CMDIExternalDocsFrame diagnostics

#ifdef _DEBUG
void CMDIExternalDocsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMDIExternalDocsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CMDIExternalDocsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_EXTERNAL_DOCS;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_EXTERNAL_DOCS;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CMDIExternalDocsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CMDIExternalDocsFrame::OnSetFocus(CWnd*)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

}

// load the placement in OnShowWindow()
void CMDIExternalDocsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_EXTERNAL_DOCS_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CMDIExternalDocsFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

void CMDIExternalDocsFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())

	CMDIChildWnd::OnPaint();
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CMDIExternalDocsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
/*
	if (wParam == ID_WPARAM_VALUE_FROM + 0x02)
	{
		_doc_identifer_msg *msg = (_doc_identifer_msg*)lParam;
		if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		{
			AfxMessageBox("CMDIExternalDocsFrame::OnMessageFromShell\nID_WPARAM_VALUE_FROM + 0x02");
		}
	}
	else
	{
*/
		CDocument *pDoc = GetActiveDocument();
		if (pDoc != NULL)
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while (pos != NULL)
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage(MSG_IN_SUITE,wParam,lParam);
			}	// while (pos != NULL)
		}	// if (pDoc != NULL)
//	}
	return 0L;
}


// MY METHODS


// CMDIExternalDocsFrame message handlers

/////////////////////////////////////////////////////////////////////////////
// CSearchAndReplaceFrame

HWND CSearchAndReplaceFrame::m_TopWindow = NULL; // There can only be one top-most window

IMPLEMENT_DYNCREATE(CSearchAndReplaceFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CSearchAndReplaceFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CSearchAndReplaceFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_WM_WINDOWPOSCHANGED()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CSearchAndReplaceFrame::CSearchAndReplaceFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMICON);
}

CSearchAndReplaceFrame::~CSearchAndReplaceFrame()
{
}

void CSearchAndReplaceFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CONTACTS_SEARCH_REPL_FRAME_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;
}

void CSearchAndReplaceFrame::OnClose(void)
{
	int nIndex = 0;
	CPropertyTabView *pPropTabView = NULL;
	CContactsTabView *pContactTabView = NULL;
	CPropertyFormView* pPropView =  NULL;
	CContactsFormView* pContactView =  NULL;
	if ((pPropTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5)) != NULL)
	{
		if ((pPropView = (CPropertyFormView*)pPropTabView->getPropertyFormView()) != NULL)
		{
			// Reset properties; 090820 p�d
			CString sSQL;
			sSQL.Format(_T("select * from %s"),TBL_PROPERTY);
			pPropView->doRePopulateFromSearch(sSQL,&nIndex);
			pPropView = NULL;
		}		
		pPropTabView = NULL;
	}

	pContactTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
	if (pContactTabView != NULL)
	{
		pContactView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pContactTabView->getTabControl()->getTabPage(0)->GetHandle()));
		if (pContactView != NULL)
		{
			// Reset properties; 090820 p�d
			CString sSQL;
			sSQL.Format(_T("select * from %s"),TBL_CONTACTS);
			pContactView->doRePopulateFromSearch(sSQL,&nIndex);
			pContactView = NULL;
		}	// if (pView != NULL)
		pContactTabView = NULL;
	}	// if (pContactTabView != NULL)

 // Check if the top-most window is closing
 if (m_hWnd==m_TopWindow)
  m_TopWindow = NULL;

	CMDIChildWnd::OnClose();
}

void CSearchAndReplaceFrame::OnWindowPosChanged(WINDOWPOS* lpwndpos)
{
	// This message-handler is sent to the window
	// being activated (Changing position, size or z-order)
	// Check if another window has the status of being top-most, instead of this one
	if (m_TopWindow!=NULL && m_TopWindow!=m_hWnd && ::IsWindow(m_TopWindow))
	{
		::SetWindowPos(m_TopWindow, HWND_TOP, 0, 0, 0, 0, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
	}
	CMDIChildWnd::OnWindowPosChanged(lpwndpos);
}

int CSearchAndReplaceFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);


	m_bFirstOpen = TRUE;


	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CSearchAndReplaceFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_CONTACTS_SEARCH_REPL_FRAME_KEY);
		LoadPlacement(this, csBuf);
  }
}

BOOL CSearchAndReplaceFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style = WS_CHILD | WS_VISIBLE | WS_OVERLAPPED | WS_CAPTION | FWS_ADDTOTITLE | WS_THICKFRAME | WS_SYSMENU;

	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	m_TopWindow = m_hWnd;

	return TRUE;
}

// CSearchAndReplaceFrame diagnostics

#ifdef _DEBUG
void CSearchAndReplaceFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CSearchAndReplaceFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CSearchAndReplaceFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = MIN_X_SIZE_SEARCH_REPLACE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE_SEARCH_REPLACE;

	lpMMI->ptMaxTrackSize.x = MIN_X_SIZE_SEARCH_REPLACE+100;
	lpMMI->ptMaxTrackSize.y = MIN_Y_SIZE_SEARCH_REPLACE+100;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CSearchAndReplaceFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CSearchAndReplaceFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

void CSearchAndReplaceFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CSearchAndReplaceFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

// MY METHODS

void  CSearchAndReplaceFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}


// CSearchAndReplaceFrame message handlers







