#if !defined(__CONTACTSTABVIEW_H__)
#define __CONTACTSTABVIEW_H__

#include "ContactsFormView.h"

#include "MDIBaseFrame.h"
#include "PropertiesInContactFormView.h"

///////////////////////////////////////////////////////////////
// CContactsTabView 
class CContactsTabView : public CView
{
	DECLARE_DYNCREATE(CContactsTabView)

//private:
	BOOL m_bInitialized;
	CString	m_sLangAbrev;
	CString m_sLangFN;
	CString m_sMsg;
	CFont m_font;

public:
	CContactsTabView();           // protected constructor used by dynamic creation
	virtual ~CContactsTabView();

	void setMsg(LPCTSTR msg)
	{
		m_sMsg = msg;
		Invalidate();
	}

	CContactsFormView *getContactsFormView(void);
	CPropertiesInContactFormView *getPropertiesInContactFormView(void);

	CMyTabControl* getTabControl(void)
	{
		return &m_wndTabControl;
	}
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabbedViewView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate( );
	protected:
	//}}AFX_VIRTUAL
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle,int tab_id, int nIcon);

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	CMDIFrameDoc* GetDocument();

	void setLanguage(void);
protected:

	//{{AFX_MSG(CContactsTabView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

#ifndef _DEBUG  // debug version in TabbedViewView.cpp
inline CMDIFrameDoc* CContactsTabView::GetDocument()
	{ return (CMDIFrameDoc*)m_pDocument; }
#endif


#endif
