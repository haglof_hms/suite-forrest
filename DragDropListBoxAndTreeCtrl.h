

///////////////////////////////////////////////////////////////////////////////
// CPADExtTreeCtrl; 090819 p�d

#define CTreeCtrlBase CXTPCommandBarsSiteBase<CXTTreeCtrl>

class CPADExtTreeCtrl : public CTreeCtrlBase
{
	//private:
	CEdit *m_pTreeEdit;

	BOOL m_bIsTopLevelItem;
	BOOL m_bIsDropTarget;
	CString m_sSuitePathAndFN;

	UINT m_menuItemSelected;
	int m_nSelectedLevel;

protected:
	CImageList*	m_pDragImage;
	BOOL		m_bLDragging;
	HTREEITEM	m_hitemDrag,m_hitemDrop;// Construction

	HTREEITEM copy_branch( HTREEITEM htiBranch, HTREEITEM htiNewParent,HTREEITEM htiAfter = TVI_LAST );
	HTREEITEM copy_item( HTREEITEM hItem, HTREEITEM htiNewParent,HTREEITEM htiAfter = TVI_LAST );

	int get_level(HTREEITEM hItem);

public:
	DECLARE_DYNAMIC( CPADExtTreeCtrl )
	CPADExtTreeCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPADExtTreeCtrl)
	//}}AFX_VIRTUAL

	virtual ~CPADExtTreeCtrl();


	// Generated message map functions
protected:
	//{{AFX_MSG(CPADTreeCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

	// Drag and drop handling
	afx_msg void OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);


	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
}; // class CObjectHierarchyTreeCtrl

///////////////////////////////////////////////////////////////////////////////
// CMyDDListBox

class CMyDDListBox : public CListCtrl
{
	DECLARE_DYNAMIC(CMyDDListBox)

	int get_level(const CTreeCtrl& tree,HTREEITEM hItem);

	CString m_sMsgCap;
	CString m_sMsgHasChildren;
	CString m_sMsgWrongLevel;

public:
	CMyDDListBox();
	virtual ~CMyDDListBox();

	void setMessageStr(LPCTSTR msg_cap,LPCTSTR msg1,LPCTSTR msg2)
	{
		m_sMsgCap = msg_cap;
		m_sMsgHasChildren = msg1;
		m_sMsgWrongLevel = msg2;
	}

protected:
	DECLARE_MESSAGE_MAP()

	// Message handlers
	afx_msg void OnBeginDrag(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

	// Drag-drop vars
	CWnd		*m_pDropWnd;
	HTREEITEM	m_hDropItem;
	BOOL		m_bDragging;
	int			m_nDragIndex;
};
