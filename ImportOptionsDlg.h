#pragma once

#include "Resource.h"

// CImportOptionsDlg dialog

class CImportOptionsDlg : public CDialog
{
	DECLARE_DYNAMIC(CImportOptionsDlg)

	CString m_sLangFN;

	CMyExtStatic m_wndLbl1;

	CButton m_wndCBox1;
	CButton m_wndCBox2;
	CButton m_wndCBox3;

	CXTCheckListBox m_wndLB1;

	CMyExtEdit m_wndEdit3_1;

	CButton m_wndBtnCategory;
	CButton m_wndBtnOK;
	CButton m_wndBtnCancel;

	BOOL m_bIsImportProperty;
	BOOL m_bIsImportOwners;
	BOOL m_bIsMatchOwnersToProperty;

	vecExcel_import m_vecExcel_import;

	void isOkToImportProperties(void);
	void isOkToImportContacts(void);

	vecTransactionCategoriesForContacts m_vecSelectedCatgoriesForContactData;
	
public:
	CImportOptionsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CImportOptionsDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

	void setImportData(vecExcel_import &v) { m_vecExcel_import = v; }

	BOOL isImportProperty(void)	{ return m_bIsImportProperty; 	}
	BOOL isImportOwners(void)	{ return m_bIsImportOwners; }
	BOOL isMatchOwnersToProperty(void)	{ return m_bIsMatchOwnersToProperty; }

	vecTransactionCategoriesForContacts& getCategoriesForContatcs()	{ return m_vecSelectedCatgoriesForContactData; }

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImportOptionsDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedContacts();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCategory31();
};
