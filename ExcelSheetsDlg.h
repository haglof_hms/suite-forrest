#pragma once

#include "Resource.h"
#include "ExcelImportLibXl.h"
#include "afxwin.h"
#include "afxcmn.h"

// CExcelSheetsDlg dialog

class CExcelSheetsDlg : public CDialog
{
	DECLARE_DYNAMIC(CExcelSheetsDlg)

public:
	CExcelSheetsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CExcelSheetsDlg();

// Dialog Data
	enum { IDD = IDD_EXCEL_SHEETS };

	CExcelImportLibXl *m_pExcel;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

	void PopulateReport();
	void setLanguage(void);

public:
	CComboBox m_cbSheets;
	afx_msg void OnCbnSelchangeComboSheet();
	CListCtrl m_clSheetData;
};
