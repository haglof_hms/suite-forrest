#pragma once

#include "ForrestDB.h"

#include "Resource.h"


/////////////////////////////////////////////////////////////////////////////////
// CSelectedPropertiesDataRec

class CSelectedPropertiesDataRec : public CXTPReportRecord
{
	//private:
	UINT m_nIndex;
	CTransaction_property propRec;
	CString sPropName;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};

public:
	CSelectedPropertiesDataRec(void)
	{
		m_nIndex = -1;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CSelectedPropertiesDataRec(UINT index,CTransaction_property data)	 
	{
		m_nIndex = index;
		propRec = data;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem((data.getObjectID())));
		AddItem(new CTextItem((data.getCountyName())));
		AddItem(new CTextItem((data.getMunicipalName())));
		AddItem(new CTextItem((data.getPropertyNum())));
		sPropName.Format(_T("%s %s:%s"),
			data.getPropertyName(),
			data.getBlock(),
			data.getUnit());
		AddItem(new CTextItem((sPropName)));
		
	}

	CString getColumnText(int item)		{ return ((CTextItem*)GetItem(item))->getTextItem();	}

	BOOL getColumnCheck(int item)	{	return ((CCheckItem*)GetItem(item))->getChecked();	}

	void setColumnCheck(int item,BOOL bChecked)	{	((CCheckItem*)GetItem(item))->setChecked(bChecked);	}

	UINT getIndex(void)	{	return m_nIndex;	}
	
	CTransaction_property &getRecord(void)	{	return propRec;	}

};


// CChangeObjIDDlg dialog

class CChangeObjIDDlg : public CDialog
{
	DECLARE_DYNAMIC(CChangeObjIDDlg)

	// Setup language filename; 051214 p�d
	CString m_sLangFN;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;
	CMyExtStatic m_wndLbl10;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;
	CMyExtEdit m_wndEdit8;

	CButton m_wndSearchNewBtn;
	CButton m_wndSearchAddBtn;
	CButton m_wndAddAllBtn;

	CButton m_wndCheckAll;
	CButton m_wndUncheckAll;
	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CMyReportCtrl m_wndReport;
	CImageList m_ilIcons;

	CForrestDB *m_pDB;

	BOOL m_bIsDirty;	//#4483

	vecTransactionProperty m_vecProperties;
	void getProperties(void);

	void setupSQLFromSelections(bool clear);

	void setupReport(void);
	void populateData(void);

	BOOL isItemSelected(void);

	BOOL isItemAlreadyInReport(CTransaction_property &prop);

	void setItemsMatched(void);

	void clearAll(void);
public:
	CChangeObjIDDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChangeObjIDDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG7 };

	void setDBConnection(CForrestDB *db)	{	m_pDB = db;	}

protected:
	//{{AFX_VIRTUAL(CLoggMessageDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnBnClickedBtnSearchNew();
	afx_msg void OnBnClickedBtnSearchAdd();
	afx_msg void OnBnClickedBtnAll();
	afx_msg void OnBnClickedBtnOK();
	afx_msg void OnBnClickedBtnSelectAll();
	afx_msg void OnBnClickedBtnDeSelectAll();
	afx_msg void OnEdObjIDChange();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnClose();
};
