#pragma once

#include "Resource.h"

#include "ForrestDB.h"

#include "picturectrl.h"
#include "pad_hms_miscfunc.h"

/////////////////////////////////////////////////////////////////////////////////////
//	Report classes for Species; 060317 p�d

/////////////////////////////////////////////////////////////////////////////
// CContactsReportRec

class CContactsReportRec : public CXTPReportRecord
{
	int m_nID;
protected:

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CContactsReportRec(void)
	{
		m_nID	= -1;	
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CContactsReportRec(int id,CTransaction_category rec)
	{
		m_nID	= id;
		AddItem(new CIntItem(rec.getID()));
		AddItem(new CTextItem(rec.getCategory()));
		AddItem(new CTextItem(rec.getNotes()));
	}

	int getID(void)
	{
		return m_nID;
	}

	int getColumnInt(int item)	
	{ 
		if (item == 0)
			return ((CIntItem*)GetItem(item))->getIntItem();
		else
			return 0;

	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

// CContactsFormView form view

class CContactsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CContactsFormView)

private:
	BOOL m_bInitialized;
	BOOL m_bSetFocusOnInitDone;
	BOOL m_bIsDataEnabled;
	//CString	m_sLangAbrev;
	CString m_sLangFN;
	CString m_sErrCap;
	CString m_sSaveMsg;
	CString m_sDoneSavingMsg;
	CString m_sDeleteMsg;
	CString m_sMsgCap;
	CString m_sPNR_ORGNR;
	CString m_sName;
	CString m_sCompany;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sContactActiveMsg;
	CString m_sNoContactsMsg;
	CString m_sNoResultInSearch;
	CString m_sNameMissing;
	CString m_sContactNameMissing;

	BOOL m_bIsDirty;

	enumACTION m_enumAction;
	FOCUSED_EDIT m_enumFocusedEdit;

	//Object id - Added 2016-06-13 PH
	int objectId;
	BOOL disableNavButtons;

	//#5010 PH 20160614
	int propId;

	void setLanguage(void);

	void setLanguages(void);

	BOOL getContacts(void);
	
	vecTransactionContacts * m_vecContactsData;
	CTransaction_contacts m_enteredContactData;
	CTransaction_contacts m_activeContactData;
	BOOL isContactUsed(CTransaction_contacts &);
	
	BOOL getCategories(void);
	vecTransactionCategory m_vecCategoriesData;

	BOOL getCategoriesForContact(void);
	vecTransactionCategoriesForContacts m_vecCatgoriesForContactData;
	vecTransactionCategoriesForContacts m_vecSelectedCatgoriesForContactData;

	int m_nDBIndex;
	void populateData(int idx);
	void setNavigationButtons(BOOL start_prev, BOOL end_next, BOOL bList = TRUE);

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	std::vector<CString>m_vecAddedLanguages;

	CString m_sSQLContacts;

	CPictureCtrl m_wndPicCtrl;

	BOOL m_bSavePic;
	CString m_sPicFilePath;

	SEARCH_REPLACE_INFO m_info;
protected:
	CContactsFormView();           // protected constructor used by dynamic creation
	virtual ~CContactsFormView();

	CXTResizeGroupBox m_wndGroup1;
	CXTResizeGroupBox m_wndGroup2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl18;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;
	CMyExtStatic m_wndLbl10;
	CMyExtStatic m_wndLbl11;
	CMyExtStatic m_wndLbl12;
	CMyExtStatic m_wndLbl13;
	CMyExtStatic m_wndLbl14;
	CMyExtStatic m_wndLbl15;
	CMyExtStatic m_wndLbl16;
	CMyExtStatic m_wndLbl17;

	CMyExtStatic m_wndLbl19;
	CMyExtStatic m_wndLbl20;
	CMyExtStatic m_wndLbl21;
	CMyExtStatic m_wndLbl22;

	CComboBox m_wndCBox2;
	CComboBox m_wndCBox1;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;
	CMyExtEdit m_wndEdit9;
	CMyExtEdit m_wndEdit10;
	CMyExtEdit m_wndEdit11;
	CMyExtEdit m_wndEdit12;
	CMyExtEdit m_wndEdit13;
	CMyExtEdit m_wndEdit14;
	CMyExtEdit m_wndEdit15;
	CMyExtEdit m_wndEdit16;
	CMyExtEdit m_wndEdit17;
	CMyExtEdit m_wndEdit18;
	CMyExtEdit m_wndEdit19;
	CMyExtEdit m_wndEdit20;
	CMyExtEdit m_wndEdit21;

	CButton	m_wndBtn1;

	CXTButton m_wndAddCompanyBtn;
	CXTButton m_wndPostNumBtn;
	CXTButton m_wndPostAdrBtn;

	CButton	m_wndBtnAddPic;
	CButton	m_wndBtnDelPic;


	BOOL m_bIsAtFirstItem;
	BOOL m_bIsAtLastItem;

	// My methods
	BOOL getEnteredData(void);

	BOOL addContact(void);
	BOOL removeContact(void);


	CStringArray m_sarrTypeOfContact;

	void resetIsDirty(void);
	BOOL getIsDirty(void);

	void clearAll();

	CString setCategoriesPerContactStr(void);

	void setEnableData(BOOL);
	
	void setSearchToolbarBtn(BOOL enable);

public:
	//Feature #2323 20111005 J�
	int getActiveContactId();
	// Need to be PUBLIC
	void isDataChanged(void);

	//#5010 PH 20160614
	void newOwnerToProperty(int propertyId);
	int getPropId();

	BOOL saveContact(BOOL action, BOOL save_categories = FALSE);

	void setPostNumberAndPostAddress(CTransaction_postnumber);

	BOOL getContactsForObject(int);
	void doSetNavigationBar(void);
	BOOL doPopulateNext(int *id);
	BOOL doPopulatePrev(int *id);
	void doPopulate(int);
	void doPopulate(int,vecTransactionContacts *);
	bool doPopulateOnContactID(int);
	void doRePopulate(void);

	vecTransactionContacts * getTransactionContacts() { return m_vecContactsData; }

	void refreshContacts(void);

	int getDBIndex(void)	{	return m_nDBIndex;	}

	BOOL doRePopulateFromSearch(LPCTSTR sql,int *index);

	void setCompany(LPCTSTR company_name)	{ m_wndEdit3.SetWindowText(company_name); }

	inline BOOL isAtFirstItem(void)	{ return m_bIsAtFirstItem; }
	inline BOOL isAtLastItem(void)	{ return m_bIsAtLastItem; }


	inline SEARCH_REPLACE_INFO& getSEARCH_REPLACE_INFO()		{ return m_info; }

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	enum { IDD = IDD_FORMVIEW3 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnDestroy();

	afx_msg void OnImport();
	afx_msg void OnSearchReplace();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedBtnAddPic();
	afx_msg void OnBnClickedBtnDelPic();
	afx_msg void OnEnSetfocusEdit11();
	afx_msg void OnEnSetfocusEdit12();
	afx_msg void OnEnSetfocusEdit13();
	afx_msg void OnEnSetfocusEdit14();
	afx_msg void OnEnSetfocusEdit15();
	afx_msg void OnEnSetfocusEdit16();
	afx_msg void OnEnSetfocusEdit17();
	afx_msg void OnEnSetfocusEdit19();
	afx_msg void OnEnSetfocusEdit115();
	afx_msg void OnEnSetfocusEdit116();
	afx_msg void OnEnSetfocusEdit110();
	afx_msg void OnEnSetfocusEdit111();
	afx_msg void OnEnSetfocusEdit112();
	afx_msg void OnEnSetfocusEdit113();
	afx_msg void OnEnSetfocusEdit114();
	afx_msg void OnEnSetfocusEdit117();
	afx_msg void OnEnSetfocusEdit118();
	afx_msg void OnEnSetfocusEdit119();
	afx_msg void OnEnSetfocusEdit120();
	afx_msg void OnEnSetfocusEdit121();
	afx_msg void OnCbSetfocusComobox11();
	afx_msg void OnCbSetfocusComobox12();

	afx_msg void OnCbnSelchangeCombo12();
	afx_msg void OnEnSetfocusEdit18();
protected:
	CMyExtEdit m_wndEdit8;
};


