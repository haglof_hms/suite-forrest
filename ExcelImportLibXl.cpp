#include "stdafx.h"
#include "Forrest.h"
#include "ExcelImportLibXl.h"
#include "libxl.h"

using namespace libxl;

#define EXCEL 1
#define CSV 2

// "normal" constructor
CExcelImportLibXl::CExcelImportLibXl()
{
	m_Book = NULL;
}

// Open spreadsheet for reading and writing
CExcelImportLibXl::CExcelImportLibXl(CString File, CString SheetOrSeparator, bool Backup, bool Convert_Numbers)
{
}

int CExcelImportLibXl::Open(CString csFile, CString csSheetOrSeparator, bool bBackup, bool bConvert_Numbers)
{
	m_Book = NULL;

	if(csFile.Right(4) == _T(".xls") || csFile.Right(5) == _T(".xlsx"))
	{
		m_nFiletype = EXCEL;
		return Open(csFile);
	}
	else // if file is a text delimited file
	{
		m_sSeparator = csSheetOrSeparator;
		m_nFiletype = CSV;
		try
		{
			readUNICODESelectedFile(CStdioFileEx::GetCurrentLocaleCodePage(), csFile, m_aRows);

			if (m_aRows.GetCount() > 0)
			{
				// Read and store all rows in memory
				ReadRow(m_aFieldNames, 0); // Get field names i.e header row
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		catch(...)
		{
			return FALSE;
		}
	}

	return TRUE;
}

int CExcelImportLibXl::Open(CString csFile)
{
	if(csFile.Right(4) == _T(".xls") || csFile.Right(5) == _T(".xlsx"))
	{
		m_nFiletype = EXCEL;
		m_nSheetIndex = 0;	// use first sheet as default

		if(csFile.Right(4) == _T(".xls"))
			m_Book = xlCreateBook();
		else if(csFile.Right(5) == _T(".xlsx"))
			m_Book = xlCreateXMLBook();

		if(m_Book)
		{
			if( m_Book->load(csFile) )
			{
				//m_Book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
				//m_Book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
				m_Book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");
				return TRUE;
			}
			else	// unable to open Excel-file
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}

	return FALSE;
}

// Perform some cleanup functions
CExcelImportLibXl::~CExcelImportLibXl()
{
	if(m_Book)
		m_Book->release();
}

long CExcelImportLibXl::GetTotalRows()
{
	if(m_nFiletype == EXCEL)
	{
		if(!m_Book) return 0;

		Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
		if(sheet)
		{
			return sheet->lastRow();
		}
	}
	else if(m_nFiletype == CSV)
	{
		// Get total number of rows
		return m_aRows.GetSize();
	}

	return 0;
}

// get the top row
void CExcelImportLibXl::GetFieldNames(CStringArray &FieldNames)
{
	if(m_nFiletype == EXCEL)
	{
		CString csBuf;
		int row = 0;

		FieldNames.RemoveAll();
		if(!m_Book) return;
		Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
		if(sheet)
		{
			row = sheet->firstRow();
			for(int col = sheet->firstCol(); col < sheet->lastCol(); ++col)
			{
				CellType cellType = sheet->cellType(row, col);
				if(cellType == CELLTYPE_STRING)
				{
					csBuf = sheet->readStr(row, col);
					FieldNames.Add(csBuf);
				}
				else if(cellType == CELLTYPE_EMPTY) { //#5063 PH 20160708, �ven tomma kolumner m�ste tas med s� index blir r�tt.
					FieldNames.Add(L"");
				}
			}
		}
	}
	else if(m_nFiletype == CSV)
	{
		FieldNames.Copy(m_aFieldNames);
	}
}

bool CExcelImportLibXl::ReadRow(CStringArray &RowValues, long row) // Read a row from spreadsheet. Default is read the next row
{
	if(m_nFiletype == EXCEL)
	{
		CString csBuf;

		RowValues.RemoveAll();
		if(!m_Book) return false;
		Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
		if(sheet)
		{
			for(int col = sheet->firstCol(); col < sheet->lastCol(); ++col)
			{
				CellType cellType = sheet->cellType(row, col);
				if(cellType == CELLTYPE_STRING)
				{
					csBuf = sheet->readStr(row, col);
					RowValues.Add(csBuf);
				}
				else if(cellType == CELLTYPE_NUMBER)
				{
					csBuf.Format(_T("%g"), sheet->readNum(row, col));
					RowValues.Add(csBuf);
				}
				else
				{
					RowValues.Add(_T(""));
				}
			}
		}
	}
	else if(m_nFiletype == CSV)
	{
		row++;

		CString S;
		// Check if row entered is more than number of rows in sheet
		if (row <= m_aRows.GetSize())
		{
			if (row != 0)
			{
				m_dCurrentRow = row;
			}
			else if (m_dCurrentRow > m_aRows.GetSize())
			{
				return false;
			}
			// Read the desired row
			RowValues.RemoveAll();
			CString csTmp;
			csTmp = m_aRows.GetAt(m_dCurrentRow-1);
			m_dCurrentRow++;

			// Search for separator to split row
			int separatorPosition;
			CString csTmpSql;
			csTmpSql.Format(_T("\"%s\""), m_sSeparator);
			separatorPosition = csTmp.Find(csTmpSql); // If separator is "?"
			if (separatorPosition != -1)
			{
				// Save columns
				int nCount = 0;
				int stringStartingPosition = 0;
				while (separatorPosition != -1)
				{
					nCount = separatorPosition - stringStartingPosition;
					RowValues.Add(csTmp.Mid(stringStartingPosition, nCount));
					stringStartingPosition = separatorPosition + csTmpSql.GetLength();
					separatorPosition = csTmp.Find(csTmpSql, stringStartingPosition);
				}
				nCount = csTmp.GetLength() - stringStartingPosition;
				RowValues.Add(csTmp.Mid(stringStartingPosition, nCount));
				// Remove quotes from first column
				csTmp = RowValues.GetAt(0);
				csTmp.Delete(0, 1);
				RowValues.SetAt(0, csTmp);

				// Remove quotes from last column
				csTmp = RowValues.GetAt(RowValues.GetSize()-1);
				csTmp.Delete(csTmp.GetLength()-1, 1);
				RowValues.SetAt(RowValues.GetSize()-1, csTmp);
				return true;
			}
			else
			{
				// Save columns
				separatorPosition = csTmp.Find(m_sSeparator); // if separator is ?
				if (separatorPosition != -1)
				{
					int nCount = 0;
					int stringStartingPosition = 0;
					while (separatorPosition != -1)
					{
						nCount = separatorPosition - stringStartingPosition;
						RowValues.Add(csTmp.Mid(stringStartingPosition, nCount));
						stringStartingPosition = separatorPosition + m_sSeparator.GetLength();
						separatorPosition = csTmp.Find(m_sSeparator, stringStartingPosition);
					}
					nCount = csTmp.GetLength() - stringStartingPosition;
					RowValues.Add(csTmp.Mid(stringStartingPosition, nCount));
					return true;
				}
				else	// Treat spreadsheet as having one column
				{
					// Remove opening and ending quotes if any
					int quoteBegPos = csTmp.Find('\"');
					int quoteEndPos = csTmp.ReverseFind('\"');
					if ((quoteBegPos == 0) && (quoteEndPos == csTmp.GetLength()-1))
					{
						csTmp.Delete(0, 1);
						csTmp.Delete(csTmp.GetLength()-1, 1);
					}

					RowValues.Add(csTmp);
				}
			}
		}
//		m_sLastError = "Desired row is greater than total number of rows in spreadsheet\n";
		return false;
	}

	return true;
}

int CExcelImportLibXl::GetSheetCount()
{
	if(!m_Book) return 0;

	return m_Book->sheetCount();
}

CString CExcelImportLibXl::GetSheetName(int nIndex)
{
	if(!m_Book) return _T("");

	CString csSheetName;
	
	Sheet* sheet = m_Book->getSheet(nIndex);
	if(sheet)
	{
		return sheet->name();
	}

	return csSheetName;
}

int CExcelImportLibXl::SetCurrentSheet(int nIndex)
{
	if(!m_Book) return FALSE;
	
	if( nIndex <= m_Book->sheetCount())
	{
		m_nSheetIndex = nIndex;
		return TRUE;
	}

	return FALSE;
}

int CExcelImportLibXl::GetColCount()
{
	if(!m_Book) return 0;

	Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
	if(sheet)
	{
		return sheet->lastCol();
	}

	return 0;
}

CString CExcelImportLibXl::GetCellValue(int nRow, int nCol)
{
	CString csBuf = _T("");

	if(!m_Book) return false;
	Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
	if(sheet)
	{
		CellType cellType = sheet->cellType(nRow, nCol);
		if(cellType == CELLTYPE_STRING)
		{
			csBuf = sheet->readStr(nRow, nCol);
		}
		else if(cellType == CELLTYPE_NUMBER)
		{
			csBuf.Format(_T("%g"), sheet->readNum(nRow, nCol));
		}
		else
		{
			csBuf = _T("");
		}
	}

	return csBuf;
}

//////////////////////////////////////////////////////////////////////////////////////
// CExcelibXl_create_template()

CExcelibXl_create_template::CExcelibXl_create_template()
	:	m_book(NULL),m_sheet(NULL)
{
}
CExcelibXl_create_template::CExcelibXl_create_template(CString file_name)
	:	m_book(NULL),m_sheet(NULL),m_sFileName(file_name)
{
}


CExcelibXl_create_template::~CExcelibXl_create_template()
{
}


void CExcelibXl_create_template::createTemplate(HWND hWnd,CString msg_cap,CString msg)
{
	CString sPath = L"";
	if (m_book = xlCreateBook())
	{
		//m_book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
		//m_book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0qfkfob");
		m_book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0pfnfkb");

		if (m_sheet = m_book->addSheet(L"Blad1"))
		{
			Format* fmtTmp;
			fmtTmp = m_book->addFormat();
			fmtTmp->setNumFormat(NUMFORMAT_TEXT);
			m_sheet->setCol(0, 26, 10, fmtTmp);

			libxl::Font *fnt = m_book->addFont();
			//fnt->setSize(10);
			fnt->setBold(true);

			Format* fmt;
			fmt = m_book->addFormat();
			fmt->setFont(fnt);

			m_sheet->writeStr(0,0,COLUMN_NAMES[0],fmt);
			m_sheet->writeStr(0,1,COLUMN_NAMES[1],fmt);
			m_sheet->writeStr(0,2,COLUMN_NAMES[2],fmt);
			m_sheet->writeStr(0,3,COLUMN_NAMES[3],fmt);
			m_sheet->writeStr(0,4,COLUMN_NAMES[4],fmt);
			m_sheet->writeStr(0,5,COLUMN_NAMES[5],fmt);
			m_sheet->writeStr(0,6,COLUMN_NAMES[6],fmt);
			m_sheet->writeStr(0,7,COLUMN_NAMES[7],fmt);
			m_sheet->writeStr(0,8,COLUMN_NAMES[8],fmt);
			m_sheet->writeStr(0,9,COLUMN_NAMES[9],fmt);
			m_sheet->writeStr(0,10,COLUMN_NAMES[10],fmt);
			m_sheet->writeStr(0,11,COLUMN_NAMES[11],fmt);
			m_sheet->writeStr(0,12,COLUMN_NAMES[12],fmt);
			m_sheet->writeStr(0,13,COLUMN_NAMES[13],fmt);
			m_sheet->writeStr(0,14,COLUMN_NAMES[14],fmt);
			m_sheet->writeStr(0,15,COLUMN_NAMES[15],fmt);
			m_sheet->writeStr(0,16,COLUMN_NAMES[16],fmt);
			m_sheet->writeStr(0,17,COLUMN_NAMES[17],fmt);
			m_sheet->writeStr(0,18,COLUMN_NAMES[18],fmt);
			m_sheet->writeStr(0,19,COLUMN_NAMES[19],fmt);
			m_sheet->writeStr(0,20,COLUMN_NAMES[20],fmt);
			m_sheet->writeStr(0,21,COLUMN_NAMES[21],fmt);
			m_sheet->writeStr(0,22,COLUMN_NAMES[22],fmt);
			m_sheet->writeStr(0,23,COLUMN_NAMES[23],fmt);
			m_sheet->writeStr(0,24,COLUMN_NAMES[24],fmt);
			m_sheet->writeStr(0,25,COLUMN_NAMES[25],fmt);
			m_sheet->writeStr(0,26,COLUMN_NAMES[26],fmt);
		
			sPath = regGetStr(REG_ROOT,EXCEL_IMPORT_TMPL_DIR,DIRECTORY_KEY);
			sPath += m_sFileName;

			CFileDialog dlg(FALSE,L".xls",sPath,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,L"EXCEL *.xls|*.xls|");
			if (dlg.DoModal() == IDOK)
			{
				if(m_book->save(dlg.GetPathName())) 
				{
					if (!msg.IsEmpty())
					{
						if (::MessageBox(hWnd,msg,msg_cap,MB_ICONQUESTION | MB_YESNO) == IDYES)
							::ShellExecute(NULL, L"open", dlg.GetPathName(), NULL, NULL, SW_SHOW);        
					}
					else
					{
						::ShellExecute(NULL, L"open", dlg.GetPathName(), NULL, NULL, SW_SHOW);        
					}
				}
			}
			regSetStr(REG_ROOT,EXCEL_IMPORT_TMPL_DIR,DIRECTORY_KEY,getFilePath(dlg.GetPathName()));

		}	// if (m_sheet = m_book->addSheet(L"Blad1"))
	
		m_book->release();

	}	// if (m_book = xlCreateBook())
}
