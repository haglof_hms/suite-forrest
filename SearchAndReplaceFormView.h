#pragma once


#include "Resource.h"

// CSearchAndReplaceFormView form view

class CSearchAndReplaceFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSearchAndReplaceFormView)

	CXTResizeGroupBox m_wndGroup1;
	CXTResizeGroupBox m_wndGroup2;

	CString m_sMsgCap;
	CString m_sMsgDoReplace;

	CString m_sNumOfItems;

	BOOL m_bInitialized;
	CString m_sLangFN;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;

	CButton m_wndSearchToFirstBtn;
	CButton m_wndSearchNextBtn;
	CButton m_wndSearchPrevBtn;
	CButton m_wndReplaceBtn;

	CButton m_wndRadioWholeWord;
	CButton m_wndRadioEndOfWord;
	CButton m_wndRadioAnyPartOfWord;

	SEARCH_REPLACE_INFO m_Info;

	CString getColName(FOCUSED_EDIT fed);

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CString m_sSearchText;
	CString m_sSQLSearchString;

	CString m_sNoResultInSearch;

	int m_nNumOfItemsInSearch;
protected:
	CSearchAndReplaceFormView();           // protected constructor used by dynamic creation
	virtual ~CSearchAndReplaceFormView();

	int getChecked(void);
	void setSQLSearchString(ORIGIN_FOR_SEARCH origin);
public:
	enum { IDD = IDD_FORMVIEW10 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	SEARCH_REPLACE_INFO& getSEARCH_REPLACE_INFO(void)		{ AfxMessageBox(m_Info.m_sText); return m_Info; }
	
protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSearchAndReplaceFormView)
	afx_msg	void OnClose();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton101();
	afx_msg void OnBnClickedButton104();
	afx_msg void OnBnClickedButton105();
	afx_msg void OnBnClickedButton102();
};


