#pragma once

#include "Resource.h"


class CNoPropStandsReportRec : public CXTPReportRecord
{
	int m_nTraktID;
protected:
	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	class CFloatItem : public CXTPReportRecordItemNumber
	{
	//private:
		double m_fValue;
	public:
		CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
				CXTPReportRecordItemNumber(fValue)
		{
			SetFormatString(fmt_str);
			m_fValue = fValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* pItemArgs, LPCTSTR szText)
		{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);

			CXTPReportControl *pCtrl = NULL;
			if (pItemArgs != NULL)
			{
				pCtrl = pItemArgs->pControl;
				if (pCtrl != NULL)
				{
					pCtrl->GetParent()->SendMessage(WM_COMMAND,pCtrl->GetDlgCtrlID(),0);
				}
			}
		}

		void setFloatItem(double value)	
		{ 
			m_fValue = value; 
			SetValue(value);
		}
		double getFloatItem(void)	{ return m_fValue; }
	};


public:

	CNoPropStandsReportRec(void)
	{
		m_nTraktID = -1;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz2dec));
	}

	CNoPropStandsReportRec(int trakt_id,LPCTSTR stand_number,LPCTSTR stand_name,double volume,double gy)
	{
		m_nTraktID = trakt_id;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(stand_number));
		AddItem(new CTextItem(stand_name));
		AddItem(new CFloatItem(volume,sz1dec));
		AddItem(new CFloatItem(gy,sz2dec));
	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

	int getTraktID(void)		{ return m_nTraktID; };

	BOOL getColumnCheck(int item)	{	return ((CCheckItem*)GetItem(item))->getChecked(); }

};


//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CContactsSelectListFrame; 070108 p�d

class CStandsReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CStandsReportFilterEditControl)
public:
	CStandsReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

//////////////////////////////////////////////////////////////////////////////
// CStandSelListFormView form view

class CStandSelListFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CStandSelListFormView)

protected:
	CStandSelListFormView();           // protected constructor used by dynamic creation
	virtual ~CStandSelListFormView();

	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	CString m_sFilterOn;

	int m_nSelectedColumn;

	vecTransactionTrakt m_vecStandsNoProperty;
	void getStandsNoProperty(void);
	void getStandData(int trakt_id,double *m3sk,double *gy);
	BOOL setupReport(void);
	
	CXTPReportSubListControl m_wndSubList;
	CStandsReportFilterEditControl m_wndFilterEdit;
	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	void LoadReportState(void);
	void SaveReportState(void);

	int m_nDBIndex;

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CImageList m_ilIcons;

public:
	// Need to set this method public; 070126 p�d
	void populateReport(void);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

	void setFilterWindow(void);
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
	afx_msg void OnAddStandToProperty(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


