#if !defined(__PROPERTYTABVIEW_H__)
#define __PROPERTYTABVIEW_H__

#include "PropertyFormView.h"
#include "MDIPropOwnerFormView.h"
#include "StandsInPropertyFormView.h"

#include "MDIBaseFrame.h"

///////////////////////////////////////////////////////////////
// CPropertyTabView 
class CPropertyTabView : public CView
{
	DECLARE_DYNCREATE(CPropertyTabView)

//private:
	BOOL m_bInitialized;
	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sMsg;

	CFont m_font;
public:
	CPropertyTabView();           // protected constructor used by dynamic creation
	virtual ~CPropertyTabView();

	CPropertyFormView *getPropertyFormView(void);
	CMDIPropOwnerFormView *getPropOwnerFormView(void);
	CStandsInPropertyFormView *getPropStandsFormView(void);


	CMyTabControl *getTabControl(void)
	{
		return &m_wndTabControl;
	}

	void setMsg(LPCTSTR msg)
	{
		m_sMsg = msg;
		Invalidate();
	}

	void enablePage(int idx,BOOL enable)
	{
		if (idx < m_wndTabControl.getNumOfTabPages())
		{
			m_tabManager = m_wndTabControl.getTabPage(idx);
			if (m_tabManager)
			{
				m_tabManager->SetEnabled(enable);
			}	// if (m_tabManager)
		}	// if (idx < m_wndTabControl.getNumOfTabPages())
	}

	void activePage(int idx)
	{
		if (idx < m_wndTabControl.getNumOfTabPages())
		{
			m_tabManager = m_wndTabControl.getTabPage(idx);
			if (m_tabManager)
			{
				m_tabManager->Select();
			}	// if (m_tabManager)
		}	// if (idx < m_wndTabControl.getNumOfTabPages())
	}

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabbedViewView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate( );
	protected:
	//}}AFX_VIRTUAL
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle,int tab_id, int nIcon);

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	CMDIFrameDoc* GetDocument();

	void setLanguage(void);
protected:

	//{{AFX_MSG(CPropertyTabView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy();
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

#ifndef _DEBUG  // debug version in TabbedViewView.cpp
inline CMDIFrameDoc* CPropertyTabView::GetDocument()
	{ return (CMDIFrameDoc*)m_pDocument; }
#endif


#endif
