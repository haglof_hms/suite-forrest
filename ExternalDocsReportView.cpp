// ImportDataFormView.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "ExternalDocsReportView.h"

#include "ResLangFileReader.h"

#include "Resource.h"


/////////////////////////////////////////////////////////////////////////////
// CExternalDocsReportView

IMPLEMENT_DYNCREATE(CExternalDocsReportView,  CMyReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CExternalDocsReportView,  CMyReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)

	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, XTP_ID_REPORT_CONTROL, OnReportSelChanged)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportItemValueClick)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)

	ON_COMMAND(ID_TBBTN_OPEN,OnAddRow)
	ON_COMMAND(ID_TBBTN_CREATE,OnRemoveRow)
END_MESSAGE_MAP()

CExternalDocsReportView::CExternalDocsReportView()
	: CMyReportView()
{
	m_pDB = NULL;
	m_bInitialized = FALSE;
	m_enumDocsAction = DOCS_NO_ACTION;
}

CExternalDocsReportView::~CExternalDocsReportView()
{
}

void CExternalDocsReportView::OnInitialUpdate()
{
	CMyReportView::OnInitialUpdate();

	if (!m_bInitialized)
	{
		// Setup language filename; 051214 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	
		setupReport();

		LoadReportState();

		m_bInitialized = TRUE;
	}
}

BOOL CExternalDocsReportView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CExternalDocsReportView::OnDestroy()
{
	SaveReportState();

	if (m_pDB != NULL)
		delete m_pDB;

	m_ilIcons.DeleteImageList();

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
				(LPARAM)&_doc_identifer_msg(sTo,sFrom,_T(""),123,0,0));

	CXTPReportView::OnDestroy();	
}

BOOL CExternalDocsReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CMyReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CExternalDocsReportView diagnostics

#ifdef _DEBUG
void CExternalDocsReportView::AssertValid() const
{
	CMyReportView::AssertValid();
}

void CExternalDocsReportView::Dump(CDumpContext& dc) const
{
	CMyReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CExternalDocsReportView message handlers

// CExternalDocsReportView message handlers
void CExternalDocsReportView::OnSize(UINT nType,int cx,int cy)
{
	CMyReportView::OnSize(nType,cx,cy);
}

void CExternalDocsReportView::OnSetFocus(CWnd*)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
/*
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
*/
}

// Create and add Assortment settings reportwindow
BOOL CExternalDocsReportView::setupReport(void)
{

	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
				// Get text from languagefile; 080710 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{

					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP4));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					GetReportCtrl().SetImageList(&m_ilIcons);

					m_sFilterText = (xml.str(IDS_STRING316));

					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( TRUE );
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );
					GetReportCtrl().ShowWindow( SW_NORMAL );
					// "ICON"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING2930)), 20,FALSE,5));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;

					// "URL"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING2930)), 200));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->AddExpandButton();

					// "Notering"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING2931)), 150));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

					// "Inl�st av"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING2932)), 50));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

					//	"Datum"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml.str(IDS_STRING2933)), 100));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;

					GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
					GetReportCtrl().SetMultipleSelection( TRUE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().FocusSubItems(TRUE);
					GetReportCtrl().AllowEdit(TRUE);
					GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml.Load(m_sLangFN))
			xml.clean();
		}	// if (fileExists(m_sLangFN))

	return TRUE;
}


void CExternalDocsReportView::OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
}

void CExternalDocsReportView::OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
	}	// if (pItemNotify != NULL)
}

void CExternalDocsReportView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	CURLReportDataRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
		{
			// Do a hit-test; 080513 p�d
			rect = pItemNotify->pColumn->GetRect();		
			pt = pItemNotify->pt;
			// Check if the user clicked on the Icon or not; 080513 p�d
			if (hitTest_X(pt.x,rect.left,13))
			{
				pRec = (CURLReportDataRec*)pItemNotify->pRow->GetRecord();
				viewExtrenalDocument(pRec->getColumnText(COLUMN_1));
			}	// if (hitTest_X(pt.x,rect.left,13))
		}	// if (pItemNotify->pColumn->GetItemIndex() == COLUMN_0)
	}	// if (pItemNotify != NULL)
}

void CExternalDocsReportView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (lpNMKey->nVKey != VK_F8) return;
}

LRESULT CExternalDocsReportView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	CString S;
	switch (wParam)
	{
		case ID_WPARAM_VALUE_FROM + 0x02 :
		{
			m_msg = (_doc_identifer_msg*)lParam;
			if (sizeof(*m_msg) == sizeof(_doc_identifer_msg))
			{
				if (m_msg->getValue3() == 0)	// Handle Object
				{
					m_enumDocsAction = DOCS_IN_OBJECT;
					m_sTableName_pk = (TBL_TRAKT);
					m_nLinkID_pk = m_msg->getValue1();	// Object ID
					getExternalDocs();
					populateReport();
				}
				else if (m_msg->getValue3() == 1)	// Handle Property
				{
					m_enumDocsAction = DOCS_IN_PROPERTY;
					m_sTableName_pk = (TBL_ELV_OBJECT);
					sFrom = m_msg->getSendFrom();
					sTo = m_msg->getSendTo();
					m_nLinkID_pk = m_msg->getValue1();	// Property ID
					getExternalDocs();
					populateReport();
				}
			}	// if (sizeof(*msg) == sizeof(_doc_identifer_msg))
		}
		break;

		case ID_SAVE_ITEM :
			saveExternalDocs();
		break;
	};
	return 0L;
}


void CExternalDocsReportView::OnAddRow(void)
{
	// Add a row (record) to the ReportView; 080925 p�d
	GetReportCtrl().AddRecord(new CURLReportDataRec(m_sFilterText));
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CExternalDocsReportView::OnRemoveRow(void)
{
	CTransaction_external_docs rec;
	CURLReportDataRec *pRec = NULL;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	if (pRow != NULL && pRows != NULL)
	{
		pRec = (CURLReportDataRec*)pRow->GetRecord();
		pRow->GetRecord()->Delete();
		pRows->RemoveAt(pRow->GetIndex());
		saveExternalDocs();
		getExternalDocs();
		populateReport();
	}	// if (pRow != NULL && m_pDB != NULL)
}

void CExternalDocsReportView::getExternalDocs(void)
{
	if (m_pDB != NULL)
	{
		m_pDB->getExternalDocuments(m_sTableName_pk,m_nLinkID_pk,m_vecExternalDocs);
	}
}

void CExternalDocsReportView::saveExternalDocs(void)
{
	CURLReportDataRec *pRec = NULL;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CXTPReportRow *pRow = NULL;
	if (pRows == NULL) return;

	// On save, we first delete documents for THIS; 080925 p�d
	if (m_pDB != NULL)
	{
		m_pDB->delExternalDocuments(m_sTableName_pk,m_nLinkID_pk);
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRow = pRows->GetAt(i);
			if (pRow != NULL)
			{
				pRec = (CURLReportDataRec*)pRow->GetRecord();
				if (pRec != NULL)
				{
					m_recExternalDocs = CTransaction_external_docs(-1,m_sTableName_pk,m_nLinkID_pk,1,
																												 pRec->getColumnText(COLUMN_1),
																												 pRec->getColumnText(COLUMN_2),
																												 pRec->getColumnText(COLUMN_3),
																												 pRec->getColumnText(COLUMN_4));
					// Check to see if there's something to save; 080930 p�d
					if (!m_recExternalDocs.getExtDocURL().IsEmpty())
						m_pDB->addExternalDocuments(m_recExternalDocs);		

				}	// if (pRec != NULL)
			}	// if (pRow != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (m_pDB != NULL)
}

void CExternalDocsReportView::populateReport(void)
{
	GetReportCtrl().ResetContent();
	if (m_vecExternalDocs.size() > 0)
	{
		for (UINT i = 0;i < m_vecExternalDocs.size();i++)
		{
			m_recExternalDocs = m_vecExternalDocs[i];
			GetReportCtrl().AddRecord(new CURLReportDataRec(i,m_sFilterText,m_recExternalDocs));
		}	// for (UINT i = 0;i < m_vecExternalDocs.size();i++)

	}	// if (m_vecExternalDocs.size() > 0)
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CExternalDocsReportView::viewExtrenalDocument(LPCTSTR fn)
{
	// Try to run it, no matter what!; 080929 p�d
//	if (fileExists(fn))
//	{
		::ShellExecute( NULL, _T( "open" ), fn, _T(""), _T(""), SW_SHOW);
//	}	// if (fileExists(fn))
}


void CExternalDocsReportView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_EXTERNAL_DOECS_REPORT_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CExternalDocsReportView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary((REG_WP_EXTERNAL_DOECS_REPORT_KEY), _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}
