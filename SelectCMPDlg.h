#pragma once

#include "Resource.h"

// CSelectCMPDlg dialog

class CSelectCMPDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectCMPDlg)

	CString m_sLangFN;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;

	CListBox m_wndLB1;
	CListBox m_wndLB2;
	CListBox m_wndLB3;
	
	CButton m_wndBtnOK;

	CString sSelectedCounty;
	CString sSelectedMunicipal;
	CString sSelectedParish;

	CStringArray m_sarrData;

	CForrestDB *m_pDB;
	void populateListBox(void);
public:
	CSelectCMPDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectCMPDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

	void setData(CStringArray &arr)
	{
		m_sarrData.Append(arr);
	}

	CString getSelectedCounty(void)
	{
		return sSelectedCounty;
	}

	CString getSelectedMunicipal(void)
	{
		return sSelectedMunicipal;
	}

	CString getSelectedParish(void)
	{
		return sSelectedParish;
	}

	void setDBConnection(CForrestDB *db)
	{
		m_pDB = db;
	}


protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectCMPDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLbnSelchangeList1();
	afx_msg void OnLbnSelchangeList3();
	afx_msg void OnLbnSelchangeList4();
};
