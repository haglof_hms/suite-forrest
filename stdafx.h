// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT


#ifndef _AFX_NO_DAO_SUPPORT
//#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


#include <urlmon.h>

#include <vector>
#include <map>

using namespace std;
// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#include <SQLAPI.h> // main SQLAPI++ header

// Xtreeme toolkit
#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#define _XTLIB_NOAUTOLINK
#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

#include "pad_hms_miscfunc.h"					// HMSFuncLib header
#include "pad_transaction_classes.h"	// HMSFuncLib header
#include "pad_excel_import.h"					// HMSFuncLib header

#include "DBBaseClass_SQLApi.h"	// ... in DBTransaction_lib
#include "DBBaseClass_ADODirect.h"	// ... in DBTransaction_lib

#include "ForrestDB.h"
#include <afxext.h>


extern HINSTANCE m_hInstance; // Instance handle for this DLL

extern bool global_IS_SEARCH_REPLACE_OPEN;

#define MSG_IN_SUITE				 				(WM_USER + 10)		// This identifer's used to send messages internally

#define IDC_SPECIES_REPORT					0x9001
#define IDC_CATEGORY_REPORT					0x9002
#define IDC_CONTACTS_REPORT					0x9003
#define IDC_TRAKT_TYPE_REPORT				0x9004
#define IDC_PROP_OWNER_REPORT				0x9005
#define IDC_PROP_STANDS_REPORT			0x9006

#define CMDID_SPLASH_INFO						0x9013					// OnCommand

#define ID_TBBTN_COLUMNS						0x9014

#define ID_SHOWVIEW_MSG							0x9015

#define ID_REPORT_REMOVE_PROPS			0x9016

#define ID_REPORT_CHANGE_OBJID			0x9017

#define ID_INDICATOR_PROG						0x9018

#define ID_TOOLS_ADD_CHANGE_OBJID		0x9019
#define ID_TOOLS_CREATE_EXCEL_TMPL	0x9020

#define ID_SHOWVIEW_OBJECT_CONTACTS	0x9021

//////////////////////////////////////////////////////////////////////////////
// Strings in languagefile; 060208 p�d
#define IDS_STRING118							118
#define IDS_STRING200							200
#define IDS_STRING201							201
#define IDS_STRING202							202
#define IDS_STRING2021							2021
#define IDS_STRING203							203
#define IDS_STRING204							204
#define IDS_STRING205							205
#define IDS_STRING206							206
#define IDS_STRING2061						2061
#define IDS_STRING207							207
#define IDS_STRING208							208
#define IDS_STRING209							209
#define IDS_STRING210							210
#define IDS_STRING211							211
#define IDS_STRING212							212
#define IDS_STRING213							213
#define IDS_STRING214							214
#define IDS_STRING215							215
#define IDS_STRING2151						2151
#define IDS_STRING216							216
#define IDS_STRING217							217
#define IDS_STRING218							218
#define IDS_STRING219							219
#define IDS_STRING220							220
#define IDS_STRING221							221
#define IDS_STRING222							222
#define IDS_STRING223							223
#define IDS_STRING224							224
#define IDS_STRING323							323
#define IDS_STRING225							225
#define IDS_STRING226							226
#define IDS_STRING227							227
#define IDS_STRING228							228
#define IDS_STRING229							229
#define IDS_STRING2290						2290
#define IDS_STRING2291						291
#define IDS_STRING230							230
#define IDS_STRING231							231
#define IDS_STRING232							232
#define IDS_STRING233							233
#define IDS_STRING234							234
#define IDS_STRING235							235
#define IDS_STRING2350						2350
#define IDS_STRING2351						2351
#define IDS_STRING2352						2352
#define IDS_STRING2353						2353
#define IDS_STRING2354						2354
#define IDS_STRING2355						2355
#define IDS_STRING2356						2356
#define IDS_STRING2357						2357
#define IDS_STRING2360						2360
#define IDS_STRING2361						2361
#define IDS_STRING2370						2370
#define IDS_STRING2371						2371
#define IDS_STRING2372						2372
#define IDS_STRING2373						2373
#define IDS_STRING237							237
#define IDS_STRING238							238
#define IDS_STRING239							239
#define IDS_STRING240							240
#define IDS_STRING241							241
#define IDS_STRING242							242
#define IDS_STRING243							243
#define IDS_STRING2430						2430
#define IDS_STRING244							244
#define IDS_STRING245							245
#define IDS_STRING246							246
#define IDS_STRING247							247
#define IDS_STRING2480						2480
#define IDS_STRING2481						2481
#define IDS_STRING2482						2482
#define IDS_STRING2483						2483
#define IDS_STRING2484						2484
#define IDS_STRING2485						2485
#define IDS_STRING2486						2486
#define IDS_STRING2487						2487
#define IDS_STRING249							249
#define IDS_STRING250							250
#define IDS_STRING251							251
#define IDS_STRING252							252
#define IDS_STRING253							253
#define IDS_STRING2530						2530
#define IDS_STRING254							254
#define IDS_STRING2540						2540
#define IDS_STRING255							255
#define IDS_STRING2550						2550
#define IDS_STRING2560						2560	// Propertynumber added; 010726 p�d
#define IDS_STRING256							256
#define IDS_STRING2561						2561
#define IDS_STRING257							257
#define IDS_STRING258							258
#define IDS_STRING259							259
#define IDS_STRING260							260
#define IDS_STRING2601						2601
#define IDS_STRING261							261
#define IDS_STRING262							262
#define IDS_STRING263							263
#define IDS_STRING264							264
#define IDS_STRING2640						2640
#define IDS_STRING265							265
#define IDS_STRING266							266
#define IDS_STRING324							324
#define IDS_STRING2670						2670
#define IDS_STRING267							267
#define IDS_STRING268							268
#define IDS_STRING269							269
#define IDS_STRING270							270
#define IDS_STRING2700						2700
#define IDS_STRING2701						2701
#define IDS_STRING2702						2702
#define IDS_STRING2703						2703
#define IDS_STRING2704						2704
#define IDS_STRING2705						2705
#define IDS_STRING2706						2706
#define IDS_STRING271							271
#define IDS_STRING272							272
#define IDS_STRING273							273
#define IDS_STRING274							274
#define IDS_STRING275							275
#define IDS_STRING276							276
#define IDS_STRING277							277
#define IDS_STRING2750						2750
#define IDS_STRING2751						2751
#define IDS_STRING2760						2760
#define IDS_STRING2761						2761
#define IDS_STRING2770						2770
#define IDS_STRING2771						2771
#define IDS_STRING278							278
#define IDS_STRING2781						2781
#define IDS_STRING279							279
#define IDS_STRING280							280
#define IDS_STRING2810						2810
#define IDS_STRING2811						2811
#define IDS_STRING290							290

#define IDS_STRING2910						2910
#define IDS_STRING2911						2911
#define IDS_STRING2912						2912
#define IDS_STRING2913						2913
#define IDS_STRING2914						2914
#define IDS_STRING2915						2915
#define IDS_STRING2916						2916
#define IDS_STRING2917						2917

#define IDS_STRING2918						2918
#define IDS_STRING2919						2919

#define IDS_STRING2920						2920
#define IDS_STRING2921						2921
#define IDS_STRING2922						2922
#define IDS_STRING2923						2923
#define IDS_STRING2924						2924

#define IDS_STRING2930						2930
#define IDS_STRING2931						2931
#define IDS_STRING2932						2932
#define IDS_STRING2933						2933
#define IDS_STRING2934						2934

#define IDS_STRING2940						2940
#define IDS_STRING2941						2941
#define IDS_STRING2942						2942
#define IDS_STRING2943						2943	//HMS-28
#define IDS_STRING2944						2944	//HMS-28

#define IDS_STRING3000						3000
#define IDS_STRING3001						3001
#define IDS_STRING3002						3002
#define IDS_STRING3003						3003
#define IDS_STRING3010						3010
#define IDS_STRING3011						3011
#define IDS_STRING3012						3012
#define IDS_STRING3013						3013

#define IDS_STRING3100						3100
#define IDS_STRING3101						3101
#define IDS_STRING3102						3102

#define IDS_STRING3110						3110
#define IDS_STRING3111						3111

#define IDS_STRING3112						3112
#define IDS_STRING3113						3113

#define IDS_STRING300							300
#define IDS_STRING301							301
#define IDS_STRING302							302
#define IDS_STRING303							303
#define IDS_STRING304							304
#define IDS_STRING305							305
#define IDS_STRING306							306
#define IDS_STRING307							307
#define IDS_STRING308							308
#define IDS_STRING309							309
#define IDS_STRING310							310
#define IDS_STRING311							311
#define IDS_STRING312							312
#define IDS_STRING313							313
#define IDS_STRING314							314
#define IDS_STRING315							315
#define IDS_STRING316							316
#define IDS_STRING317							317
#define IDS_STRING318							318
#define IDS_STRING325							325
#define IDS_STRING319							319
#define IDS_STRING320							320
#define IDS_STRING3200						3200
#define IDS_STRING3201						3201
#define IDS_STRING3202						3202
#define IDS_STRING3203						3203
#define IDS_STRING3204						3204
#define IDS_STRING3205						3205
#define IDS_STRING3206						3206
#define IDS_STRING3207						3207
#define IDS_STRING3208						3208
#define IDS_STRING3209						3209
#define IDS_STRING321							321
#define IDS_STRING3210						3210
#define IDS_STRING3211						3211
#define IDS_STRING3212						3212
#define IDS_STRING3213						3213
#define IDS_STRING322							322

#define IDS_STRING3330						3330
#define IDS_STRING3331						3331

#define IDS_STRING3332						3332

#define IDS_STRING3214						3214
#define IDS_STRING3215						3215
#define IDS_STRING3216						3216
#define IDS_STRING3217						3217
#define IDS_STRING3218						3218
#define IDS_STRING3219						3219
#define IDS_STRING3220						3220
#define IDS_STRING3221						3221
#define IDS_STRING3222						3222
#define IDS_STRING3223						3223
#define IDS_STRING3224						3224
#define IDS_STRING3225						3225
#define IDS_STRING3226						3226
#define IDS_STRING3227						3227
#define IDS_STRING3228						3228
#define IDS_STRING3229						3229

#define IDS_STRING3120						3120
#define IDS_STRING3121						3121
#define IDS_STRING3122						3122
#define IDS_STRING3123						3123

#define IDS_STRING3130						3130
#define IDS_STRING3131						3131

#define IDS_STRING3200						3200

#define IDS_STRING3230						3230
#define IDS_STRING3231						3231

#define IDS_STRING3240						3240
#define IDS_STRING3241						3241
#define IDS_STRING3242						3242
#define IDS_STRING3243						3243
#define IDS_STRING3244						3244

#define IDS_STRING3245						3245
#define IDS_STRING3246						3246
#define IDS_STRING3247						3247
#define IDS_STRING3248						3248
#define IDS_STRING3249						3249

#define IDS_STRING3300						3300
#define IDS_STRING3301						3301

#define IDS_STRING3310						3310
#define IDS_STRING3311						3311
#define IDS_STRING3312						3312
#define IDS_STRING3313						3313
#define IDS_STRING3314						3314
#define IDS_STRING3315						3315

#define IDS_STRING3316						3316
#define IDS_STRING3317						3317
#define IDS_STRING3318						3318

#define IDS_STRING3319						3319
#define IDS_STRING3320						3320

#define IDS_STRING3321						3321

#define IDS_STRING401						401
#define IDS_STRING402						402
#define IDS_STRING403						403
#define IDS_STRING340						340
#define IDS_STRING341						341
#define IDS_STRING342						342
#define IDS_STRING343						343

#define IDS_STRING32785						32785
#define IDS_STRING32786						32786
#define IDS_STRING32787						32787

// ID's for popupmenu in ReportControl; 070109 p�d
#define ID_GROUP_BYTHIS							0x9100
#define ID_SHOW_GROUPBOX						0x9101
#define ID_SHOW_FIELDCHOOSER				0x9102
#define ID_TABCONTROL								0x9103
#define ID_TABCONTROL1							0x9104
#define ID_TAB_CLICKED							0x9105

#define ID_TOOLS_CHANGE_OBJID					0x9200
#define ID_TOOLS_ADD_CONTACT					0x9201
#define ID_TOOLS_REMOVE_CONTACT				0x9202
#define ID_TOOLS_ADD_STAND_TO_PROP		0x9203
#define ID_TOOLS_DEL_STAND_FROM_PROP	0x9204


enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3,
	COLUMN_4,
	COLUMN_5,
	COLUMN_6,
	COLUMN_7,
	COLUMN_8,
	COLUMN_9,
	COLUMN_10,
	COLUMN_11,
	COLUMN_12,
	COLUMN_13,
	COLUMN_14,
	COLUMN_15,
	COLUMN_16,
	COLUMN_17,
	COLUMN_18,
	COLUMN_19,
	COLUMN_20,
	COLUMN_21,
	COLUMN_22,
	COLUMN_23,
	COLUMN_24,
	COLUMN_25,
	COLUMN_26
};


const LPCTSTR PROGRAM_NAME			  		= _T("Forrest");					// Name of suite/module, used on setting Language filename; 051214 p�d
const LPCTSTR SPECIES_NAME						= _T("SpeciesFrame");			// Used for Languagefile, registry entries etc.; 051114 p�d
const LPCTSTR ASSORTMENT_NAME					= _T("AssortmentFrame");	// Used for Languagefile, registry entries etc.; 051114 p�d

const LPCTSTR TREEDATA_NAME						= _T("TreeEntryForm");		// Used for Languagefile, registry entries etc.; 051114 p�d
const LPCTSTR ASSORTDATA_NAME					= _T("AssortEntryForm");		// Used for Languagefile, registry entries etc.; 051114 p�d

const LPCTSTR UMDATABASE_NAME					= _T("UMDataBase.dll");		// Dll handling ALL database transactions; 060209 p�d
const LPCTSTR GET_EXPORTED_FUNC_NAME	= _T("getExportedFuncName");


//////////////////////////////////////////////////////////////////////////////////////////
// Subdirectory path(s); 061120 p�d
const LPCTSTR SUBDIR_SETUP_DATA_FILES	= _T("setup");

//////////////////////////////////////////////////////////////////////////////////////////
// Filename for semicolon limited postnumber file; 061128 p�d
const LPCTSTR POSTNUMBER_FN						= _T("Postnumber_table.skv");

//////////////////////////////////////////////////////////////////////////////////////////
// Filename for semicolon limited county,municipal and parish file; 070115 p�d
const LPCTSTR COUNTY_MUNICIPAL_PARISH_FN	= _T("County_Municipal_Parish_table.skv");

//////////////////////////////////////////////////////////////////////////////////////////
// SQL Script files, used in GallBas; 061120 p�d

const LPCTSTR SQL_SERVER_SQRIPT_DIRECTORY			= _T("Microsoft SQL Server");

const LPCTSTR SPECIES_TABLE										= _T("Create_species_table.sql");
const LPCTSTR CATEGORY_AND_CONTACTS_TABLE			= _T("Create_category_and_contacts_table.sql");
const LPCTSTR POSTNUMBER_TABLE								= _T("Create_postnumber_table.sql");
const LPCTSTR PROPERTY_TABLE									= _T("Create_property_table.sql");
const LPCTSTR TRAKT_TYPES_TABLE								= _T("Create_trakt_types_table.sql");
const LPCTSTR COUNTY_MUNICIPAL_PARISH_TABLE		= _T("Create_county_municipal_parish_table.sql");

//////////////////////////////////////////////////////////////////////////////////////////
// Name of Tables in Forrest database; 061002 p�d
const LPCTSTR TBL_P30SPECIES_OBJECT	= _T("elv_object_p30_species_table");
const LPCTSTR TBL_SPECIES			= _T("fst_species_table");
const LPCTSTR TBL_POSTNUMBER		= _T("fst_postnumber_table");
const LPCTSTR TBL_CATEGORY			= _T("fst_category_table");
const LPCTSTR TBL_CONTACTS			= _T("fst_contacts_table");
const LPCTSTR TBL_PROPERTY			= _T("fst_property_table");
const LPCTSTR TBL_PROP_OWNERS		= _T("fst_prop_owner_table");
const LPCTSTR TBL_EXTERNAL_DOCS		= _T("fst_external_doc_table");

// LandValue database; 080116 p�d
const LPCTSTR TBL_ELV_PROP								= _T("elv_properties_table");

// Tablenames used in External documents table; 080925 p�d
const LPCTSTR TBL_TRAKT												= _T("esti_trakt_table");
const LPCTSTR TBL_TRAKT_DATA									= _T("esti_trakt_data_table");
const LPCTSTR TBL_ELV_OBJECT									= _T("elv_object_table");
const LPCTSTR TBL_ELV_PROPERTIES							= _T("elv_properties_table");
const LPCTSTR TBL_ELV_CRUISE									= _T("elv_cruise_table");

// This table keeps track of which categories a user
// selected for a contact; 061221 p�d
const LPCTSTR TBL_CATEGORY_CONTACTS						= _T("fst_categories_for_contacts_table");

// This table holds the Owner (from Contacts) associated with a Property; 070112 p�d
const LPCTSTR TBL_PROP_OWNER_TYPE							= _T("fst_prop_owner_table");

// This table holds information on County(L�n),Municipal(Kommun) and Parish(Socken); 070115 p�d
const LPCTSTR TBL_COUNTY_MUNICIPAL_PARISH			= _T("fst_county_municipal_parish_table");


//////////////////////////////////////////////////////////////////////////////////////////
// Window placement registry key; 060904 p�d

const LPCTSTR REG_WP_SPECIEFRAME_KEY		= _T("Forrest\\SpecieFrame\\Placement");

const LPCTSTR REG_WP_POSTNUMFRAME_KEY	= _T("Forrest\\PostnumFrame\\Placement");	// 061129 p�d

const LPCTSTR REG_WP_CATEGORYFRAME_KEY	= _T("Forrest\\CategoryFrame\\Placement");	// 061129 p�d

const LPCTSTR REG_WP_CONTACTSFRAME_KEY	= _T("Forrest\\ContactsFrame\\Placement");	// 061218 p�d

const LPCTSTR REG_WP_CONTACTS_SELLEIST_FRAME_KEY	= _T("Forrest\\ContactsSelListFrame\\Placement");	// 070104 p�d

const LPCTSTR REG_WP_STANDS_SELLIST_FRAME_KEY	= _T("Forrest\\StandsSelListFrame\\Placement");	// 090811 p�d

const LPCTSTR REG_WP_STANDS_SELLIST_REPORT_KEY	= _T("Forrest\\StandsSelListFrame\\Report");	// 090811 p�d

const LPCTSTR REG_WP_CONTACTS_SELLEIST_REPORT_KEY	= _T("Forrest\\ContactsSelListFrame\\Report");	// 070105 p�d

const LPCTSTR REG_WP_CONTACTS_POSTNR_REPORT_KEY	= _T("Forrest\\ContactsPostNrFrame\\Report");	// 070105 p�d

const LPCTSTR REG_WP_CMP_FRAME_KEY		= _T("Forrest\\CMPFrame\\Placement");	// 070115 p�d

const LPCTSTR REG_WP_PROPERTYFRAME_KEY	= _T("Forrest\\PropertyFrame\\Placement");	// 070116 p�d

const LPCTSTR REG_WP_PROPERTY_REPORT_KEY	= _T("Forrest\\PropertyListFrame\\Report");	// 070116 p�d

const LPCTSTR REG_WP_PROPOWNERS_REPORT_KEY	= _T("Forrest\\PropOwnersListFrame\\Report");	// 070118 p�d

const LPCTSTR REG_WP_PROPSTANDS_REPORT_KEY	= _T("Forrest\\PropStandsListFrame\\Report");	// 090810 p�d

const LPCTSTR REG_WP_PROPERTY_SELLEIST_FRAME_KEY	= _T("Forrest\\PropertySelListFrame\\Placement");	// 070117 p�d

const LPCTSTR REG_WP_PROPERTY_SELLEIST_REPORT_KEY	= _T("Forrest\\PropertySelListFrame\\Report");	// 070105 p�d

const LPCTSTR REG_WP_CONTACTS_REPORT_FRAME_KEY		= _T("Forrest\\ContactsReportFrame\\Placement");	// 070115 p�d

const LPCTSTR REG_WP_CONTACTS_REPORT_STATE_KEY	= _T("Forrest\\ContactsReportStateFrame\\Report");	// 070105 p�d

const LPCTSTR REG_WP_IMPORT_DATA_KEY	= _T("Forrest\\ImpoRtDataFrame\\Placement");	// 080710 p�d

const LPCTSTR REG_WP_EXTERNAL_DOCS_KEY	= _T("Forrest\\ExternalDocsFrame\\Placement");	// 080925 p�d

const LPCTSTR REG_WP_EXTERNAL_DOECS_REPORT_KEY	= _T("Forrest\\ExternalDocsFrame\\Report");	// 080925 p�d

const LPCTSTR REG_WP_CONTACTS_SEARCH_REPL_FRAME_KEY	= _T("Forrest\\SearchReplaceFrame\\Placement");	// 070104 p�d



const LPCTSTR EXCEL_IMPORT_TMPL_DIR		= L"Forest\\Directories\\ImportTmpl";
const LPCTSTR DIRECTORY_KEY						= L"Directory";


//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of Window(s); 060209 p�d

const int MIN_X_SIZE_SPECIES						= 450;
const int MIN_Y_SIZE_SPECIES						= 250;

const int MIN_X_SIZE_POSTNUM						= 300;
const int MIN_Y_SIZE_POSTNUM						= 400;

const int MIN_X_SIZE_CATEGORY						= 450;
const int MIN_Y_SIZE_CATEGORY						= 250;

const int MIN_X_SIZE_CONTACTS						= 585;
const int MIN_Y_SIZE_CONTACTS						= 400;

const int MIN_X_SIZE_CONTACTS_SELLIST		= 585;
const int MIN_Y_SIZE_CONTACTS_SELLIST		= 350;

const int MIN_X_SIZE_CMP								= 300;
const int MIN_Y_SIZE_CMP								= 400;

const int MIN_X_SIZE_PROPERTY						= 585;
const int MIN_Y_SIZE_PROPERTY						= 400;

const int MIN_X_SIZE_PROPERTY_SELLIST		= 585;
const int MIN_Y_SIZE_PROPERTY_SELLIST		= 350;

const int MIN_X_SIZE_PROPERTY_REMOVE_SELLIST		= 585;
const int MIN_Y_SIZE_PROPERTY_REMOVE_SELLIST		= 350;

const int MIN_X_SIZE_CONTACTS_REPORT		= 585;
const int MIN_Y_SIZE_CONTACTS_REPORT		= 385;

const int MIN_X_SIZE_IMPORT_DATA				= 585;
const int MIN_Y_SIZE_IMPORT_DATA				= 400;

const int MIN_X_SIZE_EXTERNAL_DOCS			= 585;
const int MIN_Y_SIZE_EXTERNAL_DOCS			= 400;

const int MIN_X_SIZE_SEARCH_REPLACE			= 510;
const int MIN_Y_SIZE_SEARCH_REPLACE			= 300;

// Buffer for
#define BUFFER_SIZE		1024*100		// 100 kb buffer


#define MAX_CMP_ID		32000			// Max value for a CountyID,MunucipalID or ParishID
///////////////////////////////////////////////////////////////////////////////
//	Resource names for icons in resource dll (HMSToolBarIcones32.dll); 051209 p�d

//const LPCTSTR TOOLBAR_RES_DLL						= _T("HMSToolBarIcons32.dll");		// Resource dll, holds icons for e.g. toolbars; 051208 p�d
const LPCTSTR TOOLBAR_RES_DLL					= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; 091014 p�d

const int RES_TB_FILTER						= 39;
const int RES_TB_FILTER_OFF				= 8;
const int RES_TB_TOOLS						= 47;
const int RES_TB_PRINT						= 36;
const int RE_TB_REFRESH						= 43;
const int RES_TB_ADD							= 0;
const int RES_TB_REMOVE						= 28;
const int RES_TB_IMPORT						= 19;
const int RES_TB_EXECUTE					= 46;
const int RES_TB_SEARCH						= 37;
const int RES_TB_SAVE							= 35;
const int RES_TB_GIS							= 74;


/*
const LPCTSTR RSTR_TB_FILTER						= _T("Tabellfilter");
const LPCTSTR RSTR_TB_FILTER_OFF				= _T("Endfilter");
const LPCTSTR RSTR_TB_TOOLS							= _T("Verktyg");
const LPCTSTR RSTR_TB_PRINT							= _T("Skrivut");
const LPCTSTR RSTR_TB_REFRESH						= _T("Uppdatera");
const LPCTSTR RSTR_TB_ADD								= _T("Add");
const LPCTSTR RSTR_TB_REMOVE						= _T("Minus");
const LPCTSTR RSTR_TB_IMPORT						= _T("Import");
const LPCTSTR RSTR_TB_EXECUTE						= _T("Utrop");
const LPCTSTR RSTR_TB_SEARCH						= _T("Sok");
const LPCTSTR RSTR_TB_SAVE							= _T("Save");
*/
/////////////////////////////////////////////////////////////////////////////////////////////////
// Column headlines for import of Property-data from Excel or Semicolon-delimited files; 080710 p�d
//Feature #2392 lagt till personnummer i importhanteringen 20111003 J�
const int NUM_OF_COLUMNS			= 27;
const LPCTSTR COLUMN_NAMES[NUM_OF_COLUMNS] = {_T("L�n"),	// 0
											_T("Kommun"),	// 1
											_T("F�rsamling"),	// 2
											_T("Fastighetsnummer"),	// 3
											_T("Fastighet"),	// 4
											_T("S�k-ID"),	// 5
											_T("Mottagarreferens"),	// 6
											_T("Andel"),	// 7
											_T("Kontakt"),	// 8
											_T("Namn"),	// 9
											_T("Gatuadress"),	// 10
											_T("C/o-adress"),	// 11
											_T("Postnr"),	// 12
											_T("Postort"),	// 13
											_T("Tele (arb)"),	// 14
											_T("Tele (hem)"),	// 15
											_T("Mobil"),	// 16
											_T("Fax"),	// 17
											_T("Region"),	// 18
											_T("Land"),	// 19
											_T("Noteringar"),	// 20
											_T("E-Post"),	// 21
											_T("Personnummer"), // 22
											_T("Bankgiro"), // 23
											_T("Plusgiro"), // 24
											_T("Bankkonto"), // 25
											_T("Levnummer") // 26
											};	

typedef std::map<CString,int> vecMapColumns;
typedef std::map<CString,BOOL> vecMapAddData;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Enumerated NEW or UPDATE

typedef enum { NEW_ITEM, UPD_ITEM } enumACTION;

// Enumerated value for Handling documents. 
typedef enum { DOCS_NO_ACTION,
							 DOCS_IN_OBJECT, 
							 DOCS_IN_PROPERTY } enumDocsAction;

/////////////////////////////////////////////////////////////////////////////////////////////////
// Color for non changeable row; 090811 p�d
#define LTCYAN				RGB(  224,  255,  255)

//////////////////////////////////////////////////////////////////////////////////////////
//
const TCHAR sz0dec[] = _T("%.0f");
const TCHAR sz1dec[] = _T("%.1f");
const TCHAR sz2dec[] = _T("%.2f");
const TCHAR sz3dec[] = _T("%.3f");

/////////////////////////////////////////////////////////////////////////////////////////////////
// Status bar progress index; 080118 p�d
#define PROGRESS_CTRL_INDEX			2	// Position on Statusbar

/////////////////////////////////////////////////////////////////////////////////////////////////
// Simple class, holds info from showFormView LPARAM; 090530 p�d
class class_LParam
{
public:
	static LPARAM m_lpValue;
};

// Added 2009-06-05 P�D
class Scripts
{
	CString _TableName;
	CString _Script;
	CString _DBName;
public:
	enum actionTypes { TBL_CREATE,TBL_ALTER };

	Scripts(void)
	{
		_TableName = _T("");
		_Script = _T("");
		_DBName = _T("");
	}

	Scripts(LPCTSTR table_name,LPCTSTR script,LPCTSTR db_name)
	{
		_TableName = table_name;
		_Script = script;
		_DBName = db_name;
	}

	CString getTableName()	{ return _TableName; }
	CString getScript()			{ return _Script; }
	CString getDBName()			{ return _DBName; }
};

typedef std::vector<Scripts> vecScriptFiles;

/////////////////////////////////////////////////////////////////////////////////////////////////

typedef enum originForSearch { ORIGIN_NONE,ORIGIN_PROPERTY,ORIGIN_CONTACT } ORIGIN_FOR_SEARCH;

typedef enum focusedEdit { ED_NONE,
													 ED_PROPNUM,							// Fastighet
													 ED_PROPNAME,							// Fastighet
													 ED_BLOCK,								// Fastighet
													 ED_UNIT,									// Fastighet
													 ED_AREAL1,								// Fastighet
													 ED_AREAL2,								// Fastighet
													 ED_OBJID,								// Fastighet
													 ED_MOTTAGARREF,  				// Fastighet
													 ED_CTACT_ORGNR,					// Kontakter
													 ED_CTACT_NAME,						// Kontakter
													 ED_CTACT_COMPANY,				// Kontakter
													 ED_CTACT_ADDRESS,				// Kontakter
													 ED_CTACT_CO_ADDRESS,			// Kontakter
													 ED_CTACT_POSTNUM,				// Kontakter
													 ED_CTACT_POST_ADDRESS,		// Kontakter
													 ED_CTACT_COUNTY,					// Kontakter
													 ED_CTACT_COUNTRY,				// Kontakter
													 ED_CTACT_VAT_NUMBER,			// Kontakter
													 ED_CTACT_PHONE_WORK,			// Kontakter
													 ED_CTACT_PHONE_HOME,			// Kontakter
													 ED_CTACT_FAX_NUMBER,			// Kontakter
													 ED_CTACT_MOBILE,					// Kontakter
													 ED_CTACT_EMAIL,					// Kontakter
													 ED_CTACT_WEBSITE,				// Kontakter
													 ED_CTACT_BANKGIRO,				// Kontakter
													 ED_CTACT_PLUSGIRO,				// Kontakter
													 ED_CTACT_BANKKONTO,			// Kontakter
													 ED_CTACT_LEVNUMMER				// Kontakter
												} FOCUSED_EDIT;

typedef struct _search_replace_info
{
	int m_nID;
	ORIGIN_FOR_SEARCH m_origin;
	CString m_sText;
	int m_nNumber;
	double m_fNumber;
	FOCUSED_EDIT m_focusedEdit;
	bool m_bIsNumeric;
	CString m_sFocus;	// caption of the selected search item
	
	_search_replace_info()
	{
		m_nID = -99;
		m_origin = ORIGIN_NONE;
		m_sText = _T("");
		m_nNumber = 0;
		m_fNumber = 0.0;
		m_focusedEdit = ED_NONE;
		m_bIsNumeric = 0.0;
		m_sFocus = _T("");	// caption of the selected search item
	}


} SEARCH_REPLACE_INFO;


/////////////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions
CString getResStr(UINT id);

CString getToolBarResourceFN(void);

// Get name of an Exported function in UMDataBase usermodule; 060317 p�d
CString getDBExportedFuncName(enumFuncNames);

BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table);
// Create database table from string; 080627 p�d
BOOL connectToDB(CSQLServer_ADODirect *db);
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,BOOL do_create_table = TRUE,CString database_name = _T(""));

BOOL runSQLScriptFileEx1(vecScriptFiles &vec,Scripts::actionTypes action);

BOOL setupPostnumberTable(LPCTSTR fn);
BOOL setupCountyMunicipalAndParishTable(LPCTSTR fn);

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp = -1);
CView *showFormViewEx(int idd,LPCTSTR lang_fn,LPARAM lp = -1);

CView *getFormViewByID(int idd);

CWnd *getFormViewParentByID(int idd);

void setupForDBConnection(HWND hWndReciv,HWND hWndSend);

void splitName_Block_Unit(CString in,CString& name,CString& block,CString& unit);

CString split_str(LPCTSTR str,int colNum,char sep);

BOOL hitTest_X(int hit_x,int limit_x,int w_x);

//BOOL isURL(CString url);

BOOL readSpecies(vecTransactionSpecies &);

void enableTabPage(int idx,BOOL enable);

void activeTabPage(int idx);

CString StringFromNumeric(CString value);

BOOL readPostnumberTable(LPCTSTR fn,vecTransactionPostnumber &);

BOOL readCountyMunicipalAndParishTable(LPCTSTR fn,vecTransatcionCMP &);

void ReadSelectedFile(IN const UINT nCodePage,LPCTSTR fn,CStringArray &arr);

void setToolbarBtn(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show = TRUE);

//Added max length on owner_share 20110126 J� #1937
#define MAX_OWNER_SHARE_CHARS 20		//HMS-28 �ndrat fr�n 10 till 20 tecken

// HMS-76 �ndra antal tecken f�r adressf�ltet f�r kontakter till 150, 20220209 J�
#define MAX_ADDRESS_CHARS 150

//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying attachments icons.
class CExIconItem : public CXTPReportRecordItem
{
	CString m_sText;
	double m_fValue;
	int m_dec;
public:
	// Constructs record item with the initial value.
	CExIconItem(CString text,int icon_id);
	CExIconItem(int num_of,int icon_id);
	CExIconItem(double value,int dec,int icon_id);
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText);

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
	void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
	void setTextItem(CString text)
	{
		m_sText = text;
		SetCaption((m_sText));
	}

	CString getExIconText(void)		{ return m_sText; }
	void setExIconText(LPCTSTR v)	
	{ 
		m_sText = v; 
		SetCaption((m_sText));
	}

	double getExIconFloat(void)		{ return m_fValue; }
	void setExIconFloat(double v)	
	{ 
		TCHAR tmp[50];
		m_fValue = v; 
		_stprintf(tmp, _T("%.*f"), m_dec,m_fValue);
		SetCaption((tmp));
	}
};


CString getFilePath(LPCTSTR path);


class CExTextItem : public CXTPReportRecordItemText
{
//private:
	CString m_sText;
public:

	CExTextItem(CString sValue);
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText);
	
	CString getTextItem(void);
	void setTextItem(LPCTSTR text);
	void setIsBold(void);

	void setTextColor(COLORREF rgb_value);
	void setTextBkColor(COLORREF rgb_value);
};