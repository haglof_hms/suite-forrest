#pragma once

#include "Resource.h"

#include "ForrestDB.h"

/////////////////////////////////////////////////////////////////////////////////////
//	Report classes for Species; 060317 p�d

/////////////////////////////////////////////////////////////////////////////
// CProeprtyReportRec

class CProeprtyReportRec : public CXTPReportRecord
{
	int m_nID;
protected:

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CProeprtyReportRec(void)
	{
		m_nID	= -1;	
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CProeprtyReportRec(int id,CTransaction_category rec)
	{
		m_nID	= id;
		AddItem(new CIntItem(rec.getID()));
		AddItem(new CTextItem(rec.getCategory()));
		AddItem(new CTextItem(rec.getNotes()));
	}

	int getID(void)
	{
		return m_nID;
	}

	int getColumnInt(int item)	
	{ 
		if (item == 0)
			return ((CIntItem*)GetItem(item))->getIntItem();
		else
			return 0;

	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

};

// CPropertyFormView form view

class CPropertyFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPropertyFormView)

private:
	BOOL m_bInitialized;
	BOOL m_bSetFocusOnInitDone;
	BOOL m_bIsDataEnabled;
	CString	m_sLangAbrev;
	CString m_sLangFN;
	CString m_sErrCap;
	CString m_sSaveMsg;
	CString m_sDoneSavingMsg;
	CString m_sDeleteMsg;
	CString m_sMsgCap;
	CString m_sMsgCap1;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sCounty;
	CString m_sMunicipal;
	CString m_sParish;
	CString m_sName;
	CString m_sOwnerTabCaption;
	CString m_sFrameCaption;
	CString m_sDataMissinMsg;
	CString m_sPropertyActiveMsg;
	CString m_sNoPropertiesMsg;
	CString m_sNoResultInSearch;
	CString m_sPropNameMissing;
	CString m_sPropertyAlreadyExists;

	CString m_sSQLProps;

	BOOL m_bIsDirty;

	// Status for navigation buttons; 070125 p�d
	BOOL m_bNavButtonStartPrev;
	BOOL m_bNavButtonEndNext;



	enumACTION m_enumAction;

	void setLanguage(void);

	FOCUSED_EDIT m_enumFocusedEdit;

	BOOL getProperties(void);
	vecTransactionProperty m_vecPropertyData;
	CTransaction_property m_enteredPropertyData;
	CTransaction_property m_activePropertyData;
	BOOL doIsPropertyUsed(CTransaction_property &);

	CTransaction_county_municipal_parish m_recSelectedCMP;
	
	int m_nDBIndex;
	int m_nObjId;
	void populateData(int idx);
	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void setEnableData(BOOL enable);
	void setEnableOwnerTab(BOOL enable);

	CForrestDB *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL checkFocus(CWnd *pWnd);
protected:
	CPropertyFormView();           // protected constructor used by dynamic creation
	virtual ~CPropertyFormView();

	CXTResizeGroupBox m_wndGroup;
	CXTResizeGroupBox m_wndGroup2;

	CMyExtStatic m_wndLbl1;
	CMyExtStatic m_wndLbl2;
	CMyExtStatic m_wndLbl3;
	CMyExtStatic m_wndLbl4;
	CMyExtStatic m_wndLbl5;
	CMyExtStatic m_wndLbl6;
	CMyExtStatic m_wndLbl7;
	CMyExtStatic m_wndLbl8;
	CMyExtStatic m_wndLbl9;
	CMyExtStatic m_wndLbl11;
	CMyExtStatic m_wndLbl12;

	CMyExtEdit m_wndEdit1;
	CMyExtEdit m_wndEdit2;
	CMyExtEdit m_wndEdit3;
	CMyExtEdit m_wndEdit4;
	CMyExtEdit m_wndEdit5;
	CMyExtEdit m_wndEdit6;
	CMyExtEdit m_wndEdit7;
	CMyExtEdit m_wndEdit8;
	CMyExtEdit m_wndEdit9;
	CMyExtEdit m_wndEdit10;
	CMyExtEdit m_wndEdit11;

	CXTButton m_wndCountyBtn;
	CXTButton m_wndMunicipalBtn;
	CXTButton m_wndParishBtn;

	CButton m_wndCBox1;

	BOOL m_bIsAtFirstItem;
	BOOL m_bIsAtLastItem;

	// My methods
	BOOL getEnteredData(void);

	BOOL checkEnteredProperty();

	BOOL addProperty(void);

	void resetIsDirty(void);
	BOOL getIsDirty(void);

	void clearAll();

	void setSearchToolbarBtn(BOOL enable);

	static bool staticKeepData;
public:
	// Need to be PUBLIC
	// S�tter denna till public f�r at tn� denna fr�n selectlistformview
	// f�r att kunna anv�nda den n�r man skall radera flera fstigheter Feature #2453 20111103 J�
		void setActiveTab(int tab);

	BOOL isDataChanged(short action);
	BOOL saveProperty(short action);
	void setCountyMunicpalAndParish(CTransaction_county_municipal_parish);
	BOOL doPopulateNext(int *id);
	BOOL doPopulatePrev(int *id);
	void doPopulate(int,bool set_by_index = true);
	void doRePopulate(void);
	void refreshNavButtons(void);
	void refreshProperties(void);

	//#5007 PH 20160622
	bool enableList;
	void getPropertiesForObject(int objid) { 	
		if (m_pDB != NULL)
		{
			m_pDB->getPropertiesForObjectId(objid,m_vecPropertyData);
			enableList=false;
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,enableList);
			doSetSearchBtn(false);
			m_nObjId = objid;
		}
	}


	
	void doSetSearchBtn(BOOL enable);
	// Set public, removeProperty is called from
	// CPropertyTabView. This is also the case
	// with removePropOwner in CMDIPropOwnerFormView; 070126 p�d
	BOOL removeProperty(bool bDoAll,bool *bToAll,bool *bYesToAll,bool *bNoToAll);

	BOOL getStartPrevStatus(void)	{	return m_bNavButtonStartPrev;	}

	BOOL getEndNextStatus(void)		{	return m_bNavButtonEndNext;	}
	
	int getDBIndex(void)					{	return m_nDBIndex;	}

	void doSetNavigationBar(void);

	BOOL doRePopulateFromSearch(LPCTSTR sql,int *index);

	CTransaction_property &getActiveProperty(void)	{ return m_activePropertyData; }

	inline BOOL isAtFirstItem(void)	{ return m_bIsAtFirstItem; }
	inline BOOL isAtLastItem(void)	{ return m_bIsAtLastItem; }

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	enum { IDD = IDD_FORMVIEW5 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnImport();
	afx_msg void OnSearchReplace();

	afx_msg void OnChangeObjID();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton55();
	afx_msg void OnEnSetfocusEdit51();
	afx_msg void OnEnSetfocusEdit52();
	afx_msg void OnEnSetfocusEdit53();
	afx_msg void OnEnSetfocusEdit517();
	afx_msg void OnEnSetfocusEdit54();
	afx_msg void OnEnSetfocusEdit55();
	afx_msg void OnEnSetfocusEdit56();
	afx_msg void OnEnSetfocusEdit57();
	afx_msg void OnEnSetfocusEdit58();
	afx_msg void OnEnSetfocusEdit59();
	afx_msg void OnEnSetfocusEdit510();
	afx_msg void OnBnClickedCBox1();
};


