#pragma once

#include "Resource.h"


/////////////////////////////////////////////////////////////////////////////////////
//	Report classes for Species; 060317 p�d

/////////////////////////////////////////////////////////////////////////////
// CPropOwnerReportRec

class CPropOwnerReportRec : public CXTPReportRecord
{
	int m_nID;
	int m_nPropID;
	int m_nContactID;
	CString m_sOwnerName;
protected:

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_nValue = _tstoi(szText);
				SetValue(m_nValue);
		}

		int getIntItem(void)	{ return m_nValue; }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

	//////////////////////////////////////////////////////////////////////////
	// Customized record item, used for displaying checkboxes.
	class CCheckItem : public CXTPReportRecordItem
	{
	public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};


public:

	CPropOwnerReportRec(void)
	{
		m_nID	= -1;	
		m_nPropID	= -1;
		m_nContactID = -1;
		m_sOwnerName = _T("");

		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(_T("")));
	}

	CPropOwnerReportRec(int id,int prop_id,int contact_id,int is_contact,LPCTSTR owner_name,LPCTSTR company,LPCTSTR share_off,LPCTSTR lang_file,HWND parent)
	{
		m_nID	= id;
		m_nPropID	= prop_id;
		m_nContactID = contact_id;
		m_sOwnerName = owner_name;
		AddItem(new CTextItem(owner_name));
		AddItem(new CTextItem(company));
		AddItem(new CCheckItem(is_contact));
		AddItem(new CTextItem(share_off));
	}

	int getID(void)
	{
		return m_nID;
	}

	int getPropID(void)
	{
		return m_nPropID;
	}

	int getContactID(void)
	{
		return m_nContactID;
	}

	CString& getOwnerName(void)
	{
		return m_sOwnerName;
	}

	int getColumnInt(int item)	
	{ 
		if (item == 0)
			return ((CIntItem*)GetItem(item))->getIntItem();
		else
			return 0;
	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

	BOOL getColumnCheck(int item)
	{
		return ((CCheckItem*)GetItem(item))->getChecked();
	}

	void setColumnCheck(int item,BOOL bChecked)
	{
		((CCheckItem*)GetItem(item))->setChecked(bChecked);
	}


};

/////////////////////////////////////////////////////////////////////////////
// CPropOwnerReportChildRec

class CPropOwnerReportChildRec : public CXTPReportRecord
{
protected:

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CPropOwnerReportChildRec(void)
	{
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));

	}

	CPropOwnerReportChildRec(LPCTSTR text)
	{
		AddItem(new CTextItem(text));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	
	{ 
		if (item > 0)
			((CTextItem*)GetItem(item))->setTextItem(text);
	}

};


// CMDIPropOwnerFormView form view

class CMDIPropOwnerFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CMDIPropOwnerFormView)

// private:
	BOOL m_bInitialized;
	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sAddress;
	CString m_sCoAddress;
	CString m_sPostAddress;
	CString m_sPhoneWork;
	CString m_sPhoneHome;
	CString m_sMobile;
	CString m_sFax;

	CString m_sErrCap;
	CString m_sMsgCap;
	CString m_sDeleteMsg;
	CString m_sSaveMsg;
	CString m_sDoneSavingMsg;
	CString m_sOKBtn;
	CString m_sCancelBtn;
	CString m_sName;
	CString m_sCompany;
	CString m_sOwnerShare;

	CString m_sMsgRemovePropOwner1;
	CString m_sMsgRemovePropOwner2;

	CString m_sAlreadyRegOwnerMsg;

	CTransaction_prop_owners_for_property_data dataPOPD;
	BOOL getPropOwners(void);
	BOOL getPropOwnersForProperty(void);
	vecTransactionPropOwners m_vecPropOwnersData;
	vecTransactionPropOwnersForPropertyData m_vecPropOwnersForPropertyData;
	BOOL isCategoryUsed(CTransaction_category &);

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

protected:
	CMDIPropOwnerFormView();           // protected constructor used by dynamic creation
	virtual ~CMDIPropOwnerFormView();

	// My data members
	CMyReportCtrl m_wndReport1;

	// My methods
	BOOL setupReport1(void);
	
	BOOL clearReport(void);

	BOOL addPropOwner(void);

	void LoadReportState(void);
	void SaveReportState(void);

	void setLanguage(void);

	PROP_OWNERS_FOR_PROPERTY_DATA* getPropOwnerForPropertyData(int prop_id,int owner_id);
public:
	// Need to be PUBLIC
	BOOL isDataChanged(void);
	BOOL savePropOwner(void);
	BOOL populateReport(void);
	// Set public, removePropOwner is called from
	// CPropertyTabView. This is also the case
	// with removeProperty in CPropertyFormView; 070126 p�d
	BOOL removePropOwner(void);	

	BOOL getIsDirty(void)
	{
		if (m_wndReport1.GetSafeHwnd() != NULL)
			return m_wndReport1.isDirty();

		return FALSE;
	}

	void setPropOwner(CTransaction_contacts &);

	CMyReportCtrl& getReportCtrl(void)
	{
		return m_wndReport1;
	}

	BOOL isOwnerInProperty(void) 
	{ 
		return m_wndReport1.GetRows()->GetCount() > 0; 
	}


	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	enum { IDD = IDD_FORMVIEW6 };
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSetFocus(CWnd* pOldWnd);

	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};



