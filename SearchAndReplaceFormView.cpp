// SearchAndReplaceFormView.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "SearchAndReplaceFormView.h"

#include "PropertyTabView.h"
#include "PropertyFormView.h"
#include "ContactsTabView.h"
#include "ContactsFormView.h"


#include "ResLangFileReader.h"

// CSearchAndReplaceFormView

IMPLEMENT_DYNCREATE(CSearchAndReplaceFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CSearchAndReplaceFormView, CXTResizeFormView)
	ON_WM_CLOSE()
	ON_WM_COPYDATA()
	ON_BN_CLICKED(IDC_BUTTON10_1, &CSearchAndReplaceFormView::OnBnClickedButton101)
	ON_BN_CLICKED(IDC_BUTTON10_4, &CSearchAndReplaceFormView::OnBnClickedButton104)
	ON_BN_CLICKED(IDC_BUTTON10_2, &CSearchAndReplaceFormView::OnBnClickedButton102)
	ON_BN_CLICKED(IDC_BUTTON10_5, &CSearchAndReplaceFormView::OnBnClickedButton105)
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
END_MESSAGE_MAP()

CSearchAndReplaceFormView::CSearchAndReplaceFormView()
	: CXTResizeFormView(CSearchAndReplaceFormView::IDD)
{
	m_bInitialized = FALSE;
	m_nNumOfItemsInSearch = 0;
}

CSearchAndReplaceFormView::~CSearchAndReplaceFormView()
{
	if (m_pDB != NULL)
		delete m_pDB;

	global_IS_SEARCH_REPLACE_OPEN = false;
	CPropertyTabView *pPropTabView = NULL;
	CPropertyFormView* pPropView =  NULL;
	CContactsTabView *pContactsTabView = NULL;
	CContactsFormView* pContactView =  NULL;
	// Where do we direct the search; 090820 p�d
	if (m_Info.m_origin == ORIGIN_PROPERTY)
	{
		if ((pPropTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5)) != NULL)
		{
			if ((pPropView = (CPropertyFormView*)pPropTabView->getPropertyFormView()) != NULL)
			{
				pPropView->SetFocus();
			}
		}
	}
	else if (m_Info.m_origin == ORIGIN_CONTACT)
	{
		pContactsTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pContactsTabView != NULL)
		{
			pContactView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pContactsTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pContactView != NULL)
			{
				pContactView->SetFocus();
			}
		}
	}

}

void CSearchAndReplaceFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP10_1, m_wndGroup1);
	DDX_Control(pDX, IDC_GROUP10_2, m_wndGroup2);

	DDX_Control(pDX, IDC_LBL10_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL10_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL10_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL10_4, m_wndLbl4);

	DDX_Control(pDX, IDC_EDIT10_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT10_2, m_wndEdit2);

	DDX_Control(pDX, IDC_BUTTON10_1, m_wndSearchToFirstBtn);
	DDX_Control(pDX, IDC_BUTTON10_4, m_wndSearchNextBtn);
	DDX_Control(pDX, IDC_BUTTON10_5, m_wndSearchPrevBtn);
	DDX_Control(pDX, IDC_BUTTON10_2, m_wndReplaceBtn);

	DDX_Control(pDX, IDC_RADIO10_1, m_wndRadioWholeWord);
	DDX_Control(pDX, IDC_RADIO10_2, m_wndRadioEndOfWord);
	DDX_Control(pDX, IDC_RADIO10_3, m_wndRadioAnyPartOfWord);
	//}}AFX_DATA_MAP

}

BOOL CSearchAndReplaceFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CSearchAndReplaceFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}

void CSearchAndReplaceFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	int nNumOfItems = 0;
	CString sLblText;
//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{

		// Setup language filename; 051214 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_wndEdit1.SetDisabledColor(BLACK,INFOBK);
		m_wndEdit2.SetDisabledColor(BLACK,COL3DFACE);
		SEARCH_REPLACE_INFO *m_pInfo = (SEARCH_REPLACE_INFO*)class_LParam::m_lpValue;
		m_Info = *m_pInfo;

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_wndEdit1.SetWindowText(m_pInfo->m_sText);
				m_wndLbl2.SetWindowText(xml.str(IDS_STRING3215));
				m_wndLbl3.SetWindowText(xml.str(IDS_STRING3216));
				m_wndLbl4.SetWindowText(xml.str(IDS_STRING3219));
				m_wndSearchToFirstBtn.SetWindowText(xml.str(IDS_STRING3217));
				m_wndSearchNextBtn.SetWindowText(xml.str(IDS_STRING3220));
				m_wndSearchPrevBtn.SetWindowText(xml.str(IDS_STRING3225));
				m_wndReplaceBtn.SetWindowText(xml.str(IDS_STRING3218));

				m_wndRadioWholeWord.SetWindowText(xml.str(IDS_STRING3226));
				m_wndRadioEndOfWord.SetWindowText(xml.str(IDS_STRING3227));
				m_wndRadioAnyPartOfWord.SetWindowText(xml.str(IDS_STRING3229));
				// Set "Hela texten" as default; 100211 p�d
				m_wndRadioWholeWord.SetCheck(TRUE);

				m_sMsgCap = xml.str(IDS_STRING3219);
				m_sMsgDoReplace.Format(_T("%s\n\n%s\n\n"),xml.str(IDS_STRING3222),xml.str(IDS_STRING3223));
				m_sNumOfItems = xml.str(IDS_STRING3214);
		
				m_sNoResultInSearch = xml.str(IDS_STRING3221);

				OnBnClickedButton101();

				xml.clean();
			}	// if (xml.Load(m_sLangFN))
		}	// if (fileExists(m_sLangFN))
		m_bInitialized = TRUE;
		
		global_IS_SEARCH_REPLACE_OPEN = true;

	}	// if (! m_bInitialized )
}

int CSearchAndReplaceFormView::getChecked(void)
{
	if (m_wndRadioWholeWord.GetCheck()) return 1;
	else if (m_wndRadioEndOfWord.GetCheck()) return 2;
	else if (m_wndRadioAnyPartOfWord.GetCheck()) return 3;
	else return 0;
}

void CSearchAndReplaceFormView::setSQLSearchString(ORIGIN_FOR_SEARCH origin)
{
	CString sSearchText(m_wndEdit1.getText());
	switch (getChecked())
	{
		// "Hela texten" = no wildcards in string; 100211 p�d
		case 1 : m_sSearchText = sSearchText; break;
		// "Slutet av texten"
		case 2 : m_sSearchText = sSearchText + _T("%"); break;
		// "Del av texten"
		case 3 : m_sSearchText = _T("%") + sSearchText + _T("%");  break;
	};
	if (origin == ORIGIN_PROPERTY)
		m_sSQLSearchString.Format(_T("select * from %s where %s LIKE '%s' order by id"),TBL_PROPERTY,getColName(m_Info.m_focusedEdit),m_sSearchText);
	else if (origin == ORIGIN_CONTACT)
		m_sSQLSearchString.Format(_T("select * from %s where %s LIKE '%s' order by id"),TBL_CONTACTS,getColName(m_Info.m_focusedEdit),m_sSearchText);
}

BOOL CSearchAndReplaceFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}

	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


// CSearchAndReplaceFormView diagnostics

#ifdef _DEBUG
void CSearchAndReplaceFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CSearchAndReplaceFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CSearchAndReplaceFormView message handlers

CString CSearchAndReplaceFormView::getColName(FOCUSED_EDIT fed)
{
	CString sColumnName(_T(""));
	switch (fed)
	{
		// Fastigheter
		case ED_PROPNUM :	sColumnName = _T("prop_number");	break;			
		case ED_PROPNAME : sColumnName = _T("prop_name");	break;
		case ED_BLOCK :	sColumnName = _T("block_number");	break;
		case ED_UNIT : sColumnName = _T("unit_number");	break;
		case ED_OBJID :	sColumnName = _T("obj_id");	break;
		case ED_MOTTAGARREF :	sColumnName = _T("mottagarref");	break;
		// Kontakter
		case ED_CTACT_ORGNR :	sColumnName = _T("pnr_orgnr");	break;			
		case ED_CTACT_NAME : sColumnName = _T("name_of");	break;
		case ED_CTACT_COMPANY :	sColumnName = _T("company");	break;
		case ED_CTACT_ADDRESS : sColumnName = _T("address");	break;
		case ED_CTACT_CO_ADDRESS : sColumnName = _T("co_address");	break;
		case ED_CTACT_POSTNUM :	sColumnName = _T("post_num");	break;
		case ED_CTACT_VAT_NUMBER : sColumnName = _T("vat_number"); break;
		case ED_CTACT_POST_ADDRESS :	sColumnName = _T("post_address");	break;
		case ED_CTACT_COUNTY :	sColumnName = _T("county");	break;
		case ED_CTACT_COUNTRY :	sColumnName = _T("country");	break;
		case ED_CTACT_PHONE_WORK :	sColumnName = _T("phone_work");	break;
		case ED_CTACT_PHONE_HOME :	sColumnName = _T("phone_home");	break;
		case ED_CTACT_FAX_NUMBER :	sColumnName = _T("fax_number");	break;
		case ED_CTACT_MOBILE :	sColumnName = _T("mobile");	break;
		case ED_CTACT_EMAIL :	sColumnName = _T("e_mail");	break;
		case ED_CTACT_WEBSITE :	sColumnName = _T("web_site");	break;
		case ED_CTACT_BANKGIRO :	sColumnName = _T("bankgiro");	break;
		case ED_CTACT_PLUSGIRO :	sColumnName = _T("plusgiro");	break;
		case ED_CTACT_BANKKONTO :	sColumnName = _T("bankkonto");	break;
		case ED_CTACT_LEVNUMMER :	sColumnName = _T("levnummer");	break;
	};

	return sColumnName;
}


//-------------------------------------------------------------------------------------------------------
// DO SEARCHES
// Do a search based on item in m_wndEdit1 and field to search in; 090820 p�d

// Goto first
void CSearchAndReplaceFormView::OnBnClickedButton101()
{
	int nIndex = 0;
	BOOL bFound = FALSE;
	CString sText;
	CPropertyTabView *pPropTabView = NULL;
	CPropertyFormView* pPropView =  NULL;
	CContactsTabView *pContactsTabView = NULL;
	CContactsFormView* pContactView =  NULL;

	// Where do we direct the search; 090820 p�d
	if (m_Info.m_origin == ORIGIN_PROPERTY)
	{
		if ((pPropTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5)) != NULL)
		{
			if ((pPropView = (CPropertyFormView*)pPropTabView->getPropertyFormView()) != NULL)
			{
				setSQLSearchString(m_Info.m_origin);
				bFound = pPropView->doRePopulateFromSearch(m_sSQLSearchString,&nIndex);
				if(nIndex != -1) m_Info.m_nID = nIndex;

				if (m_pDB) m_nNumOfItemsInSearch = m_pDB->getNumOfRecordsInProperty(getColName(m_Info.m_focusedEdit),m_sSearchText);
				sText.Format(_T("%s : %d"),m_sNumOfItems,m_nNumOfItemsInSearch);
				m_wndLbl1.SetWindowText(sText);

				m_wndSearchNextBtn.EnableWindow((pPropView->isAtFirstItem() || (!pPropView->isAtFirstItem() && !pPropView->isAtLastItem())) && (m_nNumOfItemsInSearch > 1));
				m_wndSearchPrevBtn.EnableWindow((pPropView->isAtLastItem() || (!pPropView->isAtFirstItem() && !pPropView->isAtLastItem())) && (m_nNumOfItemsInSearch > 1));

				if (nIndex == -1)
				{
					sText.Format(L"%s\n\n >> %s <<\n\n",m_sNoResultInSearch,m_wndEdit1.getText());
					::MessageBox(this->GetSafeHwnd(),sText,m_sMsgCap,MB_ICONASTERISK | MB_OK);
				}

				pPropView = NULL;
			}		
			pPropTabView = NULL;
		}
	}
	else if (m_Info.m_origin == ORIGIN_CONTACT)
	{
		pContactsTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pContactsTabView != NULL)
		{
			pContactView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pContactsTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pContactView != NULL)
			{
				// Check if there's a wildcard * character, if so replace with % sign for SQL select; 090820 p�d
				setSQLSearchString(m_Info.m_origin);
				bFound = pContactView->doRePopulateFromSearch(m_sSQLSearchString,&nIndex);
				if(nIndex != -1) m_Info.m_nID = nIndex;

				if (m_pDB) m_nNumOfItemsInSearch = m_pDB->getNumOfRecordsInContacts(getColName(m_Info.m_focusedEdit),m_sSearchText);
				sText.Format(_T("%s : %d"),m_sNumOfItems,m_nNumOfItemsInSearch);
				m_wndLbl1.SetWindowText(sText);

				m_wndSearchNextBtn.EnableWindow((pContactView->isAtFirstItem() || (!pContactView->isAtFirstItem() && !pContactView->isAtLastItem())) && (m_nNumOfItemsInSearch > 1));
				m_wndSearchPrevBtn.EnableWindow((pContactView->isAtLastItem() || (!pContactView->isAtFirstItem() && !pContactView->isAtLastItem())) && (m_nNumOfItemsInSearch > 1));

				if (nIndex == -1)
				{
					sText.Format(L"%s\n\n >> %s <<\n\n",m_sNoResultInSearch,m_wndEdit1.getText());
					::MessageBox(this->GetSafeHwnd(),sText,m_sMsgCap,MB_ICONASTERISK | MB_OK);
				}
				pContactView = NULL;
			}		
			pContactsTabView = NULL;
		}
	}
}

// Goto last
void CSearchAndReplaceFormView::OnBnClickedButton104()
{
	CString sSQL,sColumnName;
	CString sText;
	int nNumOfItems = 0,nID = 0;
	CPropertyTabView *pPropTabView = NULL;
	CPropertyFormView* pPropView =  NULL;
	CContactsTabView *pContactsTabView = NULL;
	CContactsFormView* pContactView =  NULL;
	// Where do we direct the search; 090820 p�d
	if (m_Info.m_origin == ORIGIN_PROPERTY)
	{
		if ((pPropTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5)) != NULL)
		{
			if ((pPropView = (CPropertyFormView*)pPropTabView->getPropertyFormView()) != NULL)
			{
				// Check if there's a wildcard * character, if so replace with % sign for SQL select; 090820 p�d
				if (pPropView->doPopulateNext(&nID))
				{
					m_wndSearchNextBtn.EnableWindow(TRUE); 
					m_wndSearchPrevBtn.EnableWindow(TRUE);

				}
				else
				{
					m_wndSearchPrevBtn.EnableWindow(TRUE);
					m_wndSearchNextBtn.EnableWindow(FALSE);
				}
				m_Info.m_nID = nID;
				pPropView = NULL;
			}		
			pPropTabView = NULL;
		}
	}
	else if (m_Info.m_origin == ORIGIN_CONTACT)
	{
		pContactsTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pContactsTabView != NULL)
		{
			pContactView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pContactsTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pContactView != NULL)
			{
				// Check if there's a wildcard * character, if so replace with % sign for SQL select; 090820 p�d
				if (pContactView->doPopulateNext(&nID))
				{
					m_wndSearchPrevBtn.EnableWindow(TRUE);
					m_wndSearchNextBtn.EnableWindow(TRUE); 

				}
				else
				{
					m_wndSearchPrevBtn.EnableWindow(TRUE);
					m_wndSearchNextBtn.EnableWindow(FALSE);
				}
				m_Info.m_nID = nID;
				sText.Format(_T("%s : %d"),m_sNumOfItems,m_nNumOfItemsInSearch);
				m_wndLbl1.SetWindowText(sText);
				pContactView = NULL;
			}		
			pContactsTabView = NULL;
		}
	}
}
//-------------------------------------------------------------------------------------------------------
// DO UPDATES
// Just replace the active item; 090820 p�d
void CSearchAndReplaceFormView::OnBnClickedButton102()
{
	int nIndex = 0;
	CString sText(m_wndEdit2.getText());
	CPropertyTabView *pPropTabView = NULL;
	CPropertyFormView* pPropView =  NULL;
	CContactsTabView *pContactsTabView = NULL;
	CContactsFormView* pContactView =  NULL;
	CString sReplace(m_wndEdit2.getText()),sSQL;
	int nNumOfItems = 0;
	CString sSearchText(m_wndEdit1.getText());
	CString csBuf;


	if (!sReplace.IsEmpty())
	{
		if (m_pDB)
		{
			csBuf = CString(m_Info.m_sText).MakeUpper();
			int nFound = csBuf.Find(CString(sSearchText).MakeUpper());
			if(nFound != -1)
			{
				csBuf = m_Info.m_sText.Mid(nFound, sSearchText.GetLength());
				m_Info.m_sText.Replace(csBuf, sReplace);
				csBuf = m_Info.m_sText;

				if (m_Info.m_origin == ORIGIN_PROPERTY)
				{
					// Do update and try to find number of items
					sSQL.Format(_T("update %s set %s='%s' where id=%d"), TBL_PROPERTY, getColName(m_Info.m_focusedEdit), csBuf, m_Info.m_nID);
				}
				else if (m_Info.m_origin == ORIGIN_CONTACT)
				{
					// Do update
					sSQL.Format(_T("update %s set %s='%s' where id=%d"), TBL_CONTACTS, getColName(m_Info.m_focusedEdit), csBuf, m_Info.m_nID);
				}

				m_pDB->runSQLQuestion(sSQL);
			}
		}
	}	// if (!sReplace.IsEmpty())

	// Where do we direct the search; 090820 p�d
	if (m_Info.m_origin == ORIGIN_PROPERTY)
	{
		if ((pPropTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5)) != NULL)
		{
			if ((pPropView = (CPropertyFormView*)pPropTabView->getPropertyFormView()) != NULL)
			{
				setSQLSearchString(m_Info.m_origin);
				pPropView->doRePopulateFromSearch(m_sSQLSearchString,&nIndex);
		
				if (m_pDB) m_nNumOfItemsInSearch = m_pDB->getNumOfRecordsInProperty(getColName(m_Info.m_focusedEdit), m_sSearchText);
				sText.Format(_T("%s : %d"), m_sNumOfItems, m_nNumOfItemsInSearch);
				m_wndLbl1.SetWindowText(sText);

				m_wndSearchNextBtn.EnableWindow((pPropView->isAtFirstItem() || (!pPropView->isAtFirstItem() && !pPropView->isAtLastItem())) && (m_nNumOfItemsInSearch > 1));
				m_wndSearchPrevBtn.EnableWindow((pPropView->isAtLastItem() || (!pPropView->isAtFirstItem() && !pPropView->isAtLastItem())) && (m_nNumOfItemsInSearch > 1));

				if (nIndex == -1)
				{
					m_wndSearchPrevBtn.EnableWindow(FALSE);
					m_wndSearchNextBtn.EnableWindow(FALSE);

					// reload current record in the property form
					if (!sReplace.IsEmpty())
					{
						m_sSQLSearchString.Format(_T("select * from %s where id=%d"),TBL_PROPERTY, m_Info.m_nID);
						pPropView->doRePopulateFromSearch(m_sSQLSearchString,&nIndex);
					}

					sText.Format(L"%s\n\n >> %s <<\n\n",m_sNoResultInSearch,m_wndEdit1.getText());
					::MessageBox(this->GetSafeHwnd(),sText,m_sMsgCap,MB_ICONASTERISK | MB_OK);
				}

				pPropView = NULL;
			}		
			pPropTabView = NULL;
		}
	}
	else if (m_Info.m_origin == ORIGIN_CONTACT)
	{
		pContactsTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pContactsTabView != NULL)
		{
			pContactView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pContactsTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pContactView != NULL)
			{
				setSQLSearchString(m_Info.m_origin);
				pContactView->doRePopulateFromSearch(m_sSQLSearchString,&nIndex);

				if (m_pDB) m_nNumOfItemsInSearch = m_pDB->getNumOfRecordsInContacts(getColName(m_Info.m_focusedEdit),m_sSearchText);
				sText.Format(_T("%s : %d"),m_sNumOfItems,m_nNumOfItemsInSearch);
				m_wndLbl1.SetWindowText(sText);

				m_wndSearchNextBtn.EnableWindow((pContactView->isAtFirstItem() || (!pContactView->isAtFirstItem() && !pContactView->isAtLastItem())) && (m_nNumOfItemsInSearch > 1));
				m_wndSearchPrevBtn.EnableWindow((pContactView->isAtLastItem() || (!pContactView->isAtFirstItem() && !pContactView->isAtLastItem())) && (m_nNumOfItemsInSearch > 1));

				if (nIndex == -1)
				{
					m_wndSearchPrevBtn.EnableWindow(FALSE);
					m_wndSearchNextBtn.EnableWindow(FALSE);

					// reload current record in the contact form
					if (!sReplace.IsEmpty())
					{
						m_sSQLSearchString.Format(_T("select * from %s where id=%d"),TBL_CONTACTS, m_Info.m_nID);
						pContactView->doRePopulateFromSearch(m_sSQLSearchString,&nIndex);
					}

					sText.Format(L"%s\n\n >> %s <<\n\n",m_sNoResultInSearch,m_wndEdit1.getText());
					::MessageBox(this->GetSafeHwnd(),sText,m_sMsgCap,MB_ICONASTERISK | MB_OK);
				}

				pContactView = NULL;
			}		
			pContactsTabView = NULL;
		}
	}

}

void CSearchAndReplaceFormView::OnBnClickedButton105()
{
	CString sSQL,sColumnName;
	CString sText;
	int nNumOfItems = 0,nID = 0;
	CPropertyTabView *pPropTabView = NULL;
	CPropertyFormView* pPropView =  NULL;
	CContactsTabView *pContactsTabView = NULL;
	CContactsFormView* pContactView =  NULL;
	// Where do we direct the search; 090820 p�d
	if (m_Info.m_origin == ORIGIN_PROPERTY)
	{
		if ((pPropTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5)) != NULL)
		{
			if ((pPropView = (CPropertyFormView*)pPropTabView->getPropertyFormView()) != NULL)
			{
				// Check if there's a wildcard * character, if so replace with % sign for SQL select; 090820 p�d
				if (pPropView->doPopulatePrev(&nID))
				{
					m_wndSearchPrevBtn.EnableWindow(TRUE);
					m_wndSearchNextBtn.EnableWindow(TRUE);
				}
				else
				{
					m_wndSearchPrevBtn.EnableWindow(FALSE);
					m_wndSearchNextBtn.EnableWindow(TRUE);
				}
				m_Info.m_nID = nID;
				pPropView = NULL;
			}		
			pPropTabView = NULL;
		}
	}
	else if (m_Info.m_origin == ORIGIN_CONTACT)
	{
		pContactsTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pContactsTabView != NULL)
		{
			pContactView = DYNAMIC_DOWNCAST(CContactsFormView, CWnd::FromHandle(pContactsTabView->getTabControl()->getTabPage(0)->GetHandle()));
			if (pContactView != NULL)
			{
				// Check if there's a wildcard * character, if so replace with % sign for SQL select; 090820 p�d
				if (pContactView->doPopulatePrev(&nID))
				{
					m_wndSearchPrevBtn.EnableWindow(TRUE);
					m_wndSearchNextBtn.EnableWindow(TRUE);
				}
				else
				{
					m_wndSearchPrevBtn.EnableWindow(FALSE);
					m_wndSearchNextBtn.EnableWindow(TRUE);
				}
				m_Info.m_nID = nID;
				sText.Format(_T("%s : %d"),m_sNumOfItems,m_nNumOfItemsInSearch);
				m_wndLbl1.SetWindowText(sText);
				pContactView = NULL;
			}		
			pContactsTabView = NULL;
		}
	}
}

// Catch message sent from (WM_USER_MSG_SUITE)
LRESULT CSearchAndReplaceFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_SHOWVIEW_MSG :
		{
			SEARCH_REPLACE_INFO *m_pInfo = (SEARCH_REPLACE_INFO*)lParam;	//class_LParam::m_lpValue;

			// do a re-search
			if( global_IS_SEARCH_REPLACE_OPEN && (m_Info.m_sText != (*m_pInfo).m_sText || m_Info.m_focusedEdit != (*m_pInfo).m_focusedEdit))
			{
				// make sure that we are searching on a variable that is set to be searchable
				if(m_pInfo->m_focusedEdit != ED_NONE)
				{
					m_Info = *m_pInfo;
					m_wndEdit1.SetWindowText(m_pInfo->m_sText);
					OnBnClickedButton101();
				}

				break;
			}

			m_Info = *m_pInfo;
			
			break;
		}
	}	// switch (wParam)

	return 0L;
}
