#include "StdAfx.h"

#include "ForrestDB.h"


/////////////////////////////////////////////////////////////////////////////
// PrivItem for inplace button; 080925 p�d

void CInplaceBtn::OnInplaceButtonDown(CXTPReportInplaceButton* pButton)
{

	CFileDialog f(TRUE,_T(""),_T(""),4|2,m_sFilterText);
	if (f.DoModal() == IDOK)
	{
		m_sValue = f.GetPathName();
		SetValue(f.GetPathName());
//		SetTextColor(BLACK);
	}	// if (f.DoModal() == IDOK)
}

//////////////////////////////////////////////////////////////////////////////////
// CForrestDB; Handle ALL transactions for Forrest suite specifics; 061116 p�d

CForrestDB::CForrestDB(void)
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{

}

CForrestDB::CForrestDB(SAClient_t client,LPCTSTR db_name,LPCTSTR user_name,LPCTSTR psw)
	: CDBBaseClass_SQLApi(client,db_name,user_name,psw)
{
}

CForrestDB::CForrestDB(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}

// PRIVATE
BOOL CForrestDB::spcExist(CTransaction_species &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where spc_id=%d"),
		TBL_SPECIES,rec.getSpcID());
	return exists(sSQL);
}

BOOL CForrestDB::postnumberExists(CTransaction_postnumber &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_POSTNUMBER,rec.getID());
	return exists(sSQL);
}

BOOL CForrestDB::cmpExists(CTransaction_county_municipal_parish &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where county_id=%d and municipal_id=%d and parish_id=%d"),
		TBL_COUNTY_MUNICIPAL_PARISH,
		rec.getCountyID(),
		rec.getMunicipalID(),
		rec.getParishID());
	return exists(sSQL);
}

BOOL CForrestDB::categoryExists(CTransaction_category &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_CATEGORY,rec.getID());
	return exists(sSQL);
}

BOOL CForrestDB::contactExists(CTransaction_contacts &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
		TBL_CONTACTS,rec.getID());
	return exists(sSQL);
}

BOOL CForrestDB::propertyExists(CTransaction_property &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where id=%d"),
							TBL_PROPERTY,rec.getID());
	return exists(sSQL);
}


BOOL CForrestDB::propOwnersExists(CTransaction_prop_owners &rec)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where prop_id=%d and contact_id=%d"),
							TBL_PROP_OWNERS,rec.getPropID(),rec.getContactID());
	return exists(sSQL);
}

BOOL CForrestDB::propOwnersExists(int prop_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where prop_id=%d"),
							TBL_PROP_OWNERS,prop_id);
	return exists(sSQL);
}

BOOL CForrestDB::extDocExists(LPCTSTR tbl_name,int link_id)
{
	CString sSQL;
	sSQL.Format(_T("select 1 from %s where extdoc_link_tbl='%s' and extdoc_link_id=%d"),
							TBL_EXTERNAL_DOCS,tbl_name,link_id);
	return exists(sSQL);
}

// PUBLIC
BOOL CForrestDB::getSpecies(vecTransactionSpecies &vec)
{
	CString sSQL;
	try
	{
		vec.clear();

		sSQL.Format(_T("select * from %s order by spc_id"),TBL_SPECIES);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_species(-1,
				m_saCommand.Field(1).asLong(),
				m_saCommand.Field(_T("p30_spec")).asLong(),
				(LPCTSTR)(m_saCommand.Field(2).asString()),
				(LPCTSTR)(m_saCommand.Field(3).asString()),
				_T("")));
		}
		
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

int CForrestDB::getSpecieNextID(void)
{
	CString sSQL;
	int nSpcID = -1;
	try
	{
		sSQL.Format(_T("select max(spc_id) from %s"),TBL_SPECIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();	
		while(m_saCommand.FetchNext())
		{
			nSpcID = m_saCommand.Field(1).asLong();
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return 0;
	}

	return nSpcID;
}


BOOL CForrestDB::addSpecie(CTransaction_species &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!spcExist(rec))
		{
			sSQL.Format(_T("insert into %s (spc_id,spc_name,notes,p30_spec) values(:1,:2,:3,:4)"),
									TBL_SPECIES);
			m_saCommand.setCommandText((SAString)(sSQL));

			m_saCommand.Param(1).setAsLong()	= rec.getSpcID();
			m_saCommand.Param(2).setAsString()	= rec.getSpcName();
			m_saCommand.Param(3).setAsString()	= rec.getNotes();
			m_saCommand.Param(4).setAsLong()	= rec.getP30SpcID();
			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::updSpecie(CTransaction_species &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (spcExist(rec))
		{

			sSQL.Format(_T("update %s set spc_name=:1,notes=:2,p30_spec=:3 where spc_id=:4"),TBL_SPECIES);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsString()	= rec.getSpcName();
			m_saCommand.Param(2).setAsString()	= rec.getNotes();
			m_saCommand.Param(3).setAsLong()		= rec.getP30SpcID();
			m_saCommand.Param(4).setAsLong()		= rec.getSpcID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::delSpecie(CTransaction_species &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (spcExist(rec))
		{
			sSQL.Format(_T("delete from %s where spc_id=:1"),TBL_SPECIES);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsLong() = rec.getSpcID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

//////////////////////////////////////////////////////////////////////////////////////////
// Handle Postnumbers

BOOL CForrestDB::getPostnumbers(vecTransactionPostnumber &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s"),TBL_POSTNUMBER);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
			while(m_saCommand.FetchNext())
			{

				vec.push_back(CTransaction_postnumber(m_saCommand.Field(1).asLong(),
																							 (LPCTSTR)(m_saCommand.Field(3).asString()),
																							 (LPCTSTR)(m_saCommand.Field(2).asString()),
																							 (LPCTSTR)(m_saCommand.Field(4).asString())));

			}

			doCommit();
	}
	catch(SAException &e)
	{

		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::addPostnumber(CTransaction_postnumber &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!postnumberExists(rec))
		{
			sSQL.Format(_T("insert into %s (id,postnumber,postname) values(:1,:2,:3)"),
									TBL_POSTNUMBER);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsLong()	= rec.getID();
			m_saCommand.Param(2).setAsString()	= rec.getNumber();
			m_saCommand.Param(3).setAsString()	= rec.getName();
			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::updPostnumber(CTransaction_postnumber &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (postnumberExists(rec))
		{

			sSQL.Format(_T("update %s set postnumber=:1,postname=:2 where id=:3"),
									 TBL_POSTNUMBER);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsString()	= rec.getNumber();
			m_saCommand.Param(2).setAsString()	= rec.getName();
			m_saCommand.Param(3).setAsLong()		= rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::removePostnumber(CTransaction_postnumber &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (postnumberExists(rec))
		{
			sSQL.Format(_T("delete from %s where id=:1"),
				TBL_POSTNUMBER);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

int CForrestDB::getMaxPostnumberID(void)
{
	CString sSQL;
	CString sMaxID;
	int nReturn = 0;
	try
	{
		sSQL.Format(_T("select max(id) as 'max_id' from %s"),TBL_POSTNUMBER);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();	
		while(m_saCommand.FetchNext())
		{
			sMaxID = _T("max_id");
			nReturn = m_saCommand.Field((SAString)(sMaxID)).asLong();
		}

		doCommit();

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return nReturn;
	}

	return nReturn;
}


//////////////////////////////////////////////////////////////////////////////////////////
// Handle Categories

// PUBLIC
BOOL CForrestDB::getCategories(vecTransactionCategory &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by id"),TBL_CATEGORY);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{				
			vec.push_back(CTransaction_category(m_saCommand.Field(1).asLong(),
				(LPCTSTR)(m_saCommand.Field(2).asString()),
				(LPCTSTR)(m_saCommand.Field(3).asString()),
				_T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::addCategory(CTransaction_category &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!categoryExists(rec))
		{
			sSQL.Format(_T("insert into %s (category,notes) values(:1,:2)"),
									TBL_CATEGORY);

			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsString()	= rec.getCategory();
			m_saCommand.Param(2).setAsString()	= rec.getNotes();

			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::updCategory(CTransaction_category &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (categoryExists(rec))
		{

			sSQL.Format(_T("update %s set category=:1,notes=:2 where id=:3"),TBL_CATEGORY);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsString() = rec.getCategory();
			m_saCommand.Param(2).setAsString() = rec.getNotes();
			m_saCommand.Param(3).setAsLong()  = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::removeCategory(CTransaction_category &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (categoryExists(rec))
		{
			sSQL.Format(_T("delete from %s where id=:1"),TBL_CATEGORY);
			m_saCommand.setCommandText((SAString)(sSQL));
			m_saCommand.Param(1).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

//////////////////////////////////////////////////////////////////////////////////////////
// Handle Contacts

int CForrestDB::getNumOfRecordsInContacts(void)
{
	return num_of_records(TBL_CONTACTS);
}

int CForrestDB::getNumOfRecordsInContacts(LPCTSTR count_field,LPCTSTR count_value)
{
	CString sSQL,sValue(count_value);
	int nReturn = 0;
	try
	{
		sSQL.Format(_T("select count(id) as 'result' from %s where %s LIKE '%s'"),
			TBL_CONTACTS,count_field,count_value);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			nReturn = m_saCommand.Field(_T("result")).asLong();
		}
	
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return -1;
	}

	return nReturn;
}


// PUBLIC
BOOL CForrestDB::getContacts(vecTransactionContacts &vec)
{
	CString sSQL;
	sSQL.Format(_T("IF EXISTS(SELECT * \
							FROM INFORMATION_SCHEMA.TABLES \
							WHERE TABLE_NAME = '%s') \
				BEGIN \
					SELECT * FROM %s AS c \
					WHERE NOT EXISTS(SELECT null \
					FROM %s AS o, \
					%s AS p, \
					%s AS po \
					WHERE p.id=o.prop_id \
					AND p.id=po.prop_id \
					AND po.contact_id=c.id) \
				END ELSE BEGIN \
					SELECT * FROM %s \
				END\
				"), TBL_ELV_PROP, TBL_CONTACTS, TBL_ELV_PROP, TBL_PROPERTY,TBL_PROP_OWNERS,TBL_CONTACTS);

	return getContacts(sSQL,vec);
}

BOOL CForrestDB::getContacts(vecTransactionContacts &vec,BOOL showAll)
{
	CString sSQL;
	if(showAll)
		sSQL.Format(_T("SELECT * FROM %s"),TBL_CONTACTS);
	else
		return getContacts(vec);
	return getContacts(sSQL,vec);
}

//Added 2016-06-10 PH
BOOL CForrestDB::getContactsForObjectId(int id,vecTransactionContacts &vec,bool clearVector) {
	CString sSQL;
	sSQL.Format(_T("IF EXISTS(SELECT * \
							FROM INFORMATION_SCHEMA.TABLES \
							WHERE TABLE_NAME = '%s') \
				BEGIN \
					SELECT * FROM %s AS c \
					WHERE EXISTS(SELECT null \
					FROM %s AS o, \
					%s AS p, \
					%s AS po \
					WHERE p.id=o.prop_id \
					AND p.id=po.prop_id \
					AND po.contact_id=c.id \
					AND o.prop_object_id=%d) \
				END ELSE BEGIN \
					SELECT * FROM %s \
				END\
				"), TBL_ELV_PROP, TBL_CONTACTS, TBL_ELV_PROP, TBL_PROPERTY,TBL_PROP_OWNERS,id,TBL_CONTACTS);

	return getContacts(sSQL,vec,clearVector);
}

BOOL CForrestDB::getContacts(CString sql,vecTransactionContacts &vec,bool clearVector)
{
	CString sSQL(sql);
	try
	{
		if(clearVector)
			vec.clear();
		
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			 vec.push_back(CTransaction_contacts(m_saCommand.Field(_T("id")).asLong(),
																					(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
																					(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
																					m_saCommand.Field(_T("connect_id")).asLong(),
																					m_saCommand.Field(_T("type_of")).asLong(),
																					(LPCTSTR)(m_saCommand.Field(_T("bankgiro")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("plusgiro")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("bankkonto")).asString()),
																					(LPCTSTR)(m_saCommand.Field(_T("levnummer")).asString())));


		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::addContact(CTransaction_contacts &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!contactExists(rec))
		{
			sSQL.Format(_T("insert into %s (pnr_orgnr,name_of,company,address,co_address,post_num,post_address,county,country,phone_work,phone_home,fax_number,mobile,e_mail,web_site,notes,created_by,\
										 vat_number,connect_id,type_of,bankgiro,plusgiro,bankkonto,levnummer) \
										 values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24)"),
									TBL_CONTACTS);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getPNR_ORGNR().Left(50);
			m_saCommand.Param(2).setAsString()	= rec.getName().Left(50);
			m_saCommand.Param(3).setAsString()	= rec.getCompany().Left(50);
			m_saCommand.Param(4).setAsString()	= rec.getAddress().Left(MAX_ADDRESS_CHARS);
			m_saCommand.Param(5).setAsString()	= rec.getCoAddress().Left(MAX_ADDRESS_CHARS);
			m_saCommand.Param(6).setAsString()	= rec.getPostNum().Left(10);
			m_saCommand.Param(7).setAsString()	= rec.getPostAddress().Left(60);
			m_saCommand.Param(8).setAsString()	= rec.getCounty().Left(40);
			m_saCommand.Param(9).setAsString()	= rec.getCountry().Left(40);
			m_saCommand.Param(10).setAsString()	= rec.getPhoneWork().Left(25);
			m_saCommand.Param(11).setAsString()	= rec.getPhoneHome().Left(25);
			m_saCommand.Param(12).setAsString()	= rec.getFaxNumber().Left(25);
			m_saCommand.Param(13).setAsString() = rec.getMobile().Left(30);
			m_saCommand.Param(14).setAsString() = rec.getEMail().Left(50);
			m_saCommand.Param(15).setAsString() = rec.getWebSite().Left(50);
			m_saCommand.Param(16).setAsLongChar()	= rec.getNotes();
			m_saCommand.Param(17).setAsString() = getUserName();
			m_saCommand.Param(18).setAsString() = rec.getVATNumber();
			m_saCommand.Param(19).setAsLong()	= rec.getConnectID();
			m_saCommand.Param(20).setAsLong()	= rec.getTypeOf();
			m_saCommand.Param(21).setAsString() = rec.getBankgiro().Left(40);
			m_saCommand.Param(22).setAsString() = rec.getPlusgiro().Left(40);
			m_saCommand.Param(23).setAsString() = rec.getBankkonto().Left(40);
			m_saCommand.Param(24).setAsString() = rec.getLevnummer().Left(40);

			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

//Lagt till en funktion som bara uppdaterar datat som kommer fr�n excelimporten samt bara de v�rden som har data, bug #2274 20111020 J�
/*
BOOL CForrestDB::updContact(CString sOwnerName,CString sAddress,CString sCoAddress,CString sPostNum,CString sPostAddress,CString sPhoneWork,CString sPhoneHome
														,CString sMobile,CString sFax,CString sRegion,CString sCountry,CString sNotes,CString sEMail,CString sSecNr,CString bankgiro,CString plusgiro
														,CString bankkonto,CString levnummer,int nId)
*/
BOOL CForrestDB::updContactImport(CTransaction_contacts& rec)
{

	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (contactExists(rec))
		{

			sSQL.Format(_T("update %s set name_of=:1,address=:2,co_address=:3,post_num=:4,post_address=:5,country=:6,phone_work=:7,phone_home=:8,fax_number=:9,")
									_T("mobile=:10,e_mail=:11,pnr_orgnr=:12,bankgiro=:13,plusgiro=:14,bankkonto=:15,levnummer=:16 where id=:17"),
									TBL_CONTACTS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getName().Left(50);
			m_saCommand.Param(2).setAsString()	= rec.getAddress().Left(MAX_ADDRESS_CHARS);
			m_saCommand.Param(3).setAsString()	= rec.getCoAddress().Left(MAX_ADDRESS_CHARS);
			m_saCommand.Param(4).setAsString()	= rec.getPostNum().Left(10);
			m_saCommand.Param(5).setAsString()	= rec.getPostAddress().Left(60);
			m_saCommand.Param(6).setAsString()	= rec.getCountry().Left(40);
			m_saCommand.Param(7).setAsString()	= rec.getPhoneWork().Left(25);
			m_saCommand.Param(8).setAsString()	= rec.getPhoneHome().Left(25);
			m_saCommand.Param(9).setAsString()	= rec.getFaxNumber().Left(25);
			m_saCommand.Param(10).setAsString() = rec.getMobile().Left(30);
			m_saCommand.Param(11).setAsString() = rec.getEMail().Left(50);
			m_saCommand.Param(12).setAsString() = rec.getPNR_ORGNR();
			m_saCommand.Param(13).setAsString() = rec.getBankgiro().Left(40);
			m_saCommand.Param(14).setAsString() = rec.getPlusgiro().Left(40);
			m_saCommand.Param(15).setAsString() = rec.getBankkonto().Left(40);
			m_saCommand.Param(16).setAsString() = rec.getLevnummer().Left(40);
			m_saCommand.Param(17).setAsLong()		= rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}


/*
	CString sSql,sTmp;
	BOOL bReturn = FALSE;
	CString sArray[14];
	CString sColNameArray[14];
	int nOrder[15],nIndex=1,nFirst=0;
	int i=0;


	sColNameArray[i]=_T("pnr_orgnr");	i++;
	sColNameArray[i]=_T("name_of");		i++;
	sColNameArray[i]=_T("address");		i++;
	sColNameArray[i]=_T("co_address");	i++;
	sColNameArray[i]=_T("post_num");	i++;
	sColNameArray[i]=_T("post_address");i++;
	sColNameArray[i]=_T("county");		i++;
	sColNameArray[i]=_T("country");		i++;
	sColNameArray[i]=_T("phone_work");	i++;
	sColNameArray[i]=_T("phone_home");	i++;
	sColNameArray[i]=_T("fax_number");	i++;
	sColNameArray[i]=_T("mobile");		i++;
	sColNameArray[i]=_T("e_mail");		i++;
	sColNameArray[i]=_T("notes");		i++;
	
	i=0;
	if(sSecNr.GetLength()>0)
	{
		sArray[i]=sSecNr.Left(50);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sOwnerName.GetLength()>0)
	{
		sArray[i]=sOwnerName.Left(50);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sAddress.GetLength()>0)
	{
		sArray[i]=sAddress.Left(50);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sCoAddress.GetLength()>0)
	{
		sArray[i]=sCoAddress.Left(50);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sPostNum.GetLength()>0)
	{
		sArray[i]=sPostNum.Left(10);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sPostAddress.GetLength()>0)
	{
		sArray[i]=sPostAddress.Left(60);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sRegion.GetLength()>0)
	{
		sArray[i]=sRegion.Left(40);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sCountry.GetLength()>0)
	{
		sArray[i]=sCountry.Left(40);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sPhoneWork.GetLength()>0)
	{
		sArray[i]=sPhoneWork.Left(25);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sPhoneHome.GetLength()>0)
	{
		sArray[i]=sPhoneHome.Left(25);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sFax.GetLength()>0)
	{
		sArray[i]=sFax.Left(25);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sMobile.GetLength()>0)
	{
		sArray[i]=sMobile.Left(30);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sEMail.GetLength()>0)
	{
		sArray[i]=sEMail.Left(50);
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(sNotes.GetLength()>0)
	{
		sArray[i]=sNotes;
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	nOrder[i]=nIndex;

	sSql.Format(_T("update %s set "),TBL_CONTACTS);
	nFirst=1;
	for(i=0;i<14;i++)
	{
		if(nOrder[i]!=-1)
		{
			if(nFirst)
			sTmp.Format(_T("%s=:%d"),sColNameArray[i],nOrder[i]);
			else
			sTmp.Format(_T(",%s=:%d"),sColNameArray[i],nOrder[i]);
				nFirst=0;
			sSql=sSql+sTmp;
		}
	}
	sTmp.Format(_T(" where id=:%d"),nOrder[14]);
	sSql=sSql+sTmp;

	try
	{
		if (contactExists(rec))
		{

			/*sSQL.Format(_T("update %s set pnr_orgnr=:1,name_of=:2,address=:3,co_address=:4,post_num=:5,post_address=:6,county=:7,country=:8, \
						   phone_work=:9,phone_home=:10,fax_number=:11,mobile=:12,e_mail=:13,notes=:14,where id=:15"),
						   TBL_CONTACTS);* /
			m_saCommand.setCommandText((SAString)sSql);
			for(i=0;i<15;i++)
			{
				if(nOrder[i]!=-1)
				{
					if(i==13)
						m_saCommand.Param(nOrder[i]).setAsCLob()=sArray[i];
					else
					{
						if(i==14)
							m_saCommand.Param(nOrder[i]).setAsLong()=nId;
						else
							m_saCommand.Param(nOrder[i]).setAsString()=sArray[i];
					}
				}
			}

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		// print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}
*/
	return bReturn;		
}

BOOL CForrestDB::updContact(CTransaction_contacts &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (contactExists(rec))
		{

			sSQL.Format(_T("update %s set pnr_orgnr=:1,name_of=:2,company=:3,address=:4,co_address=:5,post_num=:6,post_address=:7,county=:8,country=:9,phone_work=:10,phone_home=:11,fax_number=:12,")
									_T("mobile=:13,e_mail=:14,web_site=:15,notes=:16,created_by=:17,vat_number=:18,connect_id=:19,type_of=:20,bankgiro=:21,plusgiro=:22,bankkonto=:23,levnummer=:24 where id=:25"),
									TBL_CONTACTS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getPNR_ORGNR().Left(50);
			m_saCommand.Param(2).setAsString()	= rec.getName().Left(50);
			m_saCommand.Param(3).setAsString()	= rec.getCompany().Left(50);
			m_saCommand.Param(4).setAsString()	= rec.getAddress().Left(MAX_ADDRESS_CHARS);
			m_saCommand.Param(5).setAsString()	= rec.getCoAddress().Left(MAX_ADDRESS_CHARS);
			m_saCommand.Param(6).setAsString()	= rec.getPostNum().Left(10);
			m_saCommand.Param(7).setAsString()	= rec.getPostAddress().Left(60);
			m_saCommand.Param(8).setAsString()	= rec.getCounty().Left(40);
			m_saCommand.Param(9).setAsString()	= rec.getCountry().Left(40);
			m_saCommand.Param(10).setAsString()	= rec.getPhoneWork().Left(25);
			m_saCommand.Param(11).setAsString()	= rec.getPhoneHome().Left(25);
			m_saCommand.Param(12).setAsString()	= rec.getFaxNumber().Left(25);
			m_saCommand.Param(13).setAsString() = rec.getMobile().Left(30);
			m_saCommand.Param(14).setAsString() = rec.getEMail().Left(50);
			m_saCommand.Param(15).setAsString() = rec.getWebSite().Left(50);
			m_saCommand.Param(16).setAsLongChar()	= rec.getNotes();
			m_saCommand.Param(17).setAsString() = getUserName();
			m_saCommand.Param(18).setAsString() = rec.getVATNumber();
			m_saCommand.Param(19).setAsLong()	= rec.getConnectID();
			m_saCommand.Param(20).setAsLong()	= rec.getTypeOf();
			m_saCommand.Param(21).setAsString() = rec.getBankgiro();
			m_saCommand.Param(22).setAsString() = rec.getPlusgiro();
			m_saCommand.Param(23).setAsString() = rec.getBankkonto();
			m_saCommand.Param(24).setAsString() = rec.getLevnummer();
			m_saCommand.Param(25).setAsLong()	= rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::removeContact(CTransaction_contacts &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (contactExists(rec))
		{
			sSQL.Format(_T("delete from %s where id=:1"),TBL_CONTACTS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

//#5010 PH 20160628, Get id of object connected to property
int CForrestDB::getObjectIdFromPropertyId(int propId) {
	int objId = -1;
	CString sSQL;
	try
	{

		sSQL.Format(L"IF EXISTS(SELECT * "
							L"FROM INFORMATION_SCHEMA.TABLES "
							L"WHERE TABLE_NAME = '%s') "
					L"BEGIN "
						L"SELECT prop_object_id "
						L"FROM %s "
						L"WHERE prop_id=%d "
					L"END",TBL_ELV_PROP,TBL_ELV_PROP,propId);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			objId = m_saCommand.Field(1).asLong();
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return objId;
}

// Gets the last id created in the IDENTITY field; 070102 p�d
int CForrestDB::getLastContactID(void)
{
	int nIdentity;
	CString sSQL;
	try
	{
		sSQL.Format(_T("select ident_current('%s')"),TBL_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		nIdentity = -1;
		while(m_saCommand.FetchNext())
		{
			nIdentity = m_saCommand.Field(1).asLong();
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return nIdentity;
}

// OBS! THIS FUNCTION IS USED ONLY ON AN EMPTY TABLE; 070102 p�d
// Resets the IDENTITY field to 0, i.e. first id = 1; 070102 p�d
BOOL CForrestDB::resetContactIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 1)"),TBL_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


// Handle Categories for Contacts

BOOL CForrestDB::getCategoriesForContact(vecTransactionCategoriesForContacts &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_CATEGORY_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_categories_for_contacts(m_saCommand.Field(1).asLong(),
																												 m_saCommand.Field(2).asLong(),
																												(LPCTSTR)(m_saCommand.Field(3).asString() )) );
		}

		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::addCategoriesForContact(vecTransactionCategoriesForContacts &list)
{
	CTransaction_categories_for_contacts data;
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (list.size() > 0)
		{
			// First remove categories set for this contact; 061221 p�d
			removeCategoriesForContact(list[0].getContactID());

			for (UINT i = 0;i < list.size();i++)
			{
				data = list[i];			
				sSQL.Format(_T("insert into %s (contact_id,category_id) values(:1,:2)"),
									TBL_CATEGORY_CONTACTS);

				m_saCommand.setCommandText((SAString)sSQL);
				m_saCommand.Param(1).setAsLong()	= data.getContactID();
				m_saCommand.Param(2).setAsLong()	= data.getCategoryID();

				m_saCommand.Prepare();
				m_saCommand.Execute();
			}
			doCommit();
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::addCategoriesForContact(int contact_id,int category_id)
{
	CTransaction_categories_for_contacts data;
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("IF NOT EXISTS (SELECT 1 FROM %s WHERE contact_id = :1 AND category_id = :2) insert into %s (contact_id,category_id) values(:1,:2)"),
									TBL_CATEGORY_CONTACTS, TBL_CATEGORY_CONTACTS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= contact_id;
		m_saCommand.Param(2).setAsLong()	= category_id;

		m_saCommand.Prepare();
		m_saCommand.Execute();
		doCommit();
			bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::removeCategoriesForContact(int contact_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("delete from %s where contact_id=:1"),TBL_CATEGORY_CONTACTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = contact_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

// Handle County,Municipality and Parish


// PUBLIC
BOOL CForrestDB::getCMPs(vecTransatcionCMP &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s order by county_id,municipal_id,parish_id"),
								TBL_COUNTY_MUNICIPAL_PARISH);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CTransaction_county_municipal_parish(-1,
																												 m_saCommand.Field(1).asLong(),
																												 m_saCommand.Field(2).asLong(),
																												 m_saCommand.Field(3).asLong(),
																												 (LPCTSTR)m_saCommand.Field(4).asString(),
																												 (LPCTSTR)m_saCommand.Field(5).asString(),
																												 (LPCTSTR)m_saCommand.Field(6).asString(),
																												 _T("")));
		}

		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CForrestDB::getCounties(vecTransatcionCMP &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select distinct county_id,county_name from %s order by county_name"),
								TBL_COUNTY_MUNICIPAL_PARISH);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				vec.push_back(CTransaction_county_municipal_parish(-1,m_saCommand.Field(1).asLong(),
																													-1,
																													-1,
																												 (LPCTSTR)(m_saCommand.Field(2).asString()),
																												 _T(""),
																												 _T(""),
																												 _T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::getCountiesAndMunicipality(vecTransatcionCMP &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select distinct county_id,municipal_id,county_name,municipal_name from %s order by county_name,municipal_name"),
								TBL_COUNTY_MUNICIPAL_PARISH);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_county_municipal_parish(-1,m_saCommand.Field(1).asLong(),
																												 m_saCommand.Field(2).asLong(),
																												 -1,
																												 (LPCTSTR)(m_saCommand.Field(3).asString()),
																												 (LPCTSTR)(m_saCommand.Field(4).asString()),
																												 _T(""),
																												 _T("")));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::addCMP(CTransaction_county_municipal_parish &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!cmpExists(rec))
		{
			sSQL.Format(_T("insert into %s (county_id,municipal_id,parish_id,county_name,municipal_name,parish_name) values(:1,:2,:3,:4,:5,:6)"),
									TBL_COUNTY_MUNICIPAL_PARISH);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getCountyID();
			m_saCommand.Param(2).setAsLong()		= rec.getMunicipalID();
			m_saCommand.Param(3).setAsLong()		= rec.getParishID();
			m_saCommand.Param(4).setAsString()	= rec.getCountyName();
			m_saCommand.Param(5).setAsString()	= rec.getMunicipalName();
			m_saCommand.Param(6).setAsString()	= rec.getParishName();

			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::updCMP(CTransaction_county_municipal_parish &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (cmpExists(rec))
		{
			sSQL.Format(_T("update %s set county_name=:1,municipal_name=:2,parish_name=:3 \
											where county_id=:4 and municipal_id=:5 and parish_id=:6"),
									 TBL_COUNTY_MUNICIPAL_PARISH);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getCountyName();
			m_saCommand.Param(2).setAsString()	= rec.getMunicipalName();
			m_saCommand.Param(3).setAsString()	= rec.getParishName();
			m_saCommand.Param(4).setAsLong()		= rec.getCountyID();
			m_saCommand.Param(5).setAsLong()		= rec.getMunicipalID();
			m_saCommand.Param(6).setAsLong()		= rec.getParishID();
			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::delCMP(CTransaction_county_municipal_parish &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (cmpExists(rec))
		{

			sSQL.Format(_T("delete from %s where county_id=:1 and municipal_id=:2 and parish_id=:3"),
								TBL_COUNTY_MUNICIPAL_PARISH);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= rec.getCountyID();
			m_saCommand.Param(2).setAsLong()	= rec.getMunicipalID();
			m_saCommand.Param(3).setAsLong()	= rec.getParishID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

// Special methods for collecting data from "fst_county_minicipal_parish_table"; 080710 p�d
BOOL CForrestDB::getCounties(vecCMP_data &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select distinct county_id,county_name from %s order by county_name"),
								TBL_COUNTY_MUNICIPAL_PARISH);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CCMP_data(m_saCommand.Field(1).asLong(),
														 (LPCTSTR)(m_saCommand.Field(2).asString())));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::getMunicipals(int county_id,vecCMP_data &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select distinct municipal_id,municipal_name from %s where county_id=:1 order by municipal_name"),
								TBL_COUNTY_MUNICIPAL_PARISH);

		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Param(1).setAsLong()	= county_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CCMP_data(m_saCommand.Field(1).asLong(),(LPCTSTR)(m_saCommand.Field(2).asString())));
		}
		doCommit();
	}
	catch(SAException &e)
	{

		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::getMunicipals(LPCTSTR county,vecCMP_data &vec)
{
	CString sSQL;

	try
	{
		vec.clear();
		
		sSQL.Format(_T("select distinct municipal_id,municipal_name from %s where county_name=:1 order by municipal_name"),
								TBL_COUNTY_MUNICIPAL_PARISH);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Param(1).setAsString()	= (county);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CCMP_data(m_saCommand.Field(1).asLong(),
														 (LPCTSTR)(m_saCommand.Field(2).asString())));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::getParishs(int county_id,int municipal_id,vecCMP_data &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select distinct parish_id,parish_name from %s where county_id=:1 and municipal_id=:2 order by parish_name"),
								TBL_COUNTY_MUNICIPAL_PARISH);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Param(1).setAsLong()	= county_id;
		m_saCommand.Param(2).setAsLong()	= municipal_id;
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CCMP_data(m_saCommand.Field(1).asLong(),
														 (LPCTSTR)(m_saCommand.Field(2).asString())));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}


BOOL CForrestDB::getParishs(LPCTSTR county,LPCTSTR municipal,vecCMP_data &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select distinct parish_id,parish_name from %s where county_name=:1 and municipal_name=:2 order by parish_name"),
								TBL_COUNTY_MUNICIPAL_PARISH);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Param(1).setAsString()	= (county);
		m_saCommand.Param(2).setAsString()	= (municipal);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
			vec.push_back(CCMP_data(m_saCommand.Field(1).asLong(),
														 (LPCTSTR)(m_saCommand.Field(2).asString())));
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// Handle Property


// PUBLIC
BOOL CForrestDB::getProperties(vecTransactionProperty &vec)
{
	if(savedSQL.IsEmpty()) {
		CString sSQL;
		
		sSQL.Format(_T("IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES\
							WHERE TABLE_NAME = '%s')\
							BEGIN\
							SELECT * FROM %s AS p\
							WHERE NOT EXISTS(SELECT null\
							FROM %s AS o\
							WHERE p.id=o.prop_id)\
							END ELSE BEGIN\
							SELECT * FROM %s END\
							"),TBL_ELV_PROP,TBL_PROPERTY,TBL_ELV_PROP,TBL_PROPERTY);
		savedSQL=sSQL;
		return getProperties(sSQL,vec);
	}
	else
		return getProperties(savedSQL,vec);
}

//#5007 PH 20160622
BOOL CForrestDB::getPropertiesForObjectId(int id,vecTransactionProperty &vec)
{
	CString sSQL;
		
	sSQL.Format(L"IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES "
						L"WHERE TABLE_NAME = '%s') "
						L"BEGIN "
						L"SELECT * FROM %s AS p "
						L"WHERE EXISTS(SELECT DISTINCT p.id "
						L"FROM %s AS o "
						L"WHERE p.id=o.prop_id "
						L"AND o.prop_object_id=%d) "
						L"END ELSE BEGIN "
						L"SELECT * FROM %s END"
						,TBL_ELV_PROP,TBL_PROPERTY,TBL_ELV_PROP,id,TBL_PROPERTY);

	savedSQL=sSQL;

	return getProperties(sSQL,vec);
}

BOOL CForrestDB::getProperties(vecTransactionProperty &vec,BOOL showAll)
{
	CString sSQL;

	if(showAll)
		sSQL.Format(_T("SELECT * FROM %s"),TBL_PROPERTY);
	else
		return getProperties(vec);

	return getProperties(sSQL,vec);
}

//Lagt till metod att h�mta fastigheter med en viss kontakt
//20111005 J� Feature #2323
BOOL CForrestDB::getProperties(vecTransactionPropertyObj &vec,int nContactId)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("SELECT fst_property_table.id, fst_property_table.county_code, fst_property_table.municipal_code, fst_property_table.parish_code,\
fst_property_table.county_name, fst_property_table.municipal_name, fst_property_table.parish_name, fst_property_table.prop_number,\
fst_property_table.prop_name, fst_property_table.block_number, fst_property_table.unit_number, fst_property_table.areal_ha,\
fst_property_table.areal_measured_ha, fst_property_table.created_by, fst_property_table.created, fst_property_table.obj_id,\
fst_property_table.prop_full_name, elv_object_table.object_id_number, elv_object_table.object_name, elv_object_table.object_littra \
FROM fst_property_table \
Left join fst_prop_owner_table on fst_property_table.id=fst_prop_owner_table.prop_id \
Left join elv_properties_table on fst_property_table.id=elv_properties_table.prop_id \
Left join elv_object_table on elv_properties_table.prop_object_id=elv_object_table.object_id \
where fst_prop_owner_table.contact_id=%d \
order by fst_property_table.id"),nContactId);

    	m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_prop_obj(m_saCommand.Field(1).asLong(),
																					 (LPCTSTR)(m_saCommand.Field(2).asString()),
																					 (LPCTSTR)(m_saCommand.Field(3).asString()),
																					 (LPCTSTR)(m_saCommand.Field(4).asString()),
																					 (LPCTSTR)(m_saCommand.Field(5).asString()),
																					 (LPCTSTR)(m_saCommand.Field(6).asString()),
																					 (LPCTSTR)(m_saCommand.Field(7).asString()),
																					 (LPCTSTR)(m_saCommand.Field(8).asString()),
																					 (LPCTSTR)(m_saCommand.Field(9).asString()),
																					 (LPCTSTR)(m_saCommand.Field(10).asString()),
																					 (LPCTSTR)(m_saCommand.Field(11).asString()),
																					 m_saCommand.Field(12).asDouble(),
																					 m_saCommand.Field(13).asDouble(),
																					 (LPCTSTR)(m_saCommand.Field(14).asString()),
																					 (LPCTSTR)(m_saCommand.Field(15).asString()),
																					 (LPCTSTR)(m_saCommand.Field(16).asString()),
																					 (LPCTSTR)(m_saCommand.Field(17).asString()), 
																					 (LPCTSTR)(m_saCommand.Field(18).asString()), 
																					 (LPCTSTR)(m_saCommand.Field(19).asString()), 
																					 (LPCTSTR)(m_saCommand.Field(20).asString())) 
																					 );
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::getProperties(CString sql,vecTransactionProperty &vec)
{
	CString sSQL = sql;
	try
	{
		vec.clear();
		
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{

			vec.push_back(CTransaction_property(m_saCommand.Field(L"id").asLong(),
																					 (LPCTSTR)(m_saCommand.Field(L"county_code").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"municipal_code").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"parish_code").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"county_name").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"municipal_name").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"parish_name").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"prop_number").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"prop_name").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"block_number").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"unit_number").asString()),
																					 m_saCommand.Field(L"areal_ha").asDouble(),
																					 m_saCommand.Field(L"areal_measured_ha").asDouble(),
																					 (LPCTSTR)(m_saCommand.Field(L"created_by").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"created").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"obj_id").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"prop_full_name").asString()),
																					 (LPCTSTR)(m_saCommand.Field(L"mottagarref").asString()) ));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((const TCHAR*)e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::addProperty(CTransaction_property &rec)
{
	CString sSQL,sPropFullName;
	BOOL bReturn = FALSE;
	try
	{
		if (!propertyExists(rec))
		{
			// Setup the full name of property. I.e. Name + Block:Unit; 091005 p�d
			if ((rec.getBlock().IsEmpty() || rec.getBlock() == _T("0")) &&
					(rec.getUnit().IsEmpty() || rec.getUnit() == _T("0")))
			{
				sPropFullName = rec.getPropertyName();
			}
			else if ((!rec.getBlock().IsEmpty() || rec.getBlock() != _T("0")) &&
							 (rec.getUnit().IsEmpty() || rec.getUnit() == _T("0")))
			{
				sPropFullName.Format(_T("%s %s"),rec.getPropertyName(),rec.getBlock());
			}
			else if ((!rec.getBlock().IsEmpty() || rec.getBlock() != _T("0")) &&
							 (!rec.getUnit().IsEmpty() || rec.getUnit() != _T("0")))
			{
				sPropFullName.Format(_T("%s %s:%s"),rec.getPropertyName(),rec.getBlock(),rec.getUnit());
			}

			// Added a check to see if proeprty already exists in database; 091217 p�d
			// #5008 only properties that is not connected to object should be checked. PH 20160616

			sSQL.Format(L"IF EXISTS(SELECT * "
						L"FROM INFORMATION_SCHEMA.TABLES "
						L"WHERE TABLE_NAME = '%s') "
						L"BEGIN "
							L"IF NOT EXISTS(SELECT 1 FROM %s AS p "
							L"WHERE p.county_name=\':1\' "
							L"AND p.municipal_name=\':5\' "
							L"AND p.parish_name=\':6\' "
							L"AND p.prop_name=\':8\' "
							L"AND p.block_number=\':9\' "
							L"AND p.unit_number=\':10\' "
							L"AND NOT EXISTS(SELECT 1 FROM %s AS o "
											L"WHERE p.id=o.prop_id)) "
							L"BEGIN "
								L"INSERT INTO %s (county_code,municipal_code,"
								L"parish_code,county_name,municipal_name,parish_name,"
								L"prop_number,prop_name,block_number,unit_number,"
								L"areal_ha,areal_measured_ha,created_by,"
								L"obj_id,prop_full_name,mottagarref) "
								L"VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16) "
							L"END "
						L"END ELSE BEGIN "
							L"IF NOT EXISTS(SELECT 1 FROM %s AS p "
							L"WHERE p.county_name=\':1\' "
							L"AND p.municipal_name=\':5\' "
							L"AND p.parish_name=\':6\' "
							L"AND p.prop_name=\':8\' "
							L"AND p.block_number=\':9\' "
							L"AND p.unit_number=\':10\') "
							L"BEGIN "
								L"INSERT INTO %s (county_code,municipal_code,"
								L"parish_code,county_name,municipal_name,parish_name,"
								L"prop_number,prop_name,block_number,unit_number,"
								L"areal_ha,areal_measured_ha,created_by,"
								L"obj_id,prop_full_name,mottagarref) "
								L"VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16) "
							L"END "
						L"END", TBL_ELV_PROP, TBL_PROPERTY, TBL_ELV_PROP,TBL_PROPERTY,TBL_PROPERTY,TBL_PROPERTY);

			m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsString()	= rec.getCountyCode();
			m_saCommand.Param(2).setAsString()	= rec.getMunicipalCode();
			m_saCommand.Param(3).setAsString()	= rec.getParishCode();
			m_saCommand.Param(4).setAsString()	= rec.getCountyName().Left(31);
			m_saCommand.Param(5).setAsString()	= rec.getMunicipalName().Left(31);
			m_saCommand.Param(6).setAsString()	= rec.getParishName().Left(31);
			m_saCommand.Param(7).setAsString()	= rec.getPropertyNum().Left(20);
			m_saCommand.Param(8).setAsString()	= rec.getPropertyName().Left(40);
			m_saCommand.Param(9).setAsString()	= rec.getBlock().Left(5);
			m_saCommand.Param(10).setAsString()	= rec.getUnit().Left(5);
			m_saCommand.Param(11).setAsDouble()	= rec.getAreal();
			m_saCommand.Param(12).setAsDouble()	= rec.getArealMeasured();
			m_saCommand.Param(13).setAsString()	= getUserName().Left(20);
			m_saCommand.Param(14).setAsString()	= rec.getObjectID().Left(30);
			m_saCommand.Param(15).setAsString()	= sPropFullName.Left(40);
			m_saCommand.Param(16).setAsString()	= rec.getMottagarref().Left(40);
			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

//Lagt till funktion f�r att enbart uppdatera befintlig fastighet med data fr�n excel, bara updatera kolumner med data i Bug #2474 20111020 J�
BOOL CForrestDB::updPropertyExcel(CTransaction_property &rec)
{
	CString sSql,sTmp=_T(""),sPropFullName=_T("");
	BOOL bReturn = FALSE;
	CString sArray[12];
	CString sColNameArray[12];
	int nOrder[13],nIndex=1,nFirst=0;
	int i=0;
	try
	{
		if (propertyExists(rec))
		{
			// Setup the full name of property. I.e. Name + Block:Unit; 091005 p�d
			if ((rec.getBlock().IsEmpty() || rec.getBlock() == _T("0")) &&
					(rec.getUnit().IsEmpty() || rec.getUnit() == _T("0")))
			{
				sPropFullName = rec.getPropertyName();
			}
			else if ((!rec.getBlock().IsEmpty() || rec.getBlock() != _T("0")) &&
							 (rec.getUnit().IsEmpty() || rec.getUnit() == _T("0")))
			{
				sPropFullName.Format(_T("%s %s"),rec.getPropertyName(),rec.getBlock());
			}
			else if ((!rec.getBlock().IsEmpty() || rec.getBlock() != _T("0")) &&
							 (!rec.getUnit().IsEmpty() || rec.getUnit() != _T("0")))
			{
				sPropFullName.Format(_T("%s %s:%s"),rec.getPropertyName(),rec.getBlock(),rec.getUnit());
			}


	//--------------------------------------------------------------------------
			sColNameArray[i]=_T("county_code");		i++;
			sColNameArray[i]=_T("municipal_code");	i++;
			sColNameArray[i]=_T("parish_code");		i++;
			sColNameArray[i]=_T("county_name");		i++;
			sColNameArray[i]=_T("municipal_name");	i++;
			sColNameArray[i]=_T("parish_name");		i++;
			sColNameArray[i]=_T("prop_number");		i++;
			sColNameArray[i]=_T("prop_name");		i++;
			sColNameArray[i]=_T("block_number");	i++;
			sColNameArray[i]=_T("unit_number");		i++;
			sColNameArray[i]=_T("obj_id");			i++;
			sColNameArray[i]=_T("prop_full_name");	i++;
			sColNameArray[i]=_T("mottagarref");	i++;

			i=0;
			if(rec.getCountyCode().GetLength()>0)
			{
				sArray[i]=rec.getCountyCode();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getMunicipalCode().GetLength()>0)
			{
				sArray[i]=rec.getMunicipalCode();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getParishCode().GetLength()>0)
			{
				sArray[i]=rec.getParishCode();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getCountyName().GetLength()>0)
			{
				sArray[i]=rec.getCountyName();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getMunicipalName().GetLength()>0)
			{
				sArray[i]=rec.getMunicipalName();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getParishName().GetLength()>0)
			{
				sArray[i]=rec.getParishName();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getPropertyNum().GetLength()>0)
			{
				sArray[i]=rec.getPropertyNum();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getPropertyName().GetLength()>0)
			{
				sArray[i]=rec.getPropertyName();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getBlock().GetLength()>0)
			{
				sArray[i]=rec.getBlock();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getUnit().GetLength()>0)
			{
				sArray[i]=rec.getUnit();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getObjectID().GetLength()>0)
			{
				sArray[i]=rec.getObjectID();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(sPropFullName.GetLength()>0)
			{
				sArray[i]=sPropFullName;
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;
			i++;
			if(rec.getMottagarref().GetLength()>0)
			{
				sArray[i]=rec.getMottagarref();
				nOrder[i]=nIndex;
				nIndex++;
			}
			else
				nOrder[i]=-1;

			i++;
			nOrder[i]=nIndex;

			sSql.Format(_T("update %s set "),TBL_PROPERTY);
			nFirst=1;
			for(i=0;i<13;i++)
			{
				if(nOrder[i]!=-1)
				{
					if(nFirst)
						sTmp.Format(_T("%s=:%d"),sColNameArray[i],nOrder[i]);
					else
						sTmp.Format(_T(",%s=:%d"),sColNameArray[i],nOrder[i]);
					nFirst=0;
					sSql=sSql+sTmp;
				}
			}
			sTmp.Format(_T(" where id=:%d"),nOrder[13]);
			sSql=sSql+sTmp;
			m_saCommand.setCommandText((SAString)sSql);

			for(i=0;i<13;i++)
			{
				if(nOrder[i]!=-1)
				{
					if(i==13)
						m_saCommand.Param(nOrder[i]).setAsLong()=rec.getID();
					else
						m_saCommand.Param(nOrder[i]).setAsString()=sArray[i];
				}
			}
			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::updProperty(CTransaction_property &rec)
{
	CString sSQL,sPropFullName;
	BOOL bReturn = FALSE;
	try
	{
		if (propertyExists(rec))
		{
			// Setup the full name of property. I.e. Name + Block:Unit; 091005 p�d
			if ((rec.getBlock().IsEmpty() || rec.getBlock() == _T("0")) &&
					(rec.getUnit().IsEmpty() || rec.getUnit() == _T("0")))
			{
				sPropFullName = rec.getPropertyName();
			}
			else if ((!rec.getBlock().IsEmpty() || rec.getBlock() != _T("0")) &&
							 (rec.getUnit().IsEmpty() || rec.getUnit() == _T("0")))
			{
				sPropFullName.Format(_T("%s %s"),rec.getPropertyName(),rec.getBlock());
			}
			else if ((!rec.getBlock().IsEmpty() || rec.getBlock() != _T("0")) &&
							 (!rec.getUnit().IsEmpty() || rec.getUnit() != _T("0")))
			{
				sPropFullName.Format(_T("%s %s:%s"),rec.getPropertyName(),rec.getBlock(),rec.getUnit());
			}

			sSQL.Format(_T("update %s set county_code=:1,municipal_code=:2,parish_code=:3,county_name=:4,municipal_name=:5,parish_name=:6,\
										 prop_number=:7,prop_name=:8,block_number=:9,unit_number=:10,areal_ha=:11,areal_measured_ha=:12,obj_id=:13,prop_full_name=:14,mottagarref=:15 where id=:16"),
											TBL_PROPERTY);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getCountyCode();
			m_saCommand.Param(2).setAsString()	= rec.getMunicipalCode();
			m_saCommand.Param(3).setAsString()	= rec.getParishCode();
			m_saCommand.Param(4).setAsString()	= rec.getCountyName();
			m_saCommand.Param(5).setAsString()	= rec.getMunicipalName();
			m_saCommand.Param(6).setAsString()	= rec.getParishName();
			m_saCommand.Param(7).setAsString()	= rec.getPropertyNum();
			m_saCommand.Param(8).setAsString()	= rec.getPropertyName();
			m_saCommand.Param(9).setAsString()	= rec.getBlock();
			m_saCommand.Param(10).setAsString()	= rec.getUnit();
			m_saCommand.Param(11).setAsDouble()	= rec.getAreal();
			m_saCommand.Param(12).setAsDouble()	= rec.getArealMeasured();
			m_saCommand.Param(13).setAsString()	= rec.getObjectID();		
			m_saCommand.Param(14).setAsString()	= sPropFullName;		
			m_saCommand.Param(15).setAsString()	= rec.getMottagarref();		
			m_saCommand.Param(16).setAsLong()	= rec.getID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CForrestDB::runSQLQuestion(LPCTSTR sql)
{
	CString sSQL(sql);
	BOOL bReturn = FALSE;
	try
	{
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();	
		doCommit();
		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::updPropertyObjID(int prop_id,LPCTSTR obj_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set obj_id=:1 where id=:2"),TBL_PROPERTY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= obj_id;		
		m_saCommand.Param(2).setAsLong()		= prop_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::removeProperty(CTransaction_property &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (propertyExists(rec))
		{

			sSQL.Format(_T("delete from %s where id=:1"),TBL_PROPERTY);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getID();

			m_saCommand.Execute();
			
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

// Gets the last id created in the IDENTITY field; 070102 p�d
int CForrestDB::getLastPropertyID(void)
{
	int nIdentity;
	CString sSQL;
	try
	{

		sSQL.Format(_T("select ident_current('%s')"),TBL_PROPERTY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		nIdentity = -1;
		while(m_saCommand.FetchNext())
		{
			nIdentity = m_saCommand.Field(1).asLong();
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return nIdentity;
}

// OBS! THIS FUNCTION IS USED ONLY ON AN EMPTY TABLE; 070116 p�d
// Resets the IDENTITY field to 0, i.e. first id = 1; 070116 p�d
BOOL CForrestDB::resetPropertyIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_PROPERTY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

int CForrestDB::getNumOfRecordsInProperty(void)
{
	return num_of_records(TBL_PROPERTY);
}

BOOL CForrestDB::isPropertyUnique(CTransaction_property &rec)
{
	CString sSQL,S;
	int nNumOfItemsInResultSet = 0;
	try
	{
		if (rec.getID() == -1) // New item
		{
			
			sSQL.Format(L"IF EXISTS(SELECT * "
							L"FROM INFORMATION_SCHEMA.TABLES "
							L"WHERE TABLE_NAME = '%s') "
							L"BEGIN "
								L"SELECT 1 "
								L"FROM %s AS p, %s AS o "
								L"WHERE p.county_name=:1 "
								L"AND p.municipal_name=:2 "
								L"AND p.prop_name=:3 "
								L"AND p.prop_number=:4 "
								L"AND p.block_number=:5 "
								L"AND p.unit_number=:6 "
								L"AND NOT EXISTS(SELECT 1 FROM %s AS o "
								L"WHERE p.id=o.prop_id) "
							L"END ELSE BEGIN "
								L"SELECT 1 "
								L"FROM %s "
								L"WHERE county_name=:1 "
								L"AND municipal_name=:2 "
								L"AND prop_name=:3 "
								L"AND prop_number=:4 "
								L"AND block_number=:5 "
								L"AND unit_number=:6 "
							L"END",TBL_ELV_PROP,TBL_PROPERTY,TBL_ELV_PROP,TBL_ELV_PROP,TBL_PROPERTY);
			
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getCountyName();
			m_saCommand.Param(2).setAsString()	= rec.getMunicipalName();
			m_saCommand.Param(3).setAsString()	= rec.getPropertyName();
			m_saCommand.Param(4).setAsString()	= rec.getPropertyNum();
			m_saCommand.Param(5).setAsString()	= rec.getBlock();
			m_saCommand.Param(6).setAsString()	= rec.getUnit();
		}
		else if (rec.getID() > -1)
		{
			sSQL.Format(L"IF EXISTS(SELECT * "
				L"FROM INFORMATION_SCHEMA.TABLES "
				L"WHERE TABLE_NAME = '%s') "
				L"BEGIN "
					L"SELECT 1 "
					L"FROM %s AS p, %s AS o "
					L"WHERE p.county_name=:1 "
					L"AND p.municipal_name=:2 "
					L"AND p.prop_name=:3 "
					L"AND p.prop_number=:4 "
					L"AND p.block_number=:5 "
					L"AND p.unit_number=:6 "
					L"AND p.id<>:7 "
					L"AND NOT EXISTS(SELECT 1 FROM %s AS o "
					L"WHERE p.id=o.prop_id) "
				L"END ELSE BEGIN "
					L"SELECT 1 "
					L"FROM %s "
					L"WHERE county_name=:1 "
					L"AND municipal_name=:2 "
					L"AND prop_name=:3 "
					L"AND prop_number=:4 "
					L"AND block_number=:5 "
					L"AND unit_number=:6 "
					L"AND id<>:7 "
				L"END",TBL_ELV_PROP,TBL_PROPERTY,TBL_ELV_PROP,TBL_ELV_PROP,TBL_PROPERTY);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= rec.getCountyName();
			m_saCommand.Param(2).setAsString()	= rec.getMunicipalName();
			m_saCommand.Param(3).setAsString()	= rec.getPropertyName();
			m_saCommand.Param(4).setAsString()	= rec.getPropertyNum();
			m_saCommand.Param(5).setAsString()	= rec.getBlock();
			m_saCommand.Param(6).setAsString()	= rec.getUnit();
			m_saCommand.Param(7).setAsLong()		= rec.getID();
		}
		m_saCommand.Execute();	
		while(m_saCommand.FetchNext()) 
		{
			nNumOfItemsInResultSet++;
		}

		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}
	return (nNumOfItemsInResultSet == 0);	// Om > 0, finns redan inl�st; 120925 p�d
}

int CForrestDB::getNumOfRecordsInProperty(LPCTSTR count_field,LPCTSTR count_value)
{
	CString sSQL,sValue(count_value);
	int nReturn = 0;
	try
	{
		sSQL.Format(_T("select count(id) as 'result' from %s where %s LIKE '%s'"),
			TBL_PROPERTY,count_field,count_value);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			nReturn = m_saCommand.Field(_T("result")).asLong();
		}
	
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return -1;
	}

	return nReturn;
}

BOOL CForrestDB::isPropertyUsed(int prop_id)
{
	CString sSQL,S;
	int nNumOfItemsInResultSet = 0;
	try
	{
		sSQL.Format(_T("select 1 from %s where prop_id=:1"),TBL_ELV_PROPERTIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= prop_id;
		m_saCommand.Execute();	
		while(m_saCommand.FetchNext()) 
		{
			nNumOfItemsInResultSet++;
		}

		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}
	return (nNumOfItemsInResultSet > 0);	// Used; 081016 p�d
}

BOOL CForrestDB::isPropertyUsed_list(int trakt_id,int prop_id,vecPropInObjects &vecPInObj)
{
	CString sSQL,sObj,S;
	CStringArray sarrObjList;
	vecPInObj.clear();
	try
	{
		sSQL.Format(_T("select b.object_id,b.object_name,b.object_id_number from %s a,%s b \
									 where a.ecru_prop_id=:1 and a.ecru_id=:2 and a.ecru_object_id=b.object_id"),TBL_ELV_CRUISE,TBL_ELV_OBJECT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= prop_id;
		m_saCommand.Param(2).setAsShort()	= trakt_id;
		m_saCommand.Execute();	
		while(m_saCommand.FetchNext()) 
		{
			vecPInObj.push_back(CPropInObjects(m_saCommand.Field(_T("object_id")).asLong(),m_saCommand.Field(_T("object_name")).asString()));
			/*sObj.Format(_T("%s - %s"),
				m_saCommand.Field(_T("object_name")).asString(),
				m_saCommand.Field(_T("object_id_number")).asString());
			sarrObjList.Add(sObj);*/
		}
		/*
		if (sarrObjList.GetCount() == 1) sObjList = sarrObjList.GetAt(0);
		else if (sarrObjList.GetCount() > 1)
		{
			for (int i = 0;i < sarrObjList.GetCount();i++)
			{
				if (i < sarrObjList.GetCount()-1)
					sObjList += sarrObjList.GetAt(i) + _T("\n");
				else if (i == sarrObjList.GetCount()-1)
					sObjList += sarrObjList.GetAt(i) ;
			}	// for (int i = 0;i < sarrObjList.GetCount();i++)
		}
		
		obj_list = sObjList;*/
			
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	//return (!sObjList.IsEmpty());	// Used; 081016 p�d
	return (vecPInObj.size()!=0);
}


// Handle Property owners


// PUBLIC
BOOL CForrestDB::getPropOwners(vecTransactionPropOwners &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		
		sSQL.Format(_T("select * from %s"),TBL_PROP_OWNERS);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				vec.push_back(CTransaction_prop_owners(m_saCommand.Field(1).asLong(),
																						 m_saCommand.Field(2).asLong(),
   																					 (LPCTSTR)(m_saCommand.Field(3).asString()),
																						 m_saCommand.Field(4).asShort(),
																						 (LPCTSTR)(m_saCommand.Field(5).asString())) );
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::getPropOwnersForProperty(int prop_id,vecTransactionPropOwnersForPropertyData &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select a.prop_id,a.is_contact,a.owner_share,b.* \
								 from %s a,%s b where a.contact_id = b.id and a.prop_id=:1"),
								 TBL_PROP_OWNERS,TBL_CONTACTS);
		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Param(1).setAsLong()		= prop_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
				vec.push_back(CTransaction_prop_owners_for_property_data(m_saCommand.Field(1).asLong(),
																															m_saCommand.Field(2).asLong(),
								 																							(LPCTSTR)(m_saCommand.Field(3).asString()),
																															CTransaction_contacts(m_saCommand.Field(_T("id")).asLong(),
																															(LPCTSTR)(m_saCommand.Field(_T("pnr_orgnr")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("name_of")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("company")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("address")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("co_address")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("post_num")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("post_address")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("county")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("country")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("phone_work")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("phone_home")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("fax_number")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("mobile")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("e_mail")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("web_site")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("notes")).asLongChar()),
																															(LPCTSTR)(m_saCommand.Field(_T("created_by")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("created")).asString()),
																															(LPCTSTR)(m_saCommand.Field(_T("vat_number")).asString()), 
																															m_saCommand.Field(_T("connect_id")).asLong(),
																															m_saCommand.Field(_T("type_of")).asLong(),
																															(LPCTSTR)(m_saCommand.Field(_T("bankgiro")).asString()), 
																															(LPCTSTR)(m_saCommand.Field(_T("plusgiro")).asString()), 
																															(LPCTSTR)(m_saCommand.Field(_T("bankkonto")).asString()), 
																															(LPCTSTR)(m_saCommand.Field(_T("levnummer")).asString())																													
																															))
																															);
		}
		doCommit();
	}
	catch(SAException &e)
	{
		// print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::updPropOwner(CTransaction_prop_owners &rec)
{
	CString sSql,sTmp,sColArray[4];
	BOOL bReturn = FALSE;
	int nOrder[4],nFirst=1,i=0,nIndex=1;

	sColArray[0]=_T("is_contact");
	sColArray[1]=_T("owner_share");
	sColArray[2]=_T("prop_id");
	sColArray[3]=_T("contact_id");

	if(rec.getIsContact()>0)
	{
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	i++;
	if(rec.getShareOff().GetLength()>0)
	{
		nOrder[i]=nIndex;
		nIndex++;
	}
	else
		nOrder[i]=-1;
	if(nIndex==1)
		return bReturn;
	i++;
	nOrder[i]=nIndex;
	nIndex++;
	i++;
	nOrder[i]=nIndex;	

	sSql.Format(_T("update %s set "),TBL_PROP_OWNERS);
	nFirst=1;
	for(i=0;i<2;i++)
	{
		if(nOrder[i]!=-1)
		{
			if(nFirst)
				sTmp.Format(_T("%s=:%d"),sColArray[i],nOrder[i]);
			else
				sTmp.Format(_T(",%s=:%d"),sColArray[i],nOrder[i]);
			nFirst=0;
			sSql=sSql+sTmp;
		}
	}
	sTmp.Format(_T(" where %s=:%d and %s=:%d"),sColArray[2],nOrder[2],sColArray[3],nOrder[3]);
	sSql=sSql+sTmp;


	try
	{
		if (propOwnersExists(rec))
		{
			m_saCommand.setCommandText((SAString)sSql);
			for(i=0;i<4;i++)
			{
				if(nOrder[i]!=-1)
				{
					switch(i)
					{
					case 0:
						m_saCommand.Param(nOrder[i]).setAsLong()=rec.getIsContact();
						break;
					case 1:
						m_saCommand.Param(nOrder[i]).setAsString()=rec.getShareOff();
						break;
					case 2:
						m_saCommand.Param(nOrder[i]).setAsLong()=rec.getPropID();
						break;
					case 3:
						m_saCommand.Param(nOrder[i]).setAsLong()=rec.getContactID();
						break;
					}
				}
			}
			m_saCommand.Prepare();
			m_saCommand.Execute();
			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::addPropOwner(CTransaction_prop_owners &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (!propOwnersExists(rec))
		{
			sSQL.Format(_T("insert into %s (prop_id,contact_id,is_contact,owner_share) values(:1,:2,:3,:4)"),TBL_PROP_OWNERS);

			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()		= rec.getPropID();
			m_saCommand.Param(2).setAsLong()		= rec.getContactID();
			m_saCommand.Param(3).setAsShort()		= rec.getIsContact();
			m_saCommand.Param(4).setAsString()	= rec.getShareOff();
			m_saCommand.Prepare();
			m_saCommand.Execute();

			doCommit();
			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

//#5010 PH 20160614
BOOL CForrestDB::addPropOwner(int propId,int contactId) {
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("INSERT INTO %s (prop_id,contact_id,is_contact) VALUES(:1,:2,:3)"),TBL_PROP_OWNERS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= propId;
		m_saCommand.Param(2).setAsLong()		= contactId;
		m_saCommand.Param(3).setAsShort()		= 0;
		m_saCommand.Prepare();
		m_saCommand.Execute();

		doCommit();
		return TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

}

BOOL CForrestDB::removePropOwner(CTransaction_prop_owners &rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (propOwnersExists(rec))
		{
			sSQL.Format(_T("delete from %s where prop_id=:1 and contact_id=:2"),TBL_PROP_OWNERS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = rec.getPropID();
			m_saCommand.Param(2).setAsShort() = rec.getContactID();

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::removePropOwner(int prop_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (propOwnersExists(prop_id))
		{
			sSQL.Format(_T("delete from %s where prop_id=:1"),TBL_PROP_OWNERS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = prop_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CForrestDB::removePropOwner(int prop_id,int contact_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (propOwnersExists(prop_id))
		{
			sSQL.Format(_T("delete from %s where prop_id=:1 and contact_id=:2"),TBL_PROP_OWNERS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong() = prop_id;
			m_saCommand.Param(2).setAsShort() = contact_id;

			m_saCommand.Execute();	
			doCommit();

			bReturn = TRUE;
		}	// if (!spcExist(rec))
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::isPropertyOwnerUsed(int contact_id)
{
	CString sSQL;
	int nNumOfItemsInResultSet = 0;
	try
	{
		sSQL.Format(_T("select * from %s where contact_id=:1"),TBL_PROP_OWNERS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsShort()	= contact_id;
		m_saCommand.Execute();	
		while(m_saCommand.FetchNext()) nNumOfItemsInResultSet++;
		
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return (nNumOfItemsInResultSet > 0);	// Used; 081016 p�d
}

BOOL CForrestDB::getExternalDocuments(LPCTSTR tbl_name,int link_id,vecTransaction_external_docs &vec)
{
	CString sSQL;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where extdoc_link_tbl=:1 and extdoc_link_id=:2"),TBL_EXTERNAL_DOCS);

		m_saCommand.setCommandText((SAString)(sSQL));
		m_saCommand.Param(1).setAsString()	= tbl_name;
		m_saCommand.Param(2).setAsLong()		= link_id;
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
	
			vec.push_back(CTransaction_external_docs(m_saCommand.Field(1).asLong(),
																							(LPCTSTR)(m_saCommand.Field(2).asString()),
																							 m_saCommand.Field(3).asLong(),
																							 m_saCommand.Field(4).asLong(),
																							(LPCTSTR)(m_saCommand.Field(5).asString()),
																							(LPCTSTR)(m_saCommand.Field(6).asString()),
																							(LPCTSTR)(m_saCommand.Field(7).asString()),
																							(LPCTSTR)(m_saCommand.Field(8).asString())));
		}
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::addExternalDocuments(CTransaction_external_docs rec)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("insert into %s (extdoc_link_tbl,extdoc_link_id,extdoc_link_type,extdoc_url,extdoc_note,extdoc_created_by,extdoc_created) \
								values(:1,:2,:3,:4,:5,:6,:7)"),TBL_EXTERNAL_DOCS);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsString()	= rec.getExtDocLinkTbl_pk();
		m_saCommand.Param(2).setAsLong()		= rec.getExtDocLinkID();
		m_saCommand.Param(3).setAsLong()		= rec.getExtDocLinkType();
		m_saCommand.Param(4).setAsString()	= rec.getExtDocURL();
		m_saCommand.Param(5).setAsString()	= rec.getExtDocNote();
		m_saCommand.Param(6).setAsString()	= rec.getExtDocDoneBy();
		m_saCommand.Param(7).setAsString()	= rec.getExtDocCreated();
		m_saCommand.Prepare();
		m_saCommand.Execute();

		doCommit();
		bReturn = TRUE;
	}	// if (!spcExist(rec))
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::delExternalDocuments(LPCTSTR tbl_name,int link_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{

		if (extDocExists(tbl_name,link_id))
		{
			sSQL.Format(_T("delete from %s where extdoc_link_tbl=:1 and extdoc_link_id=:2"),TBL_EXTERNAL_DOCS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsString()	= tbl_name;
			m_saCommand.Param(2).setAsLong()	= link_id;

			m_saCommand.Execute();	
			doCommit();
	
			bReturn = TRUE;
		}

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}

BOOL CForrestDB::delExternalDocument(int id,LPCTSTR tbl_name,int link_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		if (extDocExists(tbl_name,link_id))
		{
			sSQL.Format(_T("delete from %s where extdoc_id=:1 and extdoc_link_tbl=:2 and extdoc_link_id=:3"),TBL_EXTERNAL_DOCS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Param(1).setAsLong()	= id;
			m_saCommand.Param(2).setAsString()	= tbl_name;
			m_saCommand.Param(3).setAsLong()	= link_id;
			m_saCommand.Execute();	
			doCommit();
	
			bReturn = TRUE;
		}
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CForrestDB::getTraktForProperty(int property_id,vecTransactionTrakt &vec)
{
	BOOL bReturnValue = FALSE;
	CString sSQL,tmp;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where trakt_prop_id=:1"),
						TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= property_id;

		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			bReturnValue = TRUE;
			vec.push_back(CTransaction_trakt(m_saCommand.Field(1).asLong(),
															 (LPCTSTR)m_saCommand.Field(2).asString(),
															 (LPCTSTR)m_saCommand.Field(3).asString(),
															 (LPCTSTR)m_saCommand.Field(4).asString(),
															 (LPCTSTR)m_saCommand.Field(5).asString(),
															 (LPCTSTR)m_saCommand.Field(6).asString(),
																m_saCommand.Field(7).asLong(),
																m_saCommand.Field(8).asDouble(),
																m_saCommand.Field(9).asDouble(),
																m_saCommand.Field(10).asDouble(),
																m_saCommand.Field(11).asLong(),
															 (LPCTSTR)m_saCommand.Field(12).asString(),
																m_saCommand.Field(13).asDouble(),
																m_saCommand.Field(14).asLong(),
																m_saCommand.Field(15).asLong(),
																m_saCommand.Field(16).asLong(),
																m_saCommand.Field(17).asLong(),
															 (LPCTSTR)m_saCommand.Field(18).asString(),
																m_saCommand.Field(19).asLong(),
																m_saCommand.Field(20).asLong(),
															 (LPCTSTR)m_saCommand.Field(21).asString(),
															 (LPCTSTR)m_saCommand.Field(22).asString(),
															 (LPCTSTR)m_saCommand.Field(23).asString(),
															 (LPCTSTR)m_saCommand.Field(24).asString(),
															 (LPCTSTR)m_saCommand.Field(25).asString(),
															 (LPCTSTR)m_saCommand.Field(26).asString(),
																m_saCommand.Field(27).asLong(),
																m_saCommand.Field(_T("trakt_near_coast")).asLong(),
																m_saCommand.Field(_T("trakt_southeast")).asLong(),
																m_saCommand.Field(_T("trakt_region5")).asLong(),
																m_saCommand.Field(_T("trakt_part_of_plot")).asLong(),
															 (LPCTSTR)m_saCommand.Field(_T("trakt_si_h100_pine")).asString(),
															 (LPCTSTR)m_saCommand.Field(_T("trakt_prop_num")).asString(),
															 (LPCTSTR)m_saCommand.Field(_T("trakt_prop_name")).asString(),
															 (LPCTSTR)m_saCommand.Field(_T("trakt_notes")).asLongChar(),
															 (LPCTSTR)m_saCommand.Field(_T("trakt_created_by")).asString(),
															 (LPCTSTR)m_saCommand.Field(_T("created")).asString(),
															 m_saCommand.Field(_T("trakt_wside")).asLong(),
															 m_saCommand.Field(_T("trakt_for_stand_only")).asShort(),
															 m_saCommand.Field(_T("trakt_length")).asLong(),
															 m_saCommand.Field(_T("trakt_width")).asLong(),
															 //HMS-41 lagt in st�d f�r tillf�lligt utnyttjande 20200623 J�
															 m_saCommand.Field(_T("trakt_tillfutnyttj")).asBool(),
															 m_saCommand.Field(_T("trakt_point")).asString(),
															 m_saCommand.Field(_T("trakt_coordinates")).asString()
															 ));

		}
		doCommit();
	}
	catch(SAException &)
	{
		doRollback();
		return FALSE;
	}

	return 	bReturnValue;
}


BOOL CForrestDB::getTraktNoProperty(vecTransactionTrakt &vec)
{
	BOOL bReturnValue = FALSE;
	CString sSQL,tmp;
	try
	{
		vec.clear();
		sSQL.Format(_T("select * from %s where trakt_prop_id=:1"),
						TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= -1;	// --> No property added to this Stand (Trakt); 090812 p�d

		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			bReturnValue = TRUE;
			vec.push_back(CTransaction_trakt(m_saCommand.Field(1).asLong(),
															 (LPCTSTR)m_saCommand.Field(2).asString(),
															 (LPCTSTR)m_saCommand.Field(3).asString(),
															 (LPCTSTR)m_saCommand.Field(4).asString(),
															 (LPCTSTR)m_saCommand.Field(5).asString(),
															 (LPCTSTR)m_saCommand.Field(6).asString(),
																m_saCommand.Field(7).asLong(),
																m_saCommand.Field(8).asDouble(),
																m_saCommand.Field(9).asDouble(),
																m_saCommand.Field(10).asDouble(),
																m_saCommand.Field(11).asLong(),
															 (LPCTSTR)m_saCommand.Field(12).asString(),
																m_saCommand.Field(13).asDouble(),
																m_saCommand.Field(14).asLong(),
																m_saCommand.Field(15).asLong(),
																m_saCommand.Field(16).asLong(),
																m_saCommand.Field(17).asLong(),
															 (LPCTSTR)m_saCommand.Field(18).asString(),
																m_saCommand.Field(19).asLong(),
																m_saCommand.Field(20).asLong(),
															 (LPCTSTR)m_saCommand.Field(21).asString(),
															 (LPCTSTR)m_saCommand.Field(22).asString(),
															 (LPCTSTR)m_saCommand.Field(23).asString(),
															 (LPCTSTR)m_saCommand.Field(24).asString(),
															 (LPCTSTR)m_saCommand.Field(25).asString(),
															 (LPCTSTR)m_saCommand.Field(26).asString(),
																m_saCommand.Field(27).asLong(),
																m_saCommand.Field(_T("trakt_near_coast")).asLong(),
																m_saCommand.Field(_T("trakt_southeast")).asLong(),
																m_saCommand.Field(_T("trakt_region5")).asLong(),
																m_saCommand.Field(_T("trakt_part_of_plot")).asLong(),
															 (LPCTSTR)m_saCommand.Field(_T("trakt_si_h100_pine")).asString(),
															 (LPCTSTR)m_saCommand.Field(_T("trakt_prop_num")).asString(),
															 (LPCTSTR)m_saCommand.Field(_T("trakt_prop_name")).asString(),
															 (LPCTSTR)m_saCommand.Field(_T("trakt_notes")).asLongChar(),
															 (LPCTSTR)m_saCommand.Field(_T("trakt_created_by")).asString(),
															 (LPCTSTR)m_saCommand.Field(_T("created")).asString(),
															 m_saCommand.Field(_T("trakt_wside")).asLong(),
															 m_saCommand.Field(_T("trakt_for_stand_only")).asShort(),
															 m_saCommand.Field(_T("trakt_length")).asLong(),
															 m_saCommand.Field(_T("trakt_width")).asLong(),
															 //HMS-41 lagt in st�d f�r tillf�lligt utnyttjande 20200623 J�
															 m_saCommand.Field(_T("trakt_tillfutnyttj")).asBool(),
															 m_saCommand.Field(_T("trakt_point")).asString(),
															 m_saCommand.Field(_T("trakt_coordinates")).asString()
															 ));

		}
		doCommit();
	}
	catch(SAException &)
	{
		doRollback();
		return FALSE;
	}

	return 	bReturnValue;
}


BOOL CForrestDB::getTraktData(int trakt_id,double *m3sk,double *gy)
{
	BOOL bReturnValue = FALSE;
	CString sSQL,tmp;
	try
	{
		sSQL.Format(_T("select sum(tdata_m3sk_vol),sum(tdata_gy) from %s where tdata_trakt_id=:1 and tdata_data_type=1"),TBL_TRAKT_DATA);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()		= trakt_id;

		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			*m3sk =	m_saCommand.Field(1).asDouble();
			*gy =	m_saCommand.Field(2).asDouble();

			bReturnValue = TRUE;
		}
		doCommit();
	}
	catch(SAException &)
	{
		doRollback();
		return FALSE;
	}

	return 	bReturnValue;
}


BOOL CForrestDB::updConnectPropertyToStand(int trakt_id,int prop_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set trakt_prop_id=:1 where trakt_id=:2"),
									TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= prop_id;
		m_saCommand.Param(2).setAsLong()	= trakt_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


BOOL CForrestDB::updRemovePropertyFromStand(int trakt_id)
{
	CString sSQL;
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("update %s set trakt_prop_id=:1 where trakt_id=:2"),
									TBL_TRAKT);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= -1;	// --> -1 No property added to Stand; 090812 p�d
		m_saCommand.Param(2).setAsLong()	= trakt_id;

		m_saCommand.Execute();	
		doCommit();

		bReturn = TRUE;
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox(e.ErrText());
		doRollback();
		return FALSE;
	}

	return bReturn;
}


unsigned int FromFileWriter(SAPieceType_t &ePieceType, void *pBuf, unsigned int nLen, void *pAddlData);
SAString ReadWholeFile(/*const char **/ LPCTSTR sFilename);
void IntoFileReader(SAPieceType_t ePieceType, void *pBuf, unsigned int nLen, unsigned int nBlobSize, void *pAddlData);


SAString ReadWholeFile(/*const char **/ LPCTSTR sFilename)
{
	SAString s;
	//char sBuf[32*1024];
	TCHAR sBuf[32*1024];
	FILE *pFile = _tfopen(sFilename, _T("rb"));

	if(!pFile)
		SAException::throwUserException(-1, 
		(SAString)_T("Error opening file '%s'\n"), sFilename);
	do
	{
		unsigned int nRead = (unsigned int)fread(sBuf, 1*sizeof(TCHAR), sizeof(sBuf)/sizeof(TCHAR), pFile);
		s += SAString(sBuf, nRead);
	}
	while(!feof(pFile));
	
	fclose(pFile);
	
	return s;
}

static FILE *pFile = NULL;
static int nTotalBound;
static int nTotalRead;

unsigned int FromFileWriter(SAPieceType_t &ePieceType,	void *pBuf, unsigned int nLen, void *pAddlData)
{
	if(ePieceType == SA_FirstPiece)
	{
		//const char *sFilename = (const char *)pAddlData;
		LPCTSTR sFilename = (LPCTSTR)pAddlData;
		pFile = _tfopen(sFilename, _T("rb"));
		if(!pFile)
			SAException::throwUserException(-1, (SAString)_T("Can not open file '%s'"), sFilename);
		
		nTotalBound = 0;
	}
	
	unsigned int nRead = (unsigned int)fread(pBuf, 1, nLen, pFile);		//TODO: might need to change this if using Unicode and TCHAR
	nTotalBound += nRead;
	// show progress
	//printf("%d bytes of file bound\n", nTotalBound);
	
	if(feof(pFile))
	{
		ePieceType = SA_LastPiece;
		fclose(pFile);
		pFile = NULL;
	}
	
	return nRead;
}

void IntoFileReader(SAPieceType_t ePieceType, void *pBuf, unsigned int nLen, unsigned int nBlobSize, void *pAddlData)
{
	if(ePieceType == SA_FirstPiece || ePieceType == SA_OnePiece)
	{
		nTotalRead = 0;
		//const char *sFilename = (const char *)pAddlData;
		LPCTSTR sFilename = (LPCTSTR)pAddlData;
		pFile = _tfopen(sFilename, _T("wb"));
		if(!pFile)
			SAException::throwUserException(-1, (SAString)_T("Can not open file '%s' for writing"), sFilename);
	}

	CString csMsg;

	fwrite(pBuf, 1, nLen, pFile);
	nTotalRead += nLen;

//	csMsg.Format(_T("nLen %d\nnTotalRead %d"),nLen, nTotalRead);
//	AfxMessageBox(csMsg);

	// show progress
	//printf("%d bytes of %d read\n", nTotalRead, nBlobSize);

  if(ePieceType == SA_LastPiece || ePieceType == SA_OnePiece)
	{
		fclose(pFile);
		pFile = NULL;
	}
}

BOOL CForrestDB::saveImage(CString path, CTransaction_contacts rec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("update %s set picture=:1 where id = %d"), TBL_CONTACTS, rec.getID());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Param(1).setAsBLob() = ReadWholeFile(path);
		//m_saCommand.Param(2).setAsBLob(FromFileWriter, 10*1024, (void*)path);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::removeImage(CTransaction_contacts rec)
{
	CString csSQL;

	try
	{
		csSQL.Format(_T("update %s set picture = NULL where id = %d"), TBL_CONTACTS, rec.getID());
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &x)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/x.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CForrestDB::loadImage(CString path, int contact_id)
{
	CString csSQL,S;
	BOOL bRet = TRUE;
	short nIsNullCounter = 0;

	try
	{
		csSQL.Format(_T("select picture from %s where id=%d"), TBL_CONTACTS, contact_id);
		m_saCommand.setCommandText((SAString)csSQL);
		m_saCommand.Execute();

		m_saCommand.Field(1).setLongOrLobReaderMode(SA_LongOrLobReaderManual);
		while(m_saCommand.FetchNext())
		{
				if (m_saCommand.Field(1).isNull()) 
				{
					bRet = FALSE;
				}
				else
				{
					m_saCommand.Field(1).ReadLongOrLob(IntoFileReader,	// our callback to read BLob content into file
																						 32*1024,		// our desired piece size
																						 (void*)/*(const char*)*/(LPCTSTR)path	// additional data, filename in our example
																						 );
					nIsNullCounter++;
				}
		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nIsNullCounter > 0);
}

//#5008 PH 20160614
BOOL CForrestDB::addPropertyToObject(int prop_id,int obj_id) {

	CString sSQL;

	try {
		
		sSQL.Format(L"IF EXISTS(SELECT * "
		L"FROM INFORMATION_SCHEMA.TABLES "
		L"WHERE TABLE_NAME = '%s') "
		L"BEGIN "
			L"IF NOT EXISTS("
			L"SELECT 1 FROM %s WHERE prop_id=:1 "
			L"AND prop_object_id=:2) "
			L"BEGIN "
				L"INSERT INTO %s "
				L"(prop_id,prop_object_id,prop_name,prop_number,prop_other_comp_value,prop_status,prop_sort_order,prop_coord) "
				L"VALUES(:1,:2,'','',0,0,(SELECT ISNULL(MAX(prop_sort_order),0) FROM %s WHERE prop_object_id=:2)+1,'') "
			L"END "
		L"END",TBL_ELV_PROP,TBL_ELV_PROP,TBL_ELV_PROP,TBL_ELV_PROP);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong()	= prop_id;
		m_saCommand.Param(2).setAsLong()	= obj_id;
		m_saCommand.Execute();
		doCommit();

		return TRUE;

	}
	catch(SAException &e) {
		doRollback();
		AfxMessageBox(/*(const char*)*/e.ErrText().GetWideChars());
		return FALSE;
	}
}
