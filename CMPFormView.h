#pragma once

#include "MDIBaseFrame.h"
#include "Resource.h"
#include "ForrestDB.h"

/////////////////////////////////////////////////////////////////////////////////////
//	Report classes for Species; 060317 p�d

/////////////////////////////////////////////////////////////////////////////
// CCMPReportRec

class CCMPReportRec : public CXTPReportRecord
{
	int m_nIndex;
	int m_nID;
protected:

	class CIntItem : public CXTPReportRecordItemNumber
	{
	//private:
		int m_nValue;
	public:
		CIntItem(void) : CXTPReportRecordItemNumber(0)
		{
			m_nValue = 0;
			SetItemData(0);
		}

		CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
		{
			m_nValue = nValue;
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
			SetItemData(1);
		}

		BOOL getIsDirty(void)			
		{ 
			int nValue = (int)GetItemData();
			return (nValue == 0 ? FALSE : TRUE); 
		}

		int getIntItem(void)	{ return m_nValue; }
		void setIntItem(int v)	{ m_nValue = v; SetValue(m_nValue); }
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
			SetItemData(0);
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
				SetItemData(1);
		}

		BOOL getIsDirty(void)			
		{ 
			int nValue = (int)GetItemData();
			return (nValue == 0 ? FALSE : TRUE); 
		}
		CString getTextItem(void)	{ return m_sText; }
	};

public:

	CCMPReportRec(void)
	{
		m_nID	= -1;	
		AddItem(new CIntItem(0));
		AddItem(new CIntItem(0));
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CCMPReportRec(int id,CTransaction_county_municipal_parish rec)
	{
		m_nIndex	= id;
		m_nID	= rec.getID();
		AddItem(new CIntItem(rec.getCountyID()));
		AddItem(new CIntItem(rec.getMunicipalID()));
		AddItem(new CIntItem(rec.getParishID()));
		AddItem(new CTextItem(rec.getCountyName()));
		AddItem(new CTextItem(rec.getMunicipalName()));
		AddItem(new CTextItem(rec.getParishName()));
	}

	int getIndex(void)	{	return m_nIndex;	}

	int getID(void)	{	return m_nID;	}

	BOOL getIsDirty(int item)
	{
		if (item >= 0)
			return ((CTextItem*)GetItem(item))->getIsDirty();
		else
			return FALSE;
	}

	int getColumnInt(int item)	
	{ 
		return ((CIntItem*)GetItem(item))->getIntItem();
	}

	CString getColumnText(int item)	
	{ 
		if (item >= 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}

	void setColumnInt(int item,int value)	
	{ 
		((CIntItem*)GetItem(item))->setIntItem(value);
	}

};



/////////////////////////////////////////////////////////////////////////////////////
// CCMPFormView form view

class CCMPFormView : public CXTPReportView //CXTResizeFormView
{
	DECLARE_DYNCREATE(CCMPFormView)

// private:
	BOOL m_bInitialized;
	CString	m_sLangAbrev;
	CString m_sLangFN;

	CString m_sMessageCap;
	CString m_delCMPMsg;
	CString m_addNewCMPMsg;
	CString m_sAddCMPToProperty;

	CString m_sCMPValueError;

	vecTransatcionCMP m_vecCMPsData;
	vecTransatcionCMP m_vecCMPsDataChangedOrAdded;
	BOOL getCMPsFromDB(void);
	BOOL addCMPToReport();
	BOOL saveCMPsToDB(void);
	BOOL removeCMPFromDB();
	BOOL getCMPsFromReport();

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	enumACTION m_enumACTION;

protected:
	CCMPFormView();           // protected constructor used by dynamic creation
	virtual ~CCMPFormView();

	// My data members

	// My methods

	BOOL setupReport1(void);
	
	BOOL populateReport(void);
	BOOL clearReport(void);

	void LoadReportState(void);
	void SaveReportState(void);

	void setReportFocus(void);
public:
	void setFilterText(LPCTSTR,int);

	inline void doSaveToDB(void)	{ saveCMPsToDB(); }

	void doSetNavigationBar();

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


protected:

	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnImportDataFile(void);
	afx_msg void OnCreateCMP(void);

	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};