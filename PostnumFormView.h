#pragma once

#include "MDIBaseFrame.h"
#include "Resource.h"
#include "ForrestDB.h"

#include "ResLangFileReader.h"
/////////////////////////////////////////////////////////////////////////////////////
//	Report classes for Species; 060317 p�d

/////////////////////////////////////////////////////////////////////////////
// CPostnumReportRec

class CPostnumReportRec : public CXTPReportRecord
{
	int m_nID;
protected:
	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
			SetItemData(0);
		}

		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
				SetItemData(1);
		}

		BOOL getIsDirty(void)			
		{ 
			int nValue = (int)GetItemData();
			return (nValue == 0 ? FALSE : TRUE); 
		}
		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};
public:

	CPostnumReportRec(void)
	{
		m_nID	= -1;	
//		AddItem(new CIntItem(0));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CPostnumReportRec(int id,CTransaction_postnumber rec)
	{
		m_nID	= rec.getID();
		AddItem(new CTextItem(rec.getNumber()));
		AddItem(new CTextItem(rec.getName()));
	}

	BOOL getIsDirty(int item)
	{
		if (item >= 0)
			return ((CTextItem*)GetItem(item))->getIsDirty();
		else
			return FALSE;
	}

	int getID(void)
	{
		return m_nID;
	}

	CString getColumnText(int item)	
	{ 
		if (item >= 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}

};

/////////////////////////////////////////////////////////////////////////////////////
// CPostnumberFormView form view

class CPostnumberFormView : public CXTPReportView //CXTResizeFormView
{
	DECLARE_DYNCREATE(CPostnumberFormView)

// private:
	BOOL m_bInitialized;
	CString	m_sLangAbrev;
	CString m_sLangFN;

	RLFReader *m_pXML;
	StrMap m_LangStr;

	BOOL m_bIsPostnumberAdded;

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
	
	static long m_lLAST_RECORD_ID;
protected:
	CPostnumberFormView();           // protected constructor used by dynamic creation
	virtual ~CPostnumberFormView();

	// My data members

	// My methods
	vecTransactionPostnumber m_vecPostnumbersDataFromDB;
//	vecTransactionPostnumber m_vecPostnumbersDataChangedOrAdded;
	BOOL addPostnumbersToReport(void);
	BOOL getPostnumbersFromReport(void);
	BOOL getPostnumbersFromDB(void);
	BOOL savePostnumbersToDB(void);
	BOOL removePostnumberFromDB(void);
	int getNextPostnumberID(void);

	BOOL setupReport1(void);
	
	BOOL populateReport(void);
	BOOL clearReport(void);

	void LoadReportState(void);
	void SaveReportState(void);

public:
	void setFilterText(LPCTSTR,int);

	inline void doSaveToDB(void)	{ savePostnumbersToDB(); }

	void doSetNavigationBar();

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSetupStandReportView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


protected:

	//{{AFX_MSG(CMDIDBFormFrame)
//	afx_msg void OnKeyDown(UINT nChar,UINT nRepCnt,UINT nFlags);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnDestroy();
	afx_msg void OnShowWindow(BOOL bShow,UINT nStatus);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnImportDataFile(void);
	afx_msg void OnCreatePostnumbers(void);
//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
};