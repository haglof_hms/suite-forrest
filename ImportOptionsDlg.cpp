// ImportOptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "ImportOptionsDlg.h"

#include "SelectCategoryDlg.h"

#include "ResLangFileReader.h"

// CImportOptionsDlg dialog

IMPLEMENT_DYNAMIC(CImportOptionsDlg, CDialog)

BEGIN_MESSAGE_MAP(CImportOptionsDlg, CDialog)
	ON_BN_CLICKED(IDC_CHECK2, &CImportOptionsDlg::OnBnClickedContacts)
	ON_BN_CLICKED(IDOK, &CImportOptionsDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CATEGORY3_1, &CImportOptionsDlg::OnBnClickedCategory31)
END_MESSAGE_MAP()

CImportOptionsDlg::CImportOptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImportOptionsDlg::IDD, pParent)
{

}

CImportOptionsDlg::~CImportOptionsDlg()
{
}

void CImportOptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectCMPDlg)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);

	DDX_Control(pDX, IDC_CHECK1, m_wndCBox1);
	DDX_Control(pDX, IDC_CHECK2, m_wndCBox2);
	DDX_Control(pDX, IDC_CHECK3, m_wndCBox3);

	DDX_Control(pDX, IDC_EDIT3_1, m_wndEdit3_1);

	DDX_Control(pDX, IDC_CATEGORY3_1, m_wndBtnCategory);
	DDX_Control(pDX, IDOK, m_wndBtnOK);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP
}

BOOL CImportOptionsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_wndLbl1.SetBkColor(INFOBK);

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	m_wndEdit3_1.SetEnabledColor( BLACK, WHITE );
	m_wndEdit3_1.SetDisabledColor(  BLACK, INFOBK );
	m_wndEdit3_1.SetReadOnly( TRUE );

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING2920)));
			m_wndBtnOK.SetWindowText((xml.str(IDS_STRING2914)));
			m_wndBtnCancel.SetWindowText((xml.str(IDS_STRING2915)));

			m_wndLbl1.SetWindowText((xml.str(IDS_STRING2921)));
			m_wndCBox1.SetWindowText((xml.str(IDS_STRING2922)));
			m_wndCBox2.SetWindowText((xml.str(IDS_STRING2923)));
			m_wndCBox3.SetWindowText((xml.str(IDS_STRING2924)));

			m_wndBtnCategory.SetWindowText(xml.str(IDS_STRING211));
		}
		xml.clean();
	}

	m_wndCBox1.SetCheck( TRUE );
	m_wndCBox2.SetCheck( TRUE );
	m_wndCBox3.SetCheck( TRUE );

	isOkToImportProperties();
	isOkToImportContacts();

	return TRUE;
}


// We'll check "m_vecExcel_import", to see if there's
// sufficient data for import; 090220 p�d
// Sufficient data equals; County,Municipal,Parish and Propertyname
void CImportOptionsDlg::isOkToImportProperties(void)
{
	BOOL bFound = FALSE;
	if (m_vecExcel_import.size() == 0)
	{
		m_wndCBox1.SetCheck( FALSE );
		m_wndCBox1.EnableWindow( FALSE );
		m_wndCBox3.SetCheck( FALSE );
		m_wndCBox3.EnableWindow( FALSE );
	}
	else
	{
		for (UINT i = 0;i < m_vecExcel_import.size();i++)
		{
			CExcel_import rec = m_vecExcel_import[i];
//			if (!rec.getCounty().IsEmpty() && !rec.getMunicipal().IsEmpty()/* && !rec.getParish().IsEmpty() Not mandatory;090512 P�D*/ && !rec.getPropName().IsEmpty())
			if (!rec.getPropName().IsEmpty())	// Changed 090526 p�d
			{
				bFound = TRUE;
				break;
			}	// if (rec.getCounty().IsEmpty() || rec.getMunicipal().IsEmpty() || rec.getParish().IsEmpty() || rec.getPropName().IsEmpty())
		}	// for (UINT i = 0;i < m_vecExcel_import.size();i++)
		if (!bFound)
		{
			m_wndCBox1.SetCheck( FALSE );
			m_wndCBox1.EnableWindow( FALSE );
			m_wndCBox3.SetCheck( FALSE );
			m_wndCBox3.EnableWindow( FALSE );
		}
	}
}

void CImportOptionsDlg::isOkToImportContacts(void)
{
	BOOL bFound = FALSE;
	if (m_vecExcel_import.size() == 0)
	{
		m_wndCBox2.SetCheck( FALSE );
		m_wndCBox2.EnableWindow( FALSE );
	}
	else
	{
		for (UINT i = 0;i < m_vecExcel_import.size();i++)
		{
			CExcel_import rec = m_vecExcel_import[i];
			if (!rec.getOwnerName().IsEmpty())
			{
				bFound = TRUE;
				break;
			}	// if (rec.getCounty().IsEmpty() || rec.getMunicipal().IsEmpty() || rec.getParish().IsEmpty() || rec.getPropName().IsEmpty())
		}	// for (UINT i = 0;i < m_vecExcel_import.size();i++)
		if (!bFound)
		{
			m_wndCBox2.SetCheck( FALSE );
			m_wndCBox2.EnableWindow( FALSE );
		}
	}
}

// CImportOptionsDlg message handlers

void CImportOptionsDlg::OnBnClickedContacts()
{
	if (m_wndCBox2.GetCheck() == BST_UNCHECKED)
	{
		m_wndCBox3.SetCheck(BST_UNCHECKED);
		m_wndCBox3.EnableWindow(FALSE);
	}
	else if (m_wndCBox2.GetCheck() == BST_CHECKED)
	{
		m_wndCBox3.EnableWindow(TRUE);
		m_wndCBox3.SetCheck(BST_CHECKED);
	}
}

void CImportOptionsDlg::OnBnClickedOk()
{
	m_bIsImportProperty = m_wndCBox1.GetCheck();
	m_bIsImportOwners = m_wndCBox2.GetCheck();
	m_bIsMatchOwnersToProperty = m_wndCBox3.GetCheck();
	
	OnOK();
}

void CImportOptionsDlg::OnBnClickedCategory31()
{
	CString sCategories;
	CTransaction_category data;
	CSelectCategoryDlg *dlg = new CSelectCategoryDlg(this);
	if (dlg)
	{
		if (dlg->DoModal() == IDOK)
		{
			sCategories = dlg->getSelectedCategoriesStr();
			m_vecSelectedCatgoriesForContactData = dlg->getSelectedCategoriesForContact();
			m_wndEdit3_1.SetWindowText(sCategories);
			m_wndEdit3_1.setIsDirty();
		}

		delete dlg;
	}

}
