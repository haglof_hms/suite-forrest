// stdafx.cpp : source file that includes just the standard includes
// Forrest.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

#include "StdioFileEx.h"

#include "DBBaseClass_ADODirect.h"	// ... in DBTransaction_lib

#include "PropertyTabView.h"

#include "ResLangFileReader.h"


HINSTANCE m_hInstance;

bool global_IS_SEARCH_REPLACE_OPEN;

CString getResStr(UINT id)
{
	CString sText;
	sText.LoadString(id);

	return sText;
}

CString getToolBarResourceFN(void)
{
	CString sPath;
	CString sToolBarResPath;
	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sToolBarResPath.Format(_T("%s%s"),sPath,TOOLBAR_RES_DLL);

	return sToolBarResPath;
}

CString getDBExportedFuncName(enumFuncNames enum_func)
{
	typedef char *(*Func)(enumFuncNames);
	Func proc;
	CString	sFuncName;
	CString sPath;
	sPath.Format(_T("%s%s"),getModulesDir(),UMDATABASE_NAME);
	HINSTANCE hInst = AfxLoadLibrary(sPath);
	if (hInst != NULL)
	{
#ifdef UNICODE
				USES_CONVERSION;
				proc = (Func)GetProcAddress((HMODULE)hInst, W2A(GET_EXPORTED_FUNC_NAME) );
#else
				proc = (Func)GetProcAddress((HMODULE)hInst, (GET_EXPORTED_FUNC_NAME) );
#endif

		if (proc != NULL)
		{
			// call the function
			sFuncName = proc(enum_func);
		}	// if (proc != NULL)
		AfxFreeLibrary(hInst);
	}	// if (hInst != NULL)

	return sFuncName;
}

// Create database tables form SQL scriptfiles; 061109 p�d
BOOL runSQLScriptFile(LPCTSTR fn,LPCTSTR check_table)
{
	UINT nFileSize;
	TCHAR sBuffer[BUFFER_SIZE];	// 10 kb buffer

	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString S;

	try
	{

		if (fileExists(fn))
		{
			CFile file(fn,CFile::modeRead);
			nFileSize = file.Read(sBuffer,BUFFER_SIZE);
			file.Close();

			sBuffer[nFileSize] = '\0';

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,check_table))
								{
									pDB->setDefaultDatabase(sDBName);
									pDB->commandExecute(sBuffer);
									bReturn = TRUE;
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

BOOL connectToDB(CSQLServer_ADODirect *db)
{
	CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
	if (pDB)
	{
		db = pDB;
		return TRUE;
	}

	return FALSE;
}

// Create database table from string; 080627 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,BOOL do_create_table,CString database_name)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript;

	try
	{

		if (!script.IsEmpty())
		{

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (!database_name.IsEmpty())
					_tcscpy_s(sDBName,127,database_name);

				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d

							if (pDB->existsDatabase(sDBName))
							{
								if (do_create_table)
								{
									if (!pDB->existsTableInDB(sDBName,table_name))
									{
										pDB->setDefaultDatabase(sDBName);
										sScript.Format(script,table_name);
										pDB->commandExecute(sScript);
										bReturn = TRUE;
									}
								}
								else if (!do_create_table)
								{
									if (pDB->existsTableInDB(sDBName,table_name))
									{
										pDB->setDefaultDatabase(sDBName);
										sScript.Format(script,table_name);
										pDB->commandExecute(sScript);
										bReturn = TRUE;
									}
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (!script.IsEmpty())
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

BOOL runSQLScriptFileEx1(vecScriptFiles &vec,Scripts::actionTypes action)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	Scripts scripts;

	CString sScript;

	try
	{

		if (vec.size() > 0)
		{

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{

							for (UINT i = 0;i < vec.size();i++)
							{
								scripts = vec[i];

								if (!scripts.getDBName().IsEmpty())
											_tcscpy_s(sDBName,127,scripts.getDBName());

								// Set database as set in sDBName; 061109 p�d
								// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d

								if (pDB->existsDatabase(sDBName))
								{
									if (action == Scripts::TBL_CREATE)
									{
										if (!pDB->existsTableInDB(sDBName,scripts.getTableName()))
										{
											pDB->setDefaultDatabase(sDBName);
											sScript.Format(scripts.getScript(),scripts.getTableName());
											pDB->commandExecute(sScript);
											bReturn = TRUE;
										}	// if (!pDB->existsTableInDB(sDBName,scripts._TableName))
									}	// if (scripts._CreateTable)
									else if (action == Scripts::TBL_ALTER)
									{
										if (pDB->existsTableInDB(sDBName,scripts.getTableName()))
										{
											pDB->setDefaultDatabase(sDBName);
											sScript.Format(scripts.getScript(),scripts.getTableName(),scripts.getTableName());
											pDB->commandExecute(sScript);
											bReturn = TRUE;
										}	// if (pDB->existsTableInDB(sDBName,scripts._TableName))
									}	// else if (!scripts._CreateTable)

								}	// if (pDB->existsDatabase(sDBName))
							} // for (UINT i = 0;i < vec.size();i++)
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (!script.IsEmpty())
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}


BOOL setupPostnumberTable(LPCTSTR fn)
{
	CStringArray strArray;
	CString sSQL;
	CString sLine;
	CString sNumber;
	CString sName;
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	try
	{
		if (fileExists(fn))
		{
			CFile file(fn,CFile::modeNoTruncate | CFile::modeRead);
			CArchive ar(&file,CArchive::load);
			sLine = "";
			if (ar.ReadString(sLine))
			{
				if (sLine.Right(1) != ';')
					sLine += ';';
				do
				{
					if (!sLine.GetLength() == 0)
					{
						if (sLine.Right(1) != ';')
							sLine += ';';
						strArray.Add(sLine);
						sLine = "";
					}
				} while(ar.ReadString(sLine));
			}

			if (strArray.GetSize() == 0) return FALSE;

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);

				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
/*
					FILE *fp;

					if ((fp = fopen("C:\\Postnumbers.sql","w")) != NULL)
					{
						for (int i = 0;i < strArray.GetSize();i++)
						{
							sLine = strArray.GetAt(i);
							sNumber = split_str(sLine,0,';');
							sName = split_str(sLine,1,';');
							sSQL.Format(_T("insert into %s (id,postnumber,postname) values(%d,'%s','%s');\n"),TBL_POSTNUMBER,i+1,sNumber,sName);
							//AfxMessageBox(sSQL);
							fputs(sSQL,fp);
						}	
						fclose(fp);
					}
*/

					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists; 061129 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (pDB->existsTableInDB(sDBName,TBL_POSTNUMBER))
								{
									pDB->setDefaultDatabase(sDBName);
									for (int i = 0;i < strArray.GetSize();i++)
									{
										sLine = strArray.GetAt(i);
										sNumber = split_str(sLine,0,';');
										sName = split_str(sLine,1,';');
										sSQL.Format(_T("insert into %s (id,postnumber,postname) values(%d,'%s','%s')"),TBL_POSTNUMBER,i+1,sNumber,sName);
										pDB->commandExecute(sSQL);
									}
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)

				}	// if (_tcscmp(sLocation,"") != 0 &&
				bReturn = TRUE;
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))

		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
	return bReturn;
}

// Max buffer for split methods
#define EOL							0x0d
#define SEP							0x3b
#define MAX_BUFFER      1024  // 1 kb

BOOL setupCountyMunicipalAndParishTable(LPCTSTR fn)
{
	CStringArray strArray;
	CString sSQL;
	CString sLine;
	CString sCountyID;
	CString sMunicipalID;
	CString sParishID;
	CString sCountyName;
	CString sMunicipalName;
	CString sParishName;
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	try
	{

		if (fileExists(fn))
		{
			CFile file(fn,CFile::modeNoTruncate | CFile::modeRead);
			CArchive ar(&file,CArchive::load);
			sLine = "";
			if (ar.ReadString(sLine))
			{
				if (sLine.Right(1) != ';')
					sLine += ';';
				do
				{
					if (!sLine.GetLength() == 0)
					{
						if (sLine.Right(1) != ';')
							sLine += ';';
						strArray.Add(sLine);
						sLine = "";
					}
				} while(ar.ReadString(sLine));
			}
			if (strArray.GetSize() == 0) return FALSE;

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
/*
					FILE *fp;

					if ((fp = fopen("C:\\CountyMunicipalParish.sql","w")) != NULL)
					{
						for (int i = 0;i < strArray.GetSize();i++)
						{
							sLine = strArray.GetAt(i);
							sCountyID = split_str(sLine,0,';');
							sMunicipalID = split_str(sLine,1,';');
							sParishID = split_str(sLine,2,';');
							sCountyName = split_str(sLine,3,';');
							sMunicipalName = split_str(sLine,4,';');
							sParishName = split_str(sLine,5,';');
							sSQL.Format(_T("insert into %s (county_id,municipal_id,parish_id,county_name,municipal_name,parish_name) values(%d,%d,%d,'%s','%s','%s');\n"),
											TBL_COUNTY_MUNICIPAL_PARISH,
											_tstoi(sCountyID),
											_tstoi(sMunicipalID),
											_tstoi(sParishID),
											sCountyName,
											sMunicipalName,
											sParishName);
							fputs(sSQL,fp);
						}	
						fclose(fp);
					}
*/

					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists; 061129 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (pDB->existsTableInDB(sDBName,TBL_COUNTY_MUNICIPAL_PARISH))
								{
									pDB->setDefaultDatabase(sDBName);
									for (int i = 0;i < strArray.GetSize();i++)
									{
										sLine = strArray.GetAt(i);
										sCountyID = split_str(sLine,0,';');
										sMunicipalID = split_str(sLine,1,';');
										sParishID = split_str(sLine,2,';');
										sCountyName = split_str(sLine,3,';');
										sMunicipalName = split_str(sLine,4,';');
										sParishName = split_str(sLine,5,';');
										sSQL.Format(_T("insert into %s (county_id,municipal_id,parish_id,county_name,municipal_name,parish_name) values(%d,%d,%d,'%s','%s','%s')"),
											TBL_COUNTY_MUNICIPAL_PARISH,
											_tstoi(sCountyID),
											_tstoi(sMunicipalID),
											_tstoi(sParishID),
											sCountyName,
											sMunicipalName,
											sParishName);
										pDB->commandExecute(sSQL);
									}
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)

				}	// if (_tcscmp(sLocation,"") != 0 &&
				bReturn = TRUE;
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))

		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
	return bReturn;
}


LPARAM class_LParam::m_lpValue = -1;
CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;
	CString sDocTitle;

	class_LParam::m_lpValue = lp;
	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader xml;
	if (xml.Load(lang_fn))
	{
		sCaption = xml.str(idd);
	}
	xml.clean();

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				sDocTitle.Format(_T("%s"),sCaption);
				if(idd == IDD_FORMVIEW10 && ((SEARCH_REPLACE_INFO*)lp)->m_sFocus != _T(""))	// special case for the search window
				{
					sDocTitle = sDocTitle + _T(" (") + ((SEARCH_REPLACE_INFO*)lp)->m_sFocus + _T(")");
					pDocument->SetTitle(sDocTitle);
				}
				else if(idd != IDD_FORMVIEW10)
				{
					pDocument->SetTitle(sDocTitle);
				}

				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);

					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					sDocTitle.Format(_T("%s"),sCaption);
					if(idd == IDD_FORMVIEW10 && ((SEARCH_REPLACE_INFO*)lp)->m_sFocus != _T(""))	// special case for the search window
					{
						sDocTitle = sDocTitle + _T(" (") + ((SEARCH_REPLACE_INFO*)lp)->m_sFocus + _T(")");
						pDocument->SetTitle(sDocTitle);
					}
					else if(idd != IDD_FORMVIEW10)
					{
						pDocument->SetTitle(sDocTitle);
					}

					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}

CView *showFormViewEx(int idd,LPCTSTR lang_fn,LPARAM lp)
{
	CString sResStr=_T("");
	CString sDocName=_T("");
	CString sCaption=_T("");
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle=_T("");
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						pView->SendMessage(MSG_IN_SUITE,ID_SHOWVIEW_MSG,lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}


CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{	
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}


CWnd *getFormViewParentByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView->GetParent();
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}


void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
    COPYDATASTRUCT HSData;
    memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
	}
}

VOID splitName_Block_Unit(CString in,CString& name,CString& block,CString& unit)
{
	TCHAR szBuffer[255];
	int nColonPos = in.Find(_T(":"));
	CString sBlock;
	CString sUnit;
	CString sName;
	int nEndName = nColonPos;

	if (nColonPos == -1)
	{
		name = in;	
		block.Empty();
		unit.Empty();
		return;
	}

	_tcscpy_s(szBuffer,in);
	for (TCHAR *p=szBuffer+nColonPos-1;p > szBuffer;p--,nEndName--)
	{
		if (*p == ' ') break;
		sBlock += *p;
	}
	for (TCHAR *p=szBuffer+nColonPos+1;p < szBuffer + _tcslen(szBuffer);p++)
	{
		sUnit += *p;
	}
	for (TCHAR *p=szBuffer;p < szBuffer + nEndName - 1;p++)
	{
		sName += *p;
	}
	block = sBlock.MakeReverse();
	unit = sUnit;
	name = sName;
}

CString split_str(LPCTSTR str,int colNum,char sep)
{
	TCHAR *p = NULL,strBuff[MAX_BUFFER],strSep[5],strTemp[MAX_BUFFER];
	int poscnt = 1,cnt = 0,start = 0,end = 0,len = 0;
	_tcscpy_s(strTemp,MAX_BUFFER,str);
	strSep[0] = sep;
	strSep[1] = '\0';
	// Do a check that the last character equals a "sep" charcter
	if (strTemp[_tcslen(strTemp)-1] != sep)
	{
		_tcscat_s(strTemp,MAX_BUFFER,strSep);
	}
	TCHAR *buff = strTemp;
	len = (int)_tcslen(buff);
   // Check that the input string < MAX_BUFFER
   strBuff[0] = '\0';
   if (len < MAX_BUFFER - 1)
   {
      for (p = (TCHAR *)buff; p < buff + _tcslen(buff);p++)
      {
        if (*p == sep || *p == EOL)
        {
           end = poscnt;
           if (cnt == colNum) break;
           start = end;
           cnt++;
        }
        poscnt++;
      }
      cnt = 0;
      for (p = (TCHAR *)buff + start; p < buff + end;p++) 	
			{
        // -------------------------------------------------
        // Truncate to len, if the input string is longer
        // then the max len specified and break.
        // -------------------------------------------------
        if (cnt >= len && len != -1) break;

        if (*p != sep && *p != EOL)
        {
           strBuff[cnt++] = *p;
        }
      }
      strBuff[cnt] = '\0';
   }
   p = strBuff;
   return p;
}


BOOL hitTest_X(int hit_x,int limit_x,int w_x)
{
	BOOL bHitX_OK = FALSE;
	// Check if hit, in x, is within lim�ts; 080513 p�d
	// if w_x = 0, don't check this value; 080513 p�d
	if (w_x == 0) bHitX_OK = TRUE;
	else if (w_x > 0)
	{
		bHitX_OK = (hit_x >= limit_x && hit_x <= limit_x+w_x);
	}

	return bHitX_OK;
}
/*
BOOL isURL(CString url) 
{
	BOOL bReturn = FALSE;
	if (!url.IsEmpty())
		bReturn = (::IsValidURL(NULL, (LPCWSTR)url.GetBuffer(), 0) != S_OK);
	
	return bReturn;
}
*/

BOOL readSpecies(vecTransactionSpecies &vec)
{
	CStringArray strArray;
	CString sSQL;
	CString sLine;
	CString sCountyID;
	CString sMunicipalID;
	CString sParishID;
	CString sCountyName;
	CString sMunicipalName;
	CString sParishName;
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	try
	{
			vec.clear();
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists; 061129 p�d
							if (pDB->existsDatabase(sDBName))
							{
								pDB->setDefaultDatabase(sDBName);
								pDB->recordsetQuery(_T("select * from fst_species_table"));

								pDB->firstRec();
								do
								{
									vec.push_back(CTransaction_species(-1,
										pDB->getInt(_T("spc_id")),
										pDB->getInt(_T("p30_spec")),
										pDB->getStr(_T("spc_name")),
										pDB->getStr(_T("notes")),
										(getUserName())));

								} while (pDB->nextRec());						

								pDB->recordsetClose();
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)

				}	// if (_tcscmp(sLocation,"") != 0 &&
				bReturn = TRUE;
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP || MB_OK);
		bReturn = FALSE;
	}
	return bReturn;
}

// Enable/Disable Tabs in TabbedView; 081219 p�d
void enableTabPage(int idx,BOOL enable)
{
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
	if (pTabView != NULL)
	{
		pTabView->enablePage(idx,enable);
	}	// if (pTabView != NULL)
}

void activeTabPage(int idx)
{
	CPropertyTabView *pTabView = (CPropertyTabView *)getFormViewByID(IDD_FORMVIEW5);
	if (pTabView != NULL)
	{
		pTabView->activePage(idx);
	}	// if (pTabView != NULL)
}


CString StringFromNumeric(CString value)
{
	// Just return if string's empty; 090225 p�d
	if (value.IsEmpty()) return value;
	
	BOOL bIsNumeric = TRUE;
	double fValue = 0.0;
	CString sValue(value);
	TCHAR szBuffer[255];
	_tcscpy(szBuffer,value);
	for (TCHAR *p = szBuffer;p < _tcslen(szBuffer)+szBuffer;p++)
	{
		if (isalpha(*p) || *p == ' ' ||*p == '-' || *p == ';' || *p == '_' || *p == '>' || *p == '<' || *p == '!')
		{
			bIsNumeric = FALSE;
			break;
		}
	}
	if (bIsNumeric)
	{
		fValue = _tstof(value);
		sValue.Format(_T("%.0f"),fValue);
	}

	return sValue;
}

// Added 2009-04-28 p�d, from UMDataBase; 090428 p�d
BOOL readPostnumberTable(LPCTSTR fn,vecTransactionPostnumber &vec)
{
	CStringArray strArray;
	CString sLine;
	CString sNumber;
	CString sName;

	if (fileExists(fn))
	{
		ReadSelectedFile(CStdioFileEx::GetCurrentLocaleCodePage(),fn,strArray);
		if (strArray.GetSize() == 0) return FALSE;

		vec.clear();
		for (long i = 0;i < strArray.GetSize();i++)
		{
			sLine = strArray.GetAt(i);
			sNumber = split_str(sLine,0,';');
			sName = split_str(sLine,1,';');
			vec.push_back(CTransaction_postnumber(i+1,sName,sNumber,getUserName().MakeUpper()));
		}
		return TRUE;	
	}
	return FALSE;
}

BOOL readCountyMunicipalAndParishTable(LPCTSTR fn,vecTransatcionCMP &vec)
{
	CStringArray strArray;
	CString sLine;
	CString sCountyID;
	CString sMunicipalID;
	CString sParishID;
	CString sCountyName;
	CString sMunicipalName;
	CString sParishName;

	if (fileExists(fn))
	{

		ReadSelectedFile(CStdioFileEx::GetCurrentLocaleCodePage(),fn,strArray);

		if (strArray.GetSize() == 0) return FALSE;

		for (int i = 0;i < strArray.GetSize();i++)
		{
			sLine = strArray.GetAt(i);
			sCountyID = split_str(sLine,0,';');
			sMunicipalID = split_str(sLine,1,';');
			sParishID = split_str(sLine,2,';');
			sCountyName = split_str(sLine,3,';');
			sMunicipalName = split_str(sLine,4,';');
			sParishName = split_str(sLine,5,';');
			vec.push_back(CTransaction_county_municipal_parish(i+1,_tstoi(sCountyID),_tstoi(sMunicipalID),_tstoi(sParishID),
																												 sCountyName,sMunicipalName,sParishName,getUserName().MakeUpper()));
		}
		return TRUE;
	}	// if (fileExists(fn))
	return FALSE;
}

void ReadSelectedFile(IN const UINT nCodePage,LPCTSTR fn,CStringArray &arr)
{
	int				nCharCount = 0;
	CStdioFileEx	fileEx;
	CString			sText, sLine;

	// Set the code page. Will not make any difference for Unicode-Unicode or ANSI-ANSI using the
	// same code page
	fileEx.SetCodePage(nCodePage);

	// Open file
	if (fileEx.Open(fn, CFile::modeRead | CFile::typeText))
	{
		// Is file unicode?

		nCharCount = fileEx.GetCharCount();

		// Read first nCharCount lines
		for (long nLineCount = 0; nLineCount < nCharCount && fileEx.ReadString(sLine); nLineCount++)
		{
			if (sLine.Right(1) != ';')
				sLine += ';';
			arr.Add(sLine);
		}

		fileEx.Close();
	}
}


void setToolbarBtn(LPCTSTR rsource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show)
{
	if (pCtrl)
	{
		if (show)
		{
			pCtrl->SetTooltip(tool_tip);
			HICON hIcon = ExtractIcon(AfxGetInstanceHandle(), rsource_dll_fn, icon_id);
			if (hIcon) pCtrl->SetCustomIcon(hIcon);
		}
		else
			pCtrl->SetVisible(FALSE);
	}
}

//////////////////////////////////////////////////////////////////////////
// CExIconItem

CExIconItem::CExIconItem(CString text,int icon_id)
{
	m_sText = text;
	SetIconIndex(icon_id);
	SetCaption((text));
	m_dec = -1;
}
	
CExIconItem::CExIconItem(int num_of,int icon_id)
{
	if (num_of > 0)
	{
		m_sText.Format(_T("(%d)"),num_of);
		SetIconIndex(icon_id);
		SetCaption(m_sText);
	}
	m_dec = -1;
}

CExIconItem::CExIconItem(double value,int dec,int icon_id)
{
	TCHAR tmp[50];
	m_fValue = value;
	m_dec = dec; 
	SetIconIndex(icon_id);
	_stprintf(tmp,_T("%.*f"),m_dec,value);
	SetCaption((tmp));
}

void CExIconItem::OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
{
	TCHAR tmp[50];
	m_sText = szText;
	if (m_dec > -1)
	{
		m_fValue = _tstof(m_sText);
		_stprintf(tmp,_T("%.*f"),m_dec,_tstof(m_sText));
		SetCaption(tmp);
	}
	else
		SetCaption(m_sText);

}

CString getFilePath(LPCTSTR path)
{
  TCHAR drive[_MAX_DRIVE];
  TCHAR dir[_MAX_DIR];
  TCHAR fname[_MAX_FNAME];
  TCHAR ext[_MAX_EXT];

	_wsplitpath(path,drive,dir,fname,ext);

	CString sPath(dir),sDrive(drive);

	return sDrive+sPath;
}


//////////////////////////////////////////////////////////////////////////
// CExTextItem

CExTextItem::CExTextItem(CString sValue) 
: CXTPReportRecordItemText(sValue)
{
	m_sText = sValue;
}
void CExTextItem::OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
{
	m_sText = szText;
	SetValue(m_sText);
}

CString CExTextItem::getTextItem(void)	{ return m_sText; }
void CExTextItem::setTextItem(LPCTSTR text)	
{ 
	m_sText = text; 
	SetValue(m_sText);
}

void CExTextItem::setIsBold(void)		{ SetBold();	}

void CExTextItem::setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
void CExTextItem::setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
