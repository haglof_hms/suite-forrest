// ExcelSheetsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "ExcelSheetsDlg.h"
#include "ResLangFileReader.h"


//  CExcelSheetsDlg dialog

IMPLEMENT_DYNAMIC(CExcelSheetsDlg, CDialog)

CExcelSheetsDlg::CExcelSheetsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CExcelSheetsDlg::IDD, pParent)
{

}

CExcelSheetsDlg::~CExcelSheetsDlg()
{
}

void CExcelSheetsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_SHEET, m_cbSheets);
	DDX_Control(pDX, IDC_LIST_SHEET_DATA, m_clSheetData);
}


BEGIN_MESSAGE_MAP(CExcelSheetsDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO_SHEET, &CExcelSheetsDlg::OnCbnSelchangeComboSheet)
END_MESSAGE_MAP()


// CExcelSheetsDlg message handlers

BOOL CExcelSheetsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	setLanguage();

	for(int nLoop=0; nLoop<m_pExcel->GetSheetCount(); nLoop++)
	{
		m_cbSheets.AddString( m_pExcel->GetSheetName(nLoop) );
	}
	m_cbSheets.SetCurSel(0);

	PopulateReport();

	return TRUE;
}

void CExcelSheetsDlg::PopulateReport()
{
	m_clSheetData.DeleteAllItems();
	while( m_clSheetData.DeleteColumn(0) != 0 );

	// add columns
	for(int nCol = 0; nCol < m_pExcel->GetColCount(); nCol++)
	{
		m_clSheetData.InsertColumn(nCol, m_pExcel->GetCellValue(0, nCol));
		m_clSheetData.SetColumnWidth(nCol, 50);
	}

	// fill the columns and rows with data
	for(int nRow = 0; nRow < 5; nRow++)
	{
		m_clSheetData.InsertItem(nRow, _T(""));

		for(int nCol = 0; nCol<m_pExcel->GetColCount(); nCol++)
		{
			m_clSheetData.SetItemText(nRow, nCol, m_pExcel->GetCellValue(nRow+1, nCol));
		}
	}
}

void CExcelSheetsDlg::OnCbnSelchangeComboSheet()
{
	m_pExcel->SetCurrentSheet( m_cbSheets.GetCurSel() );
	PopulateReport();
}

void CExcelSheetsDlg::setLanguage(void)
{
	// Setup language filename; 051214 p�d
	CString sLangFN;
	sLangFN = getLanguageFN(getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT, DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			SetWindowText(xml.str(IDS_STRING2940));	// title
			GetDlgItem(IDC_STATIC_SHEET)->SetWindowText(xml.str(IDS_STRING2941));	// sheet

			GetDlgItem(IDOK)->SetWindowText((xml.str(IDS_STRING2914)));	// ok
			GetDlgItem(IDCANCEL)->SetWindowText((xml.str(IDS_STRING2915)));	// cancel
		}
		xml.clean();
	}
}
