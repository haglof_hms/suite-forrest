#include "stdafx.h"

#include "Resource.h"

#include "DragDropListBoxAndTreeCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// class CPADExtTreeCtrl

HTREEITEM GetNextTreeItem(const CTreeCtrl& treeCtrl, HTREEITEM hItem)
{
	// we return the next HTEEITEM for a tree such as:
	// Root (1)
	//		Child1 (2)
	//			xxxx (3)
	//			yyyy (4)
	//		Chiled2 (5)
	// Item (6)
	CString S;	
	// has this item got any children
	if (treeCtrl.ItemHasChildren(hItem))
	{
		return treeCtrl.GetNextItem(hItem, TVGN_CHILD);
	}
	else if (treeCtrl.GetNextItem(hItem, TVGN_NEXT) != NULL)
	{
		// the next item at this level
		return treeCtrl.GetNextItem(hItem, TVGN_NEXT);
	}
	else
	{
		// return the next item after our parent
		hItem = treeCtrl.GetParentItem(hItem);
		if (hItem == NULL)
		{
			// no parent
			return NULL;
		}

		while (treeCtrl.GetNextItem(hItem, TVGN_NEXT) == NULL)
		{
			hItem = treeCtrl.GetParentItem(hItem);
			if (hItem == NULL)
				return NULL;
		}

		// next item that follows our parent
		return treeCtrl.GetNextItem(hItem, TVGN_NEXT);
	}
}

IMPLEMENT_DYNAMIC( CPADExtTreeCtrl, CTreeCtrl )

BEGIN_MESSAGE_MAP( CPADExtTreeCtrl, CTreeCtrl )
	//{{AFX_MSG_MAP(CPADExtTreeCtrl)
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(TVN_BEGINDRAG, OnBeginDrag)

	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CPADExtTreeCtrl::CPADExtTreeCtrl()
{
	m_menuItemSelected = -1;
	m_bLDragging = FALSE;
	m_pTreeEdit = NULL;
	m_nSelectedLevel = -1;
	m_bIsTopLevelItem = FALSE;
	m_bIsDropTarget = FALSE;
}

CPADExtTreeCtrl::~CPADExtTreeCtrl()
{
}


void CPADExtTreeCtrl::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CString S;
	int nSelectedLevel;
	int nNextLevel;
	BOOL bCheckStatus;
	HTREEITEM hTreeItem = HitTest(point, &nFlags);
	HTREEITEM hHitItem = hTreeItem;
	// Get selected item and find out which items to change, based on selection.
  if ((hTreeItem != NULL))
  {
		if (TVHT_ONITEMSTATEICON & nFlags)
		{
			Select(hTreeItem, TVGN_CARET);

			nSelectedLevel = get_level(hTreeItem);

			if (ItemHasChildren(hTreeItem))
			{

				while (hTreeItem != NULL)
				{
					hTreeItem = GetNextTreeItem(*this,hTreeItem);
			
					nNextLevel = get_level(hTreeItem);
				}	// while (hTreeItem != NULL)
			}	// if (ItemHasChildren(hTreeItem))

		} // if (TVHT_ONITEMSTATEICON & nFlags)
		else
		{
			if (TVHT_ONITEM & nFlags)
			{
			}	// if (TVHT_ONITEM & nFlags)

		}
	}	// if ((hTreeItem != NULL))

	CTreeCtrl::OnLButtonDown(nFlags,point);
}


void CPADExtTreeCtrl::OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	*pResult = 0;

	m_hitemDrag = pNMTreeView->itemNew.hItem;
	m_hitemDrop = NULL;

	m_pDragImage = CreateDragImage(m_hitemDrag);  // get the image list for dragging
	// CreateDragImage() returns NULL if no image list
	// associated with the tree view control
	if( !m_pDragImage )
		return;

	m_bLDragging = TRUE;
	m_bIsDropTarget = FALSE;
	m_pDragImage->BeginDrag(0, CPoint(-15,-15));
	POINT pt = pNMTreeView->ptDrag;
	ClientToScreen( &pt );
	m_pDragImage->DragEnter(NULL, pt);
	SetCapture();

}

void CPADExtTreeCtrl::OnMouseMove(UINT nFlags, CPoint point)
{
	HTREEITEM	hitem;
	UINT		flags;

	if (m_bLDragging)
	{
		POINT pt = point;
		ClientToScreen( &pt );
		CImageList::DragMove(pt);
		if ((hitem = HitTest(point, &flags)) != NULL)
		{
			if (get_level(hitem) == 1 && !ItemHasChildren(hitem) && (TVHT_ONITEM & flags))
			{
				CImageList::DragShowNolock(FALSE);
				SelectDropTarget(hitem);
				m_hitemDrop = hitem;
				m_bIsDropTarget = TRUE;
				CImageList::DragShowNolock(TRUE);
			}
			else
			{
				SelectDropTarget(NULL);
				m_bIsDropTarget = FALSE;
			}
		}
	}

	CTreeCtrl::OnMouseMove(nFlags, point);
}

void CPADExtTreeCtrl::OnLButtonUp(UINT nFlags, CPoint point)
{
	CTreeCtrl::OnLButtonUp(nFlags, point);

  if(m_bLDragging)
	{
		m_bLDragging = FALSE;
		CImageList::DragLeave(this);
		CImageList::EndDrag();
		ReleaseCapture();

		delete m_pDragImage;

		// Remove drop target highlighting
		SelectDropTarget(NULL);

		if( m_hitemDrag == m_hitemDrop )
			return;

		// If Drag item is an ancestor of Drop item then return
		HTREEITEM htiParent = m_hitemDrop;
		while( (htiParent = GetParentItem( htiParent )) != NULL )
		{
			if( htiParent == m_hitemDrag ) 
            return;
		}

		Expand( m_hitemDrop, TVE_EXPAND ) ;
		
		if (m_bIsDropTarget)
		{
			HTREEITEM htiNew = copy_branch( m_hitemDrag, m_hitemDrop, TVI_FIRST );
			DeleteItem(m_hitemDrag);
			SelectItem( htiNew );
		}
		m_bIsDropTarget = FALSE;
	}
}

HTREEITEM CPADExtTreeCtrl::copy_branch( HTREEITEM htiBranch, HTREEITEM htiNewParent,HTREEITEM htiAfter /*= TVI_LAST*/ )
{
	HTREEITEM hChild;
  HTREEITEM hNewItem = copy_item( htiBranch, htiNewParent, htiAfter );
  hChild = GetChildItem(htiBranch);
  while( hChild != NULL)
  {
		// recursively transfer all the items
    copy_branch(hChild, hNewItem);  
    hChild = GetNextSiblingItem( hChild );
  }
  return hNewItem;
}

///////////////////////////////////////////////
///
/// 
///////////////////////////////////////////////
HTREEITEM CPADExtTreeCtrl::copy_item( HTREEITEM hItem, HTREEITEM htiNewParent,HTREEITEM htiAfter /*= TVI_LAST*/ )
{
   TV_INSERTSTRUCT         tvstruct;
   HTREEITEM                       hNewItem;
   CString                         sText;

   // get information of the source item
   tvstruct.item.hItem = hItem;
   tvstruct.item.mask = TVIF_CHILDREN | TVIF_HANDLE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
   
	 GetItem(&tvstruct.item);  
   sText = GetItemText( hItem );


   tvstruct.item.cchTextMax = sText.GetLength();
	 tvstruct.item.pszText = sText.LockBuffer();

   // Insert the item at proper location
   tvstruct.hParent = htiNewParent;
   tvstruct.hInsertAfter = htiAfter;
   tvstruct.item.mask = TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT;
   hNewItem = InsertItem(&tvstruct);
   sText.ReleaseBuffer();
   
   // Now copy item data and item state.
   SetItemData( hNewItem, GetItemData( hItem ));
   SetItemState( hNewItem, GetItemState( hItem, TVIS_STATEIMAGEMASK ),TVIS_STATEIMAGEMASK );

   return hNewItem;
}


int CPADExtTreeCtrl::get_level(HTREEITEM hItem)
{
	int nLevel = 0;
	while ((hItem = GetParentItem( hItem )) != NULL)
	{
		nLevel++;
	}
	return nLevel;
}


///////////////////////////////////////////////////////////////////////////////
//
IMPLEMENT_DYNAMIC(CMyDDListBox, CListCtrl)

CMyDDListBox::CMyDDListBox():
m_pDropWnd(NULL),
m_bDragging(FALSE),
m_nDragIndex(-1),
m_hDropItem(NULL)
{}

CMyDDListBox::~CMyDDListBox()
{}


BEGIN_MESSAGE_MAP(CMyDDListBox, CListCtrl)
	ON_NOTIFY_REFLECT(LVN_BEGINDRAG, OnBeginDrag)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomDraw)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()



// CMyDDListBox message handlers

void CMyDDListBox::OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult)
{
	// For this notification, the structure is actually a
	// NMLVCUSTOMDRAW that tells you what's going on with the custom
	// draw action. So, we'll need to cast the generic pNMHDR pointer.
	NMLVCUSTOMDRAW* pCD = (NMLVCUSTOMDRAW*)pNMHDR;
	int row = pCD->nmcd.dwItemSpec;
	if(row < 0 || row > GetItemCount() - 1)
	{
		if(pCD->nmcd.dwDrawStage == CDDS_PREPAINT)
		{
			*pResult = CDRF_NOTIFYSUBITEMDRAW;
		}
		else
		{
			*pResult = CDRF_DODEFAULT;
		}
		return;
	}

	switch(pCD->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYSUBITEMDRAW; // Ask for subitem notifications
		break;

	case CDDS_ITEMPREPAINT:
		*pResult = CDRF_NOTIFYSUBITEMDRAW;
		break;

	case CDDS_ITEMPREPAINT|CDDS_SUBITEM:
		{
			*pResult = CDRF_DODEFAULT;
			break;
		}

	default: // It wasn't a notification that was interesting to us
		*pResult = CDRF_DODEFAULT;
	}
}

void CMyDDListBox::OnBeginDrag(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	*pResult = 0;

	// Set up for dragging
	m_nDragIndex	= pNMLV->iItem;
	m_bDragging		= TRUE;
	m_hDropItem		= NULL;
	m_pDropWnd		= NULL;

	SetCapture(); // capture WM_ messages even if outside this control

	// Change mouse cursor
	::SetCursor(LoadCursor(m_hInstance, MAKEINTRESOURCE(IDC_CURSOR1)));
}

void CMyDDListBox::OnMouseMove(UINT nFlags, CPoint point)
{
	UINT flags;

	if( m_bDragging )
	{
		CPoint pt(point);
		ClientToScreen(&pt);

		// Turn off highlighting on previous drop target
		if( m_pDropWnd && m_hDropItem )
		{
			((CPADExtTreeCtrl*)m_pDropWnd)->SelectDropTarget(NULL);
		}

		// Check for new drop target
		CWnd* pDropWnd = WindowFromPoint(pt);
		if( pDropWnd )
		{
			// Store drop target
			m_pDropWnd = pDropWnd;

			// Check target kind
			if( pDropWnd->IsKindOf(RUNTIME_CLASS(CPADExtTreeCtrl)) )
			{
				pDropWnd->ScreenToClient(&pt);

				// Set up new drop target & highlight it
				m_hDropItem = ((CPADExtTreeCtrl*)pDropWnd)->HitTest(pt, &flags);
				((CPADExtTreeCtrl*)pDropWnd)->SelectDropTarget(m_hDropItem);

				// Set focus to target window
				m_pDropWnd->SetFocus();
			}
			else
			{
				m_pDropWnd = NULL;
				m_hDropItem = NULL;
			}
		}
	}

	CListCtrl::OnMouseMove(nFlags, point);
}

void CMyDDListBox::OnLButtonUp(UINT nFlags, CPoint point)
{
	HTREEITEM hItem;
	CString S;

	CListCtrl::OnLButtonUp(nFlags, point);

	if( m_bDragging )
	{
		m_bDragging = FALSE;

		ReleaseCapture();

		// Check for a valid drop target
		if( m_pDropWnd && m_hDropItem )
		{
			CPADExtTreeCtrl *pTree = (CPADExtTreeCtrl*)m_pDropWnd;
			ASSERT_KINDOF(CPADExtTreeCtrl, m_pDropWnd);

			// Turn off highlighting on drop target
			pTree->SelectDropTarget(NULL);

			// Make sure we got a root item
			if(! pTree->GetParentItem(m_hDropItem) ) return;

			// Unselect all items in tree
			pTree->SelectAll(0);

			if (pTree->ItemHasChildren(m_hDropItem) > 0)
			{
				::MessageBox(this->GetSafeHwnd(),m_sMsgHasChildren,m_sMsgCap,MB_ICONINFORMATION | MB_OK);
				return;
			}
			if (get_level(*pTree,m_hDropItem) != 1)
			{
				::MessageBox(this->GetSafeHwnd(),m_sMsgWrongLevel,m_sMsgCap,MB_ICONINFORMATION | MB_OK);
				return;
			}

			// Drop all selected items
			for( int i = 0; i < GetItemCount(); i++ )
			{
				if( GetItemState(i, LVIS_SELECTED) )
				{
					// Drop item into tree control
					hItem = pTree->InsertItem( GetItemText(i, 0), 0, 0, m_hDropItem );

					//S.Format(_T("GetItemData(%d) %d\n"),i,GetItemData(i));
					//OutputDebugString(S);
					pTree->SetItemData( hItem, GetItemData(i) );
					pTree->SetItemColor(hItem, BLUE); //0x000000ff);
					DeleteItem( i );
					i--;

					// Expand branch & select dropped item
//					pTree->SelectItem( hItem );
//					pTree->Expand( hItem, TVE_COLLAPSE );
				}
			}
		}

		// Change mouse cursor back to normal
		::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	}
}


int CMyDDListBox::get_level(const CTreeCtrl& tree,HTREEITEM hItem)
{
	CString S;
	int nLevel = 0;

	while ((hItem = tree.GetParentItem( hItem )) != NULL)
	{
		nLevel++;
	}
	return nLevel;
}
