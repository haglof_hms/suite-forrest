#pragma once

#include "Resource.h"

#include "ForrestDB.h"



//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CContactsSelectListFrame; 070108 p�d

class CContactsReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CContactsReportFilterEditControl)
public:
	CContactsReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

//////////////////////////////////////////////////////////////////////////////
// CContactsSelListFormView form view

class CContactsSelListFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CContactsSelListFormView)

protected:
	CContactsSelListFormView();           // protected constructor used by dynamic creation
	virtual ~CContactsSelListFormView();

	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	CString m_sFilterOn;

	int m_nSelectedColumn;

	//CMyReportCtrl m_wndContactsReport;
	vecTransactionContacts * m_vecContactsData; //Changed to pointer 2016-06-10 PH
	BOOL setupReport(void);

	void getContacts(void);
	void getContacts(CString); //Added 2016-06-10 PH
	
	CXTPReportSubListControl m_wndSubList;
	CContactsReportFilterEditControl m_wndFilterEdit;
	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	void LoadReportState(void);
	void SaveReportState(void);

	int m_nDBIndex;

	int objId;

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CImageList m_ilIcons;

	int m_nDoAddPropertyOwners;

	CStringArray m_sarrTypeOfContact;

	SEARCH_REPLACE_INFO m_Info;
	CString m_sSQLSearchString;
public:
	// Need to set this method public; 070126 p�d
	void populateReport(void);
	void getContactsForObjectId(int); //Added 2016-06-10 PH
	void setTransactionContacts(vecTransactionContacts * vec) {  m_vecContactsData=vec;populateReport(); } //Added 2016-06-13 PH

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

	void setFilterWindow(void);
protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnShowFieldChooser();
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
	afx_msg void OnPrintPreview();
	afx_msg void OnRefresh();
	afx_msg void OnAddContactToProperty();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	afx_msg void OnUpdateDBToolbarBtnAdd(CCmdUI* pCmdUI);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
