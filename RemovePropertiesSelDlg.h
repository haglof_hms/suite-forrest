#pragma once

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////
// CRemovePropertySelRec

class CRemovePropertySelRec : public CXTPReportRecord
{
	int m_nID;
	CTransaction_property recProp;
protected:
	class CCheckItem : public CXTPReportRecordItem
	{
		public:
		// Constructs record item with the initial checkbox value.
		CCheckItem(BOOL bCheck)
		{
			HasCheckbox(TRUE);
			SetChecked(bCheck);
		}

		virtual BOOL getChecked(void)
		{
			return IsChecked()? TRUE: FALSE;
		}

		virtual void setChecked(BOOL bCheck)
		{
			SetChecked(bCheck);
		}
	};

	class CTextItem : public CXTPReportRecordItemText
	{
	//private:
		CString m_sText;
	public:
		CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
		{
			m_sText = sValue;
		}
		void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
		{
				m_sText = szText;
				SetValue(m_sText);
		}

		CString getTextItem(void)	{ return m_sText; }
		void setTextItem(LPCTSTR text)	
		{ 
			m_sText = text; 
			SetValue(m_sText);
		}
	};

public:

	CRemovePropertySelRec(void)
	{
		m_nID	= -1;	
		recProp = CTransaction_property();
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
		AddItem(new CTextItem(_T("")));
	}

	CRemovePropertySelRec(int id,BOOL enable,BOOL check,CTransaction_property& rec)
	{
		m_nID	= id;
		recProp = rec;
		CString sPropName;
		AddItem(new CCheckItem(check));
		AddItem(new CTextItem(rec.getCountyName()));
		AddItem(new CTextItem(rec.getMunicipalName()));
		AddItem(new CTextItem(rec.getParishName()));
		sPropName.Format(_T("%s %s:%s"),
			rec.getPropertyName(),
			rec.getBlock(),
			rec.getUnit());
		AddItem(new CTextItem(sPropName));

		this->GetItem(0)->SetEditable(enable);
		this->GetItem(1)->SetEditable(enable);
		this->GetItem(2)->SetEditable(enable);
		this->GetItem(3)->SetEditable(enable);
		this->GetItem(4)->SetEditable(enable);
		if (enable)
		{
			this->GetItem(0)->SetBackgroundColor(WHITE);
			this->GetItem(1)->SetBackgroundColor(WHITE);
			this->GetItem(2)->SetBackgroundColor(WHITE);
			this->GetItem(3)->SetBackgroundColor(WHITE);
			this->GetItem(4)->SetBackgroundColor(WHITE);
		}
		else
		{
			this->GetItem(0)->SetBackgroundColor(RED);
			this->GetItem(1)->SetBackgroundColor(RED);
			this->GetItem(2)->SetBackgroundColor(RED);
			this->GetItem(3)->SetBackgroundColor(RED);
			this->GetItem(4)->SetBackgroundColor(RED);
		}
}


	CTransaction_property& getRecord(void)	{ return recProp; }

	int getID(void)	{	return m_nID;	}

	CString getColumnText(int item)	
	{ 
		if (item > 0)
			return ((CTextItem*)GetItem(item))->getTextItem();
		else
			return _T("");
	}
	
	void setColumnText(int item,LPCTSTR text)	{ if (item > 0)	((CTextItem*)GetItem(item))->setTextItem(text);	}

	BOOL getColumnCheck(int item)	{	return ((CCheckItem*)GetItem(item))->getChecked();	}

	void setColumnCheck(int item,BOOL bChecked)	{	((CCheckItem*)GetItem(item))->setChecked(bChecked);	}


};

// CRemovePropertiesSelDlg dialog

class CRemovePropertiesSelDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CRemovePropertiesSelDlg)

	CString m_sLangFN;

	BOOL m_bInitialized;

	CMyExtStatic m_wndLbl6_1;
	CMyExtStatic m_wndLbl6_2;
	CMyExtStatic m_wndLbl6_3;
	CMyExtStatic m_wndLbl6_4;
	CMyExtStatic m_wndLbl6_5;
	CMyExtStatic m_wndLbl6_6;
	CMyExtStatic m_wndLbl6_7;
	CMyExtStatic m_wndLbl6_8;
	
	CMyExtEdit m_wndEdit6_1;
	CMyExtEdit m_wndEdit6_2;
	CMyExtEdit m_wndEdit6_3;
	CMyExtEdit m_wndEdit6_4;
	CMyExtEdit m_wndEdit6_5;
	CMyExtEdit m_wndEdit6_6;
	CMyExtEdit m_wndEdit6_7;

	CButton m_wndBtnSearch;
	CButton m_wndBtnAll;
	CButton m_wndBtnRemove;
	CButton m_wndBtnSelAll;
	CButton m_wndBtnDeSelAll;
	CButton m_wndBtnCancel;

	CMyReportCtrl m_wndReport;

	vecTransactionProperty m_vecProperties;

	CForrestDB *m_pDB;
protected:
	void setupReport(void);

	void getProperties(void);

	void populateReport(void);
public:
	CRemovePropertiesSelDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRemovePropertiesSelDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG6 };

	void setDBConnection(CForrestDB *db)	{		m_pDB = db;	}

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRemovePropertiesSelDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
};
