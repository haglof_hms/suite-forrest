// ImportDataFormView.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "ImportDataFormView.h"
#include "ColumnMatchDlg.h"
#include "SelectCMPDlg.h"
#include "ImportOptionsDlg.h"
#include "AddChangeObjIDDlg.h"
#include "PropertyFormView.h"
#include "ResLangFileReader.h"
#include "ExcelImportLibXl.h"
#include "ExcelSheetsDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CImportDataReportView

IMPLEMENT_DYNCREATE(CImportDataReportView,  CMyReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CImportDataReportView,  CMyReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_COMMAND(ID_TBBTN_OPEN, OnImportDataFile)
	ON_COMMAND(ID_TBBTN_CREATE, OnCreatePropsAndOwnersFile)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_CREATE, OnUpdateCreatePropsAndOwnersTBtn)
	ON_COMMAND(ID_TOOLS_ADD_CHANGE_OBJID, OnToolsAddChangeObjID)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_ADD_CHANGE_OBJID, OnUpdateToolsTBtn1)
	ON_COMMAND(ID_TOOLS_CREATE_EXCEL_TMPL, OnToolsCreateExcelTemplate)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_CREATE_EXCEL_TMPL, OnUpdateToolsTBtn2)

	ON_NOTIFY(XTP_NM_REPORT_SELCHANGED, XTP_ID_REPORT_CONTROL, OnReportSelChanged)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportItemValueClick)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)
END_MESSAGE_MAP()

CImportDataReportView::CImportDataReportView()
	: CMyReportView()
{
	m_pDB = NULL;
	m_bInitialized = FALSE;
	m_bIsCreatePropsAndOwners = FALSE;
}

CImportDataReportView::~CImportDataReportView()
{
}

void CImportDataReportView::OnInitialUpdate()
{
	CMyReportView::OnInitialUpdate();

	if (!m_bInitialized)
	{
		// Setup language filename; 051214 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		//#5008 PH 20160614
		elvObjId=-1;

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
	
		m_pViewFrame = (CMDIImportDataFrame*)getFormViewByID(IDD_FORMVIEW7)->GetParent();

		setupReport();

		resetColumnMap();

		m_bInitialized = TRUE;

		if (m_pViewFrame != NULL)
		{
			m_pViewFrame->getProgressCtrl().SetRange(0,100);
			m_pViewFrame->getProgressCtrl().SetPos(0);
			m_pViewFrame->getProgressCtrl().SetStep(1);
			m_pViewFrame->getStatusBar().SetPaneText(1,m_sMsgStartImport);
		}

	}
}

BOOL CImportDataReportView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}
	}
	return CMyReportView::OnCopyData(pWnd, pData);
}

void CImportDataReportView::OnDestroy()
{
//	SaveReportState();

	if (m_pDB != NULL)
		delete m_pDB;

	m_mapProps.clear();
	m_vecExcel_import.clear();

	CMyReportView::OnDestroy();	
}

BOOL CImportDataReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CMyReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CImportDataReportView diagnostics

#ifdef _DEBUG
void CImportDataReportView::AssertValid() const
{
	CMyReportView::AssertValid();
}

void CImportDataReportView::Dump(CDumpContext& dc) const
{
	CMyReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CImportDataReportView message handlers

// CImportDataReportView message handlers
void CImportDataReportView::OnSize(UINT nType,int cx,int cy)
{
	CMyReportView::OnSize(nType,cx,cy);
}

void CImportDataReportView::OnSetFocus(CWnd*)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
/*
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
*/
}

// Create and add Assortment settings reportwindow
BOOL CImportDataReportView::setupReport(void)
{

	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
				// Get text from languagefile; 080710 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					m_sMsgCap = xml.str(IDS_STRING207);
					m_sMsgWrongExtension.Format(_T("%s\n%s\n%s\n%s\n\n"),
						xml.str(IDS_STRING3120),
						xml.str(IDS_STRING3121),
						xml.str(IDS_STRING3122),
						xml.str(IDS_STRING3123));
					m_sMsgMissingPropNameNumber = xml.str(IDS_STRING3247);

					//#5010 PH 20160628
					m_sMsgToManyProp = xml.str(IDS_STRING3248);
					m_sMsgToManyPropCap = xml.str(IDS_STRING3249);

					m_sMsgNumOfProps = xml.str(IDS_STRING3241);
					m_sMsgAdding = xml.str(IDS_STRING3242);
					m_sMsgImportDone = xml.str(IDS_STRING3243);
					m_sMsgStartImport = xml.str(IDS_STRING3244);

					m_sMsgOpenEXCEL = xml.str(IDS_STRING32786);

					m_sTmplFileName = xml.str(IDS_STRING32787);

					//HMS-28
					m_sMsgShareErr1 = xml.str(IDS_STRING2943);
					m_sMsgShareErr2 = xml.str(IDS_STRING2944);
					m_sMsgProp = xml.str(IDS_STRING256);
					m_sMsgOwner = xml.str(IDS_STRING2481);
					m_sMsgShare = xml.str(IDS_STRING263);

					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

					GetReportCtrl().ShowWindow( SW_NORMAL );
					GetReportCtrl().ShowGroupBy( TRUE );
					// "L�n"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml.str(IDS_STRING253)), 120));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();
					// "Kommun"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml.str(IDS_STRING254)), 120));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();
					// "F�rsamling"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml.str(IDS_STRING255)), 120));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					pCol->GetEditOptions()->AddComboButton();
					//	"Fastighetsnummer"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml.str(IDS_STRING2560)), 100));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Fastighet med block och enhet"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml.str(IDS_STRING256)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// S�k-ID
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, (xml.str(IDS_STRING2701)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// Mottagarref. added 2012-11-22 P�D (Ansj� export)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_6, (xml.str(IDS_STRING2706)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Andel"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_7, (xml.str(IDS_STRING263)), 50));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// Kontakt
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_8, (xml.str(IDS_STRING290)), 50));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "�gare"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_9, (xml.str(IDS_STRING2481)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Adress"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_10, (xml.str(IDS_STRING266)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "C/O Adress"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_11, (xml.str(IDS_STRING324)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Postnummer"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_12, (xml.str(IDS_STRING2670)), 60));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Postadress"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_13, (xml.str(IDS_STRING267)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Tfn. arb."
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_14, (xml.str(IDS_STRING268)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Tfn. hem"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_15, (xml.str(IDS_STRING269)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Mobilnummer"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_16, (xml.str(IDS_STRING270)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Fax"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_17, (xml.str(IDS_STRING271)), 80));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Region"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_18, (xml.str(IDS_STRING2702)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Land"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_19, (xml.str(IDS_STRING2703)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Noteringar"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_20, (xml.str(IDS_STRING2704)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "E-post"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_21, (xml.str(IDS_STRING2700)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
					// "Personnummer"
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_22, (xml.str(IDS_STRING2705)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

					// Bankgiro; added 2012-11-11 P�D (Ansj� export)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_23, (xml.str(IDS_STRING2370)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

					// Plusgiro; added 2012-11-11 P�D (Ansj� export)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_24, (xml.str(IDS_STRING2371)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

					// Bankkonto; added 2012-11-11 P�D (Ansj� export)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_25, (xml.str(IDS_STRING2372)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

					// Leverant�rsnummer; added 2012-11-11 P�D (Ansj� export)
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_26, (xml.str(IDS_STRING2373)), 150));
					pCol->SetHeaderAlignment(DT_WORDBREAK);
					pCol->GetEditOptions()->m_bAllowEdit = TRUE;
					pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

					GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
					GetReportCtrl().SetMultipleSelection( TRUE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().FocusSubItems(TRUE);
					GetReportCtrl().AllowEdit(TRUE);
					GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml.Load(m_sLangFN))
			xml.clean();
		}	// if (fileExists(m_sLangFN))

	return TRUE;
}


void CImportDataReportView::OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
}

void CImportDataReportView::OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		CXTPReportColumn *pCol = pItemNotify->pColumn;
		CXTPReportRow *pRow = pItemNotify->pRow;
		if (pRow != NULL && pCol != NULL)
		{
			CImportReportDataRec *pRec = (CImportReportDataRec*)pRow->GetRecord();
			if (pRec != NULL)
			{
				if (pCol->GetItemIndex() == COLUMN_0)
				{
					addMunicipalConstraints(pRec->getColumnDwData(COLUMN_0));
					pRec->setColumnText(COLUMN_1,_T(""));
					pRec->setColumnText(COLUMN_2,_T(""));
				}	// if (pCol->GetItemIndex() == COLUMN_0)
				else if (pCol->GetItemIndex() == COLUMN_1)
				{
					addParishConstraints(pRec->getColumnDwData(COLUMN_0),pRec->getColumnDwData(COLUMN_1));
					pRec->setColumnText(COLUMN_2,_T(""));
				}	// else if (pCol->GetItemIndex() == COLUMN_1)
			}	// if (pRec != NULL)
		}	// if (pRow != NULL)
	}	// if (pItemNotify != NULL)
}

void CImportDataReportView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
		CXTPReportColumn *pCol = pItemNotify->pColumn;
		CXTPReportRow *pRow = pItemNotify->pRow;
		if (pRow != NULL && pCol != NULL)
		{
			if (pCol->GetItemIndex() == COLUMN_1)
			{
				CImportReportDataRec *pRec = (CImportReportDataRec*)pRow->GetRecord();
				if (pRec != NULL)
				{
					addParishConstraints(pRec->getColumnDwData(COLUMN_0),pRec->getColumnDwData(COLUMN_1));
//					addMunicipalConstraints(pRec->getColumnDwData(COLUMN_0));
				}	// if (pRec != NULL)
			}	// if (pCol->GetItemIndex() == COLUMN_1)
		}	// if (pRow != NULL && pCol != NULL)
	}	// if (pItemNotify != NULL)
}

void CImportDataReportView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;

	if (lpNMKey->nVKey != VK_F8) return;

	BOOL bCopyARange = FALSE;
	int start_row;
	CImportReportDataRec *pRec = NULL;
	CImportReportDataRec *pLastRec = NULL;
	CXTPReportColumn *pCol = GetReportCtrl().GetFocusedColumn();
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	if (pRows != NULL && pCol != NULL && pRow != NULL)
	{
		// Try to find the first empty row and start copy data from
		// the Last nonm emty row; 080710 p�d
		for (start_row = 0;start_row < pRows->GetCount();start_row++)
		{
			pRec = (CImportReportDataRec *)pRows->GetAt(start_row)->GetRecord();
			if (pRec->getColumnText(pCol->GetItemIndex()).IsEmpty())
			{
				if (start_row > 0)
				{
					pLastRec = (CImportReportDataRec *)pRows->GetAt(start_row-1)->GetRecord();
				}
				bCopyARange = TRUE;
				break;
			}	// if (pRec->getColumnText(pCol->GetItemIndex()).IsEmty())
		}	// for (int i = 0;i < pRows->GetCount();i++)
		if (bCopyARange)
		{
			if (start_row < pRow->GetIndex())
			{
				for (int copy_row = start_row;copy_row <= pRow->GetIndex();copy_row++)
				{
					pRec = (CImportReportDataRec *)pRows->GetAt(copy_row)->GetRecord();
					pRec->setColumnText(pCol->GetItemIndex(),pLastRec->getColumnText(pCol->GetItemIndex()));
					pRec->setColumnDwData(pCol->GetItemIndex(),pLastRec->getColumnDwData(pCol->GetItemIndex()));			
					// Check, if column = "L�n", clear column "Kommun" and "F�rsamling"; 080711 p�d
					if (pCol->GetItemIndex() == COLUMN_0)
					{
						pRec->setColumnText(COLUMN_1,_T(""));
						pRec->setColumnText(COLUMN_2,_T(""));
					}
					// Check, if column = "Kommun", clear "F�rsamling"; 080711 p�d
					if (pCol->GetItemIndex() == COLUMN_1)
						pRec->setColumnText(COLUMN_2,_T(""));
				}	// for (int i = 0;i < pRows->GetCount();i++)
				GetReportCtrl().Populate();
				GetReportCtrl().UpdateWindow();
			}	// if (start_row < pRow->GetIndex())
		}	// if (bCopyARange)
		else if (!bCopyARange)
		{
			if (pRow->GetIndex() > 0)
			{
				pLastRec = (CImportReportDataRec *)pRows->GetAt(pRow->GetIndex()-1)->GetRecord();
				pRec = (CImportReportDataRec *)pRows->GetAt(pRow->GetIndex())->GetRecord();
				pRec->setColumnText(pCol->GetItemIndex(),pLastRec->getColumnText(pCol->GetItemIndex()));
				pRec->setColumnDwData(pCol->GetItemIndex(),pLastRec->getColumnDwData(pCol->GetItemIndex()));
				// Check, if column = "L�n", clear column "Kommun" and "F�rsamling"; 080711 p�d
				if (pCol->GetItemIndex() == COLUMN_0)
				{
					pRec->setColumnText(COLUMN_1,_T(""));
					pRec->setColumnText(COLUMN_2,_T(""));
				}
				// Check, if column = "Kommun", clear "F�rsamling"; 080711 p�d
				if (pCol->GetItemIndex() == COLUMN_1)
					pRec->setColumnText(COLUMN_2,_T(""));
				GetReportCtrl().Populate();
				GetReportCtrl().UpdateWindow();
			}
		}
	}	// if (pRows != NULL)
}

LRESULT CImportDataReportView::OnSuiteMessage(WPARAM wParam,LPARAM)
{
	return 0L;
}


void CImportDataReportView::addCountyConstraints(void)
{
	CCMP_data rec;
	CXTPReportColumns *pColumns = GetReportCtrl().GetColumns();
	if (pColumns == NULL) return;

	CXTPReportColumn *pFromCol = pColumns->Find(COLUMN_0);
	if (pFromCol == NULL) return;

	// Get constraints for Specie column and remove all items; 070509 p�d
	CXTPReportRecordItemConstraints *pCons = pFromCol->GetEditOptions()->GetConstraints();
	if (pCons == NULL) return;
	pCons->RemoveAll();

	if (m_vecCounties.size() > 0)
	{
		for (UINT i = 0;i < m_vecCounties.size();i++)
		{
			rec = m_vecCounties[i];
			pFromCol->GetEditOptions()->AddConstraint((rec.getName()),rec.getID());
		}	// for (UINT i = 0;i < m_vecCounties.size();i++)
	}	// if (m_vecCounties.size() > 0)
}

void CImportDataReportView::addMunicipalConstraints(int county_id)
{
	CCMP_data rec;
	CXTPReportColumns *pColumns = GetReportCtrl().GetColumns();
	if (pColumns == NULL) return;

	CXTPReportColumn *pFromCol = pColumns->Find(COLUMN_1);
	if (pFromCol == NULL) return;

	// Get constraints for Specie column and remove all items; 070509 p�d
	CXTPReportRecordItemConstraints *pCons = pFromCol->GetEditOptions()->GetConstraints();
	if (pCons == NULL) return;
	pCons->RemoveAll();

	if (m_pDB != NULL)
		m_pDB->getMunicipals(county_id,m_vecMunicipals);

	if (m_vecMunicipals.size() > 0)
	{
		for (UINT i = 0;i < m_vecMunicipals.size();i++)
		{
			rec = m_vecMunicipals[i];
			pFromCol->GetEditOptions()->AddConstraint((rec.getName()),rec.getID());
		}	// for (UINT i = 0;i < m_vecCounties.size();i++)
	}	// if (m_vecCounties.size() > 0)
}

void CImportDataReportView::addMunicipalConstraints(LPCTSTR county)
{
	CCMP_data rec;
	CXTPReportColumns *pColumns = GetReportCtrl().GetColumns();
	if (pColumns == NULL) return;

	CXTPReportColumn *pFromCol = pColumns->Find(COLUMN_1);
	if (pFromCol == NULL) return;

	// Get constraints for Specie column and remove all items; 070509 p�d
	CXTPReportRecordItemConstraints *pCons = pFromCol->GetEditOptions()->GetConstraints();
	if (pCons != NULL)
		pCons->RemoveAll();

	if (m_pDB != NULL)
		m_pDB->getMunicipals(county,m_vecMunicipals);

	if (m_vecMunicipals.size() > 0)
	{
		for (UINT i = 0;i < m_vecMunicipals.size();i++)
		{
			rec = m_vecMunicipals[i];
			pFromCol->GetEditOptions()->AddConstraint((rec.getName()),rec.getID());
		}	// for (UINT i = 0;i < m_vecCounties.size();i++)
	}	// if (m_vecCounties.size() > 0)
}

void CImportDataReportView::addParishConstraints(int county_id,int municipal_id)
{
	CCMP_data rec;
	CXTPReportColumns *pColumns = GetReportCtrl().GetColumns();
	if (pColumns == NULL) return;

	CXTPReportColumn *pFromCol = pColumns->Find(COLUMN_2);
	if (pFromCol == NULL) return;

	// Get constraints for Specie column and remove all items; 070509 p�d
	CXTPReportRecordItemConstraints *pCons = pFromCol->GetEditOptions()->GetConstraints();
	if (pCons == NULL) return;
	pCons->RemoveAll();

	if (m_pDB != NULL)
		m_pDB->getParishs(county_id,municipal_id,m_vecParish);

	if (m_vecParish.size() > 0)
	{
		for (UINT i = 0;i < m_vecParish.size();i++)
		{
			rec = m_vecParish[i];
			pFromCol->GetEditOptions()->AddConstraint((rec.getName()),rec.getID());
		}	// for (UINT i = 0;i < m_vecCounties.size();i++)
	}	// if (m_vecCounties.size() > 0)
}

void CImportDataReportView::addParishConstraints(LPCTSTR county,LPCTSTR municipal)
{
	CCMP_data rec;
	CXTPReportColumns *pColumns = GetReportCtrl().GetColumns();
	if (pColumns == NULL) return;

	CXTPReportColumn *pFromCol = pColumns->Find(COLUMN_2);
	if (pFromCol == NULL) return;

	// Get constraints for Specie column and remove all items; 070509 p�d
	CXTPReportRecordItemConstraints *pCons = pFromCol->GetEditOptions()->GetConstraints();
	if (pCons == NULL) return;
	pCons->RemoveAll();

	if (m_pDB != NULL)
		m_pDB->getParishs(county,municipal,m_vecParish);

	if (m_vecParish.size() > 0)
	{
		for (UINT i = 0;i < m_vecParish.size();i++)
		{
			rec = m_vecParish[i];
			pFromCol->GetEditOptions()->AddConstraint((rec.getName()),rec.getID());
		}	// for (UINT i = 0;i < m_vecCounties.size();i++)
	}	// if (m_vecCounties.size() > 0)
}


BOOL CImportDataReportView::newImportData(void)
{
	BOOL bDoCheckCMP = FALSE;
	BOOL bExtensionOK = FALSE;
	CString sColumns[NUM_OF_COLUMNS];
	CString sLine;
	CString sBlock;
	CString sUnit;
	CString sName;
	CExcel_import rec;
	CString S;
	BOOL bFound = FALSE;
	CTransaction_property recProp;
	CString sSheetOrSeparator;
	CString sFileName;
	int nOffsetNumOfCols = 0;
	bool bDoAdd=false;

	// Try to read counties from "fst_county_municipal_parish_table"; 080710 p�d
	if (m_pDB != NULL)
		m_pDB->getCounties(m_vecCounties);


	CFileDialog f(TRUE,_T(".xls"),_T(""),4|2|OFN_FILEMUSTEXIST,_T("Excel *.xls;*.xlsx|*.xls;*.xlsx|Semikolon *.skv|*.skv|Semikolon *.csv|*.csv|"));
	if (f.DoModal() != IDOK) return FALSE;
	sFileName = f.GetPathName();

	// Check if user selected a Excel worksheet (xls)
	// or a semicolon delimited file (skv); 080710 p�d
	if (sFileName.Right(4) == _T(".xls") || sFileName.Right(5) == _T(".xlsx"))
	{
		sSheetOrSeparator = _T("Blad1");
		bExtensionOK = TRUE;
	}
	else if (sFileName.Right(4) == _T(".skv") || sFileName.Right(4) == _T(".csv"))
	{
		sSheetOrSeparator = _T(";");
		bExtensionOK = TRUE;
		nOffsetNumOfCols = -1;
	}

	if (!bExtensionOK)
	{
		// Check that file-extension is valid; 090205 p�d
		::MessageBox(this->GetSafeHwnd(),m_sMsgWrongExtension,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		return FALSE;
	}

	CExcelImportLibXl SS;
	if( SS.Open(f.GetPathName(), sSheetOrSeparator, false, true) == FALSE )
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				::MessageBox(this->GetSafeHwnd(), xml.str(IDS_STRING2942), xml.str(IDS_STRING207), MB_ICONERROR | MB_OK);
				xml.clean();
			}
		}

		return FALSE;
	}


	// ask user which sheet he/she wants to use
	if (sFileName.Right(4) == _T(".xls") || sFileName.Right(5) == _T(".xlsx"))
	{
		CExcelSheetsDlg dlgSheet;
		dlgSheet.m_pExcel = &SS;
		if( dlgSheet.DoModal() == IDCANCEL) return FALSE;
	}

	// Read Fieldnames from data-file; 080710 p�d
	SS.GetFieldNames(m_sarrColumnNames);
	// No data to work with; 080711 p�d
	if (m_sarrColumnNames.GetCount() == 0) return FALSE;

	// Check column names in imported file; 080711 p�d
	if (!checkImportData()) return FALSE;

	GetReportCtrl().ResetContent();	// Clear; 080710 p�d

	m_mapProps.clear();
	m_vecExcel_import.clear();

	CString sValue;
	CStringArray sarrRowValues;
	CStringArray sarrRowValues_added;
	::SetCursor(::LoadCursor(NULL,IDC_WAIT));
	for (long row = 1;row < SS.GetTotalRows();row++)
	{
		// Modified to accept any number of columns; 091102 Peter
		SS.ReadRow(sarrRowValues,row);
		sarrRowValues_added.RemoveAll();
		for (int col = 0;col < NUM_OF_COLUMNS;col++)
		{
			if (m_mapColumns.find(COLUMN_NAMES[col]) != m_mapColumns.end() && m_mapColumns[COLUMN_NAMES[col]] >= 0)
			{
				sValue = sarrRowValues.GetAt(m_mapColumns[COLUMN_NAMES[col]]);
				//#5044 PH 2016-08-17, Trimmed whitespaces.
				sarrRowValues_added.Add(sValue.Trim());
			}
			else
			{
				sarrRowValues_added.Add(_T(""));
			}
		}

		// Lagt till kontroll av Fastighetsnummer och ObjecktID.
		// Testar om det finns ett .0 i slutet.
		// Om s� tas detta bort. Antagande att detta kommer fr�n
		// inl�sningen och inte fr�n anv�ndaren; 091217 p�d
		int nValue;
		CString sPropNumber(sarrRowValues_added.GetAt(3));
		if (!sarrRowValues_added.GetAt(3).IsEmpty())
		{
			if (sPropNumber.Right(2) == L".0")
			{
				nValue = _tstoi(sarrRowValues_added.GetAt(3));
				sPropNumber.Format(L"%d",nValue);
			}
		}
		CString sShareOf(sarrRowValues_added.GetAt(7));
		if (!sarrRowValues_added.GetAt(6).IsEmpty())
		{
			if (sShareOf.Right(2) == L".0")
			{
				nValue = _tstoi(sarrRowValues_added.GetAt(7));
				sShareOf.Format(L"%d",nValue);
			}
		}
		CString sObjID(sarrRowValues_added.GetAt(5));
		if (!sarrRowValues_added.GetAt(18).IsEmpty())
		{
			if (sObjID.Right(2) == L".0")
			{
				nValue = _tstoi(sarrRowValues_added.GetAt(5));
				sObjID.Format(L"%d",nValue);
			}
		}
// every value except 0 means that this is a contact
		//BOOL bContact = !(sarrRowValues_added.GetAt(7) == _T("0"));
		//HMS-64 20201022 J� Kontakt ligger i kolumn 8, satt till kolumn 7 f�rut som �r andel och d� sattes alla till kontakt
		CString cContact=sarrRowValues_added.GetAt(8);
		BOOL bContact = !( cContact== _T("0") || cContact== _T("Nej") || cContact== _T("NEJ") || cContact.GetLength()<=0 || cContact== _T(""));

		splitName_Block_Unit(sarrRowValues_added[4],sName,sBlock,sUnit);
		//K�r en koll h�r om allt data, utom bcontact, p� raden �r tomt, i s� fall l�gg inte till det
		//Bug #2549 20111117 J�
		if(sarrRowValues_added.GetAt(0).GetLength()>0 || 	// county
			sarrRowValues_added.GetAt(1).GetLength()>0 || 	// municipal
			sarrRowValues_added.GetAt(2).GetLength()>0 || 	// parish
			sPropNumber.GetLength()>0 ||  //sarrRowValues_added.GetAt(3) || 
			sarrRowValues_added.GetAt(4).GetLength()>0 || 	// propname
			sName.GetLength()>0 || 
			sBlock.GetLength()>0 || 
			sUnit.GetLength()>0 || 
			sShareOf.GetLength()>0 ||  //sarrRowValues_added.GetAt(5) || 
			sarrRowValues_added.GetAt(8).GetLength()>0 || 	// name
			sarrRowValues_added.GetAt(9).GetLength()>0 || 	// address
			sarrRowValues_added.GetAt(10).GetLength()>0 || 	// c/o address
			sarrRowValues_added.GetAt(11).GetLength()>0 || 	// zip
			sarrRowValues_added.GetAt(12).GetLength()>0 || 	// city
			sarrRowValues_added.GetAt(13).GetLength()>0 || 	// phone home
			sarrRowValues_added.GetAt(14).GetLength()>0 || 	// phone work
			sarrRowValues_added.GetAt(15).GetLength()>0 || 	// cell
			sarrRowValues_added.GetAt(16).GetLength()>0 || 	// fax
			sarrRowValues_added.GetAt(17).GetLength()>0 || 	// region
			sarrRowValues_added.GetAt(18).GetLength()>0 || 	// country
			sarrRowValues_added.GetAt(19).GetLength()>0 || 	// notes
			sarrRowValues_added.GetAt(20).GetLength()>0 || 	// email
			//Feature #2392 lagt till personnummer i importhanteringen 20111003 J�
			sarrRowValues_added.GetAt(21).GetLength()>0 || 	// personnummer  
			sObjID.GetLength()>0
			)
			bDoAdd=true;
		else
			bDoAdd=false;



		if(bDoAdd)
		{
			rec = CExcel_import(_T(""),	// Keystring
				sarrRowValues_added.GetAt(0),	// county
				sarrRowValues_added.GetAt(1),	// municipal
				sarrRowValues_added.GetAt(2),	// parish
				sPropNumber, //sarrRowValues_added.GetAt(3),
				sarrRowValues_added.GetAt(4),	// propname
				sName,
				sBlock,
				sUnit,
				sShareOf, //sarrRowValues_added.GetAt(5),
				sarrRowValues_added.GetAt(9),	// name
				sarrRowValues_added.GetAt(10),	// address
				sarrRowValues_added.GetAt(11),	// c/o address
				sarrRowValues_added.GetAt(12),	// zip
				sarrRowValues_added.GetAt(13),	// city
				sarrRowValues_added.GetAt(14),	// phone home
				sarrRowValues_added.GetAt(15),	// phone work
				sarrRowValues_added.GetAt(16),	// cell
				sarrRowValues_added.GetAt(17),	// fax
				sarrRowValues_added.GetAt(18),	// region
				sarrRowValues_added.GetAt(19),	// country
				sarrRowValues_added.GetAt(20),	// notes
				sarrRowValues_added.GetAt(21),	// email
				//Feature #2392 lagt till personnummer i importhanteringen 20111003 J�
				sarrRowValues_added.GetAt(22),	// personnummer  
				sObjID,
				//Feature #2491 lagt till mottagarref i importhanteringen 20111003 J�
				sarrRowValues_added.GetAt(6),	// mottagarref  
				//Feature #3491 lagt till bankgiro i importhanteringen 20111003 J�
				sarrRowValues_added.GetAt(23), 	// bankgiro
				//Feature #3491 lagt till plusgiro i importhanteringen 20111003 J�
				sarrRowValues_added.GetAt(24), 	// plusgiro
				//Feature #3491 lagt till bankkonto i importhanteringen 20111003 J�
				sarrRowValues_added.GetAt(25), 	// bankkonto
				//Feature #3491 lagt till leverant�rsnummer i importhanteringen 20111003 J�
				sarrRowValues_added.GetAt(26), 	// leverant�rsnummer

				bContact);	// contact?
			// This vector holds all information in Excel-sheet; 080709 p�d
			m_vecExcel_import.push_back(rec);
		}
	}	// for (long row = 2;row <= SS.GetTotalRows();row++)

	// Added 2009-04-02 p�d
	// Check if there's a values set in Country,Municipal or Parish kolumn, if so try to match
	// to database register: "fst_county_municipal_parish_table"; 090402 p�d
	for (UINT i = 0;i < m_vecExcel_import.size();i++)
	{
		if (!m_vecExcel_import[i].getCounty().IsEmpty() || 
				!m_vecExcel_import[i].getMunicipal().IsEmpty() || 
				!m_vecExcel_import[i].getParish().IsEmpty())
		{
			bDoCheckCMP = TRUE;
			break;
		}	// if (!m_vecExcel_import[i].getCounty().IsEmpty())
	}	// for (UINT i = 0;i < m_vecExcel_import.size();i++)
	for (UINT i = 0;i < m_vecExcel_import.size();i++)
	{
		// Check "L�n","Kommun" and "F�rsamling", to see if
		// user's entered one or more of these.
		// Try to match the names against the
		// database register: "fst_county_municipal_parish_table"; 080812 p�d
		if (bDoCheckCMP)
			if (checkCMP(m_vecExcel_import[i].getCounty()) != IDOK) return FALSE;
		
		// Add constraints; 080812 p�d
		addCountyConstraints();
		addMunicipalConstraints(m_vecExcel_import[i].getCounty());
		addParishConstraints(m_vecExcel_import[i].getCounty(),m_vecExcel_import[i].getMunicipal());

		CImportReportDataRec *pRec = new CImportReportDataRec(i,m_vecExcel_import[i]);
		// Try to find id for County and set dwData; 080812 p�d
		if (m_vecCounties.size() > 0)
		{
			for (UINT ii = 0;ii < m_vecCounties.size();ii++)
			{
				if (m_vecCounties[ii].getName().CompareNoCase(m_vecExcel_import[i].getCounty()) == 0)
				{
					pRec->setColumnDwData(COLUMN_0,m_vecCounties[ii].getID());
				}	// if (m_vecCounties[ii].getName().CompareNoCase(m_vecExcel_import[i].getCounty()) == 0)
			}	// for (UINT ii = 0;ii < m_vecCounties.size();ii++)
		}	// if (m_vecCounties.size() > 0)
		GetReportCtrl().AddRecord(pRec);
	}	// for (UINT i = 0;i < m_vecKeys.size();i++)

	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
	// Setup map for columns; 080812 p�d
	resetColumnMap();

	::SetCursor(::LoadCursor(NULL,IDC_ARROW));

	sarrRowValues.RemoveAll();
	sarrRowValues_added.RemoveAll();

	return TRUE;
}

// Try to match headlines of imported file to
// default headlines. If they doesn't match, ask
// user to match them; 080711 p�d
BOOL CImportDataReportView::checkImportData(void)
{
	BOOL bFound = FALSE;
	int nDlgRet = 0;
/*
	CString S;
	S.Format(_T("m_sarrColumnNames.GetCount() %d\nm_mapColumns.size() %d"),
		m_sarrColumnNames.GetCount(),m_mapColumns.size());
	AfxMessageBox(S);
*/
	if (m_sarrColumnNames.GetCount() > 0)
	{
		// Open dialog for user, to match columns; 080711 p�d
		CColumnMatchDlg *pDlg = new CColumnMatchDlg();
		if (pDlg != NULL)
		{
			pDlg->setColumns(m_sarrColumnNames);
			pDlg->setMap(m_mapColumns);
			if ((nDlgRet = pDlg->DoModal()) == IDOK)
			{
				pDlg->getMap(m_mapColumns);
			}
			delete pDlg;

			if (nDlgRet == IDOK) return TRUE;
			else return FALSE;
		}

		return TRUE;
	}	// if (m_sarrColumnNames.GetCount() > 0)

	return FALSE;
}

void CImportDataReportView::resetColumnMap(void)
{
/*
		// Original setup of columns; 080711 p�d
		m_mapColumns[COLUMN_NAMES[0]] = 0; 
		m_mapColumns[COLUMN_NAMES[1]] = 1; 
		m_mapColumns[COLUMN_NAMES[2]] = 2; 
		m_mapColumns[COLUMN_NAMES[3]] = 3; 
		m_mapColumns[COLUMN_NAMES[4]] = 4; 
		m_mapColumns[COLUMN_NAMES[5]] = 5; 
		m_mapColumns[COLUMN_NAMES[6]] = 6; 
		m_mapColumns[COLUMN_NAMES[7]] = 7; 
		m_mapColumns[COLUMN_NAMES[8]] = 8; 
		m_mapColumns[COLUMN_NAMES[9]] = 9; 
		m_mapColumns[COLUMN_NAMES[10]] = 10; 
		m_mapColumns[COLUMN_NAMES[11]] = 11; 
		m_mapColumns[COLUMN_NAMES[12]] = 12; 
		m_mapColumns[COLUMN_NAMES[13]] = 13; 
		m_mapColumns[COLUMN_NAMES[14]] = 14; 
*/
}

BOOL CImportDataReportView::checkCMP(LPCTSTR county)
{
	CCMP_data rec;
	BOOL bFoundCompare = FALSE;
	BOOL bFoundFind = FALSE;
	CString sCounty;
	CString sMunicipal;
	CString sParish;
	CString sMatched;
	CStringArray sarrMatched;
	int nRetDlg = IDOK;
	if (m_vecCounties.size() > 0)
	{
		//------------------------------------------------------------------------
		// First compare, to see if we have an exact match
		// for couty-name; 080812 p�d
		for (UINT i = 0;i < m_vecCounties.size();i++)
		{
			rec = m_vecCounties[i];

			if (rec.getName().CompareNoCase(county) == 0)
			{
				bFoundCompare = TRUE;
				sCounty = rec.getName();
				break;
			}	// if (rec.getName().CompareNoCase(county) == 0)
		}	// for (UINT i = 0;i < m_vecCounties.size();i++)

		//------------------------------------------------------------------------
		// If we couldn't find an exact match, try to
		// see if we can find the county in the County-name; 080812 p�d
		for (UINT i = 0;i < m_vecCounties.size();i++)
		{
			rec = m_vecCounties[i];

			if (rec.getName().Find(county) > -1)
			{
				bFoundFind = TRUE;
				sCounty = rec.getName();
				sarrMatched.Add(rec.getName());
			}	// if (rec.getName().CompareNoCase(county) == 0)
		}	// for (UINT i = 0;i < m_vecCounties.size();i++)

		//------------------------------------------------------------------------
		// If Found and nNumOfFound > 1, the name is ambiguous; 080812 p�d
		if (bFoundFind && !bFoundCompare && sarrMatched.GetCount() > 0)
		{
			
			/*
			CSelectCMPDlg *pDlg = new CSelectCMPDlg();
			pDlg->setDBConnection(m_pDB);
			pDlg->setData(sarrMatched);
			nRetDlg = pDlg->DoModal();
			if (nRetDlg == IDOK)
			{
		
				sCounty = pDlg->getSelectedCounty();
				sMunicipal = pDlg->getSelectedMunicipal();
				sParish = pDlg->getSelectedParish();
				// We'll replace the county with the selected county; 080812 p�d
				for (UINT i = 0;i < m_vecExcel_import.size();i++)
				{
					m_vecExcel_import[i].setCounty(sCounty);
					m_vecExcel_import[i].setMunicipal(sMunicipal);
					m_vecExcel_import[i].setParish(sParish);
				}	// for (UINT i = 0;i < m_vecKeys.size();i++)
			
			}
			delete pDlg;
*/

		}	// if (bFound && nNumFound > 1)
	} // if (m_vecCounties.size() > 0)

	// Clear; 080812 p�d
	sarrMatched.RemoveAll();

	return (nRetDlg == IDOK);
}

void CImportDataReportView::OnImportDataFile(void)
{
	if (m_pViewFrame != NULL)
	{
		m_pViewFrame->getProgressCtrl().SetRange(0,100);
		m_pViewFrame->getProgressCtrl().SetPos(0);
		m_pViewFrame->getProgressCtrl().SetStep(1);
		m_pViewFrame->getStatusBar().SetPaneText(1,m_sMsgStartImport);
	}

	if (newImportData())
	{
		CXTPReportRows *pRows = GetReportCtrl().GetRows();
		if (m_pViewFrame != NULL && pRows != NULL)
		{
			CString sMsg;
			sMsg.Format(_T("%s : %d"),m_sMsgNumOfProps,pRows->GetCount());
			m_pViewFrame->getStatusBar().SetPaneText(1,sMsg);
		}
		if (!m_bIsCreatePropsAndOwners)
			m_bIsCreatePropsAndOwners = TRUE;
	}
}

void CImportDataReportView::OnCreatePropsAndOwnersFile(void)
{
	CImportOptionsDlg *pDlg = new CImportOptionsDlg();
	if (pDlg != NULL)
	{
		pDlg->setImportData(m_vecExcel_import);
		if (pDlg->DoModal() == IDOK)
		{
			if (pDlg->isImportProperty())
				importProperties();

			if (pDlg->isImportOwners())
			{
				m_vecSelectedCatgoriesForContactData = pDlg->getCategoriesForContatcs();			
				importOwners();
			}

			if (pDlg->isMatchOwnersToProperty())
				matchOwnersToProperty();

		}
	}
	delete pDlg;


	if (class_LParam::m_lpValue == 122)
	{
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
				(LPARAM)&_doc_identifer_msg(_T("Module122"),_T("Module118"),_T(""),122,0,0));
	}
	else	if (class_LParam::m_lpValue == 113)
	{
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02,
				(LPARAM)&_doc_identifer_msg(_T("Module122"),_T("Module113"),_T(""),113,0,0));
	}
	if (m_pViewFrame != NULL)
	{
		m_pViewFrame->getProgressCtrl().SetRange(0,100);
		m_pViewFrame->getProgressCtrl().SetPos(0);
		m_pViewFrame->getProgressCtrl().SetStep(1);
		m_pViewFrame->getStatusBar().SetPaneText(1,m_sMsgImportDone);
	}


	// Done import; 090922 p�d
	// Commented out 091009 p�d
	//	m_bIsCreatePropsAndOwners = FALSE;

/*
	CPropertyFormView *pFormView = (CPropertyFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pFormView != NULL)
	{
		pFormView->SendMessage(WM_USER_MSG_SUITE,ID_WPARAM_VALUE_FROM + 0xFA);
	}
*/
/*
	CString sMsg;
	sMsg.Format(_T("%s%s"),m_sMsgEnteredDataForProperties,m_sMsgEnteredDataForContacts);
	::MessageBox(this->GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
*/
}

void CImportDataReportView::OnToolsAddChangeObjID(void)
{
	CImportReportDataRec *pRec = NULL;
	CXTPReportRows *pRows = GetReportCtrl().GetRows();

	if (pRows->GetCount() == 0) return;

	CAddChangeObjIDDlg *pDlg = new CAddChangeObjIDDlg();
	if (pDlg != NULL)
	{
		if (pDlg->DoModal() == IDOK)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRec = (CImportReportDataRec*)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
					pRec->setColumnText(COLUMN_5,pDlg->getObjID());
			}
			GetReportCtrl().Populate();
			GetReportCtrl().UpdateWindow();
		}
		delete pDlg;
	}
}

void CImportDataReportView::OnToolsCreateExcelTemplate(void)
{
	CExcelibXl_create_template *pExcelTmplCreate = new CExcelibXl_create_template(m_sTmplFileName);
	if (pExcelTmplCreate)
	{
		pExcelTmplCreate->createTemplate(GetSafeHwnd(), m_sMsgCap,m_sMsgOpenEXCEL);
		delete pExcelTmplCreate;
	}
}

void CImportDataReportView::OnUpdateCreatePropsAndOwnersTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsCreatePropsAndOwners );
}

void CImportDataReportView::OnUpdateToolsTBtn1(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsCreatePropsAndOwners );
}

void CImportDataReportView::OnUpdateToolsTBtn2(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( TRUE );
}

void CImportDataReportView::importProperties(void)
{
	CString sMsg;
	CString sKeyStr;
	CString S;
	CString sCounty;
	CString sMunicipal;
	CString sParish;
	CString sPropNum;
	CString sPropName;
	CString sPropName2;
	CString sPropBlock;
	CString sPropUnit;
	CString sObjID;
	CString sCountyID;
	CString sMunicipalID;
	CString sParishID;
	CString sMottagarref;
	CString sBankgiro;
	CString sPlusgiro;
	CString sBankkonto;
	CString sLevnummer;
	BOOL bFoundCounty = FALSE;
	BOOL bFoundMunicipal = FALSE;
	BOOL bFoundParish = FALSE;
	BOOL bExists = FALSE;


	_prop_data recProps;
	CTransaction_property recTranProp;
	vecTransatcionCMP vecCMP;
	vecTransactionProperty vecProps;
	vecTransactionProperty vecProps_added;
	CTransaction_county_municipal_parish recCMP;
	vecPropData m_vecKeys;

	if (m_pDB != NULL)
	{
		m_pDB->getCMPs(vecCMP);
		if(elvObjId==-1)
			m_pDB->getProperties(vecProps);
		else
			m_pDB->getPropertiesForObjectId(elvObjId,vecProps);
	}

	CImportReportDataRec *pRec = NULL;
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	if (pRecs != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			pRec = (CImportReportDataRec*)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				sCounty = pRec->getColumnText(COLUMN_0);
				sMunicipal = pRec->getColumnText(COLUMN_1);
				sParish = pRec->getColumnText(COLUMN_2);
				sPropNum = pRec->getColumnText(COLUMN_3);
				sPropName = pRec->getColumnText(COLUMN_4);
				sObjID = pRec->getColumnText(COLUMN_5);
				sMottagarref = pRec->getColumnText(COLUMN_6);

				sCounty = trimChars(sCounty);
				sMunicipal = trimChars(sMunicipal);
				sParish = trimChars(sParish);
				sPropNum = trimChars(sPropNum);
				sPropName = trimChars(sPropName);
				sObjID = trimChars(sObjID);
				sMottagarref = trimChars(sMottagarref);

				// Require property name and number #4506
				if (sPropName.IsEmpty() || sPropNum.IsEmpty())
				{
					::MessageBox(this->GetSafeHwnd(),m_sMsgMissingPropNameNumber,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					m_vecKeys.clear();
					break;
				}
				else
				{
					// Setup a 'Keystring'; 080813 p�d
					splitName_Block_Unit(sPropName,sPropName2,sPropBlock,sPropUnit);
					// If Block and Unit is empty, we need to make
					// the keystring different in some other way.
					// Just add the counter (i), to the PropertyName; 080813 p�d
					if (sPropBlock.IsEmpty() && sPropUnit.IsEmpty())
					{
						sKeyStr.Format(_T("%s%s%s%s%d"),
							sCounty,
							sMunicipal,
							sParish,
							//sPropNum,
							sPropName,
							i);
					}
					else
					{
						sKeyStr.Format(_T("%s%s%s%s"),
							sCounty,
							sMunicipal,
							sParish,
							//sPropNum,
							sPropName);
					}
					sKeyStr.Remove(L'\r');
					sKeyStr.Remove(L'\n');
					sKeyStr.Remove(L' ');
					m_mapProps[sKeyStr]++;
					if (m_mapProps[sKeyStr] == 1)
					{
						m_vecKeys.push_back(_prop_data(sKeyStr,
																					 sCounty,
																					 sMunicipal,
																					 sParish,
																					 sPropNum,
																					 sPropName,
																					 sObjID,
																					 sMottagarref,i));
					}	// if (m_mapProps[sKeyStr] == 1)
				} // if (!sPropName.IsEmpty() && !sPropNum.IsEmpty())
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)

	if (m_vecKeys.size() > 0)
	{
		if (m_pViewFrame != NULL)
		{
			m_pViewFrame->getProgressCtrl().SetRange(0,m_vecKeys.size());
			m_pViewFrame->getProgressCtrl().SetPos(0);
			m_pViewFrame->getProgressCtrl().SetStep(1);
		}
		for (UINT i = 0;i < m_vecKeys.size();i++)
		{
			recProps = m_vecKeys[i];
			// Try to find ID's for County,Municipal and Parish
			// from DB table; 080812 p�d
			if (vecCMP.size() > 0)
			{
				for (UINT ii = 0;ii < vecCMP.size();ii++)
				{
					recCMP = vecCMP[ii];
					if (compareStrings(recCMP.getCountyName(),sCounty) == 0)
					{
						sCountyID.Format(_T("%d"),recCMP.getCountyID());
						bFoundCounty = TRUE;
					}
					if (compareStrings(recCMP.getMunicipalName(),sMunicipal) == 0)
					{
						sMunicipalID.Format(_T("%d"),recCMP.getMunicipalID());
						bFoundMunicipal = TRUE;
					}
					if (compareStrings(recCMP.getParishName(),sParish) == 0)
					{
						sParishID.Format(_T("%d"),recCMP.getParishID());
						bFoundParish = TRUE;
					}

					if (bFoundCounty && bFoundMunicipal && bFoundParish)
						break;
				}	// for (UINT ii = 0;ii < vecCMP.size();ii++)
			}	// if (vecCMP.size() > 0)

			
			int pos=0;
			int count=-1;
			bool badLine=false;

			while(pos!=-1) {
				pos=recProps.m_sPropName.Find(L":",pos+1);
				count++;
			}
			if(count>1) {
				badLine=true;
				::MessageBox(this->GetSafeHwnd(),m_sMsgToManyProp,m_sMsgToManyPropCap,MB_ICONEXCLAMATION | MB_OK);
			}

			splitName_Block_Unit(recProps.m_sPropName,sPropName,sPropBlock,sPropUnit);

			// Check, to see if property already in DB; 080813 p�d
			if (vecProps.size() > 0)
			{
				for (UINT i = 0;i < vecProps.size();i++)
				{
					bExists = FALSE;
					recTranProp = vecProps[i];
					if (compareStrings(recTranProp.getCountyCode(),sCountyID) == 0 &&
						  compareStrings(recTranProp.getMunicipalCode(),sMunicipalID) == 0 &&
							compareStrings(recTranProp.getParishCode(),sParishID) == 0 &&
							compareStrings(recTranProp.getPropertyName(),sPropName) == 0 &&
							compareStrings(recTranProp.getBlock(),sPropBlock) == 0 &&
							compareStrings(recTranProp.getUnit(),sPropUnit) == 0)
					{
						bExists = TRUE;
						break;
					}
				}	// for (UINT i = 0;i < vecProps.size();i++)
			}	// if (vecProps.size() > 0)
			if (m_pViewFrame != NULL)	
			{
				sMsg.Format(_T("%s : %s"),m_sMsgAdding,sPropName);
				m_pViewFrame->getStatusBar().SetPaneText(1,sMsg);
				m_pViewFrame->getProgressCtrl().StepIt();
			}

			// Make sure there's valid data to enterer into Property table; 090220 p�d
			if (!bExists && !recProps.m_sPropName.IsEmpty() && !badLine) 
			{
/*
				S.Format(_T("County %s %s\nMunicipal %s %s\nParish %s %s\nProp %s  %s - %s\nObjID %s"),
					sCountyID,recProps.m_sCounty,
					sMunicipalID,recProps.m_sMunicipal,
					sParishID,recProps.m_sParish,
					sPropName,sPropBlock,sPropUnit,
					recProps.m_sObjID);
				AfxMessageBox(S);
*/	
				recTranProp = CTransaction_property(-1,sCountyID,sMunicipalID,sParishID,recProps.m_sCounty,recProps.m_sMunicipal,recProps.m_sParish,
					recProps.m_sPropNum,sPropName,sPropBlock,sPropUnit,0.0,0.0,getUserName(),_T(""),recProps.m_sObjID,_T(""),recProps.m_sMottagarref);
				if (m_pDB != NULL)
				{
					if(m_pDB->addProperty(recTranProp))
					{					
						vecProps_added.push_back(recTranProp);
						//B�r nog l�gga till fastigheten i vektorn som listar existerande fastigheter i DB
						//Bug #2474 20111020
						vecProps.push_back(recTranProp);
						//#5008 PH 20160628
						if(elvObjId!=-1) {
							m_pDB->addPropertyToObject(m_pDB->getLastPropertyID(),elvObjId);
						}

					}
				}
			}	// if (!bExists)		
			else if(!badLine)//Fastigheten finns, kolla om den skall uppdateras Bug #2474 20111020
			{
				if(!recProps.m_sPropName.IsEmpty())
				{
					//Skapa fastighet med bara det data som g�r att l�sa/skapa fr�n excel, dvs 5 st, Bug #2474 20111020 J�
					recTranProp = CTransaction_property(recTranProp.getID(),sCountyID,sMunicipalID,sParishID,recProps.m_sCounty,recProps.m_sMunicipal,recProps.m_sParish,
						recProps.m_sPropNum,sPropName,sPropBlock,sPropUnit,0.0,0.0,_T(""),_T(""),recProps.m_sObjID,_T(""),recProps.m_sMottagarref);
					//Uppdatera fastighet med ev ny data
//					m_pDB->updPropertyExcel(recTranProp);
					m_pDB->updProperty(recTranProp);
				
				}
			
			}

		}	// for (UINT i = 0;i < m_vecKeys.size();i++)
	}	// if (m_vecKeys.size() > 0)

	//#5008 PH 20160615
	if(elvObjId!=-1) {
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_WPARAM_VALUE_FROM + 0x02, 
		(LPARAM)&_doc_identifer_msg(_T("Module122"),_T("Module5200"),_T(""),1234,0,0));	

	}

// There were some properties added; 090220 p�d
//	m_sMsgEnteredDataForProperties.Format(_T("Inl�sta fastigheter : %d av totalt %d\n\n"),vecProps_added.size(),m_vecKeys.size());
}

void CImportDataReportView::importOwners(void)
{
	CString sMsg;
	CString sOwnerName;
	CString sAddress;
	CString sCoAddress;
	CString sPostNum;
	CString sPostAddress;
	CString sPhoneWork;
	CString sPhoneHome;
	CString sMobile;
	CString sFax;
	CString sRegion;
	CString sCountry;
	CString sNotes;
	CString sSecNr;
	CString sEMail;
	CString sVATNumber;
	CString sBankgiro;
	CString sPlusgiro;
	CString sBankkonto;
	CString sLevnummer;
	int nConnectID;
	int nTypeOf;

	CString sKeyStr;
	CString sKeyStr2;
	map<CString,int> mapContacts;
	CTransaction_contacts recContact;
	vecTransactionContacts vecContacts;
	//Verkar inte anv�ndas tagit bort denna 20111019 J�
	//vecTransactionContacts vecContactsAdded;

	std::vector<int> vecContactsIDs;

	BOOL bExists = FALSE;

	int nNumOfRecords = 0;

	CImportReportDataRec *pRec = NULL;
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	if (pRecs != NULL)
	{
		nNumOfRecords = pRecs->GetCount();
		if (m_pDB != NULL) {
			m_pDB->getContacts(vecContacts);
			if(elvObjId!=-1)
				m_pDB->getContactsForObjectId(elvObjId,vecContacts,false);
		}

		if (m_pViewFrame != NULL)
		{
			m_pViewFrame->getProgressCtrl().SetRange(0,nNumOfRecords);
			m_pViewFrame->getProgressCtrl().SetPos(0);
			m_pViewFrame->getProgressCtrl().SetStep(1);
		}

		for (int i = 0;i < nNumOfRecords;i++)
		{
			pRec = (CImportReportDataRec*)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				sOwnerName = pRec->getColumnText(COLUMN_9);
				sAddress = pRec->getColumnText(COLUMN_10);
				sCoAddress = pRec->getColumnText(COLUMN_11);
				sPostNum = pRec->getColumnText(COLUMN_12);
				sPostAddress = pRec->getColumnText(COLUMN_13);
				sPhoneWork = pRec->getColumnText(COLUMN_14);
				sPhoneHome = pRec->getColumnText(COLUMN_15);
				sMobile = pRec->getColumnText(COLUMN_16);
				sFax = pRec->getColumnText(COLUMN_17);
				sRegion = pRec->getColumnText(COLUMN_18);
				sCountry = pRec->getColumnText(COLUMN_19);
				sNotes = pRec->getColumnText(COLUMN_20);
				sEMail = pRec->getColumnText(COLUMN_21);
				sSecNr = pRec->getColumnText(COLUMN_22);
				sBankgiro = pRec->getColumnText(COLUMN_23);
				sPlusgiro = pRec->getColumnText(COLUMN_24);
				sBankkonto = pRec->getColumnText(COLUMN_25);
				sLevnummer = pRec->getColumnText(COLUMN_26);
				sVATNumber = _T("");
				nConnectID = -1;
				nTypeOf = 0;

				// Make sure we have, at least, a name; 080813 p�d
				if (!sOwnerName.IsEmpty())
				{
					sKeyStr.Format(_T("%s%s%s%s"),
						sOwnerName,
						sAddress,
						sPostNum,
						sPostAddress);

					mapContacts[sKeyStr]++;

					if (mapContacts[sKeyStr] == 1 && m_pDB != NULL)
					{
						// Check, to see if this Owner already in register.
						// If so do an update, otherwise insert; 080813 p�d
						if (vecContacts.size() > 0)
						{
						// Rensas inte mellanslag p� kontaktens keystring s� kan man d� f� 
						// dubletter, l�sning = rensa mellanslag(sKeyStr,sKeyStr2) Bug #2474 20111019 J�
							sKeyStr.Remove(' ');

							for (UINT i = 0;i < vecContacts.size();i++)
							{
								bExists = FALSE;
								recContact = vecContacts[i];
								sKeyStr2.Format(_T("%s%s%s%s"),
									recContact.getName(),
									recContact.getAddress(),
									recContact.getPostNum(),
									recContact.getPostAddress());
								// Rensas inte mellanslag p� kontaktens keystring s� kan man d� f� 
								// dubletter, l�sning = rensa mellanslag(sKeyStr,sKeyStr2) Bug #2474 20111019 J�
								sKeyStr2.Remove(' ');
								if (sKeyStr2.CompareNoCase(sKeyStr) == 0)
								{
									bExists = TRUE;
									break;
								}	// if (sKeyStr2.CompareNoCase(sKeyStr) == 0)							
							}	// for (UINT i = 0;i < vecContacts.size();i++)
						}	// if (vecContacts.size() > 0)

						if (m_pViewFrame != NULL)	
						{
							sMsg.Format(_T("%s : %s , %s  %s %s"),m_sMsgAdding,sOwnerName,sAddress,sPostNum,sPostAddress);
							m_pViewFrame->getStatusBar().SetPaneText(1,sMsg);
							m_pViewFrame->getProgressCtrl().StepIt();
						}

						if (!sOwnerName.IsEmpty())
						{
							if (bExists)
							{
								recContact = CTransaction_contacts(recContact.getID(),sSecNr,sOwnerName,_T(""),sAddress,sCoAddress,sPostNum,sPostAddress,sRegion,
									sCountry,sPhoneWork,sPhoneHome,sFax,sMobile,sEMail,_T(""),sNotes,getUserName(),_T(""),sVATNumber,nConnectID,nTypeOf,sBankgiro,sPlusgiro,sBankkonto,sLevnummer);

								//Uppdaterar endast de variabler som g�r att importera ifr�n excel 20101020 bug #2474 J�
								//recContact skickas endast med f�r att sl� upp med hj�lp av dess Id om kontakten existerar
//								if (m_pDB->updContact(sOwnerName,sAddress,sCoAddress,sPostNum,sPostAddress,sPhoneWork,sPhoneHome,sMobile,sFax,sRegion,sCountry,sNotes,sEMail,sSecNr,recContact.getID(),recContact))
								if (m_pDB->updContactImport(recContact))
									vecContactsIDs.push_back(recContact.getID());
							}
							else
							{
								recContact = CTransaction_contacts(-1,sSecNr,sOwnerName,_T(""),sAddress,sCoAddress,sPostNum,sPostAddress,sRegion,
									sCountry,sPhoneWork,sPhoneHome,sFax,sMobile,sEMail,_T(""),sNotes,getUserName(),_T(""),sVATNumber,nConnectID,nTypeOf,sBankgiro,sPlusgiro,sBankkonto,sLevnummer);
								if (m_pDB->addContact(recContact))
								{
									vecContactsIDs.push_back(m_pDB->getLastContactID());

									//Uppdatera vecContacts �ven h�r s� att kollen om kontakten redan finns fungerar �ven mellan kontakter 
									//i samma importfil, f�rut kollades det bara mot kontakterna som fanns innnan sj�lva importeringen gjordes	Bug #2274 20111019 J�
									//M�ste �ven uppdatera recContact med korrekt Id annars f�r den id -1 i vektorn som det kollas mot sedan om den redan existerar i DB
									recContact = CTransaction_contacts(m_pDB->getLastContactID(),sSecNr,sOwnerName,_T(""),sAddress,sCoAddress,sPostNum,sPostAddress,sRegion,
									sCountry,sPhoneWork,sPhoneHome,sFax,sMobile,sEMail,_T(""),sNotes,getUserName(),_T(""),sVATNumber,nConnectID,nTypeOf,sBankgiro,sPlusgiro,sBankkonto,sLevnummer);
									vecContacts.push_back(recContact);
								}
							}
							//Verkar inte anv�ndas tagit bort denna 20111019 J�
							//vecContactsAdded.push_back(recContact);
						}
					}	// if (mapContacts[sKeyStr] == 1)
				}	// if (!sOwnerName.IsEmpty())
				pRec = NULL;
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)

	if (m_pDB != NULL)
	{
		// Use vecContactsAdded to add Categories for cantacts
		if (m_vecSelectedCatgoriesForContactData.size() > 0 && vecContactsIDs.size() > 0)
		{
			for (UINT i1 = 0;i1 < vecContactsIDs.size();i1++)
			{
				for (UINT i2 = 0;i2 < m_vecSelectedCatgoriesForContactData.size();i2++)
				{
					m_pDB->addCategoriesForContact(vecContactsIDs[i1],m_vecSelectedCatgoriesForContactData[i2].getCategoryID());
				}	// for (UINT i2 = 0;i2 < m_vecSelectedCatgoriesForContactData.size();i2++)
			}	// for (UINT i1 = 0;i1 < vecContactsIDs.size();i1++)
		}	// if (m_vecSelectedCatgoriesForContactData.size() > 0 && vecContactsIDs.size() > 0)
	}

	mapContacts.clear();
	pRec = NULL;

	// There were some properties added; 090220 p�d
	// Not used 2009-05-18 P�D
	// m_sMsgEnteredDataForContacts.Format(_T("Inl�sta kontakter : %d av totalt %d\n\n"),vecContactsAdded.size(),nNumOfRecords);

}

void CImportDataReportView::matchOwnersToProperty(void)
{
	BOOL bDefaultContact = TRUE;
	CString sCounty;
	CString sMunicipal;
	CString sParish;
	CString sPropNum;
	CString sPropName;

	CString sShareInProp;

	CString sOwnerName;
	CString sAddress;
	CString sCoAddress;
	CString sPostNum;
	CString sPostAddress;

	CString sKeyStrProp1;
	CString sKeyStrProp2;
	CString sKeyStrContact1;
	CString sKeyStrContact2;

	CTransaction_property recProp;
	vecTransactionProperty vecProps;

	CTransaction_contacts recContact;
	vecTransactionContacts vecContacts;

	CImportReportDataRec *pRec = NULL;
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	if (pRecs != NULL)
	{
		if (m_pDB != NULL)
		{
			if(elvObjId==-1) {
				m_pDB->getProperties(vecProps);		
			}
			else {
				m_pDB->getPropertiesForObjectId(elvObjId,vecProps);
			}
			m_pDB->getContacts(vecContacts);

		}

		for (int i = 0;i < pRecs->GetCount();i++)
		{
			pRec = (CImportReportDataRec*)pRecs->GetAt(i);
			if (pRec != NULL)
			{
				sCounty = pRec->getColumnText(COLUMN_0);
				sMunicipal = pRec->getColumnText(COLUMN_1);
				sParish = pRec->getColumnText(COLUMN_2);
				sPropNum = pRec->getColumnText(COLUMN_3);
				sPropName = pRec->getColumnText(COLUMN_4);
				bDefaultContact = pRec->getColumnBool(COLUMN_8);

				// Setup Keystring for Property in ReportCtrl; 080813 p�d
				sKeyStrProp1.Format(_T("%s%s%s%s%s"),
						sCounty.Left(31),		// DB-values may be truncated so we need to do the same thing here
						sMunicipal.Left(31),	// Note! These values have to match DB field length
						sParish.Left(31),
						sPropNum.Left(20),
						sPropName.Left(40));

				sShareInProp = pRec->getColumnText(COLUMN_7).Left(MAX_OWNER_SHARE_CHARS);		//HMS-28 trunkera om mer �n 20 tecken, skriv ut felmeddelande om att kontrollera andelen
				if(pRec->getColumnText(COLUMN_7).GetLength() > MAX_OWNER_SHARE_CHARS)
				{
					//HMS-28 felmeddelande
					CString S;
					S.Format(_T("%s\n%s: %s\n%s: %s\n%s: %s\n%s: %s"),m_sMsgShareErr1,m_sMsgProp,sPropName,m_sMsgOwner,pRec->getColumnText(COLUMN_9),m_sMsgShare,pRec->getColumnText(COLUMN_7),m_sMsgShareErr2,sShareInProp);
					AfxMessageBox(S);
				}

				sOwnerName = pRec->getColumnText(COLUMN_9);
				sAddress = pRec->getColumnText(COLUMN_10);
				sCoAddress = pRec->getColumnText(COLUMN_11);
				sPostNum = pRec->getColumnText(COLUMN_12);
				sPostAddress = pRec->getColumnText(COLUMN_13);
				// Setup Keystring for Contact in ReportCtrl; 080813 p�d
				sKeyStrContact1.Format(_T("%s%s%s%s"),
																sOwnerName.Left(50),	// DB-values may be truncated so we need to do the same thing here
																sAddress.Left(50),		// Note! These values have to match DB field length
																sPostNum.Left(10),
																sPostAddress.Left(60));

				// Try to Match the Property and Contact from ReportCtrl to
				// Property and Contact in DB-tables; 080813 p�d
				if (vecProps.size() > 0 && vecContacts.size() > 0)
				{
					for (UINT prop_cnt = 0;prop_cnt < vecProps.size();prop_cnt++)
					{
						recProp = vecProps[prop_cnt];
						// Setup Keystring for Property in DB-Table; 080813 p�d
						sKeyStrProp2.Format(_T("%s%s%s%s%s"),
								recProp.getCountyName(),
								recProp.getMunicipalName(),
								recProp.getParishName(),
								recProp.getPropertyNum(),
								recProp.getPropFullName());
						// Remove all 'blanksteg', from string; 080814 p�d
						// To make sure the compare'll work; 080814 p�d
						sKeyStrProp1.Remove(' ');
						sKeyStrProp2.Remove(' ');

						if (sKeyStrProp2.CompareNoCase(sKeyStrProp1) == 0)
						{

							for (UINT contact_cnt = 0;contact_cnt < vecContacts.size();contact_cnt++)
							{
								recContact = vecContacts[contact_cnt];
								// Setup Keystring for Contact in ReportCtrl; 080813 p�d
								sKeyStrContact2.Format(_T("%s%s%s%s"),
																				recContact.getName(),
																				recContact.getAddress(),
																				recContact.getPostNum(),
																				recContact.getPostAddress());
								//Rensa mellanslag s� j�mf�relsen mellan kontakter fungerar �ven vid matcing av fastighet-->kontakt
								//Bug #2474 20111020 J�
								sKeyStrContact1.Remove(' ');
								sKeyStrContact2.Remove(' ');
								if (sKeyStrContact2.CompareNoCase(sKeyStrContact1) == 0)
								{
									if (m_pDB != NULL)
									{
										//Kolla om �garen redan finns, i s� fall uppdatera bara "kontakt-Ja elle nej info" samt �garandel Bug #2474 20111020 J�
										if(m_pDB->propOwnersExists(CTransaction_prop_owners(recProp.getID(),recContact.getID(),sShareInProp,bDefaultContact,_T(""))))
											m_pDB->updPropOwner(CTransaction_prop_owners(recProp.getID(),recContact.getID(),sShareInProp,bDefaultContact,_T("")));
										else
											m_pDB->addPropOwner(CTransaction_prop_owners(recProp.getID(),recContact.getID(),sShareInProp,bDefaultContact,_T("")));

									}	// if (m_pDB != NULL)
								}	// if (sKeyStrContact2.CompareNoCase(sKeyStrContact1) == 0)
							}	// for (UINT contact_cnt = 0;contact_cnt < vecContacts.size();contact_cnt++)
						}	// if (sKeyStrProp2.CompareNoCase(sKeyStrProp1) == 0)
					}	// for (UINT prop_cnt = 0;prop_cnt < vecProps.size();prop_cnt++)


				}	// if (vecProps.size() > 0 && vecContacts.size() > 0)
				pRec = NULL;
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)

	vecProps.clear();
	vecContacts.clear();

}

//#5008 PH 20160615
void CImportDataReportView::setObjectId(int objectId) {
	elvObjId=objectId;
}
