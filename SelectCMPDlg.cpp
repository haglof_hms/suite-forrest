// SelectCMPDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "SelectCMPDlg.h"

#include "ResLangFileReader.h"

// CSelectCMPDlg dialog

IMPLEMENT_DYNAMIC(CSelectCMPDlg, CDialog)


BEGIN_MESSAGE_MAP(CSelectCMPDlg, CDialog)
	ON_LBN_SELCHANGE(IDC_LIST1, &CSelectCMPDlg::OnLbnSelchangeList1)
	ON_LBN_SELCHANGE(IDC_LIST3, &CSelectCMPDlg::OnLbnSelchangeList3)
	ON_LBN_SELCHANGE(IDC_LIST4, &CSelectCMPDlg::OnLbnSelchangeList4)
END_MESSAGE_MAP()

CSelectCMPDlg::CSelectCMPDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectCMPDlg::IDD, pParent)
{

}

CSelectCMPDlg::~CSelectCMPDlg()
{
	m_sarrData.RemoveAll();
}

void CSelectCMPDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectCMPDlg)
	DDX_Control(pDX, IDC_LBL1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL4, m_wndLbl4);

	DDX_Control(pDX, IDC_LIST1, m_wndLB1);
	DDX_Control(pDX, IDC_LIST3, m_wndLB2);
	DDX_Control(pDX, IDC_LIST4, m_wndLB3);

	DDX_Control(pDX, IDOK, m_wndBtnOK);
	//}}AFX_DATA_MAP
}

BOOL CSelectCMPDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_wndLbl1.SetBkColor(INFOBK);

	m_wndBtnOK.EnableWindow( FALSE );

	populateListBox();
	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING2918)));
			m_wndBtnOK.SetWindowText((xml.str(IDS_STRING2914)));
			m_wndLbl1.SetWindowText((xml.str(IDS_STRING2919)));
			m_wndLbl2.SetWindowText((xml.str(IDS_STRING253)));
			m_wndLbl3.SetWindowText((xml.str(IDS_STRING254)));
			m_wndLbl4.SetWindowText((xml.str(IDS_STRING255)));
		}
		xml.clean();
	}


	return TRUE;
}


void CSelectCMPDlg::populateListBox(void)
{
	m_wndLB1.ResetContent();	// Clear
	for (int i = 0;i < m_sarrData.GetCount();i++)
	{
		m_wndLB1.AddString(m_sarrData.GetAt(i));
	}	// for (int i = 0;i < arr.GetCount();i++)
}

// CSelectCMPDlg message handlers

void CSelectCMPDlg::OnLbnSelchangeList1()
{
	vecCMP_data vecMunicipals;
	int nIndex = m_wndLB1.GetCurSel();
	sSelectedCounty.Empty();
	if (nIndex > -1)
	{
		m_wndLB1.GetText(nIndex,sSelectedCounty);
		// Try to select Municpals for County, from DB; 080812 p�d
		if (m_pDB != NULL)
		{
			m_pDB->getMunicipals(sSelectedCounty,vecMunicipals);

			if (vecMunicipals.size() > 0)
			{
				m_wndLB2.ResetContent();
				for (UINT i = 0;i < vecMunicipals.size();i++)
				{
					m_wndLB2.AddString(vecMunicipals[i].getName());
				}	// for (UINT i = 0;i < vecMunicipals.size();i++)
			}	// if (vecMunicipals.size() > 0)
		}	// if (m_pDB != NULL)
	}	// if (nIndex > -1)
}

void CSelectCMPDlg::OnLbnSelchangeList3()
{
	vecCMP_data vecParish;
	int nIndex = m_wndLB2.GetCurSel();
	m_wndBtnOK.EnableWindow( nIndex > -1 );
	sSelectedMunicipal.Empty();
	if (nIndex > -1)
	{
		m_wndLB2.GetText(nIndex,sSelectedMunicipal);
		// Try to select Municpals for County, from DB; 080812 p�d
		if (m_pDB != NULL)
		{
			m_pDB->getParishs(sSelectedCounty,sSelectedMunicipal,vecParish);

			if (vecParish.size() > 0)
			{
				m_wndLB3.ResetContent();
				for (UINT i = 0;i < vecParish.size();i++)
				{
					m_wndLB3.AddString(vecParish[i].getName());
				}	// for (UINT i = 0;i < vecMunicipals.size();i++)
			}	// if (vecMunicipals.size() > 0)
		}	// if (m_pDB != NULL)
	}	// if (nIndex > -1)
}

void CSelectCMPDlg::OnLbnSelchangeList4()
{
	int nIndex = m_wndLB3.GetCurSel();
	m_wndBtnOK.EnableWindow( nIndex > -1 );
	if (nIndex > -1)
	{
		m_wndLB3.GetText(nIndex,sSelectedParish);
	}
}
