#pragma once

#include "Resource.h"
#include "MDIBaseFrame.h"


struct _prop_data
{
	//private:
	CString m_sKey;
	CString m_sCounty;
	CString m_sMunicipal;
	CString m_sParish;
	CString m_sPropNum;
	CString m_sPropName;
	CString m_sObjID;
	CString m_sMottagarref;
	int row;

	_prop_data(void)
	{
		m_sKey = _T("");
		m_sCounty = _T("");
		m_sMunicipal = _T("");
		m_sParish = _T("");
		m_sPropNum = _T("");
		m_sPropName = _T("");
		m_sObjID = _T("");
		m_sMottagarref = _T("");
		row=0;
	}
	_prop_data(LPCTSTR key,LPCTSTR county,LPCTSTR municipal,LPCTSTR parish,LPCTSTR prop_num,LPCTSTR prop_name,LPCTSTR obj_id,LPCTSTR mottagarref)
	{
		m_sKey = (key);
		m_sCounty = (county);
		m_sMunicipal = (municipal);
		m_sParish = (parish);
		m_sPropNum = (prop_num);
		m_sPropName = (prop_name);
		m_sObjID = (obj_id);
		m_sMottagarref = mottagarref;
	}
	_prop_data(LPCTSTR key,LPCTSTR county,LPCTSTR municipal,LPCTSTR parish,LPCTSTR prop_num,LPCTSTR prop_name,LPCTSTR obj_id,LPCTSTR mottagarref,int rowNr)
	{
		m_sKey = (key);
		m_sCounty = (county);
		m_sMunicipal = (municipal);
		m_sParish = (parish);
		m_sPropNum = (prop_num);
		m_sPropName = (prop_name);
		m_sObjID = (obj_id);
		m_sMottagarref = mottagarref;
		row=rowNr;
	}
	
};

typedef std::vector<_prop_data> vecPropData;

//////////////////////////////////////////////////////////////////////////////
// CImportDataReportView form view

class CImportDataReportView : public  CMyReportView
{
	DECLARE_DYNCREATE(CImportDataReportView)

	BOOL m_bInitialized;

	CString m_sLangFN;

	BOOL m_bIsDirty;

	CStringArray m_sarrColumnNames;

	vecMapColumns m_mapColumns;

	BOOL m_bIsCreatePropsAndOwners;

	CString m_sMsgCap;
	CString m_sMsgWrongExtension;
	CString m_sMsgMissingPropNameNumber;

	CString m_sMsgToManyProp;
	CString m_sMsgToManyPropCap;

	CString m_sMsgNumOfProps;
	CString m_sMsgAdding;
	CString m_sMsgImportDone;
	CString m_sMsgStartImport;
	CString m_sMsgOpenEXCEL;

	CString m_sTmplFileName;

	CMDIImportDataFrame *m_pViewFrame;

	CXTPStatusBar m_wndStatusBar;

	//#5008 PH 20160614
	int elvObjId;

	//HMS-28 felmeddelanden
	CString m_sMsgShareErr1;
	CString m_sMsgProp;
	CString m_sMsgOwner;
	CString m_sMsgShare;
	CString m_sMsgShareErr2;

protected:
	CImportDataReportView();           // protected constructor used by dynamic creation
	virtual ~CImportDataReportView();

	BOOL setupReport(void);
	BOOL newImportData(void);

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	// .. for constraints; 080811 p�d
	vecCMP_data m_vecCounties;
	vecCMP_data m_vecMunicipals;
	vecCMP_data m_vecParish;

	void addCountyConstraints(void);
	void addMunicipalConstraints(int county_id);
	void addMunicipalConstraints(LPCTSTR county);
	void addParishConstraints(int county_id,int municipal_id);
	void addParishConstraints(LPCTSTR county,LPCTSTR municipal);

	CString removeStupidCharacters(CString s) { 
												s.Remove(L' ');
												s.Remove(L'\r');
												s.Remove(L'\n');
												return s;
	}

	int compareStrings(CString s1,CString s2) {
		return removeStupidCharacters(s1).CompareNoCase(removeStupidCharacters(s2));
	}

	CString trimChars(CString s) {
		s.Trim(L'\r');
		s.Trim(L'\n');
		s.Trim(L' ');
		return s;
	}

	BOOL checkImportData(void);

	void resetColumnMap(void);

	void importProperties(void);
	void importOwners(void);
	void matchOwnersToProperty(void);

	vecExcel_import m_vecExcel_import;
	map<CString,int> m_mapProps;
	

	BOOL checkCMP(LPCTSTR county);	// Couny,Municipal and Parish

	CString m_sMsgEnteredDataForProperties;
	CString m_sMsgEnteredDataForContacts;

	vecTransactionCategoriesForContacts m_vecSelectedCatgoriesForContactData;

public:

	void setObjectId(int objectId);//#5008 PH 20160615

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	afx_msg void OnImportDataFile(void);
	afx_msg void OnCreatePropsAndOwnersFile(void);
	afx_msg void OnUpdateCreatePropsAndOwnersTBtn(CCmdUI* pCmdUI);
	afx_msg void OnToolsAddChangeObjID(void);
	afx_msg void OnUpdateToolsTBtn1(CCmdUI* pCmdUI);
	afx_msg void OnToolsCreateExcelTemplate(void);
	afx_msg void OnUpdateToolsTBtn2(CCmdUI* pCmdUI);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};



