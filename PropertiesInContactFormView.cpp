
#include "stdafx.h"
#include "Forrest.h"
#include "PropertiesInContactFormView.h"
#include "ResLangFileReader.h"
#include "ContactsTabView.h"
#include "XTPPreviewView.h"

// CPropertiesInContactFormView

IMPLEMENT_DYNCREATE(CPropertiesInContactFormView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPropertiesInContactFormView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_COMMAND(ID_TBBTN_REFRESH, OnRefresh)
END_MESSAGE_MAP()

CPropertiesInContactFormView::CPropertiesInContactFormView()
	: CXTPReportView()
{
	m_pDB = NULL;
	m_nSelectedColumn = -1;
	m_bInitialized=FALSE;
}

CPropertiesInContactFormView::~CPropertiesInContactFormView()
{
}

void CPropertiesInContactFormView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();
	if (! m_bInitialized )
	{
		m_nContactId=-1;
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
		setupReport();
		m_bInitialized = TRUE;

	}
}

BOOL CPropertiesInContactFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CPropertiesInContactFormView::OnDestroy()
{
	if (m_pDB != NULL)
		delete m_pDB;
	CXTPReportView::OnDestroy();	
}

BOOL CPropertiesInContactFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CPropertiesInContactFormView diagnostics

#ifdef _DEBUG
void CPropertiesInContactFormView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CPropertiesInContactFormView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CPropertiesInContactFormView message handlers

// CPropertiesInContactFormView message handlers
void CPropertiesInContactFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CPropertiesInContactFormView::OnSetFocus(CWnd*)
{
	/*
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);*/
}

					
void CPropertiesInContactFormView::SetColBehaviour(CXTPReportColumn *pCol)
{
	pCol->AllowRemove(false);
	pCol->SetEditable( FALSE );
	pCol->SetHeaderAlignment( DT_CENTER );
	pCol->SetAlignment( DT_CENTER );
}

// Create and add Assortment settings reportwindow
BOOL CPropertiesInContactFormView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
				m_sGroupByThisField	= (xml.str(IDS_STRING244));
				m_sGroupByBox				= (xml.str(IDS_STRING245));
				m_sFieldChooser			= (xml.str(IDS_STRING246));

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					GetReportCtrl().ShowWindow( SW_NORMAL );
					// Add these 3 lines to add scrollbars for View; 070319 p�d
					GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
					GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
					GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(0, (xml.str(IDS_STRING2701)), 40));
					SetColBehaviour(pCol);					
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(1, (xml.str(IDS_STRING253)), 150));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(2, (xml.str(IDS_STRING254)), 150));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(3, (xml.str(IDS_STRING255)), 150));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(4, (xml.str(IDS_STRING2560)), 150));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(5, (xml.str(IDS_STRING256)), 150));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(6, (xml.str(IDS_STRING257)), 50));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(7, (xml.str(IDS_STRING258)), 50));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(8, (xml.str(IDS_STRING259)), 100));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(9, (xml.str(IDS_STRING260)), 100));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(10, (xml.str(IDS_STRING401)), 150));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(11, (xml.str(IDS_STRING402)), 100));
					SetColBehaviour(pCol);
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(12, (xml.str(IDS_STRING403)), 100));
					SetColBehaviour(pCol);

					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
					GetReportCtrl().GetRecords()->SetCaseSensitive(FALSE);
					GetReportCtrl().FocusSubItems(TRUE);

					//populateReport();		

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml.Load(m_sLangFN))
			xml.clean();
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CPropertiesInContactFormView::populateReport(void)
{
	GetReportCtrl().GetRecords()->RemoveAll();
	if (m_vecPropertyData.size()<1)
	{
		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();
		return;
	}
	
	for (UINT i = 0;i < m_vecPropertyData.size();i++)
	{
		CTransaction_prop_obj data = m_vecPropertyData[i];
		GetReportCtrl().AddRecord(new CPropObjReportDataRec(i,data));
	}
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

//Lagt till metod att populera en rapport med kontaktid som filter
//20111005 J� Feature #2323
void CPropertiesInContactFormView::populateReport(int nContactId)
{
	if (m_pDB != NULL)
	{
		m_pDB->getProperties(m_vecPropertyData,nContactId);
	}	// if (m_pDB != NULL)
	m_nContactId=nContactId;
	populateReport();
}
     
void CPropertiesInContactFormView::OnRefresh()
{
	if(m_nContactId!=-1)
	populateReport(m_nContactId);
}

// CPropertiesInContactFormView message handlers





