// AddChangeObjIDDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "AddChangeObjIDDlg.h"

#include "ResLangFileReader.h"

// CAddChangeObjIDDlg dialog

IMPLEMENT_DYNAMIC(CAddChangeObjIDDlg, CDialog)


BEGIN_MESSAGE_MAP(CAddChangeObjIDDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CAddChangeObjIDDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CAddChangeObjIDDlg::CAddChangeObjIDDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddChangeObjIDDlg::IDD, pParent)
{

}

CAddChangeObjIDDlg::~CAddChangeObjIDDlg()
{
}

void CAddChangeObjIDDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddChangeObjIDDlg)
	DDX_Control(pDX, IDC_LBL8_1, m_wndLbl1);
	DDX_Control(pDX, IDC_EDIT8_1, m_wndEdit1);

	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP

}

BOOL CAddChangeObjIDDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	setLanguage();

	return TRUE;
}

// CAddChangeObjIDDlg message handlers

void CAddChangeObjIDDlg::setLanguage(void)
{
	// Setup language filename; 051214 p�d
	CString sLangFN;
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			SetWindowText((xml.str(IDS_STRING3230)));
			m_wndLbl1.SetWindowText((xml.str(IDS_STRING3231)));

			m_btnOK.SetWindowText((xml.str(IDS_STRING2914)));
			m_btnCancel.SetWindowText((xml.str(IDS_STRING2915)));
		}
		xml.clean();
	}
}

void CAddChangeObjIDDlg::OnBnClickedOk()
{
	m_sObjID = m_wndEdit1.getText();
	OnOK();
}
