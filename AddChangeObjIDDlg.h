#pragma once

#include "Resource.h"

// CAddChangeObjIDDlg dialog

class CAddChangeObjIDDlg : public CDialog
{
	DECLARE_DYNAMIC(CAddChangeObjIDDlg)

	CMyExtStatic m_wndLbl1;

	CMyExtEdit m_wndEdit1;

	CButton m_btnOK;
	CButton m_btnCancel;

	CString m_sObjID;

	void setLanguage(void);
public:
	CAddChangeObjIDDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddChangeObjIDDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG8 };


	inline CString getObjID(void) { return m_sObjID; }

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddChangeObjIDDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
