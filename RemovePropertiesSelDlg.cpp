// RemovePropertiesSelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Forrest.h"
#include "RemovePropertiesSelDlg.h"

#include "ResLangFileReader.h"

// CRemovePropertiesSelDlg dialog

IMPLEMENT_DYNAMIC(CRemovePropertiesSelDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CRemovePropertiesSelDlg, CXTResizeDialog)
END_MESSAGE_MAP()

CRemovePropertiesSelDlg::CRemovePropertiesSelDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CRemovePropertiesSelDlg::IDD, pParent)
{
	m_bInitialized = FALSE;
}

CRemovePropertiesSelDlg::~CRemovePropertiesSelDlg()
{
}

void CRemovePropertiesSelDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL6_1, m_wndLbl6_1);
	DDX_Control(pDX, IDC_LBL6_2, m_wndLbl6_2);
	DDX_Control(pDX, IDC_LBL6_3, m_wndLbl6_3);
	DDX_Control(pDX, IDC_LBL6_4, m_wndLbl6_4);
	DDX_Control(pDX, IDC_LBL6_5, m_wndLbl6_5);
	DDX_Control(pDX, IDC_LBL6_6, m_wndLbl6_6);
	DDX_Control(pDX, IDC_LBL6_7, m_wndLbl6_7);
	DDX_Control(pDX, IDC_LBL6_8, m_wndLbl6_8);

	DDX_Control(pDX, IDC_EDIT6_1, m_wndEdit6_1);
	DDX_Control(pDX, IDC_EDIT6_2, m_wndEdit6_2);
	DDX_Control(pDX, IDC_EDIT6_3, m_wndEdit6_3);
	DDX_Control(pDX, IDC_EDIT6_4, m_wndEdit6_4);
	DDX_Control(pDX, IDC_EDIT6_5, m_wndEdit6_5);
	DDX_Control(pDX, IDC_EDIT6_6, m_wndEdit6_6);

	DDX_Control(pDX, IDOK, m_wndBtnRemove);
	DDX_Control(pDX, IDC_BUTTON6_1, m_wndBtnSelAll);
	DDX_Control(pDX, IDC_BUTTON6_2, m_wndBtnDeSelAll);
	DDX_Control(pDX, IDC_BUTTON6_3, m_wndBtnSearch);
	DDX_Control(pDX, IDC_BUTTON6_4, m_wndBtnAll);
	DDX_Control(pDX, IDCANCEL, m_wndBtnCancel);
	//}}AFX_DATA_MAP
}

BOOL CRemovePropertiesSelDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (!m_bInitialized)
	{
		m_wndLbl6_1.SetTextColor(BLUE);

		setupReport();

		getProperties();

		populateReport();

		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CRemovePropertiesSelDlg message handlers

void CRemovePropertiesSelDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	StrMap vecStrings;

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		xml->LoadEx(m_sLangFN,vecStrings);
		delete xml;
	}

	if (m_wndReport.GetSafeHwnd() == 0)
	{
		if (!m_wndReport.Create(this,ID_REPORT_REMOVE_PROPS, FALSE, FALSE))
		{
			return;
		}
	}
	if (m_wndReport.GetSafeHwnd() != NULL)
	{	
		SetWindowText(vecStrings[IDS_STRING3300]);

		m_wndLbl6_1.SetWindowText(vecStrings[IDS_STRING3310]);
		m_wndLbl6_2.SetWindowText(vecStrings[IDS_STRING2701]);
		m_wndLbl6_3.SetWindowText(vecStrings[IDS_STRING253]);
		m_wndLbl6_4.SetWindowText(vecStrings[IDS_STRING254]);
		m_wndLbl6_5.SetWindowText(vecStrings[IDS_STRING255]);
		m_wndLbl6_6.SetWindowText(vecStrings[IDS_STRING256]);
		m_wndLbl6_7.SetWindowText(vecStrings[IDS_STRING257]);
		m_wndLbl6_8.SetWindowText(vecStrings[IDS_STRING258]);

		m_wndBtnRemove.SetWindowText(vecStrings[IDS_STRING3311]);
		m_wndBtnSelAll.SetWindowText(vecStrings[IDS_STRING3312]);
		m_wndBtnDeSelAll.SetWindowText(vecStrings[IDS_STRING3313]);
		m_wndBtnSearch.SetWindowText(vecStrings[IDS_STRING3314]);
		m_wndBtnAll.SetWindowText(vecStrings[IDS_STRING3315]);
		m_wndBtnCancel.SetWindowText(vecStrings[IDS_STRING217]);

		m_wndReport.ShowWindow(SW_NORMAL);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_0, _T(""), 20));
		pCol->AllowRemove(FALSE);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
		pCol->SetAlignment(DT_CENTER);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_1, (vecStrings[IDS_STRING253]), 80));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
		pCol->SetAlignment(DT_CENTER);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_2, (vecStrings[IDS_STRING254]), 80));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
		pCol->SetAlignment(DT_LEFT);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_3, (vecStrings[IDS_STRING255]), 80));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
		pCol->SetAlignment(DT_LEFT);

		pCol = m_wndReport.AddColumn(new CXTPReportColumn(COLUMN_4, (vecStrings[IDS_STRING256]), 200));
		pCol->GetEditOptions()->m_bAllowEdit = FALSE;
		pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
		pCol->SetAlignment(DT_LEFT);

		m_wndReport.GetReportHeader()->AllowColumnRemove(FALSE);
		m_wndReport.SetMultipleSelection( FALSE );
		m_wndReport.SetGridStyle( TRUE, xtpReportGridSolid );
		m_wndReport.SetGridStyle( FALSE, xtpReportGridSolid );
		m_wndReport.FocusSubItems(TRUE);
		m_wndReport.AllowEdit(TRUE);
		m_wndReport.GetPaintManager()->SetFixedRowHeight(FALSE);

		// Need to set size of Report control; 071109 p�d
		CWnd *pWnd = GetDlgItem(ID_REPORT_REMOVE_PROPS);
		if (pWnd != NULL)
		{
			RECT rect;
			GetWindowRect(&rect);
			pWnd->SetWindowPos(&CWnd::wndTop,10,80,rect.right-30,rect.bottom-180,SWP_SHOWWINDOW);
		}

	}
	vecStrings.clear();

}

void CRemovePropertiesSelDlg::getProperties(void)
{
	if (m_pDB != NULL)
		m_pDB->getProperties(m_vecProperties);
}

void CRemovePropertiesSelDlg::populateReport(void)
{
	BOOL bIsUsed;
	m_wndReport.ResetContent();
	if (m_vecProperties.size() > 0)
	{
		for (UINT i = 0;i < m_vecProperties.size();i++)
		{
			CTransaction_property rec = m_vecProperties[i];
			bIsUsed = FALSE;
			if (m_pDB != NULL)
				bIsUsed = m_pDB->isPropertyUsed(rec.getID());

			m_wndReport.AddRecord(new CRemovePropertySelRec(rec.getID(),bIsUsed,FALSE,rec));
		}	// for (UINT i = 0;i < m_vecProperties.size();i++)

		m_wndReport.Populate();
		m_wndReport.UpdateWindow();
	}	// if (m_vecProperties.size() > 0)
}
