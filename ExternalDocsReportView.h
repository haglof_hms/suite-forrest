
//////////////////////////////////////////////////////////////////////////////
// CExternalDocsReportView form view

class CExternalDocsReportView : public  CMyReportView
{
	DECLARE_DYNCREATE(CExternalDocsReportView)

	BOOL m_bInitialized;

	CString m_sLangFN;
	BOOL m_bIsDirty;

	CString m_sFilterText;

	_doc_identifer_msg *m_msg;
	CString sFrom;
	CString sTo;
protected:
	CExternalDocsReportView();           // protected constructor used by dynamic creation
	virtual ~CExternalDocsReportView();

	BOOL setupReport(void);

	enumDocsAction m_enumDocsAction;

	CTransaction_external_docs m_recExternalDocs;
	vecTransaction_external_docs m_vecExternalDocs;
	void getExternalDocs(void);
	void saveExternalDocs(void);
	void populateReport(void);

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;

	CString m_sTableName_pk;
	int m_nLinkID_pk;

	CImageList m_ilIcons;

	void LoadReportState(void);
	void SaveReportState(void);

	void viewExtrenalDocument(LPCTSTR fn);
public:

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

	inline void doSaveExternalDocs(void)	{	saveExternalDocs();	}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnSuiteMessage(WPARAM,LPARAM);
	afx_msg void OnReportSelChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	afx_msg void OnAddRow(void);
	afx_msg void OnRemoveRow(void);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

