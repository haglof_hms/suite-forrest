// ContactsFormView.cpp : implementation file
//
#include "stdafx.h"
#include "Forrest.h"
#include "ContactsFormView.h"

#include "ResLangFileReader.h"
#include ".\contactsformview.h"

#include "SelectCategoryDlg.h"

#include "PostnumFormView.h"

#include "PropertyTabView.h"
#include "PropertyFormView.h"

#include "ContactsSelListFormView.h"

#include "SearchAndReplaceFormView.h"

#include <algorithm>

//20111005 J� Feature #2323
#include "ContactsTabView.h"
#include "PropertiesInContactFormView.h"
// CContactsFormView

IMPLEMENT_DYNCREATE(CContactsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CContactsFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_KEYUP()
	ON_WM_COPYDATA()
	ON_WM_DESTROY()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_BN_CLICKED(IDC_BUTTON1_1, OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON1_2, OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON1_3, OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON1_5, OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BTN_ADD_PIC, OnBnClickedBtnAddPic)
	ON_BN_CLICKED(IDC_BTN_DEL_PIC, OnBnClickedBtnDelPic)

	ON_COMMAND(ID_TBBTN_OPEN, OnImport)
 	ON_COMMAND(ID_TBBTN_CREATE, OnSearchReplace)	// Used to do refresh; 090530 p�d

	ON_EN_SETFOCUS(IDC_EDIT1_1, &CContactsFormView::OnEnSetfocusEdit11)
	ON_EN_SETFOCUS(IDC_EDIT1_2, &CContactsFormView::OnEnSetfocusEdit12)
	ON_EN_SETFOCUS(IDC_EDIT1_3, &CContactsFormView::OnEnSetfocusEdit13)
	ON_EN_SETFOCUS(IDC_EDIT1_4, &CContactsFormView::OnEnSetfocusEdit14)
	ON_EN_SETFOCUS(IDC_EDIT1_5, &CContactsFormView::OnEnSetfocusEdit15)
	ON_EN_SETFOCUS(IDC_EDIT1_6, &CContactsFormView::OnEnSetfocusEdit16)
	ON_EN_SETFOCUS(IDC_EDIT1_7, &CContactsFormView::OnEnSetfocusEdit17)
	ON_EN_SETFOCUS(IDC_EDIT1_9, &CContactsFormView::OnEnSetfocusEdit19)
	ON_EN_SETFOCUS(IDC_EDIT1_15, &CContactsFormView::OnEnSetfocusEdit115)
	ON_EN_SETFOCUS(IDC_EDIT1_16, &CContactsFormView::OnEnSetfocusEdit116)
	ON_EN_SETFOCUS(IDC_EDIT1_10, &CContactsFormView::OnEnSetfocusEdit110)
	ON_EN_SETFOCUS(IDC_EDIT1_11, &CContactsFormView::OnEnSetfocusEdit111)
	ON_EN_SETFOCUS(IDC_EDIT1_12, &CContactsFormView::OnEnSetfocusEdit112)
	ON_EN_SETFOCUS(IDC_EDIT1_13, &CContactsFormView::OnEnSetfocusEdit113)
	ON_EN_SETFOCUS(IDC_EDIT1_14, &CContactsFormView::OnEnSetfocusEdit114)
	ON_EN_SETFOCUS(IDC_EDIT1_17, &CContactsFormView::OnEnSetfocusEdit117)
	ON_EN_SETFOCUS(IDC_EDIT1_18, &CContactsFormView::OnEnSetfocusEdit118)
	ON_EN_SETFOCUS(IDC_EDIT1_19, &CContactsFormView::OnEnSetfocusEdit119)
	ON_EN_SETFOCUS(IDC_EDIT1_20, &CContactsFormView::OnEnSetfocusEdit120)
	ON_EN_SETFOCUS(IDC_EDIT1_21, &CContactsFormView::OnEnSetfocusEdit121)
	ON_CBN_SETFOCUS(IDC_COMBO1_1, &CContactsFormView::OnCbSetfocusComobox11)
	ON_CBN_SETFOCUS(IDC_COMBO1_2, &CContactsFormView::OnCbSetfocusComobox12)
	ON_CBN_SELCHANGE(IDC_COMBO1_2, &CContactsFormView::OnCbnSelchangeCombo12)

	ON_EN_SETFOCUS(IDC_EDIT1_8, &CContactsFormView::OnEnSetfocusEdit18)
END_MESSAGE_MAP()

CContactsFormView::CContactsFormView()
	: CXTResizeFormView(CContactsFormView::IDD)
{
	m_vecContactsData = new vecTransactionContacts;
	m_bInitialized = FALSE;
	m_bSetFocusOnInitDone = FALSE;
	m_bIsDataEnabled = FALSE;
	m_pDB = NULL;
	m_bSavePic = FALSE;
	objectId=-1;
	propId=-1;
	disableNavButtons=FALSE;

}

CContactsFormView::~CContactsFormView()
{
}

void CContactsFormView::OnDestroy()
{
	m_vecAddedLanguages.clear();

	if (m_pDB != NULL)
		delete m_pDB;

	CXTResizeFormView::OnDestroy();
}

void CContactsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_CONTACT_GROUP, m_wndGroup1);
	DDX_Control(pDX, IDC_PICTURE_GROUP, m_wndGroup2);
	DDX_Control(pDX, IDC_LBL1_1, m_wndLbl1);
	DDX_Control(pDX, IDC_LBL1_2, m_wndLbl2);
	DDX_Control(pDX, IDC_LBL1_3, m_wndLbl3);
	DDX_Control(pDX, IDC_LBL1_4, m_wndLbl4);
	DDX_Control(pDX, IDC_LBL1_18, m_wndLbl18);
	DDX_Control(pDX, IDC_LBL1_5, m_wndLbl5);
	DDX_Control(pDX, IDC_LBL1_6, m_wndLbl6);
	DDX_Control(pDX, IDC_LBL1_7, m_wndLbl7);
	DDX_Control(pDX, IDC_LBL1_8, m_wndLbl8);
	DDX_Control(pDX, IDC_LBL1_9, m_wndLbl9);
	DDX_Control(pDX, IDC_LBL1_10, m_wndLbl10);
	DDX_Control(pDX, IDC_LBL1_11, m_wndLbl11);
	DDX_Control(pDX, IDC_LBL1_12, m_wndLbl12);
	DDX_Control(pDX, IDC_LBL1_13, m_wndLbl13);
	DDX_Control(pDX, IDC_LBL1_14, m_wndLbl14);
	DDX_Control(pDX, IDC_LBL1_15, m_wndLbl15);
	DDX_Control(pDX, IDC_LBL1_16, m_wndLbl16);
	DDX_Control(pDX, IDC_LBL1_17, m_wndLbl17);
	DDX_Control(pDX, IDC_LBL1_19, m_wndLbl19);
	DDX_Control(pDX, IDC_LBL1_20, m_wndLbl20);
	DDX_Control(pDX, IDC_LBL1_21, m_wndLbl21);
	DDX_Control(pDX, IDC_LBL1_22, m_wndLbl22);

	DDX_Control(pDX, IDC_COMBO1_1, m_wndCBox1);
	DDX_Control(pDX, IDC_COMBO1_2, m_wndCBox2);

	DDX_Control(pDX, IDC_EDIT1_1, m_wndEdit1);
	DDX_Control(pDX, IDC_EDIT1_2, m_wndEdit2);
	DDX_Control(pDX, IDC_EDIT1_3, m_wndEdit3);
	DDX_Control(pDX, IDC_EDIT1_4, m_wndEdit4);
	DDX_Control(pDX, IDC_EDIT1_5, m_wndEdit5);
	DDX_Control(pDX, IDC_EDIT1_6, m_wndEdit6);
	DDX_Control(pDX, IDC_EDIT1_7, m_wndEdit7);
	DDX_Control(pDX, IDC_EDIT1_9, m_wndEdit9);
	DDX_Control(pDX, IDC_EDIT1_10, m_wndEdit10);
	DDX_Control(pDX, IDC_EDIT1_11, m_wndEdit11);
	DDX_Control(pDX, IDC_EDIT1_12, m_wndEdit12);
	DDX_Control(pDX, IDC_EDIT1_13, m_wndEdit13);
	DDX_Control(pDX, IDC_EDIT1_14, m_wndEdit14);
	DDX_Control(pDX, IDC_EDIT1_15, m_wndEdit15);
	DDX_Control(pDX, IDC_EDIT1_16, m_wndEdit16);
	DDX_Control(pDX, IDC_EDIT1_17, m_wndEdit17);
	DDX_Control(pDX, IDC_EDIT1_18, m_wndEdit18);
	DDX_Control(pDX, IDC_EDIT1_19, m_wndEdit19);
	DDX_Control(pDX, IDC_EDIT1_20, m_wndEdit20);
	DDX_Control(pDX, IDC_EDIT1_21, m_wndEdit21);

	DDX_Control(pDX, IDC_BUTTON1_1, m_wndBtn1);

	DDX_Control(pDX, IDC_BUTTON1_2, m_wndPostNumBtn);
	DDX_Control(pDX, IDC_BUTTON1_3, m_wndAddCompanyBtn);
	DDX_Control(pDX, IDC_BUTTON1_5, m_wndPostAdrBtn);

	DDX_Control(pDX, IDC_PICTURE, m_wndPicCtrl);
	DDX_Control(pDX, IDC_BTN_ADD_PIC, m_wndBtnAddPic);
	DDX_Control(pDX, IDC_BTN_DEL_PIC, m_wndBtnDelPic);
	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_EDIT1_8, m_wndEdit8);

}

BOOL CContactsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CXTResizeFormView::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_STATICEDGE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;

	return TRUE;

}

void CContactsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

//	SetScaleToFitSize(CSize(90, 1));

//	m_wndCBox1.SetBkColor( WHITE );

	m_wndEdit1.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit2.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit3.SetDisabledColor(  BLACK, INFOBK );
	m_wndEdit3.SetReadOnly();
	m_wndEdit4.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit8.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit5.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit6.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit7.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit9.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit10.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit11.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit12.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit13.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit15.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit16.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit17.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit18.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit19.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit20.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit21.SetDisabledColor(  BLACK, COL3DFACE );

	m_wndEdit14.SetEnabledColor( BLACK, WHITE );
	m_wndEdit14.SetDisabledColor(  BLACK, INFOBK );
	m_wndEdit14.SetReadOnly( TRUE );
	if (! m_bInitialized )
	{

		// Setup language filename; 051214 p�d
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		setLanguage();
		setLanguages();

		//m_sSQLContacts.Format(_T("select * from %s where type_of>=0"),TBL_CONTACTS);
		m_sSQLContacts.Format(_T("select * from %s"),TBL_CONTACTS);
		getContacts();
		getCategories();
		getCategoriesForContact();

		m_nDBIndex = m_vecContactsData->size()-1;
		populateData(m_nDBIndex);
		setEnableData(m_vecContactsData->size() > 0);
		m_enumAction = UPD_ITEM;

		m_wndPostNumBtn.SetBitmap(CSize(18,14),IDB_BITMAP2);
		m_wndPostNumBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_wndAddCompanyBtn.SetBitmap(CSize(18,14),IDB_BITMAP2);
		m_wndAddCompanyBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_wndPostAdrBtn.SetBitmap(CSize(18,14),IDB_BITMAP2);
		m_wndPostAdrBtn.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_bInitialized = TRUE;
		m_bIsDirty = FALSE;
	}
}

BOOL CContactsFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		m_bConnected = m_dbConnectionData.conn->isConnected();
		if (m_bConnected)
		{
			m_pDB = new CForrestDB(m_dbConnectionData);
		}
	}
	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CContactsFormView::OnSetFocus(CWnd *pWnd)
{
	if(!disableNavButtons)
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,!global_IS_SEARCH_REPLACE_OPEN);

	CXTResizeFormView::OnSetFocus(pWnd);
}

// Handle key strokes and send message to
// HMSShell, �which the sell interptret and sends back
// a message like ID_NEW_ITEM etc; 070103 p�d
void CContactsFormView::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	//BOOL bControlKey = (GetKeyState(VK_CONTROL) < 0);
	//AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, nChar,bControlKey);
}

// CContactsFormView diagnostics

#ifdef _DEBUG
void CContactsFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

void CContactsFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

// PRIVATE
void CContactsFormView::setLanguage(void)
{
	CString sPhoneStr;
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_wndLbl1.SetWindowText(xml.str(IDS_STRING221));
			m_wndLbl2.SetWindowText(xml.str(IDS_STRING222));
			m_wndLbl3.SetWindowText(xml.str(IDS_STRING223));
			m_wndLbl4.SetWindowText(xml.str(IDS_STRING224));
			m_wndLbl18.SetWindowText(xml.str(IDS_STRING323));
			m_wndLbl5.SetWindowText(xml.str(IDS_STRING225));
			m_wndLbl6.SetWindowText(xml.str(IDS_STRING226));
			m_wndLbl7.SetWindowText(xml.str(IDS_STRING227));
			m_wndLbl8.SetWindowText(xml.str(IDS_STRING228));
			sPhoneStr.Format(_T("%s %s"),
												xml.str(IDS_STRING229),
												xml.str(IDS_STRING2290));
			m_wndLbl9.SetWindowText(sPhoneStr);
			sPhoneStr.Format(_T("%s %s"),
												xml.str(IDS_STRING229),
												xml.str(IDS_STRING2291));
			m_wndLbl14.SetWindowText(sPhoneStr);
			m_wndLbl10.SetWindowText(xml.str(IDS_STRING230));
			m_wndLbl11.SetWindowText(xml.str(IDS_STRING231));
			m_wndLbl12.SetWindowText(xml.str(IDS_STRING232));
			m_wndLbl13.SetWindowText(xml.str(IDS_STRING233));
			m_wndLbl15.SetWindowText(xml.str(IDS_STRING247));
			m_wndLbl16.SetWindowText(xml.str(IDS_STRING2350));
			m_wndLbl17.SetWindowText(xml.str(IDS_STRING2351));
			m_wndLbl19.SetWindowText(xml.str(IDS_STRING2370));
			m_wndLbl20.SetWindowText(xml.str(IDS_STRING2371));
			m_wndLbl21.SetWindowText(xml.str(IDS_STRING2372));
			m_wndLbl22.SetWindowText(xml.str(IDS_STRING2373));

			m_wndBtn1.SetWindowText(xml.str(IDS_STRING234));

			m_sOKBtn = xml.str(IDS_STRING240);
			m_sCancelBtn = xml.str(IDS_STRING241);

			m_sErrCap = xml.str(IDS_STRING213);
			m_sSaveMsg.Format(_T("%s\n\n%s\n"),
								xml.str(IDS_STRING2360),
								xml.str(IDS_STRING237));

			m_sDoneSavingMsg =	xml.str(IDS_STRING2361);

			m_sContactActiveMsg.Format(_T("%s\n%s\n\n%s\n%s\n"),
								xml.str(IDS_STRING3010),
								xml.str(IDS_STRING3011),
								xml.str(IDS_STRING3012),
								xml.str(IDS_STRING3013));

			m_sMsgCap = xml.str(IDS_STRING213);
			m_sDeleteMsg = xml.str(IDS_STRING239);
			m_sPNR_ORGNR	= xml.str(IDS_STRING221);
			m_sName	= xml.str(IDS_STRING222);
			m_sCompany = xml.str(IDS_STRING223);

			m_sNoContactsMsg.Format(_T("%s\n\n%s"),
				xml.str(IDS_STRING3112),
				xml.str(IDS_STRING3113));

			m_sNoResultInSearch = xml.str(IDS_STRING3221);


			m_sarrTypeOfContact.Add(xml.str(IDS_STRING2353));
			m_sarrTypeOfContact.Add(xml.str(IDS_STRING2354));

			m_wndBtnAddPic.SetWindowTextW(xml.str(IDS_STRING2355));
			m_wndBtnDelPic.SetWindowTextW(xml.str(IDS_STRING2356));

			m_sNameMissing = xml.str(IDS_STRING2357);

			m_sContactNameMissing = xml.str(IDS_STRING343);

			xml.clean();
		}
	}
}

// Function to sort vecLanguages, makin' sure we get
// languages in ascending order; 081020 p�d
bool SortData(CString& lhs, CString& rhs)
{
	return lhs.CompareNoCase(rhs) < 0;
}

// Add languages to ComboBox and set to slectec languages
// in HMS registry; 061220 p�d
void CContactsFormView::setLanguages(void)
{
	CString sLangSet,sLang,sFound,sPrevLang;
	//BOOL bFound = FALSE;
	_languages rec;
	CList<_languages>listLang;
	std::vector<CString>vecLanguages;
	getLangFiles(listLang);

	if (listLang.GetCount() > 0)
	{
		POSITION pos = listLang.GetHeadPosition();
		for (int i = 0;i < listLang.GetCount();i++)
		{
			rec = (_languages)listLang.GetNext(pos);
			vecLanguages.push_back(rec.szLng);
			// Try to find out language selected and set to nIndex; 061220 p�d
			// 20200814 H�nder aldrig eftersom inte m_sLangAbrev anv�nds 
			/*if (_tcscmp(m_sLangAbrev,rec.szLngAbrev) == 0)
			{
				sLangSet = rec.szLng;
				bFound = TRUE;
			}	// if (_tcscmp(m_sLangAbrev,rec.szLngAbrev) == 0)*/
		}	// for (int i = 0;i < listLang.GetCount();i++)
	}	// if (listLang.GetCount() > 0)
	std::sort(vecLanguages.begin(),vecLanguages.end(),SortData);

	m_vecAddedLanguages.push_back(_T("")); // 20200814 L�gg till tomt val som land #HMS-60
	m_wndCBox1.AddString(_T(""));		   // 20200814 L�gg till tomt val som land #HMS-60	

	if (vecLanguages.size() > 0)
	{
		for (UINT i = 0;i < vecLanguages.size();i++)
		{
			sLang = vecLanguages[i];
			if (sLang.CompareNoCase(sPrevLang) != 0)
			{
				m_wndCBox1.AddString(sLang);
				m_vecAddedLanguages.push_back(sLang);
			}
			sPrevLang = sLang;
		}	// for (int i = 0;i < listLang.GetCount();i++)
	}	// if (listLang.GetCount() > 0)
	//20200814 H�nder aldrig eftersom inte m_sLangAbrev anv�nds 
	/*if (bFound)
		m_wndCBox1.SetCurSel(m_wndCBox1.FindString(-1,sLangSet));*/

	// Add to Combocox for TypeOf Contact; 090825 p�d
	if (m_sarrTypeOfContact.GetCount() > 0)
	{
		for (short i = 0;i < m_sarrTypeOfContact.GetCount();i++)
		{
			m_wndCBox2.AddString(m_sarrTypeOfContact.GetAt(i));
		}	// for (short i = 0;i < m_sarrTypeOfContact.GetCount();i++)
	}	// if (m_sarrTypeOfContact.GetCount() > 0)

	vecLanguages.clear();
	listLang.RemoveAll();
}

void CContactsFormView::populateData(int idx)
{
	CString sTmp=_T("");
	if (m_vecContactsData->size() > 0 && 
		  idx >= 0 && 
			idx < m_vecContactsData->size())
	{
		m_activeContactData = (*m_vecContactsData)[idx];

		//Lagt till f�r feature #2323 20111007 J�
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pTabView != NULL)
		{		
			CPropertiesInContactFormView* pView = pTabView->getPropertiesInContactFormView();
				if(pView)
					pView->populateReport(m_activeContactData.getID());


			sTmp.Format(_T("%s %s %s %s"),
								m_activeContactData.getName(),
								m_activeContactData.getAddress(),
								cleanCRLF(m_activeContactData.getPostNum(),L""),
								m_activeContactData.getPostAddress());
			pTabView->setMsg(sTmp);

		}


		m_wndEdit1.SetWindowText(m_activeContactData.getPNR_ORGNR());
		m_wndEdit2.SetWindowText(m_activeContactData.getName());
		m_wndEdit3.SetWindowText(m_activeContactData.getCompany());
		CString cAdrTest=_T("");
		cAdrTest=m_activeContactData.getAddress();
		m_wndEdit4.SetWindowText(cAdrTest);
		m_wndEdit8.SetWindowText(m_activeContactData.getCoAddress());
		m_wndEdit5.SetWindowText(cleanCRLF(m_activeContactData.getPostNum(),L""));
		m_wndEdit6.SetWindowText(cleanCRLF(m_activeContactData.getPostAddress(),L""));
		m_wndEdit7.SetWindowText(m_activeContactData.getCounty());
/*		
		CString S;
		S.Format(_T("CContactsFormView::populateData\nLand %s\n\n"),m_activeContactData.getCountry());
		AfxMessageBox(S);
*/
		m_wndCBox1.SetCurSel(m_wndCBox1.FindString(0,m_activeContactData.getCountry()));
		//m_wndCBox1.SetWindowText(m_activeContactData.getCountry());
		m_wndEdit9.SetWindowText(m_activeContactData.getPhoneWork());
		m_wndEdit15.SetWindowText(m_activeContactData.getPhoneHome());
		m_wndEdit16.SetWindowText(m_activeContactData.getFaxNumber());
		m_wndEdit17.SetWindowText(m_activeContactData.getVATNumber());
		m_wndEdit10.SetWindowText(m_activeContactData.getMobile());
		m_wndEdit11.SetWindowText(m_activeContactData.getEMail());
		m_wndEdit12.SetWindowText(m_activeContactData.getWebSite());
		m_wndEdit13.SetWindowText(m_activeContactData.getNotes());
		m_wndEdit14.SetWindowText(setCategoriesPerContactStr());

		m_wndEdit18.SetWindowText(m_activeContactData.getBankgiro());
		m_wndEdit19.SetWindowText(m_activeContactData.getPlusgiro());
		m_wndEdit20.SetWindowText(m_activeContactData.getBankkonto());
		m_wndEdit21.SetWindowText(m_activeContactData.getLevnummer());

		if (m_activeContactData.getTypeOf() > -1)
			m_wndCBox2.SetCurSel(m_activeContactData.getTypeOf());
		else
			m_wndCBox2.SetCurSel(0);
		// TypeOf 0 = Person, 1 = Company
		m_wndAddCompanyBtn.EnableWindow(m_activeContactData.getTypeOf() == 0);

		// Try to load image from DB and save to disk; 090825 p�d
		CString sPicPath;
		TCHAR lpTempPathBuffer[1025];
		DWORD dwRetVal = GetTempPath(1024, lpTempPathBuffer);
		CString csBuf = lpTempPathBuffer;
	
		sPicPath.Format(_T("%s%s"),csBuf,_T("tmp.jpg"));
		if (m_pDB)
		{
			if (m_pDB->loadImage(sPicPath, m_activeContactData.getID()))
			{
				if (fileExists(sPicPath)) m_wndPicCtrl.Load(sPicPath);
			}
			else
			{
				m_wndPicCtrl.FreeData();
			}
		}
		Invalidate();
		UpdateWindow();
		m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);		
		removeFile(sPicPath);

		if (m_vecContactsData->size() <= 1)
		{
			setNavigationButtons(FALSE, FALSE, FALSE);
		}
		else
		{
			setNavigationButtons(idx > 0,idx < m_vecContactsData->size() - 1);
		}

		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,TRUE);

	}
	else
	{
		// Not used; 090219 p�d
		//::MessageBox(this->GetSafeHwnd(),(m_sNoContactsMsg),(m_sMsgCap),MB_ICONASTERISK | MB_OK);

		//Lagt till f�r feature #2323 20111007 J�
		CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
		if (pTabView != NULL)
		{		
			CPropertiesInContactFormView* pView = pTabView->getPropertiesInContactFormView();
				if(pView)
					pView->populateReport(-1);
		}

		setNavigationButtons(FALSE,FALSE);
		setEnableData(FALSE);
		setSearchToolbarBtn(FALSE);
		clearAll();
		m_nDBIndex = -1;
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		if (m_enumAction == NEW_ITEM)
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		}
		else
		{
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
			AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
		}
	}
}

// Check if active data's chnged by user.
// I.e. manually enterd data; 061113 p�d
void CContactsFormView::isDataChanged(void)
{
	if(getIsDirty() == TRUE) {
		saveContact(0);
		resetIsDirty();
	}
}

BOOL CContactsFormView::getIsDirty(void)
{
	if (!m_bIsDataEnabled) return FALSE;

	if (m_bIsDirty)
		return m_bIsDirty;

	if (m_wndEdit1.isDirty() ||
			m_wndEdit2.isDirty() ||
			m_wndEdit3.isDirty() ||
			m_wndEdit4.isDirty() ||
			m_wndEdit8.isDirty() ||
			m_wndEdit5.isDirty() ||
			m_wndEdit6.isDirty() ||
			m_wndEdit7.isDirty() ||
			//#2017 A check must also be done on the 2 dropdown boxes.
			m_wndCBox1.FindString(0,m_activeContactData.getCountry()) != m_wndCBox1.GetCurSel() ||
			m_activeContactData.getTypeOf() != m_wndCBox2.GetCurSel() ||
			m_wndEdit9.isDirty() ||
			m_wndEdit15.isDirty() ||
			m_wndEdit16.isDirty() ||
			m_wndEdit10.isDirty() ||
			m_wndEdit11.isDirty() ||
			m_wndEdit12.isDirty() ||
			m_wndEdit13.isDirty() ||
			m_wndEdit14.isDirty() ||
			m_wndEdit15.isDirty() ||
			m_wndEdit16.isDirty() ||
			m_wndEdit17.isDirty() ||
			m_wndEdit18.isDirty() ||
			m_wndEdit19.isDirty() ||
			m_wndEdit20.isDirty() ||
			m_wndEdit21.isDirty() ) 
	{
		return TRUE;
	}

	return FALSE;
}

void CContactsFormView::resetIsDirty(void)
{
	m_wndEdit1.resetIsDirty();
	m_wndEdit2.resetIsDirty();
	m_wndEdit3.resetIsDirty();
	m_wndEdit4.resetIsDirty();
	m_wndEdit8.resetIsDirty();
	m_wndEdit5.resetIsDirty();
	m_wndEdit6.resetIsDirty();
	m_wndEdit7.resetIsDirty();
	m_wndEdit9.resetIsDirty();
	m_wndEdit15.resetIsDirty();
	m_wndEdit16.resetIsDirty();
	m_wndEdit10.resetIsDirty();
	m_wndEdit11.resetIsDirty();
	m_wndEdit12.resetIsDirty();
	m_wndEdit13.resetIsDirty();
	m_wndEdit14.resetIsDirty();
	m_wndEdit15.resetIsDirty();
	m_wndEdit16.resetIsDirty();
	m_wndEdit17.resetIsDirty();
	m_wndEdit18.resetIsDirty();
	m_wndEdit19.resetIsDirty();
	m_wndEdit20.resetIsDirty();
	m_wndEdit21.resetIsDirty();
}

void CContactsFormView::clearAll()
{
	//Lagt till f�r bug #4560 20151012 J�
	CString sTmp=_T("");	
	CContactsTabView *pTabView = (CContactsTabView *)getFormViewByID(IDD_FORMVIEW3);
	if (pTabView != NULL)
	{		
		pTabView->setMsg(sTmp);
		CPropertiesInContactFormView* pView = pTabView->getPropertiesInContactFormView();
		if(pView)
			pView->populateReport(-1);
	}

	m_wndEdit1.SetWindowText(_T(""));
	m_wndEdit2.SetWindowText(_T(""));
	m_wndEdit3.SetWindowText(_T(""));
	m_wndEdit4.SetWindowText(_T(""));
	m_wndEdit8.SetWindowText(_T(""));
	m_wndEdit5.SetWindowText(_T(""));
	m_wndEdit6.SetWindowText(_T(""));
	m_wndEdit7.SetWindowText(_T(""));
	m_wndEdit9.SetWindowText(_T(""));
	m_wndEdit15.SetWindowText(_T(""));
	m_wndEdit16.SetWindowText(_T(""));
	m_wndEdit10.SetWindowText(_T(""));
	m_wndEdit11.SetWindowText(_T(""));
	m_wndEdit12.SetWindowText(_T(""));
	m_wndEdit13.SetWindowText(_T(""));
	m_wndEdit14.SetWindowText(_T(""));
	m_wndEdit15.SetWindowText(_T(""));
	m_wndEdit16.SetWindowText(_T(""));
	m_wndEdit17.SetWindowText(_T(""));
	m_wndEdit18.SetWindowText(_T(""));
	m_wndEdit19.SetWindowText(_T(""));
	m_wndEdit20.SetWindowText(_T(""));
	m_wndEdit21.SetWindowText(_T(""));

	m_wndPicCtrl.FreeData();
	Invalidate();
	UpdateWindow();
	m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);		

	m_wndCBox1.SetCurSel(-1); //20200814 J� Nolla land vid ny kontakt #HMS-60

	m_wndCBox2.SetCurSel(0); //20200814 J� s�tt till person vid ny kontakt #HMS-60
	m_wndCBox2.SetFocus();
}

// Create a string for categories for selected contact; 070102 p�d
CString CContactsFormView::setCategoriesPerContactStr(void)
{
	CTransaction_categories_for_contacts dataCatForContact;
	CTransaction_category dataCat;
	CString S;
	if (m_vecCatgoriesForContactData.size() > 0 && 
		m_vecCategoriesData.size() > 0)
	{
		for (UINT i = 0;i < m_vecCatgoriesForContactData.size();i++)
		{
			dataCatForContact = m_vecCatgoriesForContactData[i];
			if (dataCatForContact.getContactID() == m_activeContactData.getID())
			{	
				// Find categories set for this contact; 070102 p�d
				for (UINT i = 0;i < m_vecCategoriesData.size();i++)
				{
					dataCat = m_vecCategoriesData[i];
					if (dataCat.getID() == dataCatForContact.getCategoryID())
					{
						S += dataCat.getCategory() + ';';
					}	// if (dataCat.getID() == dataCatForContact.getCategory())
				}	// for (UINT i = 0;i < m_vecCatgoriesData.size();i++)
			}	// if (dataCat.getCategoryID() == m_activeContactData.getID())
		}	// for (UINT i = 0;i < m_vecCategoriesData.size();i++)
	}	// if (m_vecContactsData.size() > 0 && 

	S.Delete(S.GetLength() - 1);
	return S;
}

// CContactsFormView message handlers

BOOL CContactsFormView::getContacts(void)
{
	if (m_pDB != NULL)
	{
		if(objectId<0&&propId<0) {
			if (m_pDB->getContacts((*m_vecContactsData)))
				return TRUE;
		}
		else if(propId>-1) {
			if (m_pDB->getContacts((*m_vecContactsData),TRUE))
				return TRUE;
		}
		else {
			if (m_pDB->getContactsForObjectId(objectId,(*m_vecContactsData)))
				return TRUE;
		}
	}	// if (pDB != NULL)
	return FALSE;
}

BOOL CContactsFormView::getContactsForObject(int objId)
{
	objectId=objId;	
	BOOL bReturn = FALSE;
	if (m_pDB != NULL)
	{
		if (m_pDB->getContactsForObjectId(objId,*m_vecContactsData))
				bReturn = TRUE;
	}	// if (pDB != NULL)
	return bReturn;
}

BOOL CContactsFormView::doRePopulateFromSearch(LPCTSTR sql,int *index)
{
	m_sSQLContacts = sql;
	BOOL bFound = FALSE;
	
	if (m_pDB != NULL)
	{
		m_pDB->getContacts(sql,(*m_vecContactsData));
	}	// if (pDB != NULL)

	if (m_vecContactsData->size() > 0)
	{
		*index = (*m_vecContactsData)[0].getID();	// return the first contact found

		// We'll try to find index of active property and set m_nDBIndex to point 
		// to this item; 091008 p�d
		for (UINT i = 0;i < m_vecContactsData->size();i++)
		{
			if (m_activeContactData.getID() == (*m_vecContactsData)[i].getID())
			{
				m_nDBIndex = i;	
				bFound = TRUE;
				m_bIsAtLastItem = (i == m_vecContactsData->size()-1);
				m_bIsAtFirstItem = (i == 0);
				break;
			}	// if (m_activePropertyData.getID() == m_vecPropertyData[i].getID())
		}	// for (UINT i = 0;i < m_vecPropertyData.size();i++)

		// If we couldn't find the item, set to first or last, depending on
		// "goto_first"; 091008 p�d
		if (!bFound)
		{
			// Go to last entry; 090820 p�d
			if(m_nDBIndex >= m_vecContactsData->size())
				m_nDBIndex = (m_vecContactsData->size() - 1);
			else if(m_nDBIndex < 0)
				m_nDBIndex = 0;
		}

		populateData(m_nDBIndex);
		doSetNavigationBar();
		OnSearchReplace();	// send the updated m_Info struct aswell
	}
	else
	{
		m_bIsAtLastItem = FALSE;
		m_bIsAtFirstItem = FALSE;
		*index = -1;
	}

	return TRUE;
}

BOOL CContactsFormView::getCategories(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getCategories(m_vecCategoriesData))
				bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}

BOOL CContactsFormView::getCategoriesForContact(void)
{
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		if (m_pDB != NULL)
		{
			if (m_bConnected = m_pDB->getCategoriesForContact(m_vecCatgoriesForContactData))
				bReturn = TRUE;
		}	// if (pDB != NULL)
	}
	return bReturn;
}



// PROTECTED METHODS

void CContactsFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CContactsFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{

	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			saveContact(0);	// Save Contact, before adding a new one; 090511 p�d
			addContact();
			populateData(-1);	//20200814 Nolla formul�r vid ny kontakt J� #HMS-60
			setEnableData(TRUE);
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			if (saveContact(1))	// Don't Save categories here; 081014 p�d
			{
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			if (m_enumAction == UPD_ITEM)
			{
				removeContact();
			}
			break;
		}	// case ID_DELETE_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (saveContact(2))
			{
				m_nDBIndex = 0;
				populateData(m_nDBIndex);
			}
			//setNavigationButtons(FALSE,TRUE);
			break;
		} // case ID_DBNAVIG_START :
		case ID_DBNAVIG_PREV :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (saveContact(2))
			{
				m_nDBIndex--;
				if (m_nDBIndex < 0)
					m_nDBIndex = 0;
				populateData(m_nDBIndex);
			}
			break;
		} // case ID_DBNAVIG_PREV :
		case ID_DBNAVIG_NEXT :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (saveContact(2))
			{
				m_nDBIndex++;
				if (m_nDBIndex > ((int)m_vecContactsData->size() - 1))
						m_nDBIndex = (int)m_vecContactsData->size() - 1;
				populateData(m_nDBIndex);
			}
			break;
		}// case ID_DBNAVIG_NEXT :
		case ID_DBNAVIG_END :
		{
			// Before movin' on, check that user haven changed
			// active data; 061113 p�d
			if (saveContact(2))
			{
				m_nDBIndex = (int)m_vecContactsData->size()-1;
				populateData(m_nDBIndex);
			}
			break;
		}	// case ID_DBNAVIG_END :


	}	// switch (wParam)

	return 0L;
}

//Added function for showing specific contact id, feature #2265 20110808 J�
//Shows message if not found
/*
void CContactsFormView::doShowContact(int nContactId)
{
	int nIdx=-1;
	//Find corresponding DBIndex to contact id nId
	if (m_vecContactsData.size() > 0)
	{
		for(nIdx=0;nIdx<m_vecContactsData.size();nIdx++)
		{
			if(m_vecContactsData[nIdx].getID()==nContactId)
			{
				doPopulateOnContactID(nIdx);
				break;
			}
		}
	}
	if(nIdx==-1)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				AfxMessageBox(xml.str(IDS_STRING2934));
			}
		}
	}
}*/

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CContactsFormView::setNavigationButtons(BOOL start_prev,BOOL end_next, BOOL bList)
{
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_START, start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_PREV, start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_NEXT, end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_END, end_next);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,!global_IS_SEARCH_REPLACE_OPEN);

	if(disableNavButtons) {
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_START, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_PREV, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_NEXT, FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE, ID_DBNAVIG_END, FALSE);

		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	}

}


// Handle transaction on database species table; 060317 p�d

BOOL CContactsFormView::getEnteredData(void)
{
	CString sLand;
	int nIndex_TypeOf = m_wndCBox2.GetCurSel();
	int nIndex = m_wndCBox1.GetCurSel();

	if (nIndex > -1 && m_vecAddedLanguages.size() > 0 && nIndex < m_vecAddedLanguages.size())
	{
		sLand = m_vecAddedLanguages[nIndex];
	}
	CString cTestAdress=_T("");
	cTestAdress=m_wndEdit4.getText();
	m_enteredContactData = CTransaction_contacts(m_activeContactData.getID(),
																							m_wndEdit1.getText(),
																							m_wndEdit2.getText(),
																							m_wndEdit3.getText(),
																							cTestAdress,
																							m_wndEdit8.getText(),
																							m_wndEdit5.getText(),
																							m_wndEdit6.getText(),
																							m_wndEdit7.getText(),
																							sLand, //m_wndCBox1.getText(),
																							m_wndEdit9.getText(),
																							m_wndEdit15.getText(),
																							m_wndEdit16.getText(),
																							m_wndEdit10.getText(),
																							m_wndEdit11.getText(),
																							m_wndEdit12.getText(),
																							m_wndEdit13.getText(),
																							getUserName(),
																							_T(""),
																							m_wndEdit17.getText(),
																							-1 /* Connected to id -1 = It's a Company*/,
																							nIndex_TypeOf,
																							m_wndEdit18.getText(),
																							m_wndEdit19.getText(),
																							m_wndEdit20.getText(),
																							m_wndEdit21.getText());
	return TRUE;

}

BOOL CContactsFormView::addContact(void)
{
	// If data's changed save, before adding a new contact; 080903 p�d
	//	isDataChanged();
	
	clearAll();

	m_activeContactData = CTransaction_contacts();

	m_enumAction = NEW_ITEM;

	m_bIsDirty = TRUE;

	m_wndCBox2.SetCurSel(0);	// Default = Person; 090825 p�d
	m_wndAddCompanyBtn.EnableWindow(TRUE);

	// Set toolbar-buttons on new item; 091127 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,(m_vecContactsData->size() > 0));


	return TRUE;
}

BOOL CContactsFormView::saveContact(BOOL action, BOOL save_categories)
{
	BOOL bReturn = TRUE;
	CString S;
	int nID,nNumOf;

	if (!m_bIsDataEnabled) return FALSE;

	if (m_pDB != NULL)
	{
		if (!getEnteredData())
			return FALSE;

		if (!m_enteredContactData.getName().IsEmpty())
		{

			// Check number of entries in the contacts table.
			// If there's no entries, reset the identity field to start
			// from 1; 070102 p�d
			nNumOf = m_pDB->getNumOfRecordsInContacts();
			if (nNumOf < 1)
			{
				m_pDB->resetContactIdentityField();
			}
			if (m_pDB->addContact(m_enteredContactData))
			{
				// Check if there's any katrgories for this contact.
				// If so, add to table categories for contact; 070102 p�d
				if (m_vecSelectedCatgoriesForContactData.size() >= 0)
				{
					// Get last Identifer created. I.e. this new items id; 070102 p�d
					nID = m_pDB->getLastContactID();
					// Make sure it's an leagal id; 070102 p�d
					if (nID > 0)
					{
						// If all is ok, add id to m_vecSelectedCatgoriesForContactData array; 070102 p�d
						for (UINT i = 0;i < m_vecSelectedCatgoriesForContactData.size();i++)
						{
							m_vecSelectedCatgoriesForContactData[i].setContactID(nID);

						}	// for (UINT i = 0;i < m_vecSelectedCatgoriesForContactData.size();i++)
						
						// Add categories for this contact to database table; 070102 p�d
						m_pDB->addCategoriesForContact(m_vecSelectedCatgoriesForContactData);

						if(propId!=-1) {
							m_pDB->addPropOwner(propId,nID);
						}

					}	// if (nID > 0)
				}	// if (m_vecSelectedCatgoriesForContactData.size() > 0)
				// Reload information
				getContacts();
				getCategories();
				getCategoriesForContact();

				// Set db to point to last entered property; 090216 p�d
				m_nDBIndex = (m_vecContactsData->size()-1);


			}	// else	-> New entry
			else if (m_pDB->updContact(m_enteredContactData))
			{
				// Get last Identifer created. I.e. this new items id; 070102 p�d
				nID = m_enteredContactData.getID();
				// Make sure it's an leagal id; 070102 p�d
				if (nID > 0)
				{
					if (save_categories)
					{
						// Check if there's any katrgories for this contact.
						// If so, add to table categories for contact; 070102 p�d
						if (m_vecSelectedCatgoriesForContactData.size() > 0)
						{
							// Remove ALL entries in categories for contact table for this contact,
							// before adding categories selected on change of data; 070102 p�d
							m_pDB->removeCategoriesForContact(nID);

							// If all is ok, add id to m_vecSelectedCatgoriesForContactData array; 070102 p�d
							for (UINT i = 0;i < m_vecSelectedCatgoriesForContactData.size();i++)
							{
								m_vecSelectedCatgoriesForContactData[i].setContactID(nID);
							}	// for (UINT i = 0;i < m_vecSelectedCatgoriesForContactData.size();i++)

							// Add categories for this contact to database table; 070102 p�d
							m_pDB->addCategoriesForContact(m_vecSelectedCatgoriesForContactData);

						}	// if (m_vecSelectedCatgoriesForContactData.size() > 0)
						else
						{
							m_pDB->removeCategoriesForContact(nID);
						}
					}
				}	// if (nID > 0)
				// Reload information
				getContacts();
				getCategories();
				getCategoriesForContact();
			}	// if (pDB->updContact(m_enteredContactData))
			
			if (m_vecContactsData->size() == 0)
			{
				m_nDBIndex = -1;
				setNavigationButtons(FALSE, FALSE, FALSE);
				clearAll();
			}
			else
			{
				setNavigationButtons(m_nDBIndex > 0,
														m_nDBIndex < (m_vecContactsData->size()-1));
			}

			populateData(m_nDBIndex);
			// Clear data for Categories for contact; 070103 p�d
			m_vecSelectedCatgoriesForContactData.clear();

			m_enumAction = UPD_ITEM;
			m_bIsDirty = FALSE;
			resetIsDirty();

			bReturn = TRUE;
			
		}
		else if (action == 1)
		{
			::MessageBox(this->GetSafeHwnd(),m_sContactNameMissing,m_sErrCap,MB_ICONINFORMATION | MB_OK);
			bReturn = FALSE;

		}


	}	// if (pDB != NULL)
/*
	m_enumAction = UPD_ITEM;
	m_bIsDirty = FALSE;
	resetIsDirty();
*/
	setSearchToolbarBtn(TRUE);

	return bReturn;
}

int CContactsFormView::getPropId() {
	//#5009 PH 20160616
	return propId;

}

BOOL CContactsFormView::removeContact(void)
{
	CTransaction_contacts data;
	CString sMsg;
	int nIndex = -1;
	BOOL bReturn = FALSE;
	if (m_bConnected)
	{
		// Check that there's any data in m_vecMachineData vector; 061010 p�d
		if (m_vecContactsData->size() > 0)
		{
			if (m_pDB != NULL)
			{
				// Get Regions in database; 061002 p�d	
				data = (*m_vecContactsData)[m_nDBIndex];
				if (!isContactUsed(data))
				{
					sMsg.Format(_T("%s\n\n%s : %s\n%s : %s\n%s : %s\n\n%s"),
									m_sMsgCap,
									m_sPNR_ORGNR,
									data.getPNR_ORGNR(),
									m_sName,
									data.getName(),
									m_sCompany,
									data.getCompany(),
									m_sDeleteMsg);
					if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sErrCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					{
						// Delete Machine and ALL underlying 
						//	data (Trakt(s),Compartment(s) and Plot(s)); 061010 p�d
						m_pDB->removeContact(data);
						bReturn = TRUE;
					}	// if (::MessageBox(this->GetSafeHwnd(),sMsg,m_sErrCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				}	// if (!isContactUsed(data))
				else
					::MessageBox(this->GetSafeHwnd(),m_sContactActiveMsg,m_sErrCap,MB_ICONINFORMATION | MB_OK);
			}	// if (m_pDB != NULL)
		}	// if (m_vecMachineData.size() > 0)

		if (bReturn)
		{
			// Reload information
			getContacts();
			getCategories();
			getCategoriesForContact();

			if (m_vecContactsData->size() == 0)
			{
				setNavigationButtons(FALSE, FALSE, FALSE);
				clearAll();
				setEnableData(FALSE);
			}
			else if (m_vecContactsData->size() == 1)
			{
				setNavigationButtons(FALSE, FALSE, FALSE);
				setEnableData(TRUE);
			}
			else
			{
				setNavigationButtons(m_nDBIndex > 0,
  													 m_nDBIndex < (m_vecContactsData->size()-1));
			}

			// After a delete, set to last item; 060103 p�d
			m_nDBIndex = (int)m_vecContactsData->size() - 1;
			populateData(m_nDBIndex);
			m_enumAction = UPD_ITEM;
			m_bIsDirty = FALSE;
		}	// if (bReturn)
		return TRUE;
	}
	else
		return FALSE;

}

BOOL CContactsFormView::isContactUsed(CTransaction_contacts &rec)
{
	if (m_pDB != NULL)
	{
		return m_pDB->isPropertyOwnerUsed(rec.getID());
	}
	return FALSE;
}

void CContactsFormView::doSetNavigationBar()
{
	// If there's more than one item in list, set Navigationbar; 090217 p�d
	// If only one item, disable Navigationbar; 090217 p�d
	if (m_vecContactsData->size() > 1)
		setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecContactsData->size()-1));
	else
		setNavigationButtons(FALSE, FALSE, FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,TRUE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,(m_vecContactsData->size() >= 1) || (m_enumAction == NEW_ITEM));
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,(m_vecContactsData->size() >= 1));

}

BOOL CContactsFormView::doPopulateNext(int *id)
{
	m_nDBIndex++;
	if (m_nDBIndex > ((int)m_vecContactsData->size() - 1))
				m_nDBIndex = (int)m_vecContactsData->size() - 1;
	populateData(m_nDBIndex);

	*id = m_activeContactData.getID();

	return (m_nDBIndex < (int)m_vecContactsData->size() - 1);
}

BOOL CContactsFormView::doPopulatePrev(int *id)
{
	m_nDBIndex--;
	if (m_nDBIndex < 0)	m_nDBIndex = 0;
	populateData(m_nDBIndex);

	*id = m_activeContactData.getID();

	return (m_nDBIndex > 0);
}

void CContactsFormView::doPopulate(int index)
{
	
	// Do a check, if user has changed
	// data for active contact and if so
	// ask user to save; 070111 p�d
	isDataChanged();

	m_nDBIndex = index;
	populateData(m_nDBIndex);
}

//Added 2016-06-10 PH
void CContactsFormView::doPopulate(int index,vecTransactionContacts * vec) {
	m_vecContactsData=vec;
	doPopulate(index);
}

bool CContactsFormView::doPopulateOnContactID(int contact_id)
{
	if(m_pDB!=NULL) {
		m_pDB->getContacts((*m_vecContactsData),TRUE);
	}
	// Find out index for selected contact, based
	// on contact id; 070125 p�d
	if (m_vecContactsData->size() > 0)
	{
		for (UINT i = 0;i < m_vecContactsData->size();i++)
		{
			if ((*m_vecContactsData)[i].getID() == contact_id)
			{
				m_nDBIndex = i;
				populateData(m_nDBIndex);
				setEnableData(TRUE);
				//setNavigationButtons(m_nDBIndex > 0,m_nDBIndex < (m_vecContactsData->size()-1));

				//Disable navigation buttons - Added 2016-06-13 PH
				disableNavButtons=TRUE;
				setNavigationButtons(0,0,0);
				return true;
			}
		}
	}
	return false;
}

void CContactsFormView::doRePopulate(void)
{
	populateData(m_nDBIndex);
}

void CContactsFormView::setPostNumberAndPostAddress(CTransaction_postnumber data)
{
	m_wndEdit5.SetWindowText(data.getNumber());
	m_wndEdit6.SetWindowText(data.getName());
	m_wndEdit6.setIsDirty();
}

void CContactsFormView::OnImport()
{
	showFormView(IDD_FORMVIEW7,m_sLangFN,113);
}

void CContactsFormView::OnSearchReplace()
{
	// save any changed data first
	if(getIsDirty() == TRUE) isDataChanged();

	m_info.m_nID = m_activeContactData.getID();
	m_info.m_origin = ORIGIN_CONTACT;
	// Setup SEARCH_REPLACE_INFO info depending on
	// which edit items selected; 090820 p�d
	m_info.m_focusedEdit = m_enumFocusedEdit; 
	m_info.m_nNumber = 0;
	m_info.m_fNumber = 0.0;
	m_info.m_bIsNumeric = FALSE;
	// Numeric edit on these: 090820 p�d
	if (m_enumFocusedEdit == ED_CTACT_ORGNR)
	{
		m_info.m_sText = m_wndEdit1.getText();
		m_wndLbl1.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_NAME)
	{
		m_info.m_sText = m_wndEdit2.getText();
		m_wndLbl2.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_COMPANY)
	{
		m_info.m_sText = m_wndEdit3.getText();
		m_wndLbl3.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_ADDRESS)
	{
		m_info.m_sText = m_wndEdit4.getText();
		m_wndLbl4.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_CO_ADDRESS)
	{
		m_info.m_sText = m_wndEdit8.getText();
		m_wndLbl18.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_POSTNUM)
	{
		m_info.m_sText = m_wndEdit5.getText();
		m_wndLbl5.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_POST_ADDRESS)
	{
		m_info.m_sText = m_wndEdit6.getText();
		m_wndLbl6.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_COUNTY)
	{
		m_info.m_sText = m_wndEdit7.getText();
		m_wndLbl7.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_COUNTRY)
	{
		// Not used for now; 090821 p�d
	}
	else if (m_enumFocusedEdit == ED_CTACT_VAT_NUMBER)
	{
		m_info.m_sText = m_wndEdit17.getText();
		m_wndLbl17.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_PHONE_WORK)
	{
		m_info.m_sText = m_wndEdit9.getText();
		m_wndLbl9.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_PHONE_HOME)
	{
		m_info.m_sText = m_wndEdit15.getText();
		m_wndLbl14.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_FAX_NUMBER)
	{
		m_info.m_sText = m_wndEdit16.getText();
		m_wndLbl15.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_MOBILE)
	{
		m_info.m_sText = m_wndEdit10.getText();
		m_wndLbl10.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_EMAIL)
	{
		m_info.m_sText = m_wndEdit11.getText();
		m_wndLbl11.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_WEBSITE)
	{
		m_info.m_sText = m_wndEdit12.getText();
		m_wndLbl12.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_BANKGIRO)
	{
		m_info.m_sText = m_wndEdit18.getText();
		m_wndLbl19.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_PLUSGIRO)
	{
		m_info.m_sText = m_wndEdit19.getText();
		m_wndLbl20.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_BANKKONTO)
	{
		m_info.m_sText = m_wndEdit20.getText();
		m_wndLbl21.GetWindowText(m_info.m_sFocus);
	}
	else if (m_enumFocusedEdit == ED_CTACT_LEVNUMMER)
	{
		m_info.m_sText = m_wndEdit21.getText();
		m_wndLbl22.GetWindowText(m_info.m_sFocus);
	}

	CContactsSelListFormView* pWnd1 = (CContactsSelListFormView *)getFormViewParentByID(IDD_REPORTVIEW1);
	if (pWnd1) pWnd1->SendMessage(WM_CLOSE);

	if(m_info.m_focusedEdit != ED_NONE)	// do not search if focus is not on a edit-field
		showFormView(IDD_FORMVIEW10,m_sLangFN,(LPARAM)&m_info);
	if(!disableNavButtons)
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

void CContactsFormView::refreshContacts(void)
{
	// Reload information
	getContacts();

	if (m_vecContactsData->size() == 1)
	{
		setNavigationButtons(FALSE, FALSE, FALSE);
	}
	else if (m_vecContactsData->size() > 1)
	{
		setNavigationButtons(m_nDBIndex > 0,
												 m_nDBIndex < (m_vecContactsData->size()-1));
	}

	// After a delete, set to last item; 060103 p�d
	m_nDBIndex = (int)m_vecContactsData->size() - 1;

	populateData(m_nDBIndex);

}

void CContactsFormView::OnBnClickedButton1()
{
	CString sCategories;
	CTransaction_category data;
	CSelectCategoryDlg *dlg = new CSelectCategoryDlg(this);
	if (dlg)
	{
		// Save contact before adding a picture; 091102 p�d
		if (m_enumAction == UPD_ITEM)
		{
			dlg->setContact(m_activeContactData);
		}
		else
		{
			dlg->setContact(CTransaction_contacts());
		}
		if (dlg->DoModal() == IDOK)
		{
			sCategories = dlg->getSelectedCategoriesStr();
			m_vecSelectedCatgoriesForContactData = dlg->getSelectedCategoriesForContact();
			m_wndEdit14.SetWindowText(sCategories);
			m_wndEdit14.setIsDirty();

			saveContact(0,TRUE);	// Save Category also; 090211 p�d
			populateData(m_nDBIndex);
		}

		delete dlg;
	}

}

void CContactsFormView::OnBnClickedButton2()
{
	showFormView(IDD_FORMVIEW1,m_sLangFN);
	CPostnumberFormView *pView = (CPostnumberFormView *)getFormViewByID(IDD_FORMVIEW1); 
	if (pView)
	{
		pView->setFilterText(m_wndEdit5.getText(),0);
		// Clear Postaddress filed, when user selects
		// Postnumber table; 070111 p�d
		m_wndEdit6.SetWindowText(_T(""));
		m_wndEdit6.setIsDirty();
	}
}

void CContactsFormView::OnBnClickedButton3()
{
	showFormView(IDD_REPORTVIEW1,m_sLangFN,2);
	if(!disableNavButtons)
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

void CContactsFormView::OnBnClickedButton5()
{
	showFormView(IDD_FORMVIEW1,m_sLangFN);
	CPostnumberFormView *pView = (CPostnumberFormView *)getFormViewByID(IDD_FORMVIEW1); 
	if (pView)
	{
		pView->setFilterText(m_wndEdit6.getText(),1);
		// Clear Postnumber filed, when user selects
		// Postnumber table; 070117 p�d
		m_wndEdit5.SetWindowText(_T(""));
		m_wndEdit5.setIsDirty();
	}
}

void CContactsFormView::OnBnClickedBtnAddPic()
{
	CFileDialog *pOpenFileDlg = new CFileDialog(TRUE);

	pOpenFileDlg->m_ofn.lpstrFilter = _T("Pictures (*.bmp; *.jpg)\0*.bmp;*.jpg\0\0"); 
	pOpenFileDlg->m_ofn.Flags = OFN_EXPLORER|OFN_HIDEREADONLY;

	if(pOpenFileDlg->DoModal() != IDOK)
	{
		delete pOpenFileDlg;
		return;
	}
	
	m_sPicFilePath = pOpenFileDlg->GetPathName();

	delete pOpenFileDlg;

	if(m_wndPicCtrl.Load(m_sPicFilePath) == TRUE)
	{
		m_bSavePic = TRUE;
		if (saveContact(0))
		{
			CTransaction_contacts recContactData = CTransaction_contacts(m_activeContactData);
			if (m_pDB)
				m_pDB->saveImage(m_sPicFilePath,recContactData);
			// Repopulate, set m_activeContactData
			populateData(m_nDBIndex);
		}	// if (saveContact())
	}
	else
	{
		m_bSavePic = FALSE;
		m_sPicFilePath.Empty();
	}

}

void CContactsFormView::OnBnClickedBtnDelPic()
{
	m_wndPicCtrl.FreeData();

	if(m_pDB != NULL)
	{
		m_pDB->removeImage(m_activeContactData);
	}
	
	Invalidate();
	UpdateWindow();
	m_wndPicCtrl.RedrawWindow(0,0,RDW_FRAME|RDW_INVALIDATE|RDW_UPDATENOW);
}

void CContactsFormView::setEnableData(BOOL enable)
{
	m_wndCBox1.EnableWindow(enable);
	m_wndCBox2.EnableWindow(enable);

	m_wndEdit1.EnableWindow(enable);
	m_wndEdit1.SetReadOnly(!enable);
	m_wndEdit2.EnableWindow(enable);
	m_wndEdit2.SetReadOnly(!enable);
	if (enable)
		m_wndEdit3.SetDisabledColor(  BLACK, INFOBK );
	else
		m_wndEdit3.SetDisabledColor(  BLACK, COL3DFACE );
	m_wndEdit3.SetReadOnly();	// Alwayas set as read only; 090825 p�d
	m_wndEdit4.EnableWindow(enable);
	m_wndEdit4.SetReadOnly(!enable);
	m_wndEdit8.EnableWindow(enable);
	m_wndEdit8.SetReadOnly(!enable);
	m_wndEdit5.EnableWindow(enable);
	m_wndEdit5.SetReadOnly(!enable);
	m_wndEdit6.EnableWindow(enable);
	m_wndEdit6.SetReadOnly(!enable);
	m_wndEdit7.EnableWindow(enable);
	m_wndEdit7.SetReadOnly(!enable);
	m_wndEdit9.EnableWindow(enable);
	m_wndEdit9.SetReadOnly(!enable);
	m_wndEdit10.EnableWindow(enable);
	m_wndEdit10.SetReadOnly(!enable);
	m_wndEdit11.EnableWindow(enable);
	m_wndEdit11.SetReadOnly(!enable);
	m_wndEdit12.EnableWindow(enable);
	m_wndEdit12.SetReadOnly(!enable);
	m_wndEdit13.EnableWindow(enable);
	m_wndEdit13.SetReadOnly(!enable);
	m_wndEdit14.EnableWindow(FALSE);
	m_wndEdit14.SetReadOnly();
	m_wndEdit15.EnableWindow(enable);
	m_wndEdit15.SetReadOnly(!enable);
	m_wndEdit16.EnableWindow(enable);
	m_wndEdit16.SetReadOnly(!enable);
	m_wndEdit17.EnableWindow(enable);
	m_wndEdit17.SetReadOnly(!enable);
	m_wndEdit18.EnableWindow(enable);
	m_wndEdit18.SetReadOnly(!enable);
	m_wndEdit19.EnableWindow(enable);
	m_wndEdit19.SetReadOnly(!enable);
	m_wndEdit20.EnableWindow(enable);
	m_wndEdit20.SetReadOnly(!enable);
	m_wndEdit21.EnableWindow(enable);
	m_wndEdit21.SetReadOnly(!enable);
	m_wndBtn1.EnableWindow(enable);
	m_wndPostNumBtn.EnableWindow(enable);
	m_wndAddCompanyBtn.EnableWindow(enable);
	m_wndPostAdrBtn.EnableWindow(enable);
	m_wndPicCtrl.EnableWindow(enable);
	m_wndBtnAddPic.EnableWindow(enable);
	m_wndBtnDelPic.EnableWindow(enable);
	m_bIsDataEnabled = enable;

	m_wndCBox2.SetFocus();
}

void CContactsFormView::setSearchToolbarBtn(BOOL enable)
{
	//enable = FALSE; // DISABLED
	CMDIContactsFrame *pContactsFrame = (CMDIContactsFrame *)getFormViewByID(IDD_FORMVIEW3)->GetParent();
	if (pContactsFrame)
		pContactsFrame->setEnableToolbar(TRUE,enable && m_enumFocusedEdit != ED_NONE);
}

void CContactsFormView::OnEnSetfocusEdit11()
{
	m_enumFocusedEdit = ED_CTACT_ORGNR;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit12()
{
	m_enumFocusedEdit = ED_CTACT_NAME;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit13()
{
	m_enumFocusedEdit = ED_CTACT_COMPANY;
	setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit14()
{
	m_enumFocusedEdit = ED_CTACT_ADDRESS;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit15()
{
	m_enumFocusedEdit = ED_CTACT_POSTNUM;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit16()
{
	m_enumFocusedEdit = ED_CTACT_POST_ADDRESS;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit17()
{
	m_enumFocusedEdit = ED_CTACT_COUNTY;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit19()
{
	m_enumFocusedEdit = ED_CTACT_PHONE_WORK;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit115()
{
	m_enumFocusedEdit = ED_CTACT_PHONE_HOME;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit116()
{
	m_enumFocusedEdit = ED_CTACT_FAX_NUMBER;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit110()
{
	m_enumFocusedEdit = ED_CTACT_MOBILE;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit111()
{
	m_enumFocusedEdit = ED_CTACT_EMAIL;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit112()
{
	m_enumFocusedEdit = ED_CTACT_WEBSITE;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit113()
{
	m_enumFocusedEdit = ED_NONE;
	setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit114()
{
	m_enumFocusedEdit = ED_NONE;
	setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit117()
{
	m_enumFocusedEdit = ED_CTACT_VAT_NUMBER;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit118()
{
	m_enumFocusedEdit = ED_CTACT_BANKGIRO;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit119()
{
	m_enumFocusedEdit = ED_CTACT_PLUSGIRO;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit120()
{
	m_enumFocusedEdit = ED_CTACT_BANKKONTO;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnEnSetfocusEdit121()
{
	m_enumFocusedEdit = ED_CTACT_LEVNUMMER;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}



void CContactsFormView::OnCbSetfocusComobox11()
{
	m_enumFocusedEdit = ED_NONE;
	setSearchToolbarBtn(FALSE);
}

void CContactsFormView::OnCbSetfocusComobox12()
{
	m_enumFocusedEdit = ED_NONE;
	setSearchToolbarBtn(FALSE);
}


void CContactsFormView::OnCbnSelchangeCombo12()
{
	int nIndex = m_wndCBox2.GetCurSel();
	m_wndAddCompanyBtn.EnableWindow(nIndex == 0 /* 0 = A person */);
}

void CContactsFormView::OnEnSetfocusEdit18()
{
	m_enumFocusedEdit = ED_CTACT_CO_ADDRESS;
	if (m_vecContactsData->size() > 0)
		setSearchToolbarBtn(TRUE);
	else
		setSearchToolbarBtn(FALSE);
}

//Feature #2323 20111005 J�
int CContactsFormView::getActiveContactId()
{
	return m_activeContactData.getID();
}

//#5010 PH 20160614
void CContactsFormView::newOwnerToProperty(int propertyId) {
	propId=propertyId;
	disableNavButtons=TRUE;
	setEnableData(TRUE);
	addContact();
}
