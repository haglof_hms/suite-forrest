#pragma once

#include "Resource.h"

#include "ForrestDB.h"

//////////////////////////////////////////////////////////////////////////////
// CPropertiesInContactFormView form view

class CPropertiesInContactFormView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CPropertiesInContactFormView)
	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	int m_nSelectedColumn;

protected:
	CPropertiesInContactFormView();           // protected constructor used by dynamic creation
	virtual ~CPropertiesInContactFormView();

	vecTransactionPropertyObj m_vecPropertyData;
	BOOL setupReport(void);
	void populateReport(void);
	//void getProperties(void);

	CMyExtStatic m_wndLbl;
	CMyExtStatic m_wndLbl1;

	int m_nContactId;
	BOOL m_bInitialized;

	CForrestDB *m_pDB;
	BOOL m_bConnected;
	DB_CONNECTION_DATA m_dbConnectionData;
	void SetColBehaviour(CXTPReportColumn *pCol);

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif
    void populateReport(int nContactId);

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnRefresh();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
